package com.communicationapp.comman;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.util.Patterns;
import android.util.TypedValue;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import com.communicationapp.R;

import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommonMethod {
	/** Called for Showing Alert in Application */
	public static boolean flag;
	public static boolean isReceved;
	public static void showAlert(String message, Activity context) {

		AlertDialog.Builder builder = new AlertDialog.Builder(context);
		builder.setMessage(message).setCancelable(false)
				.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

					}
				});
		try {
			builder.show();
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	/** Called for checking Internet connection */
	public static boolean isOnline(Context context) {

		ConnectivityManager cm = (ConnectivityManager) context
				.getSystemService(Context.CONNECTIVITY_SERVICE);

		NetworkInfo netInfo;
		try {
			netInfo = cm.getActiveNetworkInfo();
			if (netInfo != null && netInfo.isConnectedOrConnecting()) {
				return true;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return false;
	}
	public static boolean isEmailValid(String email){
		Pattern pattern = Patterns.EMAIL_ADDRESS;
		Matcher matcher = pattern.matcher(email);
		return matcher.matches();
	}

	 public static void closeKeyboard(final Context context, final View view){
	    	InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
	        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
	    }
	 
	 public static void showNetworkSnack(View view, String message, String action){
		 Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG) .setAction(action, new View.OnClickListener() {
					 @Override
					 public void onClick(View view) {

					 }
				 });
		 snackbar.setActionTextColor(Color.RED);

		 View sbView = snackbar.getView();
		 TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
		 textView.setTextColor(Color.YELLOW);
		 snackbar.show();
	 }

	public static void showSnack(View view, String message){
		Snackbar snackbar = Snackbar
				.make(view, message, Snackbar.LENGTH_LONG);
		snackbar.show();
	}

	public static void  write(String msg )
	{
		System.out.println(msg);
	}

	public static void  overridePendingTransitionslide(Activity activity )
	{
		activity.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
	}

	public static void hideKeyboard(Context context,View view) {
		InputMethodManager imm = (InputMethodManager)context.getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
	}

	public static void showSnackbar(Context ctx, View view, String msg) {

		Snackbar snackbar = Snackbar.make(view, msg, Snackbar.LENGTH_LONG).setAction("Action", null);
		snackbar.getView().setBackgroundColor(ContextCompat.getColor(ctx, R.color.colorPrimaryDark));
		TextView tv = (TextView) snackbar.getView().findViewById(android.support.design.R.id.snackbar_text);
		tv.setTextColor(Color.WHITE);
		tv.setTypeface(Typeface.SANS_SERIF);
		snackbar.show();
	}


	public static int dp2px(Context mContext,int dp) {
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp,
				mContext.getResources().getDisplayMetrics());
	}

	public static List<Address> getAddress(Context mContext,double lat, double lng) {
		List<Address> addresses = null;
		Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());
		try {
			 addresses = geocoder.getFromLocation(lat, lng, 1);
			Address obj = addresses.get(0);
			String add = obj.getAddressLine(0);
			add = add + "\n" + obj.getCountryName();
			add = add + "\n" + obj.getCountryCode();
			add = add + "\n" + obj.getAdminArea();
			add = add + "\n" + obj.getPostalCode();
			add = add + "\n" + obj.getSubAdminArea();
			add = add + "\n" + obj.getLocality();
			add = add + "\n" + obj.getSubThoroughfare();

			Log.v("IGA", "Address" + add);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			Toast.makeText(mContext, e.getMessage(), Toast.LENGTH_SHORT).show();
		}
		return addresses;
	}


	public static String date(){
		Calendar calander;
		SimpleDateFormat simpledateformat;
		String Date;
		calander = Calendar.getInstance();
		simpledateformat = new SimpleDateFormat("MMMM dd, yyyy | HH:mm");
		Date = simpledateformat.format(calander.getTime());

		return Date;
	}

	public static String date1(){
		Calendar calander;
		SimpleDateFormat simpledateformat;
		String Date;
		calander = Calendar.getInstance();
		simpledateformat = new SimpleDateFormat("MMMM dd, yyyy");
		Date = simpledateformat.format(calander.getTime());

		return Date;
	}

	public static String getRealPathFromURI(Context context, Uri contentUri) {
		String[] proj = {MediaStore.Images.Media.DATA};

		//This method was deprecated in API level 11
		//Cursor cursor = managedQuery(contentUri, proj, null, null, null);

		CursorLoader cursorLoader = new CursorLoader(
				context,
				contentUri, proj, null, null, null);
		Cursor cursor = cursorLoader.loadInBackground();

		int column_index =
				cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}

	public static String removeLastComma(String str) {
		final int len = str.length();
		return len > 0 && str.charAt(len - 1) == ',' ?
				str.substring(0, len - 1) :
				str;
	}

	public static boolean isLocationServiceEnabled(Context mContext){
		LocationManager locationManager = null;
		boolean gps_enabled= false,network_enabled = false;

		if(locationManager ==null)
			locationManager = (LocationManager) mContext.getSystemService(Context.LOCATION_SERVICE);
		try{
			gps_enabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
		}catch(Exception ex){
			//do nothing...
		}

		try{
			network_enabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
		}catch(Exception ex){
			//do nothing...
		}

		return gps_enabled || network_enabled;

	}

	public static String getPath(Activity activity,Uri uri)
	{
		String[] projection = {MediaStore.Images.Media.DATA};
		Cursor cursor = activity.managedQuery(uri, projection, null, null, null);
		int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
		cursor.moveToFirst();
		return cursor.getString(column_index);
	}
}
