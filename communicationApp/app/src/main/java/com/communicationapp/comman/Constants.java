package com.communicationapp.comman;

public class Constants {

    public static final String BASE_URL= "http://alobhatechnology.com/staging/communicationapp_api/";

    public static final String LOGIN_URL=BASE_URL+"login";
    public static final String REGISTER_URL=BASE_URL+"register";
    public static final String GET_PROFILE=BASE_URL+"get_profile";
    public static final String UPDATE_PROFILE=BASE_URL+"update_profile";
    public static final String GET_CONTACT_LIST=BASE_URL+"get_contact_list";
    public static final String SEND_LOCATION=BASE_URL+"sendLocation";
    public static final String GETSEND_LOCATIONLIST=BASE_URL+"getSendedLocationList";
    public static final String ReceiveLocationbyuser=BASE_URL+"ReceiveLocationbyuser";
    public static final String RESET_PASSWORD=BASE_URL+"reset_password";
    public static final String SEND_MAIL=BASE_URL+"sendMail";
    public static final String RECEIVE_MAIL=BASE_URL+"ReceiveMail";
    public static final String SEND_FILE=BASE_URL+"sendFile";
    public static final String ReceiveFiles=BASE_URL+"ReceiveFiles";
    public static final String Creating_Conversation=BASE_URL+"Creating_Conversation";
    public static final String getLastConversation=BASE_URL+"getLastConversation";
    public static final String get_conversation=BASE_URL+"get_conversation";
    public static final String forgot_password=BASE_URL+"forgot_password";
    public static final String get_veg_count=BASE_URL+"get_veg_count";
    public static final String Gethistory=BASE_URL+"Gethistory";
    public static final String delete_receive=BASE_URL+"delete_receive";

    public static final String get_receivemessage_list=BASE_URL+"get_receivemessage_list";
    public static final String get_Chatlis=BASE_URL+"get_Chatlist";
    public static final String post_chat=BASE_URL+"post_chat";
    public static final String LOGOUT_URL=BASE_URL+"logout";
    public static final String UPDATE_BADGE=BASE_URL+"RemoveVegCount";



}
