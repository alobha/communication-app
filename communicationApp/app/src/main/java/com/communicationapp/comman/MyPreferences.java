package com.communicationapp.comman;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;


public class MyPreferences {
    private static MyPreferences preferences = null;
    private SharedPreferences mPreferences;
    private SharedPreferences.Editor editor;
    private String isLogedIn = "isLogedIn";
    private String isSecure = "isSecure";
    private String userId = "userId";
    private String FirstName = "first_name";

    public MyPreferences(Context context) {
        setmPreferences(PreferenceManager.getDefaultSharedPreferences(context));
    }


    public SharedPreferences getmPreferences() {
        return mPreferences;
    }

    public void setmPreferences(SharedPreferences mPreferences) {
        this.mPreferences = mPreferences;
    }


    public static MyPreferences getActiveInstance(Context context) {
        if (preferences == null) {
            preferences = new MyPreferences(context);
        }
        return preferences;
    }

    public boolean getIsLoggedIn() {
        return mPreferences.getBoolean(this.isLogedIn, false);
    }

    public void setIsLoggedIn(boolean isLoggedin) {
        editor = mPreferences.edit();
        editor.putBoolean(this.isLogedIn, isLoggedin);
        editor.commit();
    }
    public boolean getIsSecure() {
        return mPreferences.getBoolean(this.isSecure, false);
    }

    public void setIsSecure(boolean isSecure) {
        editor = mPreferences.edit();
        editor.putBoolean(this.isSecure, isSecure);
        editor.commit();
    }
    public String getUserId() {
        return mPreferences.getString(this.userId, "");

    }
    public void setUserId(String userId) {
        editor = mPreferences.edit();
        editor.putString(this.userId, userId);
        editor.commit();
    }
    public String getFirstName() {
        return mPreferences.getString(this.FirstName, "");

    }
    public void setFirstName(String first_name) {
        editor = mPreferences.edit();
        editor.putString(this.FirstName, first_name);
        editor.commit();
    }




}
