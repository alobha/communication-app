package com.communicationapp.comman;

import android.app.Activity;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.ImageView;

import com.communicationapp.R;
import com.github.nukc.stateview.StateView;


/**
 * Created by ather on 12/12/17.
 */

public class MyStateview {
    private ImageView iv_loading;
    private Context context;
    private StateView mStateView;

    public MyStateview(Activity activity, View view, int emptyView, int retryView, int loadingView) {
        this.context = activity;
        if (view == null) {
            mStateView = StateView.inject(activity);
        } else {
            mStateView = StateView.inject(view);
        }
        mStateView.setRetryResource(retryView);
        mStateView.setEmptyResource(emptyView);
        mStateView.setLoadingResource(loadingView);

    }

    public MyStateview(Activity activity, View view) {
        this.context = activity;

        if (view == null) {
            mStateView = StateView.inject(activity);
        } else {
            mStateView = StateView.inject(view);
        }
        mStateView.setRetryResource(R.layout.view_retry);
        mStateView.setEmptyResource(R.layout.view_empty);

    }


    public MyStateview(Fragment fragment, View view) {
        this.context = fragment.getActivity();

        if (view == null) {
            mStateView = StateView.inject(fragment.getView());
        } else {
            mStateView = StateView.inject(view);
        }

        mStateView.setRetryResource(R.layout.view_retry);
        mStateView.setEmptyResource(R.layout.view_empty);

    }



    public void showLoading() {
        mStateView.showLoading();
    }

    public void showRetry() {
        mStateView.showRetry();
    }

    public void showContent() {
        mStateView.showContent();
    }


    public void showEmpty() {
        mStateView.showEmpty();
    }

    public void setEmptyResource(int emptyResource) {
        mStateView.setEmptyResource(emptyResource);
    }

    public void setLoadingResource(int emptyResource) {
        mStateView.setLoadingResource(emptyResource);
    }

    public void setRetryResource(int emptyResource) {
        mStateView.setRetryResource(emptyResource);
    }

}
