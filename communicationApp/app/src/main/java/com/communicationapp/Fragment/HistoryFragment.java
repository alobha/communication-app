package com.communicationapp.fragment;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.communicationapp.adapter.ContactAdapter;
import com.communicationapp.adapter.HistoryAdapter;
import com.communicationapp.R;
import com.communicationapp.comman.CommonMethod;
import com.communicationapp.comman.Constants;
import com.communicationapp.comman.MyPreferences;
import com.communicationapp.comman.ProgressBarDialog;
import com.communicationapp.interfaces.ProgressClickListener;
import com.communicationapp.model.ContactList;
import com.communicationapp.model.HistoryList;
import com.communicationapp.responseparser.ContactResponse;
import com.communicationapp.responseparser.HistoryResponse;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.List;

public class HistoryFragment  extends Fragment implements ProgressClickListener {
    private View view;
    private Activity mcontext;
    private  RecyclerView recyclerViewContactList;
    private ProgressBarDialog mProgressDialog;
    private LinearLayout container;
    @Override
    public void onAttach(Activity activity) {
        this.mcontext = activity;
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        view = inflater.inflate( R.layout.activity_history, viewGroup, false);
        getRefrenceId();
        if(CommonMethod.isOnline(mcontext)){
            validateGetHistory();
        }else {
            CommonMethod.showSnack(container,getResources().getString(R.string.error_no_internet));
        }

        return view;
    }



    private void getRefrenceId(){
        container = (LinearLayout)view.findViewById(R.id.container);
        recyclerViewContactList = (RecyclerView)view.findViewById(R.id.recylerview);
        recyclerViewContactList.setLayoutManager(new LinearLayoutManager(getActivity()));
        mProgressDialog = new ProgressBarDialog(this, container);
    }


    @Override
    public void onRetryClick() {
        mProgressDialog.showContent();
        if(CommonMethod.isOnline(mcontext)){
            validateGetHistory();

        }else {
            CommonMethod.showSnackbar(mcontext,container,getResources().getString(R.string.error_no_internet));
        }
    }
    /*******************method network connection*********************************/

    private void validateGetHistory() {

        mProgressDialog.showLoading();

        AndroidNetworking.post(Constants.Gethistory)
                .addBodyParameter("user_id", MyPreferences.getActiveInstance(mcontext).getUserId())
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        mProgressDialog.showContent();
                        Gson gson = new Gson();
                        HistoryResponse mHistoryResponse = gson.fromJson(response.toString(), HistoryResponse.class);
                        if (mHistoryResponse != null) {
                            if (mHistoryResponse.getSuccess().equals("1")) {
                                mProgressDialog.showContent();
                                parseData(mHistoryResponse.getHistoryList());

                            } else {
                                mProgressDialog.showEmpty();

                            }
                        } else {

                            mProgressDialog.showRetry();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        mProgressDialog.showRetry();
                        CommonMethod.showNetworkSnack(container,getResources().getString(R.string.error_server),"Ok");
                    }

                });
    }

    private void parseData(List<HistoryList> historyList){
        recyclerViewContactList.setAdapter(new HistoryAdapter(mcontext,historyList));
    }
}