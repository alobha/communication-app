package com.communicationapp.fragment;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.communicationapp.adapter.ContactAdapter;
import com.communicationapp.adapter.CustomAdapter;
import com.communicationapp.AudioActivity;
import com.communicationapp.CloudeActivity;
import com.communicationapp.EmailActivity;
import com.communicationapp.LocationActivity;
import com.communicationapp.MessageActivity;
import com.communicationapp.VideoActivity;
import com.communicationapp.comman.CommonMethod;
import com.communicationapp.comman.Constants;
import com.communicationapp.comman.MyPreferences;
import com.communicationapp.comman.ProgressBarDialog;
import com.communicationapp.interfaces.ProgressClickListener;
import com.communicationapp.model.Arraylist;
import com.communicationapp.model.ContactList;
import com.communicationapp.model.MyData;
import com.communicationapp.R;
import com.communicationapp.responseparser.BadgeResponse;
import com.communicationapp.responseparser.ContactResponse;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.gson.Gson;

import android.location.LocationListener;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Dell on 5/24/2018.
 */

public class HomeFragment extends Fragment implements CustomAdapter.gridClicListner, LocationListener {
    private CustomAdapter horizontalAdapter;

    private ArrayList<Arraylist> horizontalList;
    private RecyclerView horizontal_recycler_view;
    private View view;
    private Activity mActivity;
    private TextView tvCurrntLocation;
    private TextView tvDateTime;
    protected LocationManager locationManager;
    private LinearLayout container;
    private BadgeResponse mBadgeResponse;

    //To store longitude and latitude from map
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.mActivity = activity;
        locationManager = (LocationManager) activity.getSystemService(Context.LOCATION_SERVICE);

    }

    @SuppressLint("MissingPermission")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_home, null);
        init();
        locationManager = (LocationManager) mActivity.getSystemService(Context.LOCATION_SERVICE);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);


        if (CommonMethod.isOnline(mActivity)) {
            validateGetBadge();

        } else {
            CommonMethod.showSnackbar(mActivity, container, getResources().getString(R.string.error_no_internet));
        }


        return view;

    }



    private void init() {
        container = (LinearLayout) view.findViewById(R.id.container);
        tvCurrntLocation = (TextView) view.findViewById(R.id.tvCurrntLocation);
        tvDateTime = (TextView) view.findViewById(R.id.tvDateTime);
        horizontal_recycler_view = (RecyclerView) view.findViewById(R.id.recylerview);

        LinearLayoutManager horizontalLayoutManagaer
                = new GridLayoutManager(getActivity(), 2, GridLayoutManager.VERTICAL, false);

        horizontal_recycler_view.setLayoutManager(horizontalLayoutManagaer);

        horizontalList = new ArrayList<Arraylist>();
        for (int i = 0; i < MyData.nameArray.length; i++) {
            horizontalList.add(new Arraylist(MyData.drawableArray[i], MyData.nameArray[i]));
        }

        horizontalAdapter = new CustomAdapter(getActivity(), horizontalList, HomeFragment.this, mBadgeResponse);
        horizontal_recycler_view.setAdapter(horizontalAdapter);
    }

    @Override
    public void getPosition(int position) {

        switch (position) {
            case 0:
                Intent intent = new Intent(mActivity, LocationActivity.class);
                startActivity(intent);
                break;

            case 1:
                Intent intent1 = new Intent(mActivity, MessageActivity.class);
                startActivity(intent1);
                break;
            case 2:

                Intent cloudeintent = new Intent(mActivity, CloudeActivity.class);
                startActivity(cloudeintent);
                CommonMethod.isReceved=false;
                break;
            case 3:
                Intent emailintent = new Intent(mActivity, EmailActivity.class);
                startActivity(emailintent);
                CommonMethod.isReceved=false;
                break;

            case 4:
                Intent videointent = new Intent(mActivity, VideoActivity.class);
                startActivity(videointent);
                CommonMethod.isReceved=false;
                break;
            case 5:
                Intent audiointent = new Intent(mActivity, AudioActivity.class);
                startActivity(audiointent);
                CommonMethod.isReceved=false;
                break;
        }


    }

    @Override
    public void onResume() {
        super.onResume();
        if (ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mActivity, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 0, 0, this);
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0, this);


        if (CommonMethod.isOnline(mActivity)) {
            validateGetBadge();

        } else {
            CommonMethod.showSnackbar(mActivity, container, getResources().getString(R.string.error_no_internet));
        }
    }

    @Override
    public void onLocationChanged(Location location) {

        tvDateTime.setText(CommonMethod.date());
        List<Address> mAddress = CommonMethod.getAddress(mActivity,location.getLatitude(),location.getLongitude());
        tvCurrntLocation.setText(mAddress.get(0).getLocality()+", " +mAddress.get(0).getCountryName());

    }

    @Override
    public void onPause() {
        super.onPause();

        locationManager.removeUpdates(this);
    }

    @Override
    public void onProviderDisabled(String provider) {
        Log.d("Latitude","disable");
    }

    @Override
    public void onProviderEnabled(String provider) {
        Log.d("Latitude","enable");
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        Log.d("Latitude","status");
    }



    /*******************method network connection*********************************/

    private void validateGetBadge() {
        AndroidNetworking.post(Constants.get_veg_count)
                .addBodyParameter("user_id", MyPreferences.getActiveInstance(mActivity).getUserId())
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Gson gson = new Gson();
                        Log.e("BadgeResponse",">>>>>>>"+response);
                        mBadgeResponse = gson.fromJson(response.toString(), BadgeResponse.class);
                        if (mBadgeResponse != null) {
                                if(mBadgeResponse.getCode().equalsIgnoreCase("200")){
                                    if(mBadgeResponse.getSuccess().equalsIgnoreCase("1")) {
                                        parseData(mBadgeResponse);
                                    }
                                }

                        } else {

                            CommonMethod.showSnackbar(mActivity,container, getString(R.string.connection_error));
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        CommonMethod.showNetworkSnack(container,getResources().getString(R.string.error_server),"Ok");
                    }

                });
    }

    private void parseData(BadgeResponse mBadgeResponse ){

        horizontalAdapter = new CustomAdapter(getActivity(),horizontalList,HomeFragment.this,mBadgeResponse);
        horizontal_recycler_view.setAdapter(horizontalAdapter);
    }


}