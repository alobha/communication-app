package com.communicationapp.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.communicationapp.AudioActivity;
import com.communicationapp.ContactListActivity;
import com.communicationapp.R;
import com.communicationapp.adapter.RecivedFileAdapter;
import com.communicationapp.adapter.RecivedFileAdapterAudio;
import com.communicationapp.adapter.SendFileAdapter;
import com.communicationapp.comman.CommonMethod;
import com.communicationapp.comman.Constants;
import com.communicationapp.comman.FileOpen;
import com.communicationapp.comman.FilePath;
import com.communicationapp.comman.MyPreferences;
import com.communicationapp.comman.ProgressBarDialog;
import com.communicationapp.interfaces.ProgressClickListener;
import com.communicationapp.model.ReceivedFilesList;
import com.communicationapp.model.SendFilesList;
import com.communicationapp.responseparser.FileResponse;
import com.communicationapp.responseparser.SendLocationResponse;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.List;

/**
 * Created by mac on 19/09/18.
 */

@SuppressLint("ValidFragment")
public class FrangmentRecevieAudio extends Fragment implements ProgressClickListener{
    private static final String TAG = "AudioActivity";

    /*****************Attributes name***************************/

    private ProgressBarDialog mProgressDialog;
    private List<ReceivedFilesList> receivedFilesList;

    private RecivedFileAdapterAudio mRecivedFilesAdapter;
    private SwipeMenuListView mListView;
    private View view;
    private Context mcontext;
    LinearLayout llCointer;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mcontext=context;
    }
    @SuppressLint("ValidFragment")
    public FrangmentRecevieAudio(List<ReceivedFilesList> receivedFilesList){
        this.receivedFilesList=receivedFilesList;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        view = inflater.inflate( R.layout.layout_recycleview, viewGroup, false);
        getRefrenceId();
        initControls();
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,int position, long arg3)
            {
                getUrlReceved(receivedFilesList.get(position).getFile());
            }
        });

        return view;
    }

    /*******************Method for get Attributes Id*********************************/

    private void getRefrenceId() {
        mListView = (SwipeMenuListView)view.findViewById(R.id.listView);
        llCointer = (LinearLayout)view.findViewById(R.id.llCointer);
        mProgressDialog = new ProgressBarDialog(FrangmentRecevieAudio.this, llCointer);

    }
    private void initControls() {

        mListView.setSwipeDirection(SwipeMenuListView.DIRECTION_LEFT);
        SwipeMenuCreator creator = new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu menu) {
                SwipeMenuItem deleteItem = new SwipeMenuItem(mcontext);
                // set item background
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                        0x3F, 0x25)));
                // set item width
                deleteItem.setWidth(CommonMethod.dp2px(mcontext, 90));
                // set item title
                deleteItem.setTitle("Clear");
                // set item title fontsize
                deleteItem.setTitleSize(18);
                // set item title font color
                deleteItem.setTitleColor(Color.WHITE);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };

        // set creator
        mListView.setMenuCreator(creator);
        mListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {

                switch (index) {
                    case 0:
                        validateDelet(receivedFilesList.get(position).getReceive_file_id(),position);
                        break;
                    case 1:
                        Toast.makeText(mcontext, "Item deleted", Toast.LENGTH_SHORT).show();

                        break;
                }
                return true;
            }
        });
        mListView.setOnMenuStateChangeListener(new SwipeMenuListView.OnMenuStateChangeListener() {
            @Override
            public void onMenuOpen(int position) {

            }

            @Override
            public void onMenuClose(int position) {

            }
        });

        mListView.setOnSwipeListener(new SwipeMenuListView.OnSwipeListener() {
            @Override
            public void onSwipeStart(int position) {

            }

            @Override
            public void onSwipeEnd(int position) {

            }
        });
    }

    @Override
    public void onRetryClick() {
        mProgressDialog.showContent();

    }

    @Override
    public void onResume() {
        super.onResume();
        if (receivedFilesList.size() > 0) {
            mRecivedFilesAdapter = new RecivedFileAdapterAudio(mcontext, receivedFilesList);
            mListView.setAdapter(mRecivedFilesAdapter);
        } else {
            mProgressDialog.showEmpty();
        }

    }
    public void getUrlReceved(String url) {
        try {
            FileOpen.openFile(mcontext, Uri.parse(url));

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }



    private void validateDelet(String id,final int position) {
        mProgressDialog.showLoading();
        AndroidNetworking.post(Constants.delete_receive)
                .addBodyParameter("user_id", MyPreferences.getActiveInstance(mcontext).getUserId())
                .addBodyParameter("id", id)
                .addBodyParameter("type", "file")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("DELETE_RESPONSE", "-------------" + response);
                        mProgressDialog.showContent();
                        Gson gson = new Gson();
                        SendLocationResponse mSendLocationResponse = gson.fromJson(response.toString(), SendLocationResponse.class);
                        if (mSendLocationResponse != null) {
                            if (mSendLocationResponse.getSuccess().equals("1")) {


                                CommonMethod.showSnackbar(mcontext, llCointer, mSendLocationResponse.getMessage());
                                mRecivedFilesAdapter.remove(position);
                            } else {
                                mProgressDialog.showEmpty();
                            }
                        } else {

                            CommonMethod.showSnackbar(mcontext, llCointer, getString(R.string.connection_error));
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        mProgressDialog.showRetry();
                        CommonMethod.showNetworkSnack(llCointer, getResources().getString(R.string.error_server), "Ok");
                    }

                });
    }
}
