package com.communicationapp.fragment;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.communicationapp.ChangePasswordActivity;
import com.communicationapp.interfaces.ProgressClickListener;
import com.communicationapp.responseparser.ProfileResponse;
import com.communicationapp.comman.CommonMethod;
import com.communicationapp.R;
import com.communicationapp.comman.Constants;
import com.communicationapp.comman.MyPreferences;
import com.communicationapp.comman.ProgressBarDialog;
import com.communicationapp.model.UserProfile;
import com.google.gson.Gson;

import org.json.JSONObject;

public class ProfileFragment  extends Fragment implements View.OnClickListener ,ProgressClickListener {

    /*****************Attributes name***************************/

    private View view;
    public static Button mbtnSave;
    public static EditText metFirstName;
    public static EditText metLastName;
    public static EditText metPhoneNumber;
    public static EditText metEmail;
    public static EditText metPassword;
    public static Button btnChangePassword;
    private LinearLayout container;
    private Activity mcontext;
    private ProgressBarDialog mProgressDialog;

    @Override
    public void onAttach(Activity activity) {
        this.mcontext = activity;
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        view = inflater.inflate( R.layout.fragment_profile, viewGroup, false);
        getRefrenceId();


        if(CommonMethod.isOnline(mcontext)){
            validategetProfileDetails();
        }else {
            CommonMethod.showSnack(container,getResources().getString(R.string.error_no_internet));
        }

        return view;


    }
    /*******************Method for get Attributes Id*********************************/

    public void getRefrenceId(){

        metFirstName = (EditText)view.findViewById( R.id.etFirstName );
        metLastName = (EditText)view.findViewById( R.id.etLastName );
        metPhoneNumber = (EditText)view.findViewById( R.id.etPhoneNumber );
        metEmail = (EditText)view.findViewById( R.id.etEmail );
        metPassword = (EditText)view.findViewById( R.id.etPassword );
        btnChangePassword = (Button) view.findViewById( R.id.btnChangePassword );
        container = (LinearLayout)view.findViewById( R.id.container );
        mbtnSave = (Button)view.findViewById( R.id.btnSave);
        mbtnSave.setOnClickListener(this);
        btnChangePassword.setOnClickListener(this);
        container.setOnClickListener(this);
        editTextdisable();
        mProgressDialog = new ProgressBarDialog(this, container);

    }
    /*******************Action perform on Attributes *********************************/

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btnSave:
                if (validationProfile(view)) {
                    updateProfileDetails();
                }
                break;

            case R.id.container:
                CommonMethod.hideKeyboard(mcontext,view);
                break;
            case R.id.btnChangePassword:
                Intent intent = new Intent(mcontext, ChangePasswordActivity.class);
                startActivity(intent);
                break;

        }

    }
    /*******************method for check button visible or not *********************************/

    public void getVisiableonOrOff(){
        if (mbtnSave.getVisibility() == View.VISIBLE) {
            // Its visible
            editTextdisable();
        } else {
            editTextEnable();

        }
    }
    /*******************method for edit text enable*********************************/

    public void editTextEnable(){
        metFirstName.setEnabled(true);
        metLastName.setEnabled(true);
        metPhoneNumber.setEnabled(true);
       /* metEmail.setEnabled(true);
        metPassword.setEnabled(true);*/
        mbtnSave.setVisibility(View.VISIBLE);
        btnChangePassword.setVisibility(View.GONE);

    }
    /*******************method for edit text disable*********************************/

    public void editTextdisable(){
        metFirstName.setEnabled(false);
        metLastName.setEnabled(false);
        metPhoneNumber.setEnabled(false);
        metEmail.setEnabled(false);
        metPassword.setEnabled(false);
        mbtnSave.setVisibility(View.GONE);
        btnChangePassword.setVisibility(View.VISIBLE);
    }
    /*******************Check Validation on Attributes *********************************/

    private boolean validationProfile(View view) {
        if (metFirstName.getText().toString().trim().length() == 0) {
            CommonMethod.showSnackbar(mcontext,view,"Please enter your first name");
            return false;
        } else if (metLastName.getText().toString().trim().length() == 0) {
            CommonMethod.showSnackbar(mcontext,view,"Please enter your last name.");
            return false;
        } else if (metPhoneNumber.getText().toString().trim().length() == 0) {
            CommonMethod.showSnackbar(mcontext,view,"Please enter your mobile number");
            return false;
        }
        return true;

    }

    @Override
    public void onRetryClick() {
        mProgressDialog.showContent();
        validategetProfileDetails();
        if(CommonMethod.isOnline(mcontext)){
            validategetProfileDetails();
            updateProfileDetails();
        }else {
            CommonMethod.showSnackbar(mcontext,container,getResources().getString(R.string.error_no_internet));
        }
    }
    /*******************method network connection*********************************/

    private void validategetProfileDetails() {

        mProgressDialog.showLoading();

        AndroidNetworking.post(Constants.GET_PROFILE)
                .addBodyParameter("user_id", MyPreferences.getActiveInstance(mcontext).getUserId())
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        mProgressDialog.showContent();
                        Gson gson = new Gson();
                        ProfileResponse mProfileResponse = gson.fromJson(response.toString(), ProfileResponse.class);
                        if (mProfileResponse != null) {
                            if (mProfileResponse.getCode().equals("200")) {

                                parseData(mProfileResponse.getUserProfile());


                            } else {
                                CommonMethod.showSnackbar(mcontext,container, mProfileResponse.getMessage());

                            }
                        } else {

                            CommonMethod.showSnackbar(mcontext,container, getString(R.string.connection_error));
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        mProgressDialog.showRetry();
                        CommonMethod.showNetworkSnack(container,getResources().getString(R.string.error_server),"Ok");
                    }

                });
    }

    private void parseData(UserProfile userData){

        metFirstName.setText(userData.getFirstName());
        metLastName.setText(userData.getLastName());
        metPhoneNumber.setText(userData.getPhone());
        metEmail.setText(userData.getEmail());
        metPassword.setText(userData.getPassword());
    }

    /*******************method network connection*********************************/

    private void updateProfileDetails() {

        mProgressDialog.showLoading();

        AndroidNetworking.post(Constants.UPDATE_PROFILE)
                .addBodyParameter("user_id", MyPreferences.getActiveInstance(mcontext).getUserId())
                .addBodyParameter("mobile",metPhoneNumber.getText().toString().trim())
                .addBodyParameter("first_name", metFirstName.getText().toString().trim())
                .addBodyParameter("last_name", metLastName.getText().toString().trim())
                .addBodyParameter("email", metEmail.getText().toString().trim())
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        mProgressDialog.showContent();
                        Gson gson = new Gson();
                        ProfileResponse mProfileResponse = gson.fromJson(response.toString(), ProfileResponse.class);
                        if (mProfileResponse != null) {
                            if (mProfileResponse.getSuccess().equals("1")) {

                                CommonMethod.showSnackbar(mcontext,container, mProfileResponse.getMessage());
                                editTextdisable();

                            } else {
                                CommonMethod.showSnackbar(mcontext,container, mProfileResponse.getMessage());

                            }
                        } else {

                            CommonMethod.showSnackbar(mcontext,container, getString(R.string.connection_error));
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        mProgressDialog.showRetry();
                        CommonMethod.showNetworkSnack(container,getResources().getString(R.string.error_server),"Ok");
                    }

                });
    }
}



