package com.communicationapp.fragment;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.communicationapp.AudioActivity;
import com.communicationapp.R;
import com.communicationapp.adapter.SendFileAdapter;
import com.communicationapp.adapter.SendFileAdapter1;
import com.communicationapp.comman.CommonMethod;
import com.communicationapp.comman.Constants;
import com.communicationapp.comman.FileOpen;
import com.communicationapp.comman.MyPreferences;
import com.communicationapp.comman.ProgressBarDialog;
import com.communicationapp.interfaces.ProgressClickListener;
import com.communicationapp.model.ReceivedFilesList;
import com.communicationapp.model.SendFilesList;
import com.communicationapp.responseparser.FileResponse;
import com.communicationapp.responseparser.SendLocationResponse;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.List;

/**
 * Created by mac on 19/09/18.
 */

@SuppressLint("ValidFragment")
public class FragmentSentAudio extends Fragment implements ProgressClickListener{
    private static final String TAG = "AudioActivity";

    /*****************Attributes name***************************/

    private ProgressBarDialog mProgressDialog;
    private LinearLayout llCointer;
    private SendFileAdapter1 mAdapter;
    private SwipeMenuListView mListView;
    private View view;
    private Context mcontext;
    private List<SendFilesList> sendFilesList;
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.mcontext=context;
    }

    @SuppressLint("ValidFragment")
    public FragmentSentAudio(List<SendFilesList> sendFilesList){
        this.sendFilesList=sendFilesList;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        view = inflater.inflate( R.layout.layout_recycleview, viewGroup, false);
        getRefrenceId();
        initControls();

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,int position, long arg3)
            {
                getUrlReceved(sendFilesList.get(position).getFile());
            }
        });

        return view;
    }

    /*******************Method for get Attributes Id*********************************/

    private void getRefrenceId() {
        mListView = (SwipeMenuListView)view.findViewById(R.id.listView);
        llCointer = (LinearLayout)view.findViewById(R.id.llCointer);
        mProgressDialog = new ProgressBarDialog(FragmentSentAudio.this, llCointer);

    }
    private void initControls() {
        mListView.setSwipeDirection(SwipeMenuListView.DIRECTION_LEFT);
        // mListView.setCloseInterpolator(new BounceInterpolator());
        SwipeMenuCreator creator = new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu menu) {
                SwipeMenuItem deleteItem = new SwipeMenuItem(mcontext);
                // set item background
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                        0x3F, 0x25)));
                // set item width
                deleteItem.setWidth(CommonMethod.dp2px(mcontext, 90));
                // set item title
                deleteItem.setTitle("Clear");
                // set item title fontsize
                deleteItem.setTitleSize(18);
                // set item title font color
                deleteItem.setTitleColor(Color.WHITE);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };

        // set creator
        mListView.setMenuCreator(creator);

        mListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {

                switch (index) {
                    case 0:
                        //Toast.makeText(MessageActivity.this, "Item deleted0", Toast.LENGTH_SHORT).show();
                        validateDelet(sendFilesList.get(position).getReceive_file_id(),position);

                        break;
                    case 1:
                        Toast.makeText(mcontext, "Item deleted", Toast.LENGTH_SHORT).show();

                        break;
                }
                return true;
            }
        });

        //mListView

        mListView.setOnMenuStateChangeListener(new SwipeMenuListView.OnMenuStateChangeListener() {
            @Override
            public void onMenuOpen(int position) {

            }

            @Override
            public void onMenuClose(int position) {

            }
        });

        mListView.setOnSwipeListener(new SwipeMenuListView.OnSwipeListener() {
            @Override
            public void onSwipeStart(int position) {

            }

            @Override
            public void onSwipeEnd(int position) {

            }
        });
    }



    @Override
    public void onResume() {
        super.onResume();
        if (sendFilesList.size() > 0) {
            mAdapter = new SendFileAdapter1(mcontext, sendFilesList);
            mListView.setAdapter(mAdapter);
        } else {
            mProgressDialog.showEmpty();
        }
    }


    public void getUrlReceved(String url) {
        try {
            FileOpen.openFile(mcontext, Uri.parse(url));

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }



    private void validateDelet(String sender_file_id,final int position) {
        mProgressDialog.showLoading();
        AndroidNetworking.post(Constants.delete_receive)
                .addBodyParameter("user_id", MyPreferences.getActiveInstance(mcontext).getUserId())
                .addBodyParameter("id", sender_file_id)
                .addBodyParameter("type", "file")
                .addBodyParameter("deleted_type", "sended")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("DELETE_RESPONSE", "-------------" + response);
                        mProgressDialog.showContent();
                        Gson gson = new Gson();
                        SendLocationResponse mSendLocationResponse = gson.fromJson(response.toString(), SendLocationResponse.class);
                        if (mSendLocationResponse != null) {
                            if (mSendLocationResponse.getSuccess().equals("1")) {


                                CommonMethod.showSnackbar(mcontext, llCointer, mSendLocationResponse.getMessage());
                                mAdapter.remove(position);
                            } else {
                                mProgressDialog.showEmpty();
                            }
                        } else {

                            CommonMethod.showSnackbar(mcontext, llCointer, getString(R.string.connection_error));
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        mProgressDialog.showRetry();
                        CommonMethod.showNetworkSnack(llCointer, getResources().getString(R.string.error_server), "Ok");
                    }

                });
    }

    @Override
    public void onRetryClick() {
        mProgressDialog.showContent();
    }
}
