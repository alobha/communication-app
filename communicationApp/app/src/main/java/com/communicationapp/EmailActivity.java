package com.communicationapp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.communicationapp.adapter.EmailAdapter;
import com.communicationapp.adapter.NewEmailAdapter;
import com.communicationapp.comman.MyPreferences;
import com.communicationapp.interfaces.ProgressClickListener;
import com.communicationapp.model.AllmailList;
import com.communicationapp.model.MessageList;
import com.communicationapp.responseparser.MailResponse;
import com.communicationapp.comman.CommonMethod;
import com.communicationapp.comman.Constants;
import com.communicationapp.comman.ProgressBarDialog;
import com.communicationapp.responseparser.SendLocationResponse;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.List;

public class EmailActivity extends AppCompatActivity implements View.OnClickListener, ProgressClickListener {

    private static final String TAG = "NewMessageActivity";
    private ImageView mivLock;
    private boolean isChecked = false;
    private ImageView ivAddButton;
    private RecyclerView recylerviewEmail;
    private CoordinatorLayout container;
    private LinearLayout container_ll;
    private ProgressBarDialog mProgressDialog;
    private Paint p = new Paint();
    private NewEmailAdapter emailAdapter;
    private List<AllmailList> allDataList;
    private SwipeRefreshLayout mSwipe;
    private boolean isClicked;
    private SwipeMenuListView mListView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email);
        getRefrenceId();
        initControls();
        setToolbarWidget();

        mSwipe = findViewById(R.id.mSwipe);
        mSwipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipe.setRefreshing(false);
                callGetAllEmailsAPI();
            }
        });
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,int position, long arg3)
            {
                Intent intent = new Intent(EmailActivity.this, EmailDetailActivity.class);
                intent.putExtra("email", allDataList.get(position).getName() + " " + allDataList.get(position).getLastName());
                intent.putExtra("subject", allDataList.get(position).getSubject());
                intent.putExtra("content", allDataList.get(position).getContent());
                intent.putExtra("receiver_id", allDataList.get(position).getSenderId());
                intent.putExtra("mailType", allDataList.get(position).getMailType());
                startActivity(intent);
            }
        });
    }

    /*******************Method for get Attributes Id*********************************/

    public void getRefrenceId() {
        container_ll = findViewById(R.id.container_ll);
        mProgressDialog = new ProgressBarDialog(EmailActivity.this, container_ll);
        ivAddButton = (ImageView) findViewById(R.id.ivAdd);
        container = (CoordinatorLayout) findViewById(R.id.container);
        recylerviewEmail = (RecyclerView) findViewById(R.id.recylerviewEmail);
        recylerviewEmail.setLayoutManager(new LinearLayoutManager(EmailActivity.this));
        ivAddButton.setOnClickListener(this);

        mivLock = (ImageView) findViewById(R.id.ivLock);
        mivLock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isChecked == false) {
                    MyPreferences.getActiveInstance(EmailActivity.this).setIsSecure(true);
                    mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_red));
                    isChecked = true;
                } else {
                    MyPreferences.getActiveInstance(EmailActivity.this).setIsSecure(false);
                    mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_green));
                    isChecked = false;
                }

            }
        });
    }

    private void initControls() {

        mListView = (SwipeMenuListView) findViewById(R.id.listView);
        mListView.setSwipeDirection(SwipeMenuListView.DIRECTION_LEFT);
        // mListView.setCloseInterpolator(new BounceInterpolator());
        SwipeMenuCreator creator = new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu menu) {
                // Create different menus depending on the view type
                // create "open" item
                /*SwipeMenuItem openItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                openItem.setBackground(new ColorDrawable(Color.rgb(0, 128,
                        0)));
                // set item width
                openItem.setWidth(CommonMethod.dp2px(MessageActivity.this, 90));
                // set item title
                openItem.setTitle("Clear");
                // set item title fontsize
                openItem.setTitleSize(18);
                // set item title font color
                openItem.setTitleColor(Color.WHITE);
                // add to menu
                menu.addMenuItem(openItem);*/
                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                        0x3F, 0x25)));
                // set item width
                deleteItem.setWidth(CommonMethod.dp2px(EmailActivity.this, 90));
                // set item title
                deleteItem.setTitle("Clear");
                // set item title fontsize
                deleteItem.setTitleSize(18);
                // set item title font color
                deleteItem.setTitleColor(Color.WHITE);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };

        // set creator
        mListView.setMenuCreator(creator);

        mListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {

                switch (index) {
                    case 0:
                        if(allDataList.get(position).getMailType().equalsIgnoreCase("Receive Mail")){
                            validateDelet(allDataList.get(position).getReceive_mail_id(),position);

                        }else {
                            validateDeletSend(allDataList.get(position).getReceive_mail_id(),position);

                        }
                        break;
                    case 1:
                        Toast.makeText(EmailActivity.this, "Item deleted", Toast.LENGTH_SHORT).show();

                        break;
                }
                return true;
            }
        });

        //mListView

        mListView.setOnMenuStateChangeListener(new SwipeMenuListView.OnMenuStateChangeListener() {
            @Override
            public void onMenuOpen(int position) {

            }

            @Override
            public void onMenuClose(int position) {

            }
        });

        mListView.setOnSwipeListener(new SwipeMenuListView.OnSwipeListener() {
            @Override
            public void onSwipeStart(int position) {

            }

            @Override
            public void onSwipeEnd(int position) {

            }
        });
    }
    /*******************Method for Action bar Setting*********************************/

    private void setToolbarWidget() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow_24dp);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                CommonMethod.closeKeyboard(EmailActivity.this, v);
            }
        });
        getSupportActionBar().setTitle("");

    }

    /*******************Action perform on Attributes *********************************/

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivAdd:
                Intent intent = new Intent(EmailActivity.this, NewMailActivity.class);
                startActivity(intent);
                break;


        }

    }

    @Override
    public void onRetryClick() {
        mProgressDialog.showContent();

        callGetAllEmailsAPI();
    }

    private void callGetAllEmailsAPI() {
        if (CommonMethod.isOnline(EmailActivity.this)) {
            validateGetMail();
        } else {
            CommonMethod.showSnack(container, getResources().getString(R.string.error_no_internet));
        }

    }


    /*******************method network connection*********************************/

    private void validateGetMail() {

        mProgressDialog.showLoading();
        AndroidNetworking.post(Constants.RECEIVE_MAIL)
                .addBodyParameter("user_id", MyPreferences.getActiveInstance(EmailActivity.this).getUserId())
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("ALL_EMAILS", "-----------" + response);


                        mProgressDialog.showContent();
                        Gson gson = new Gson();
                        MailResponse mMailResponse = gson.fromJson(response.toString(), MailResponse.class);
                        if (mMailResponse != null) {
                            if (mMailResponse.getCode().equals("200")) {

                                if (mMailResponse.getSuccess().equals("1")) {
                                    parseData(mMailResponse.getAllmailList());
                                } else {
                                    mProgressDialog.showEmpty();
                                }
                            }
                        } else {
                            mProgressDialog.showRetry();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {

                        mProgressDialog.showRetry();
                        CommonMethod.showNetworkSnack(container, getResources().getString(R.string.error_server), "Ok");
                    }

                });
    }

    private void parseData(List<AllmailList> emailList) {
        if (emailList != null) {
            this.allDataList = emailList;
            emailAdapter = new NewEmailAdapter(EmailActivity.this, emailList);
            mListView.setAdapter(emailAdapter);
            //enableSwipe();

        }
    }


    private void validateDelet(String mailId,final int posotion) {
        mProgressDialog.showLoading();
        AndroidNetworking.post(Constants.delete_receive)
                .addBodyParameter("user_id", MyPreferences.getActiveInstance(EmailActivity.this).getUserId())
                .addBodyParameter("id", mailId)
                .addBodyParameter("type", "email")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("DELETE_RESPONSE", "-------------" + response);
                        mProgressDialog.showContent();
                        Gson gson = new Gson();
                        SendLocationResponse mSendLocationResponse = gson.fromJson(response.toString(), SendLocationResponse.class);
                        if (mSendLocationResponse != null) {
                            if (mSendLocationResponse.getSuccess().equals("1")) {

                                CommonMethod.showSnackbar(EmailActivity.this, container, mSendLocationResponse.getMessage());
                                //callGetAllEmailsAPI();
                                emailAdapter.remove(posotion);
                            } else {
                                mProgressDialog.showEmpty();
                            }
                        } else {

                            CommonMethod.showSnackbar(EmailActivity.this, container, getString(R.string.connection_error));
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        mProgressDialog.showRetry();
                        CommonMethod.showNetworkSnack(container, getResources().getString(R.string.error_server), "Ok");
                    }

                });
    }

    private void validateDeletSend(String mailId,final int posotion) {
        mProgressDialog.showLoading();
        AndroidNetworking.post(Constants.delete_receive)
                .addBodyParameter("user_id", MyPreferences.getActiveInstance(EmailActivity.this).getUserId())
                .addBodyParameter("id", mailId)
                .addBodyParameter("type", "email")
                .addBodyParameter("deleted_type", "sended")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("DELETE_RESPONSE", "-------------" + response);
                        mProgressDialog.showContent();
                        Gson gson = new Gson();
                        SendLocationResponse mSendLocationResponse = gson.fromJson(response.toString(), SendLocationResponse.class);
                        if (mSendLocationResponse != null) {
                            if (mSendLocationResponse.getSuccess().equals("1")) {

                                CommonMethod.showSnackbar(EmailActivity.this, container, mSendLocationResponse.getMessage());
                                //callGetAllEmailsAPI();
                                emailAdapter.remove(posotion);
                            } else {
                                mProgressDialog.showEmpty();
                            }
                        } else {

                            CommonMethod.showSnackbar(EmailActivity.this, container, getString(R.string.connection_error));
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        mProgressDialog.showRetry();
                        CommonMethod.showNetworkSnack(container, getResources().getString(R.string.error_server), "Ok");
                    }

                });
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (MyPreferences.getActiveInstance(EmailActivity.this).getIsSecure()) {
            mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_red));
        } else {
            mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_green));
        }

        if (CommonMethod.isOnline(EmailActivity.this)) {
            validateGetMail();
            callUpdateCountAPI();
        } else {
            CommonMethod.showSnack(container, getResources().getString(R.string.error_no_internet));
        }

    }


    private void callUpdateCountAPI() {
        if (CommonMethod.isOnline(this)) {
            updateBadgeCountAPI();
        }
    }

    private void updateBadgeCountAPI() {
        AndroidNetworking.post(Constants.UPDATE_BADGE)
                .addBodyParameter("type", "email")
                .addBodyParameter("receiver_id", MyPreferences.getActiveInstance(this).getUserId())
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });

    }




}
