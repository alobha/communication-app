package com.communicationapp;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.communicationapp.comman.CommonMethod;
import com.communicationapp.comman.Constants;
import com.communicationapp.comman.MyPreferences;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class LocationActivity extends FragmentActivity implements
        OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleMap.OnMarkerDragListener,
        GoogleMap.OnMapLongClickListener,
        View.OnClickListener{

    //Our Map
    private GoogleMap mMap;
    Handler handler;

    //To store longitude and latitude from map
    private double longitude;
    private double latitude;

    //Google ApiClient
    private GoogleApiClient googleApiClient;
    /*****************Attributes name***************************/

    private ImageView ivBackArrow;
    private ImageView mivLock;
    private boolean isChecked=false;
    private TextView mtvSent;
    private TextView mtvRECEIVED;
    private TextView mtvAnotherLocation;
    private TextView tvCurrentLocation;
    private LinearLayout container;
    String City;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_location);
        getRefrenceId();


        if(CommonMethod.isLocationServiceEnabled(LocationActivity.this)){

        }else {
            showNetworkSnack(container,"Please enable locaiton","Ok");
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
        .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //Initializing googleapi client
        googleApiClient = new GoogleApiClient.Builder(this)
        .addConnectionCallbacks(this)
        .addOnConnectionFailedListener(this)
        .addApi(LocationServices.API)
        .build();

        handler=new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if(mMap!=null){
                    getCurrentLocation();
                }

            }
        },2000);



    }
    /*******************Check Validation on Attributes *********************************/

    public void getRefrenceId(){

        mtvSent = (TextView) findViewById( R.id.tvSent );
        mtvRECEIVED = (TextView) findViewById( R.id.tvRECEIVED );
        mtvAnotherLocation = (TextView) findViewById( R.id.tvAnotherLocation );
        tvCurrentLocation = (TextView) findViewById( R.id.tvCurrentLocation );
        container = (LinearLayout) findViewById( R.id.container );
        mtvSent.setOnClickListener(this);
        container.setOnClickListener(this);
        mtvRECEIVED.setOnClickListener(this);
        mtvAnotherLocation.setOnClickListener(this);

        mivLock = (ImageView) findViewById(R.id.ivLock);
        mivLock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isChecked==false){
                    MyPreferences.getActiveInstance(LocationActivity.this).setIsSecure(true);
                    mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_red));
                    isChecked=true;
                }else {
                    MyPreferences.getActiveInstance(LocationActivity.this).setIsSecure(false);
                    mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_green));
                    isChecked=false;
                }

            }
        });

        ivBackArrow =(ImageView) findViewById(R.id.ivBackArrow);
        ivBackArrow.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
                onBackPressed();
                }
        });

        tvCurrentLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(CommonMethod.isLocationServiceEnabled(LocationActivity.this)){

                    StringBuilder builder = new StringBuilder();
                    List<Address> addresses = CommonMethod.getAddress(LocationActivity.this, latitude, longitude);

                    for (int i = 0; i < addresses.size(); i++) {
                        builder.append(addresses.get(i).getSubLocality());
                        builder.append(" " + addresses.get(i).getFeatureName());
                        builder.append(" " + addresses.get(i).getLocality());
                        builder.append(" " + addresses.get(i).getAdminArea());
                        builder.append(" " + addresses.get(i).getCountryName());
                        builder.append(" " + addresses.get(i).getPostalCode());
                        City = addresses.get(i).getLocality();
                    }

                    Intent intent = new Intent(LocationActivity.this, ContactListActivity.class);
                    intent.putExtra("navigate", "Location");
                    intent.putExtra("latitude", String.valueOf(latitude));
                    intent.putExtra("langtitude", String.valueOf(longitude));
                    intent.putExtra("location", builder.toString());
                    intent.putExtra("City", City);
                    startActivity(intent);
                }else {
                    showNetworkSnack(container,"Please enable locaiton","Ok");
                }


            }
        });

    }
    @Override
    protected void onStart() {
        googleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onStop() {
        googleApiClient.disconnect();
        super.onStop();
    }

    //Getting current location
    private void getCurrentLocation() {
        mMap.clear();
        //Creating a location object
        Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if (location != null) {
            //Getting longitude and latitude
            longitude = location.getLongitude();
            latitude = location.getLatitude();

            //moving the map to location
            moveMap();
        }
    }

    //Function to move the map
    private void moveMap() {
        //String to display current latitude and longitude
        String msg = latitude + ", "+longitude;

        //Creating a LatLng Object to store Coordinates
        LatLng latLng = new LatLng(latitude, longitude);

        //Adding marker to map
        mMap.addMarker(new MarkerOptions()
                .position(latLng) //setting position
                .draggable(true) //Making the marker draggable
                .title("Current Location")); //Adding a title

        //Moving the camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        //Animating the camera
        mMap.animateCamera(CameraUpdateFactory.zoomTo(15));

        //Displaying current coordinates in toast
        //Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng latLng = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(latLng).draggable(true));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.setOnMarkerDragListener(this);
        mMap.setOnMapLongClickListener(this);

    }

    @Override
    public void onConnected(Bundle bundle) {
        getCurrentLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        //Clearing all the markers
        mMap.clear();

        //Adding a new marker to the current pressed position
        mMap.addMarker(new MarkerOptions()
                .position(latLng)
                .draggable(true));
    }

    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        //Getting the coordinates
        latitude = marker.getPosition().latitude;
        longitude = marker.getPosition().longitude;
        //Moving the map
        moveMap();
    }
    /*******************Action perform on Attributes *********************************/

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.tvSent:
                if(CommonMethod.isLocationServiceEnabled(LocationActivity.this)){

                    List<Address> mAddress = CommonMethod.getAddress(LocationActivity.this,latitude,longitude);
                    Intent intent = new Intent(LocationActivity.this,SentActivity.class);
                    intent.putExtra("address", (Serializable) mAddress);
                    intent.putExtra("latitude", String.valueOf(latitude));
                    intent.putExtra("longitude", String.valueOf(longitude));
                    startActivity(intent);
                }else {
                    showNetworkSnack(container,"Please enable locaiton","Ok");
                }

                break;

            //To redirect to welcome screen after validation check
            case R.id.tvRECEIVED:
                if(CommonMethod.isLocationServiceEnabled(LocationActivity.this)){

                    List<Address> mAddressrecive = CommonMethod.getAddress(LocationActivity.this,latitude,longitude);
                    Intent intent1 = new Intent(LocationActivity.this,RecevedActivity.class);
                    intent1.putExtra("address", (Serializable) mAddressrecive);
                    intent1.putExtra("latitude", String.valueOf(latitude));
                    intent1.putExtra("longitude", String.valueOf(longitude));
                    startActivity(intent1);
                }else {
                    showNetworkSnack(container,"Please enable locaiton","Ok");
                }

                break;
            case R.id.tvAnotherLocation:
                if(CommonMethod.isLocationServiceEnabled(LocationActivity.this)){

                    Intent intentother= new Intent(LocationActivity.this,AnotherLocationActivity.class);
                    startActivity(intentother);
                }else {
                    showNetworkSnack(container,"Please enable locaiton","Ok");
                }

                break;
            case R.id.container:
                break;
        }

    }


    @Override
    protected void onResume() {
        super.onResume();
        if(MyPreferences.getActiveInstance(LocationActivity.this).getIsSecure()){
            mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_red));
        }else {
            mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_green));
        }

       callUpdateCountAPI();

    }

    private void callUpdateCountAPI() {
        if(CommonMethod.isOnline(LocationActivity.this))
        {
            updateBadgeCountAPI();
        }
    }

    private void updateBadgeCountAPI() {
        AndroidNetworking.post(Constants.UPDATE_BADGE)
                .addBodyParameter("type","location")
                .addBodyParameter("receiver_id",MyPreferences.getActiveInstance(LocationActivity.this).getUserId())
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });

    }


    public  void showNetworkSnack(View view, String message, String action){
        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG) .setAction(action, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(myIntent);
            }
        });
        snackbar.setActionTextColor(Color.RED);

        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.YELLOW);
        snackbar.show();
    }
}
