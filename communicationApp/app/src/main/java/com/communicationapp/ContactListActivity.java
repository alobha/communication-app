package com.communicationapp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.communicationapp.adapter.ContactListAdapter;
import com.communicationapp.interfaces.ProgressClickListener;
import com.communicationapp.responseparser.ContactResponse;
import com.communicationapp.comman.CommonMethod;
import com.communicationapp.comman.Constants;
import com.communicationapp.comman.FilePath;
import com.communicationapp.comman.MyPreferences;
import com.communicationapp.comman.ProgressBarDialog;
import com.communicationapp.model.ContactList;
import com.communicationapp.responseparser.FileSendReponse;
import com.communicationapp.responseparser.SendLocationResponse;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.util.ArrayList;
import java.util.List;

public class ContactListActivity extends AppCompatActivity implements ProgressClickListener {
    private static final String TAG = "ContactListActivity";

    /*****************Attributes name***************************/

    private ImageView mivLock;
    private boolean isChecked=false;
    private ListView listviewFriends;
    private FloatingActionButton ivSend;
    private TextView tvLocation;
    StringBuilder stringBuilder;
    private ContactListAdapter adapter;
    private List<ContactList> contactList;
    ArrayList<String> stringArrayList = new ArrayList<>();
    private ProgressBarDialog mProgressDialog;
    private CoordinatorLayout container;
    private String Type;
    private String navigate;
    private String latitude;
    private String langtitude;
    private String location;
    private String mUserId;
    private File file;
    private String path;
    private String isSecure;
    private String City;

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact);
        getRefrenceId();
        setToolbarWidget();

        if(CommonMethod.isOnline(ContactListActivity.this)){
            validateGetContactList();
        }else {
            CommonMethod.showSnack(container,getResources().getString(R.string.error_no_internet));
        }



        Type  = getIntent().getStringExtra("type");
        path  = getIntent().getStringExtra("path");
        navigate  = getIntent().getStringExtra("navigate");
        latitude = getIntent().getStringExtra("latitude");
        langtitude = getIntent().getStringExtra("langtitude");
        location = getIntent().getStringExtra("location");
        City = getIntent().getStringExtra("City");

        ivSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CommonMethod.flag=false;
                if(MyPreferences.getActiveInstance(ContactListActivity.this).getIsSecure()){
                    mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_red));
                    isSecure ="1";

                }else {
                    mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_green));
                    isSecure ="0";
                }
               /* stringBuilder = new StringBuilder();
                for (String e : stringArrayList) {
                    stringBuilder.append(e);
                    stringBuilder.append(",");
                }*/
                if (stringBuilder.toString().endsWith(",")) {
                    mUserId = stringBuilder.toString().substring(0, stringBuilder.toString().length() - 1);
                }

               if(CommonMethod.isOnline(ContactListActivity.this)){
                    if(navigate.equalsIgnoreCase("Location")){
                        validateSendLocation();
                    }else {
                        file = new File(path);
                        validateSendFile();
                    }
                }else {
                    CommonMethod.showSnack(v,getResources().getString(R.string.error_no_internet));

                }

            }
        });

        listviewFriends.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long arg3) {
                view.setSelected(true);
                ImageView ivImageView = (ImageView) view.findViewById(R.id.ivImageView);
                if(contactList.get(position).isChecked())
                {
                    contactList.get(position).setChecked(false);
                    adapter.notifyDataSetChanged();
                }else {
                    contactList.get(position).setChecked(true);
                    adapter.notifyDataSetChanged();

                }
                stringBuilder = new StringBuilder();
                for (int i=0;i<contactList.size(); i++) {
                    if(contactList.get(i).isChecked()) {
                        stringBuilder.append(contactList.get(i).getUserId());
                        stringBuilder.append(",");
                    }
                }
                /*if (ivImageView.getVisibility() == View.VISIBLE) {
                    // Its visible
                    ivImageView.setVisibility(View.GONE);
                    if (stringArrayList.contains(contactList.get(position).getUserId())) {
                        stringArrayList.remove(contactList.get(position).getUserId());
                    }
                } else {
                    // Either gone or invisible
                    if (!stringArrayList.contains(contactList.get(position).getUserId())) {
                        stringArrayList.add(contactList.get(position).getUserId());
                    }
                    ivImageView.setVisibility(View.VISIBLE);
                }*/
                if (stringBuilder.length() > 0) {
                    ivSend.setVisibility(View.VISIBLE);

                } else {
                    ivSend.setVisibility(View.GONE);
                }
            }
        });


    }

    /*******************Method for get Attributes Id*********************************/

    private void getRefrenceId(){
        mProgressDialog = new ProgressBarDialog(ContactListActivity.this, null);

        listviewFriends = (ListView) findViewById(R.id.listviewFriends);
        ivSend = (FloatingActionButton) findViewById(R.id.ivSend);
        tvLocation = (TextView) findViewById(R.id.tvLocation);
        container = (CoordinatorLayout) findViewById( R.id.container);
    }
    /*******************Method for Action bar Setting*********************************/

    private void setToolbarWidget() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow_24dp);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                CommonMethod.flag=false;

            }
        });
        getSupportActionBar().setTitle("");

        mivLock = (ImageView) findViewById(R.id.ivLock);
        mivLock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isChecked==false){
                    mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_red));
                    isChecked=true;
                    MyPreferences.getActiveInstance(ContactListActivity.this).setIsSecure(true);
                }else {
                    mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_green));
                    isChecked=false;
                    MyPreferences.getActiveInstance(ContactListActivity.this).setIsSecure(false);
                }

            }
        });


    }

    @Override
    public void onRetryClick() {
        mProgressDialog.showContent();
        if(CommonMethod.isOnline(ContactListActivity.this)){
            validateGetContactList();
        }else {
            CommonMethod.showSnackbar(ContactListActivity.this,container,getResources().getString(R.string.error_no_internet));
        }
    }
    /*******************method network connection*********************************/

    private void validateGetContactList() {

        mProgressDialog.showLoading();

        AndroidNetworking.post(Constants.GET_CONTACT_LIST)
                .addBodyParameter("user_id", MyPreferences.getActiveInstance(ContactListActivity.this).getUserId())
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        mProgressDialog.showContent();
                        Gson gson = new Gson();
                        ContactResponse mProfileResponse = gson.fromJson(response.toString(), ContactResponse.class);
                        if (mProfileResponse != null) {
                            if (mProfileResponse.getSuccess().equals("1")) {

                                parseData(mProfileResponse.getContactList());

                            } else {
                                CommonMethod.showSnackbar(ContactListActivity.this,container, mProfileResponse.getMessage());

                            }
                        } else {

                            CommonMethod.showSnackbar(ContactListActivity.this,container, getString(R.string.connection_error));
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        mProgressDialog.showRetry();
                        CommonMethod.showNetworkSnack(container,getResources().getString(R.string.error_server),"Ok");
                    }

                });
    }

    private void parseData(List<ContactList> contactList ){
        if(navigate.equalsIgnoreCase("Location")){
            tvLocation.setVisibility(View.VISIBLE);
            tvLocation.setText(City+", "+ latitude +" " +langtitude);
        }else {
            tvLocation.setVisibility(View.GONE);
        }
        this.contactList =contactList;
        adapter = new ContactListAdapter(ContactListActivity.this, contactList);
        listviewFriends.setAdapter(adapter);
    }



    /*******************method network connection*********************************/

    private void validateSendLocation() {
        mProgressDialog.showLoading();
        AndroidNetworking.post(Constants.SEND_LOCATION)
                .addBodyParameter("sender_id", MyPreferences.getActiveInstance(ContactListActivity.this).getUserId())
                .addBodyParameter("receiver_id", mUserId)
                .addBodyParameter("lat",latitude )
                .addBodyParameter("long",langtitude)
                .addBodyParameter("location",location)
                .addBodyParameter("isSecure", isSecure)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        mProgressDialog.showContent();
                        Gson gson = new Gson();
                        SendLocationResponse mSendLocationResponse = gson.fromJson(response.toString(), SendLocationResponse.class);
                        if (mSendLocationResponse != null) {
                            if (mSendLocationResponse.getSuccess().equals("1")) {

                                Toast.makeText(ContactListActivity.this,mSendLocationResponse.getMessage(),Toast.LENGTH_SHORT).show();
                                finish();
                            } else {
                                CommonMethod.showSnackbar(ContactListActivity.this,container, mSendLocationResponse.getMessage());

                            }
                        } else {

                            CommonMethod.showSnackbar(ContactListActivity.this,container, getString(R.string.connection_error));
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        mProgressDialog.showRetry();
                        CommonMethod.showNetworkSnack(container,getResources().getString(R.string.error_server),"Ok");
                    }

                });
    }


    private void validateSendFile() {

        mProgressDialog.showLoading();

        AndroidNetworking.upload(Constants.SEND_FILE)
                .addMultipartParameter("sender_id", MyPreferences.getActiveInstance(ContactListActivity.this).getUserId())
                .addMultipartParameter("receiver_id", mUserId)
                .addMultipartParameter("type", Type)
                .addMultipartParameter("isSecure",isSecure)
                .addMultipartFile("file", file)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        mProgressDialog.showContent();
                        Gson gson = new Gson();
                        FileSendReponse mFileSendReponse = gson.fromJson(response.toString(), FileSendReponse.class);
                        if (mFileSendReponse != null) {
                            if (mFileSendReponse.getCode().equals("200")) {

                                Toast.makeText(ContactListActivity.this,mFileSendReponse.getMessage(),Toast.LENGTH_SHORT).show();
                                finish();
                            } else {
                                CommonMethod.showSnackbar(ContactListActivity.this,container, mFileSendReponse.getMessage());

                            }
                        } else {

                            CommonMethod.showSnackbar(ContactListActivity.this,container, getString(R.string.connection_error));
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        mProgressDialog.showRetry();
                        CommonMethod.showNetworkSnack(container,getResources().getString(R.string.error_server),"Ok");
                    }

                });
    }


    @Override
    protected void onResume() {
        super.onResume();

        if(MyPreferences.getActiveInstance(ContactListActivity.this).getIsSecure()){
            mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_red));

        }else {
            mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_green));
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        CommonMethod.flag=false;
    }
}