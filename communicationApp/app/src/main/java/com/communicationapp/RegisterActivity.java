package com.communicationapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.communicationapp.comman.MyPreferences;
import com.communicationapp.interfaces.ProgressClickListener;
import com.communicationapp.responseparser.RegisterResponse;
import com.communicationapp.comman.CommonMethod;
import com.communicationapp.comman.Constants;
import com.communicationapp.comman.ProgressBarDialog;
import com.communicationapp.model.RegisterDetails;
import com.google.gson.Gson;

import org.json.JSONObject;

public class RegisterActivity extends AppCompatActivity implements View.OnClickListener,ProgressClickListener {
    private static final String TAG = "RegisterActivity";

    /*****************Attributes name***************************/
    private Button mbtnRegister;
    private EditText metFirstName;
    private EditText metLastName;
    private EditText metPhoneNumber;
    private EditText metEmail;
    private EditText metPassword;
    private EditText metConfirmPassword;
    private LinearLayout container;
    private ProgressBarDialog mProgressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_register );

        getRefrenceId();
        setToolbarWidget();
    }

    /*******************Method for get Attributes Id*********************************/

    private void getRefrenceId(){

        metFirstName = (EditText) findViewById( R.id.etFirstName );
        metLastName = (EditText) findViewById( R.id.etLastName );
        metPhoneNumber = (EditText) findViewById( R.id.etPhoneNumber );
        metEmail = (EditText) findViewById( R.id.etEmail );
        metPassword = (EditText) findViewById( R.id.etPassword );
        metConfirmPassword = (EditText) findViewById( R.id.etConfirmPassword );
        container = (LinearLayout) findViewById( R.id.container );
        mbtnRegister = (Button) findViewById( R.id.btnRegister);
        mbtnRegister.setOnClickListener(this);
        container.setOnClickListener(this);
        mProgressDialog = new ProgressBarDialog(RegisterActivity.this, null);
    }
    /*******************Method for Action bar Setting*********************************/

    private void setToolbarWidget() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow_24dp);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                CommonMethod.closeKeyboard(RegisterActivity.this,v);
            }
        });
        getSupportActionBar().setTitle("");

    }

    /*******************Action perform on Attributes *********************************/

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btnRegister:
                if(CommonMethod.isOnline(RegisterActivity.this)){
                    if (validationRegistration(view)) {
                        validateRegister();
                    }
                }else {
                    CommonMethod.showSnackbar(RegisterActivity.this,container,getResources().getString(R.string.error_no_internet));
                }
                break;
            case R.id.container:
                CommonMethod.hideKeyboard(RegisterActivity.this,view);
                break;

        }

    }
    /*******************Check Validation on Attributes *********************************/

    private boolean validationRegistration(View view) {

        if (metFirstName.getText().toString().trim().length() == 0) {
            CommonMethod.showSnackbar(RegisterActivity.this,view,"Please enter your first name");
            return false;
        } else if (metLastName.getText().toString().trim().length() == 0) {
            CommonMethod.showSnackbar(RegisterActivity.this,view,"Please enter your last name.");
            return false;
        } else if (metPhoneNumber.getText().toString().trim().length() == 0) {
            CommonMethod.showSnackbar(RegisterActivity.this,view,"Please enter your mobile number");
            return false;
        }else if (metEmail.getText().toString().trim().length() == 0) {
            CommonMethod.showSnackbar(RegisterActivity.this,view,"Please enter your email id");
            return false;
        } else if (!CommonMethod.isEmailValid(metEmail.getText().toString().trim())) {
            CommonMethod.showSnackbar(RegisterActivity.this,view,"Please enter valid email id");
            return false;
        } else if (metPassword.getText().toString().trim().length() == 0) {
            metPassword.setText("");
            CommonMethod.showSnackbar(RegisterActivity.this,view,getString(R.string.password_error));
            return false;
        } else if (metPassword.getText().toString().trim().length()< 8 || metPassword.getText().toString().trim().length() > 30) {
            metPassword.setText("");
            CommonMethod.showSnackbar(RegisterActivity.this,view,getString(R.string.password_limit_error));
            return false;
        } else if (metConfirmPassword.getText().toString().trim().length()==0) {
            CommonMethod.showSnackbar(RegisterActivity.this,view,"Please re-enter your password");
            return false;
        } else if (!metPassword.getText().toString().trim().equalsIgnoreCase(metConfirmPassword.getText().toString().trim())) {
            CommonMethod.showSnackbar(RegisterActivity.this,view,"Password did not match");
            return false;
        }
        return true;

    }
    /*******************Method for navigate other Activity *********************************/

    private void gotoDasboartActivity() {
        Intent in = new Intent(RegisterActivity.this, MainActivity.class);
        in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(in);
        LoginActivity.mLoginActivity.finish();
        finish();
    }


    @Override
    public void onRetryClick() {
        mProgressDialog.showContent();
        if(CommonMethod.isOnline(RegisterActivity.this)){
            validateRegister();
        }else {
            CommonMethod.showSnackbar(RegisterActivity.this,container,getResources().getString(R.string.error_no_internet));
        }
    }
    /*******************method network connection*********************************/

    private void validateRegister() {

        mProgressDialog.showLoading();

        AndroidNetworking.post(Constants.REGISTER_URL)
                .addBodyParameter("first_name", metFirstName.getText().toString())
                .addBodyParameter("last_name", metLastName.getText().toString().trim())
                .addBodyParameter("mobile", metPhoneNumber.getText().toString().trim())
                .addBodyParameter("email", metEmail.getText().toString().trim())
                .addBodyParameter("password", metPassword.getText().toString().trim())
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        mProgressDialog.showContent();
                        Gson gson = new Gson();
                        RegisterResponse mRegisterResponse = gson.fromJson(response.toString(), RegisterResponse.class);
                        if (mRegisterResponse != null) {
                            if (mRegisterResponse.getSuccess().equals("1")) {

                                parseDataLogin(mRegisterResponse.getRegisterDetails());

                            } else {
                                CommonMethod.showSnackbar(RegisterActivity.this,container, mRegisterResponse.getMessage());

                            }
                        } else {

                            CommonMethod.showSnackbar(RegisterActivity.this,container, getString(R.string.connection_error));
                        }

                    }

                    @Override
                    public void onError(ANError anError) {

                        mProgressDialog.showRetry();
                        CommonMethod.showNetworkSnack(container,getResources().getString(R.string.error_server),"Ok");
                    }

                });
    }

    private void parseDataLogin(RegisterDetails userData){

        MyPreferences.getActiveInstance(RegisterActivity.this).setUserId(userData.getUserId());
        MyPreferences.getActiveInstance(RegisterActivity.this).setIsLoggedIn(true);
        MyPreferences.getActiveInstance(RegisterActivity.this).setFirstName(userData.getFirstName());

        gotoDasboartActivity();
    }

}
