package com.communicationapp;


import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.communicationapp.interfaces.ProgressClickListener;
import com.communicationapp.responseparser.LoginResponse;
import com.communicationapp.comman.CommonMethod;
import com.communicationapp.comman.Constants;
import com.communicationapp.comman.MyPreferences;
import com.communicationapp.comman.ProgressBarDialog;
import com.communicationapp.model.UserDetails;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class LoginActivity extends AppCompatActivity implements View.OnClickListener,ProgressClickListener {
    private static final String TAG = "LoginActivity";

    /*****************Attributes name***************************/

    private EditText metEmail;
    private EditText metPassword;
    private Button mbtnLogin ;
    private Button mbtnRegister ;
    private TextView mtvForgopassword;
    private String userName;
    private String password;
    private LinearLayout container;
    private ProgressBarDialog mProgressDialog;
    public static Activity mLoginActivity;
    /**********************Permission Array for App******************************/

    private static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    private String[] permissions = new String[]
            {
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION,
                    Manifest.permission.INTERNET,
                    Manifest.permission.READ_EXTERNAL_STORAGE,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,

            };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_login );
        mLoginActivity = LoginActivity.this;
        getRefernceId();
        checkPermissions();
    }

    /*******************Method for get Attributes Id*********************************/

    private void getRefernceId(){

        mProgressDialog = new ProgressBarDialog(LoginActivity.this, null);
        metEmail = (EditText) findViewById( R.id.etEmail );
        metPassword = (EditText) findViewById( R.id.etPassword );
        mbtnLogin= (Button)findViewById( R.id.btnLogin);
        mtvForgopassword = (TextView) findViewById( R.id.tvForgotPassword);
        mbtnRegister = (Button) findViewById( R.id.btnRegister);
        container = (LinearLayout) findViewById( R.id.container);
        mbtnLogin.setOnClickListener(this);
        mbtnRegister.setOnClickListener(this);
        mtvForgopassword.setOnClickListener(this);
        container.setOnClickListener(this);


    }
    /*******************Method Ask Permission*********************************/

    private boolean checkPermissions() {
        int result;
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String permission : permissions) {
            result = ContextCompat.checkSelfPermission(LoginActivity.this, permission);
            if (result != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(permission);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(LoginActivity.this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    /*******************Action perform on Attributes *********************************/

    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.btnRegister:
                Intent intent = new Intent(LoginActivity.this,RegisterActivity.class);
                startActivity(intent);
                break;
                case R.id.btnLogin:
                    if(CommonMethod.isOnline(LoginActivity.this)){
                        if (isValidData(view)) {
                            validateLogin();
                        }
                    }else {
                        CommonMethod.showSnackbar(LoginActivity.this,container,getResources().getString(R.string.error_no_internet));
                    }
                break;
            case R.id.container:
               CommonMethod.hideKeyboard(LoginActivity.this,view);
                break;

            case R.id.tvForgotPassword:
                Intent intentforgot = new Intent(LoginActivity.this,ForgotPasswordActivity.class);
                startActivity(intentforgot);
                break;
        }

    }
    /*******************Check Validation on Attributes *********************************/

    private boolean isValidData(View view) {
        boolean check = true;
        userName = metEmail.getText().toString().trim();
        password = metPassword.getText().toString().trim();

        if (userName.isEmpty()) {

            CommonMethod.showSnackbar(LoginActivity.this,view,getString(R.string.username_error));
            check = false;
        } else if (userName.contains(" ")) {

            CommonMethod.showSnackbar(LoginActivity.this,view,getString(R.string.space_error));
            check = false;
        } else if (!CommonMethod.isEmailValid(userName)) {

            CommonMethod.showSnackbar(LoginActivity.this,view,getString(R.string.valide_email_char_error));
            check = false;
        }
        if (password.isEmpty()) {
            metPassword.setText("");
            CommonMethod.showSnackbar(LoginActivity.this,view,getString(R.string.password_error));
            check = false;
        } else if (password.length() < 8 || password.length() > 30) {

            CommonMethod.showSnackbar(LoginActivity.this,view,getString(R.string.password_limit_error));
            check = false;
        }
        return check;
    }

    /*******************Method for navigate other Activity *********************************/

    private void gotoDasbord(){
        Intent in = new Intent(LoginActivity.this, MainActivity.class);
        in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(in);
        finish();
    }

    @Override
    public void onRetryClick() {
        mProgressDialog.showContent();
        if(CommonMethod.isOnline(LoginActivity.this)){
                validateLogin();
        }else {
            CommonMethod.showSnackbar(LoginActivity.this,container,getResources().getString(R.string.error_no_internet));
        }
    }

    /*******************method network connection*********************************/

    private void validateLogin() {

        mProgressDialog.showLoading();
        AndroidNetworking.post(Constants.LOGIN_URL)
                .addBodyParameter("email", metEmail.getText().toString())
                .addBodyParameter("password", metPassword.getText().toString().trim())
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        mProgressDialog.showContent();
                        Gson gson = new Gson();
                        LoginResponse mLoginResponser = gson.fromJson(response.toString(), LoginResponse.class);
                        if (mLoginResponser != null) {
                            if (mLoginResponser.getSuccess().equals("1")) {

                                parseDataLogin(mLoginResponser.getUserDetails());

                            } else {
                                CommonMethod.showSnackbar(LoginActivity.this,container, mLoginResponser.getMessage());

                            }
                        } else {

                            CommonMethod.showSnackbar(LoginActivity.this,container, getString(R.string.connection_error));
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        mProgressDialog.showRetry();
                        CommonMethod.showNetworkSnack(container,getResources().getString(R.string.error_server),"Ok");
                    }

                });
    }

    private void parseDataLogin(UserDetails userData){
        MyPreferences.getActiveInstance(LoginActivity.this).setIsLoggedIn(true);
        MyPreferences.getActiveInstance(LoginActivity.this).setUserId(userData.getUserId());
        MyPreferences.getActiveInstance(LoginActivity.this).setFirstName(userData.getFirstName());
        gotoDasbord();
    }
}