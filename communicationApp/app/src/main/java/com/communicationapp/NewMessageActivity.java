package com.communicationapp;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.communicationapp.adapter.SppinnerAdapter;
import com.communicationapp.adapter.spinnerAdapter;
import com.communicationapp.comman.MyPreferences;
import com.communicationapp.interfaces.ProgressClickListener;
import com.communicationapp.model.ContactList;
import com.communicationapp.responseparser.ContactResponse;
import com.communicationapp.responseparser.NewMessageResponse;
import com.communicationapp.comman.CommonMethod;
import com.communicationapp.comman.Constants;
import com.communicationapp.comman.ProgressBarDialog;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class NewMessageActivity extends AppCompatActivity implements  View.OnClickListener, ProgressClickListener {
    private static final String TAG = "NewMessageActivity";

    /*****************Attributes name***************************/

    private EditText metMessage;
    private Button btnSend;
    public static List<ContactList> contactList;
    private String receiver_id = "";
    private RelativeLayout container;
    private ImageView mivLock;
    private boolean isChecked = false;
    private ProgressBarDialog mProgressDialog;
    private PopupWindow mPopupWindow;
    private boolean expanded;
    private StringBuilder builder;
    private TextView SelectBox;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_message);
        getRefrenceId();
        setToolbarWidget();
        if(contactList!=null&&contactList.size()>0){
            contactList.clear();
        }
        if (CommonMethod.isOnline(NewMessageActivity.this)) {
            validateGetContactList();
        } else {
            CommonMethod.showSnack(container, getResources().getString(R.string.error_no_internet));
        }
        if (MyPreferences.getActiveInstance(NewMessageActivity.this).getIsSecure()) {
            mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_red));
        } else {
            mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_green));
        }

    }

    /*******************Method for get Attributes Id*********************************/

    public void getRefrenceId() {
        mProgressDialog = new ProgressBarDialog(NewMessageActivity.this, null);
        metMessage = (EditText) findViewById(R.id.etMessage);
        btnSend = (Button) findViewById(R.id.btnSend);
        container = (RelativeLayout) findViewById(R.id.container);

        btnSend.setOnClickListener(this);
    }

    /*******************Method for Action bar Setting*********************************/

    private void setToolbarWidget() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow_24dp);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                CommonMethod.closeKeyboard(NewMessageActivity.this, v);
            }
        });
        getSupportActionBar().setTitle("");
        mivLock = (ImageView) findViewById(R.id.ivLock);
        mivLock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isChecked == false) {

                    MyPreferences.getActiveInstance(NewMessageActivity.this).setIsSecure(true);
                    mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_red));
                    isChecked = true;
                } else {
                    MyPreferences.getActiveInstance(NewMessageActivity.this).setIsSecure(false);
                    mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_green));
                    isChecked = false;
                }

            }
        });

    }

    /*******************Action perform on Attributes *********************************/

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSend:
                CommonMethod.closeKeyboard(NewMessageActivity.this, view);
                if(CommonMethod.isOnline(NewMessageActivity.this)) {
                    receiver_id="";
                    builder =new StringBuilder();
                    for(int i =0;i<contactList.size();i++){
                        if(contactList.get(i).isChecked()){
                            builder.append(contactList.get(i).getUserId());
                            builder.append(",");
                        }
                    }
                    if (builder.toString().endsWith(",")) {
                        receiver_id = builder.toString().substring(0, builder.toString().length() - 1);
                    }
                    if (validationNewMessage(view)) {
                        validateSendMessage();
                    }
                }else {
                    CommonMethod.showSnack(view,getResources().getString(R.string.error_no_internet));

                }
                    break;

        }

    }



    /*******************Check Validation on Attributes *********************************/

    private boolean validationNewMessage(View view) {
        if (receiver_id.equalsIgnoreCase("")) {
            CommonMethod.showSnackbar(NewMessageActivity.this, view, "Please select user");
            return false;
        } else if (metMessage.getText().toString().trim().length() == 0) {
            CommonMethod.showSnackbar(NewMessageActivity.this, view, "Please enter your message");
            return false;
        }
        return true;

    }

    @Override
    public void onRetryClick() {
        mProgressDialog.showContent();
    }

    private void validateSendMessage() {
        mProgressDialog.showLoading();
        AndroidNetworking.post(Constants.Creating_Conversation)
                .addBodyParameter("reply", metMessage.getText().toString().trim())
                .addBodyParameter("user_two", receiver_id)
                .addBodyParameter("user_one", MyPreferences.getActiveInstance(NewMessageActivity.this).getUserId())
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        mProgressDialog.showContent();
                        Gson gson = new Gson();
                        NewMessageResponse mNewMessageResponse = gson.fromJson(response.toString(), NewMessageResponse.class);
                        if (mNewMessageResponse != null) {
                            if (mNewMessageResponse.getCode().equals("200")) {

                                Toast.makeText(NewMessageActivity.this,mNewMessageResponse.getMessage(),Toast.LENGTH_SHORT).show();
                                finish();

                            } else {
                                CommonMethod.showSnackbar(NewMessageActivity.this, container, mNewMessageResponse.getMessage());

                            }
                        } else {

                            CommonMethod.showSnackbar(NewMessageActivity.this, container, getString(R.string.connection_error));
                        }

                    }

                    @Override
                    public void onError(ANError anError) {

                        mProgressDialog.showRetry();
                        CommonMethod.showNetworkSnack(container, getResources().getString(R.string.error_server), "Ok");
                    }

                });
    }

    /*******************method network connection*********************************/

    private void validateGetContactList() {

        mProgressDialog.showLoading();

        AndroidNetworking.post(Constants.GET_CONTACT_LIST)
                .addBodyParameter("user_id", MyPreferences.getActiveInstance(NewMessageActivity.this).getUserId())
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        mProgressDialog.showContent();
                        Gson gson = new Gson();
                        ContactResponse mProfileResponse = gson.fromJson(response.toString(), ContactResponse.class);
                        if (mProfileResponse != null) {
                            if (mProfileResponse.getSuccess().equals("1")) {

                                parseData(mProfileResponse.getContactList());

                            } else {
                                CommonMethod.showSnackbar(NewMessageActivity.this, container, mProfileResponse.getMessage());

                            }
                        } else {

                            CommonMethod.showSnackbar(NewMessageActivity.this, container, getString(R.string.connection_error));
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        mProgressDialog.showRetry();
                        CommonMethod.showNetworkSnack(container, getResources().getString(R.string.error_server), "Ok");
                    }

                });
    }

    private void parseData(List<ContactList> contactList) {
        this.contactList = contactList;
        initialize();
    }




    /*
     * Function to set up initial settings: Creating the data source for drop-down list, initialising the checkselected[], set the drop-down list
     * */
    private void initialize(){

	/*SelectBox is the TextView where the selected values will be displayed in the form of "Item 1 & 'n' more".
    	 * When this selectBox is clicked it will display all the selected values
    	 * and when clicked again it will display in shortened representation as before.
    	 * */
        SelectBox = (TextView) findViewById(R.id.SelectBox);
        SelectBox.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if(!expanded){
                    //display all selected values
                    String selected = "";
                    int flag = 0;
                    for (int i = 0; i < contactList.size(); i++) {
                        if (contactList.get(i).isChecked() == true) {
                            selected += contactList.get(i);
                            selected += ", ";
                            flag = 1;
                        }
                    }
                    if(flag==1)
                        SelectBox.setText(selected);
                    expanded =true;
                }
                else{
                    //display shortened representation of selected values
                    SelectBox.setText(SppinnerAdapter.getSelected());
                    expanded = false;
                }
            }
        });

        //onClickListener to initiate the dropDown list
        Button createButton = (Button)findViewById(R.id.create);
        createButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub
                initiatePopUp(contactList,SelectBox);
            }
        });
    }

    /*
     * Function to set up the pop-up window which acts as drop-down list
     * */
    private void initiatePopUp(List<ContactList> contactList, TextView tv){
        LayoutInflater inflater = (LayoutInflater)NewMessageActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        //get the pop-up window i.e.  drop-down layout
        LinearLayout layout = (LinearLayout)inflater.inflate(R.layout.pop_up_window, (ViewGroup)findViewById(R.id.PopUpView));

        //get the view to which drop-down layout is to be anchored
        final RelativeLayout layout1 = (RelativeLayout)findViewById(R.id.relativeLayout);

        mPopupWindow = new PopupWindow(layout, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);
        mPopupWindow.setWidth(layout1.getWidth());
        //Pop-up window background cannot be null if we want the pop-up to listen touch events outside its window
        mPopupWindow.setBackgroundDrawable(new BitmapDrawable());
        mPopupWindow.setTouchable(true);

        //let pop-up be informed about touch events outside its window. This  should be done before setting the content of pop-up
        mPopupWindow.setOutsideTouchable(true);
        mPopupWindow.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);

        //dismiss the pop-up i.e. drop-down when touched anywhere outside the pop-up
        mPopupWindow.setTouchInterceptor(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
                    mPopupWindow.dismiss();
                    return true;
                }
                return false;
            }
        });

        //provide the source layout for drop-down
        mPopupWindow.setContentView(layout);

        //anchor the drop-down to bottom-left corner of 'layout1'
        mPopupWindow.showAsDropDown(layout1);

        //populate the drop-down list
        final ListView list = (ListView) layout.findViewById(R.id.dropDownList);
        SppinnerAdapter adapter = new SppinnerAdapter(this, contactList, tv);
        list.setAdapter(adapter);
    }

}
