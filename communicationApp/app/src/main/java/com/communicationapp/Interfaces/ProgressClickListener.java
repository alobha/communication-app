package com.communicationapp.interfaces;

/**
 * Created by ather on 12/12/17.
 */

public interface ProgressClickListener {

    void onRetryClick();
}
