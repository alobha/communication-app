package com.communicationapp;

import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.communicationapp.adapter.spinnerAdapter;
import com.communicationapp.comman.MyPreferences;
import com.communicationapp.interfaces.ProgressClickListener;
import com.communicationapp.model.ContactList;
import com.communicationapp.responseparser.ContactResponse;
import com.communicationapp.responseparser.NewMailResponse;
import com.communicationapp.comman.CommonMethod;
import com.communicationapp.comman.Constants;
import com.communicationapp.comman.ProgressBarDialog;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.List;

public class NewMailActivity extends AppCompatActivity implements View.OnClickListener, ProgressClickListener {
    private static final String TAG = "NewMailActivity";

    /*****************Attributes name***************************/

    private EditText metSubject;
    private EditText metMessage;
    private Button btnSend;
    public static List<ContactList> contactList;
    private String receiver_id = "";
    private String isSecure;
    private ImageView mivLock;
    private boolean isChecked = false;
    private RelativeLayout container;
    private ProgressBarDialog mProgressDialog;
    private PopupWindow mPopupWindow;
    private boolean expanded;
    private StringBuilder builder;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newmail);
        getRefrenceId();
        setToolbarWidget();
        if(contactList!=null&&contactList.size()>0){
            contactList.clear();
        }
        if (CommonMethod.isOnline(NewMailActivity.this)) {
            validateGetContactList();
        } else {
            CommonMethod.showSnack(container, getResources().getString(R.string.error_no_internet));
        }
        if (MyPreferences.getActiveInstance(NewMailActivity.this).getIsSecure()) {
            mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_red));
        } else {
            mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_green));
        }
    }

    /*******************Method for get Attributes Id*********************************/

    public void getRefrenceId() {
        mProgressDialog = new ProgressBarDialog(NewMailActivity.this, null);
        metSubject = (EditText) findViewById(R.id.etSubject);
        metMessage = (EditText) findViewById(R.id.etMessage);
        btnSend = (Button) findViewById(R.id.btnSend);
        container = (RelativeLayout) findViewById(R.id.container);
        btnSend.setOnClickListener(this);

        mivLock = (ImageView) findViewById(R.id.ivLock);
        mivLock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isChecked == false) {
                    MyPreferences.getActiveInstance(NewMailActivity.this).setIsSecure(true);
                    mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_red));
                    isChecked = true;
                } else {
                    MyPreferences.getActiveInstance(NewMailActivity.this).setIsSecure(false);
                    mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_green));
                    isChecked = false;
                }

            }
        });
    }

    /*******************Method for Action bar Setting*********************************/

    private void setToolbarWidget() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow_24dp);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                CommonMethod.closeKeyboard(NewMailActivity.this, v);
            }
        });
        getSupportActionBar().setTitle("");

    }

    /*******************Action perform on Attributes *********************************/


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.btnSend:
                CommonMethod.closeKeyboard(NewMailActivity.this, view);
                if (CommonMethod.isOnline(NewMailActivity.this)) {
                    if (MyPreferences.getActiveInstance(NewMailActivity.this).getIsSecure()) {
                        mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_red));
                        isSecure = "1";

                    } else {
                        mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_green));
                        isSecure = "0";
                    }
                    receiver_id = "";
                    builder = new StringBuilder();
                    for (int i = 0; i < contactList.size(); i++) {

                        if (contactList.get(i).isChecked()) {
                            builder.append(contactList.get(i).getUserId());
                            builder.append(",");
                        }
                    }
                    if (builder.toString().endsWith(",")) {
                        receiver_id = builder.toString().substring(0, builder.toString().length() - 1);
                    }
                    if (validationNewMail(view)) {
                        validateSendMail();
                    }
                } else {
                    CommonMethod.showSnack(view, getResources().getString(R.string.error_no_internet));
                }
                break;

        }

    }


    /*******************Check Validation on Attributes *********************************/

    private boolean validationNewMail(View view) {
        if (receiver_id.equalsIgnoreCase("")) {
            CommonMethod.showSnackbar(NewMailActivity.this, view, "Please select user");
            return false;
        } else if (metSubject.getText().toString().trim().length() == 0) {
            CommonMethod.showSnackbar(NewMailActivity.this, view, "Please enter your subject");
            return false;
        } else if (metMessage.getText().toString().trim().length() == 0) {
            CommonMethod.showSnackbar(NewMailActivity.this, view, "Please enter your message");
            return false;
        }
        return true;

    }

    @Override
    public void onRetryClick() {
        mProgressDialog.showContent();
    }

    /*******************method network connection*********************************/

    private void validateSendMail() {

        mProgressDialog.showLoading();
        AndroidNetworking.post(Constants.SEND_MAIL)
                .addBodyParameter("sender_id", MyPreferences.getActiveInstance(NewMailActivity.this).getUserId())
                .addBodyParameter("receiver_id", receiver_id)
                .addBodyParameter("subject", metSubject.getText().toString().trim())
                .addBodyParameter("content", metMessage.getText().toString().trim())
                .addBodyParameter("isSecure", isSecure)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        mProgressDialog.showContent();
                        Gson gson = new Gson();
                        NewMailResponse mNewMailResponse = gson.fromJson(response.toString(), NewMailResponse.class);
                        if (mNewMailResponse != null) {
                            if (mNewMailResponse.code.equals("200")) {

                                Toast.makeText(NewMailActivity.this, mNewMailResponse.message, Toast.LENGTH_SHORT).show();
                                finish();

                            } else {
                                CommonMethod.showSnackbar(NewMailActivity.this, container, mNewMailResponse.message);

                            }
                        } else {

                            CommonMethod.showSnackbar(NewMailActivity.this, container, getString(R.string.connection_error));
                        }

                    }

                    @Override
                    public void onError(ANError anError) {

                        mProgressDialog.showRetry();
                        CommonMethod.showNetworkSnack(container, getResources().getString(R.string.error_server), "Ok");
                    }

                });
    }

    /*******************method network connection*********************************/

    private void validateGetContactList() {

        mProgressDialog.showLoading();

        AndroidNetworking.post(Constants.GET_CONTACT_LIST)
                .addBodyParameter("user_id", MyPreferences.getActiveInstance(NewMailActivity.this).getUserId())
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        mProgressDialog.showContent();
                        Gson gson = new Gson();
                        ContactResponse mProfileResponse = gson.fromJson(response.toString(), ContactResponse.class);
                        if (mProfileResponse != null) {
                            if (mProfileResponse.getSuccess().equals("1")) {

                                parseData(mProfileResponse.getContactList());

                            } else {
                                CommonMethod.showSnackbar(NewMailActivity.this, container, mProfileResponse.getMessage());

                            }
                        } else {

                            CommonMethod.showSnackbar(NewMailActivity.this, container, getString(R.string.connection_error));
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        mProgressDialog.showRetry();
                        CommonMethod.showNetworkSnack(container, getResources().getString(R.string.error_server), "Ok");
                    }

                });
    }

    private void parseData(List<ContactList> contactList) {

        this.contactList = contactList;
        initialize();
    }


    /*
     * Function to set up initial settings: Creating the data source for drop-down list, initialising the checkselected[], set the drop-down list
     * */
    private void initialize() {

	/*SelectBox is the TextView where the selected values will be displayed in the form of "Item 1 & 'n' more".
         * When this selectBox is clicked it will display all the selected values
    	 * and when clicked again it will display in shortened representation as before.
    	 * */
        final TextView SelectBox = (TextView) findViewById(R.id.SelectBox);
        SelectBox.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                if (!expanded) {
                    //display all selected values
                    String selected = "";
                    int flag = 0;
                    for (int i = 0; i < contactList.size(); i++) {
                        if (contactList.get(i).isChecked() == true) {
                            selected += contactList.get(i);
                            selected += ", ";
                            flag = 1;
                        }
                    }
                    if (flag == 1)
                        SelectBox.setText(selected);
                    expanded = true;
                } else {
                    //display shortened representation of selected values
                    SelectBox.setText(spinnerAdapter.getSelected());
                    expanded = false;
                }
            }
        });

        //onClickListener to initiate the dropDown list
        Button createButton = (Button) findViewById(R.id.create);
        createButton.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // TODO Auto-generated method stub
                initiatePopUp(contactList, SelectBox);
            }
        });
    }

    /*
     * Function to set up the pop-up window which acts as drop-down list
     * */
    private void initiatePopUp(List<ContactList> contactList, TextView tv) {
        LayoutInflater inflater = (LayoutInflater) NewMailActivity.this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        //get the pop-up window i.e.  drop-down layout
        LinearLayout layout = (LinearLayout) inflater.inflate(R.layout.pop_up_window, (ViewGroup) findViewById(R.id.PopUpView));

        //get the view to which drop-down layout is to be anchored
        final RelativeLayout layout1 = (RelativeLayout) findViewById(R.id.relativeLayout);
        mPopupWindow = new PopupWindow(layout, ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT, true);
        mPopupWindow.setWidth(layout1.getWidth());
        //Pop-up window background cannot be null if we want the pop-up to listen touch events outside its window
        mPopupWindow.setBackgroundDrawable(new BitmapDrawable());
        mPopupWindow.setTouchable(true);

        //let pop-up be informed about touch events outside its window. This  should be done before setting the content of pop-up
        mPopupWindow.setOutsideTouchable(true);
        mPopupWindow.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);

        //dismiss the pop-up i.e. drop-down when touched anywhere outside the pop-up
        mPopupWindow.setTouchInterceptor(new View.OnTouchListener() {

            public boolean onTouch(View v, MotionEvent event) {
                // TODO Auto-generated method stub
                if (event.getAction() == MotionEvent.ACTION_OUTSIDE) {
                    mPopupWindow.dismiss();
                    return true;
                }
                return false;
            }
        });

        //provide the source layout for drop-down
        mPopupWindow.setContentView(layout);

        //anchor the drop-down to bottom-left corner of 'layout1'
        mPopupWindow.showAsDropDown(layout1);

        //populate the drop-down list
        final ListView list = (ListView) layout.findViewById(R.id.dropDownList);
        spinnerAdapter adapter = new spinnerAdapter(this, contactList, tv);
        list.setAdapter(adapter);
    }

}
