package com.communicationapp;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.communicationapp.interfaces.ProgressClickListener;
import com.communicationapp.responseparser.ChangePasswordResponse;
import com.communicationapp.comman.CommonMethod;
import com.communicationapp.comman.Constants;
import com.communicationapp.comman.MyPreferences;
import com.communicationapp.comman.ProgressBarDialog;
import com.google.gson.Gson;

import org.json.JSONObject;

/**
 * Created by mac on 06/07/18.
 */

public class ChangePasswordActivity extends AppCompatActivity implements View.OnClickListener,ProgressClickListener {

    /**************Attributes name**********************/
    private EditText metOldPassword;
    private EditText metNewPassword;
    private EditText metConfirmPassword;
    private Button mbtnSend;
    private LinearLayout container;
    private ProgressBarDialog mProgressDialog;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        getRefrenceId();
        setToolbarWidget();

    }
    /*******************Method for get Attributes Id*********************************/

    private void getRefrenceId(){
        container = (LinearLayout)findViewById(R.id.container);
        metOldPassword = (EditText)findViewById(R.id.etOldPassword);
        metNewPassword= (EditText)findViewById(R.id.etNewPassword);
        metConfirmPassword = (EditText)findViewById(R.id.etConfirmPassword);
        mbtnSend = (Button) findViewById(R.id.btnSend);
        mProgressDialog = new ProgressBarDialog(this, container);
        mbtnSend.setOnClickListener(this);
    }
    /*******************Method for Action bar Setting*********************************/

    private void setToolbarWidget() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow_24dp);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();

            }
        });
        getSupportActionBar().setTitle("");

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.btnSend :
                if(CommonMethod.isOnline(ChangePasswordActivity.this)){
                    if (validationResetPassword(v)) {
                        validateResetPassword();
                    }
                }else {
                    CommonMethod.showSnackbar(ChangePasswordActivity.this,container,getResources().getString(R.string.error_no_internet));
                }

            break;
        }

    }

    /*******************Check Validation on Attributes *********************************/

    private boolean validationResetPassword(View view) {

        if (metOldPassword.getText().toString().trim().length() == 0) {
            metOldPassword.setText("");
            CommonMethod.showSnackbar(ChangePasswordActivity.this,view,getString(R.string.password_olderror));
            return false;
        }
        if (metNewPassword.getText().toString().trim().length() == 0) {
            metNewPassword.setText("");
            CommonMethod.showSnackbar(ChangePasswordActivity.this,view,getString(R.string.password_newerror));
            return false;
        } else if (metNewPassword.getText().toString().trim().length()< 8 || metNewPassword.getText().toString().trim().length() > 30) {
            metNewPassword.setText("");
            CommonMethod.showSnackbar(ChangePasswordActivity.this,view,getString(R.string.password_limit_error));
            return false;
        } else if (metConfirmPassword.getText().toString().trim().length()==0) {
            CommonMethod.showSnackbar(ChangePasswordActivity.this,view,"Please re-enter your password");
            return false;
        } else if (!metNewPassword.getText().toString().trim().equalsIgnoreCase(metConfirmPassword.getText().toString().trim())) {
            CommonMethod.showSnackbar(ChangePasswordActivity.this,view,"Password did not match");
            return false;
        }
        return true;

    }

    @Override
    public void onRetryClick() {
        mProgressDialog.showContent();
        if(CommonMethod.isOnline(ChangePasswordActivity.this)){
            validateResetPassword();
        }else {
            CommonMethod.showSnackbar(ChangePasswordActivity.this,container,getResources().getString(R.string.error_no_internet));
        }
    }
    /*******************method network connection*********************************/

    private void validateResetPassword() {

        mProgressDialog.showLoading();
        AndroidNetworking.post(Constants.RESET_PASSWORD)
                .addBodyParameter("user_id", MyPreferences.getActiveInstance(ChangePasswordActivity.this).getUserId())
                .addBodyParameter("old_password", metOldPassword.getText().toString().trim())
                .addBodyParameter("new_password", metNewPassword.getText().toString().trim())
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        mProgressDialog.showContent();
                        Gson gson = new Gson();
                        ChangePasswordResponse mProfileResponse = gson.fromJson(response.toString(), ChangePasswordResponse.class);
                        if (mProfileResponse != null) {
                            if (mProfileResponse.getSuccess().equals("1")) {
                                metNewPassword.setText("");
                                metOldPassword.setText("");
                                metConfirmPassword.setText("");
                                Toast.makeText(ChangePasswordActivity.this,mProfileResponse.getMessage(),Toast.LENGTH_SHORT).show();
                                finish();

                            } else {
                                CommonMethod.showSnackbar(ChangePasswordActivity.this,container, mProfileResponse.getMessage());

                            }
                        } else {

                            CommonMethod.showSnackbar(ChangePasswordActivity.this,container, getString(R.string.connection_error));
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        mProgressDialog.showRetry();
                        CommonMethod.showNetworkSnack(container,getResources().getString(R.string.error_server),"Ok");
                    }

                });
    }

}
