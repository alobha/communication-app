package com.communicationapp;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.communicationapp.adapter.RecivedFileAdapter;
import com.communicationapp.adapter.RecivedFileAdapterAudio;
import com.communicationapp.adapter.SendFileAdapter;
import com.communicationapp.comman.CommonMethod;
import com.communicationapp.comman.Constants;
import com.communicationapp.comman.FileOpen;
import com.communicationapp.comman.FilePath;
import com.communicationapp.comman.MyPreferences;
import com.communicationapp.comman.ProgressBarDialog;
import com.communicationapp.comman.RealPathUtil;
import com.communicationapp.fragment.FragmentSentAudio;
import com.communicationapp.fragment.FrangmentRecevieAudio;
import com.communicationapp.interfaces.ProgressClickListener;
import com.communicationapp.model.ReceivedFilesList;
import com.communicationapp.model.SendFilesList;
import com.communicationapp.responseparser.FileResponse;
import com.communicationapp.responseparser.SendLocationResponse;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class VideoActivity extends AppCompatActivity implements View.OnClickListener, ProgressClickListener{
    private static final String TAG = "VideoActivity";

    /*****************Attributes name***************************/

    private ImageView mivLock;
    private boolean isChecked = false;
    private ImageView mAddbutton;
    private TextView mtvSent;
    private TextView mtvRECEIVED;
    private static final int PICK_IMAGE_REQUEST = 1;
    private static final int VIDEO_REQUEST = 2;
    private CoordinatorLayout container;
    private ProgressBarDialog mProgressDialog;
    private List<ReceivedFilesList> receivedFilesList;
    private List<SendFilesList> sendFilesList;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video);
        getRefrenceId();
        setToolbarWidget();
        if (MyPreferences.getActiveInstance(VideoActivity.this).getIsSecure()) {
            mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_red));
        } else {
            mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_green));
        }

    }

    /*******************Method for Action bar Setting*********************************/

    private void setToolbarWidget() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow_24dp);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        getSupportActionBar().setTitle("");

    }

    /*******************Method for get Attributes Id*********************************/

    private void getRefrenceId() {

        FrameLayout llCointer = (FrameLayout) findViewById(R.id.llCointer);
        mProgressDialog = new ProgressBarDialog(VideoActivity.this, llCointer);

        mAddbutton = (ImageView) findViewById(R.id.ivAdd);
        mtvSent = (TextView) findViewById(R.id.tvSent);
        mtvRECEIVED = (TextView) findViewById(R.id.tvRECEIVED);
        container = (CoordinatorLayout) findViewById(R.id.container);
        mAddbutton.setOnClickListener(this);
        mtvSent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mtvSent.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                mtvSent.setTextColor(getResources().getColor(R.color.colorWhite));

                mtvRECEIVED.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                mtvRECEIVED.setTextColor(getResources().getColor(R.color.colorPrimary));
                fragmentReplace(new FragmentSentAudio(sendFilesList));
                mAddbutton.setVisibility(View.VISIBLE);
                CommonMethod.isReceved=false;

            }
        });
        mtvRECEIVED.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mtvSent.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                mtvSent.setTextColor(getResources().getColor(R.color.colorPrimary));

                mtvRECEIVED.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                mtvRECEIVED.setTextColor(getResources().getColor(R.color.colorWhite));
                fragmentReplace(new FrangmentRecevieAudio(receivedFilesList));
                mAddbutton.setVisibility(View.GONE);
                CommonMethod.isReceved=true;


            }
        });

        mivLock = (ImageView) findViewById(R.id.ivLock);
        mivLock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isChecked == false) {
                    MyPreferences.getActiveInstance(VideoActivity.this).setIsSecure(true);
                    mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_red));
                    isChecked = true;
                } else {
                    MyPreferences.getActiveInstance(VideoActivity.this).setIsSecure(false);
                    mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_green));
                    isChecked = false;
                }

            }
        });


    }



    /*******************Action perform on Attributes *********************************/

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivAdd:
                selectDialogueVideo();
                CommonMethod.flag=true;
                break;

        }
    }

    /******************Retrive file here **********************/

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == RESULT_OK) {
            if (requestCode == PICK_IMAGE_REQUEST && data != null && data.getData() != null) {
                Uri uri = data.getData();
                String path = RealPathUtil.getRealPathFromURI_API19(VideoActivity.this, uri);
                Intent shareintent = new Intent(VideoActivity.this, ContactListActivity.class);
                shareintent.putExtra("type", "video/image");
                shareintent.putExtra("navigate", "Video");
                shareintent.putExtra("path", path);
                startActivity(shareintent);

            } else {
                //the selected audio.
                Uri uri = data.getData();
                String path = String.valueOf(uri);//FilePath.getPath(VideoActivity.this, uri);
                Intent shareintent = new Intent(VideoActivity.this, ContactListActivity.class);
                shareintent.putExtra("type", "video/image");
                shareintent.putExtra("navigate", "Video");
                shareintent.putExtra("path", path);
                startActivity(shareintent);
            }
        }

    }

    /******************method for dialogue box choose Video or image **********************/

    private void selectDialogueVideo() {
        final CharSequence[] items = {"Choose image", "Choose video",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(VideoActivity.this);
        builder.setTitle("Add Video");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Choose image")) {
                    Intent intent = new Intent();
                    // Show only images, no videos or anything else
                    intent.setType("image/*");
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    // Always show the chooser (if there are multiple options available)
                    startActivityForResult(Intent.createChooser(intent, "Select Picture"), PICK_IMAGE_REQUEST);

                }
                if (items[item].equals("Choose video")) {

                    Intent intent2 = new Intent();
                    intent2.setType("video/*");
                    intent2.setAction(Intent.ACTION_GET_CONTENT);
                    startActivityForResult(Intent.createChooser(intent2, "Select Video"), VIDEO_REQUEST);

                }
            }
        });
        builder.show();
    }

    @Override
    public void onRetryClick() {
        mProgressDialog.showContent();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!CommonMethod.flag) {
            validateGetFileList();
            callUpdateCountAPI();
        }
    }

    /*******************method network connection*********************************/

    private void validateGetFileList() {

        mProgressDialog.showLoading();
        AndroidNetworking.post(Constants.ReceiveFiles)
                .addBodyParameter("user_id", MyPreferences.getActiveInstance(VideoActivity.this).getUserId())
                .addBodyParameter("type", "video/image")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        mProgressDialog.showContent();
                        Gson gson = new Gson();
                        FileResponse mFileResponse = gson.fromJson(response.toString(), FileResponse.class);
                        if (mFileResponse != null) {
                            if (mFileResponse.getCode().equals("200")) {

                                parseData(mFileResponse.getReceivedFilesList(), mFileResponse.getSendFilesList());

                            }

                        } else {

                            mProgressDialog.showRetry();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        mProgressDialog.showRetry();
                        CommonMethod.showNetworkSnack(container, getResources().getString(R.string.error_server), "Ok");
                    }

                });
    }

    private void parseData(List<ReceivedFilesList> receivedFilesList, List<SendFilesList> sendFilesList) {
        this.receivedFilesList = receivedFilesList;
        this.sendFilesList = sendFilesList;
        if(CommonMethod.isReceved){
            fragmentReplace(new FrangmentRecevieAudio(receivedFilesList));

        }else {
            fragmentReplace(new FragmentSentAudio(sendFilesList));

        }

    }

    private void fragmentReplace(Fragment fragment) {

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.llCointer, fragment);
        transaction.commit();
    }

    private void callUpdateCountAPI() {
        if (CommonMethod.isOnline(this)) {
            updateBadgeCountAPI();
        }
    }

    private void updateBadgeCountAPI() {
        AndroidNetworking.post(Constants.UPDATE_BADGE)
                .addBodyParameter("type", "video/image")
                .addBodyParameter("receiver_id", MyPreferences.getActiveInstance(this).getUserId())
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });

    }

}
