package com.communicationapp.responseparser;

import com.communicationapp.model.AudioList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class AudioResponse {

    @SerializedName("responseCode")
    @Expose
    public String responsecode;
    @SerializedName("responseMessage")
    @Expose
    public String responsemessage;
    @SerializedName("audioList")
    @Expose
    public ArrayList<AudioList> audioList = new ArrayList<AudioList>();

}
