package com.communicationapp.responseparser;

/**
 * Created by mac on 07/07/18.
 */

import com.communicationapp.model.ReceiveLocationuserList;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ReciveLocationUserResponse {

    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("ReceiveLocationuserList")
    @Expose
    private List<ReceiveLocationuserList> receiveLocationuserList = null;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ReceiveLocationuserList> getreceiveLocationuserList() {
        return receiveLocationuserList;
    }

    public void setSendedLocationuserList(List<ReceiveLocationuserList> receiveLocationuserList) {
        this.receiveLocationuserList = receiveLocationuserList;
    }
}
