package com.communicationapp.responseparser;

import com.communicationapp.model.VideoList;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class VideoResponse {

    @SerializedName("responseCode")
    @Expose
    public String responsecode;
    @SerializedName("responseMessage")
    @Expose
    public String responsemessage;
    @SerializedName("audioList")
    @Expose
    public ArrayList<VideoList> videoList = new ArrayList<VideoList>();
}
