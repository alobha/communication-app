package com.communicationapp.responseparser;

import com.communicationapp.model.CloudeList;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CloudeResponse {
    @SerializedName("responseCode")
    @Expose
    public String responsecode;
    @SerializedName("responseMessage")
    @Expose
    public String responsemessage;
    @SerializedName("audioList")
    @Expose
    public ArrayList<CloudeList> cloudeList = new ArrayList<CloudeList>();

}
