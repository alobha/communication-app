package com.communicationapp.responseparser;

import com.communicationapp.model.RegisterDetails;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegisterResponse {

    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("registerDetails")
    @Expose
    private RegisterDetails registerDetails;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public RegisterDetails getRegisterDetails() {
        return registerDetails;
    }

    public void setRegisterDetails(RegisterDetails registerDetails) {
        this.registerDetails = registerDetails;
    }
}
