package com.communicationapp.responseparser;

import com.communicationapp.model.ReceivedFilesList;
import com.communicationapp.model.SendFilesList;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mac on 10/07/18.
 */

public class FileResponse {

    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("receivedFilesList")
    @Expose
    private List<ReceivedFilesList> receivedFilesList = null;
    @SerializedName("sendFilesList")
    @Expose
    private List<SendFilesList> sendFilesList = null;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<ReceivedFilesList> getReceivedFilesList() {
        return receivedFilesList;
    }

    public void setReceivedFilesList(List<ReceivedFilesList> receivedFilesList) {
        this.receivedFilesList = receivedFilesList;
    }

    public List<SendFilesList> getSendFilesList() {
        return sendFilesList;
    }

    public void setSendFilesList(List<SendFilesList> sendFilesList) {
        this.sendFilesList = sendFilesList;
    }
}
