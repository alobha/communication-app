package com.communicationapp.responseparser;

import com.communicationapp.model.SendedLocationuserList;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mac on 06/07/18.
 */

public class SentLocationResponse {

    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("sendedLocationuserList")
    @Expose
    private List<SendedLocationuserList> sendedLocationuserList = null;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<SendedLocationuserList> getSendedLocationuserList() {
        return sendedLocationuserList;
    }

    public void setSendedLocationuserList(List<SendedLocationuserList> sendedLocationuserList) {
        this.sendedLocationuserList = sendedLocationuserList;
    }
}
