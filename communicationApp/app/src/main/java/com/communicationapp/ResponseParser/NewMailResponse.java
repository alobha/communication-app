package com.communicationapp.responseparser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NewMailResponse {

    @SerializedName("success")
    @Expose
    public String success;
    @SerializedName("message")
    @Expose
    public String message;
    @SerializedName("code")
    @Expose
    public String code;


}
