package com.communicationapp.responseparser;

import com.communicationapp.model.RecevedList;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class RecevedResponse {

    @SerializedName("responseCode")
    @Expose
    public String responsecode;
    @SerializedName("responseMessage")
    @Expose
    public String responsemessage;
    @SerializedName("audioList")
    @Expose
    public ArrayList<RecevedList> recevedList = new ArrayList<RecevedList>();
}
