package com.communicationapp.responseparser;

import com.communicationapp.model.AllmailList;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by mac on 05/07/18.
 */

public class MailResponse {

    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("AllmailList")
    @Expose
    private List<AllmailList> allmailList = null;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<AllmailList> getAllmailList() {
        return allmailList;
    }

    public void setAllmailList(List<AllmailList> allmailList) {
        this.allmailList = allmailList;
    }
}
