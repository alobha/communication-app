package com.communicationapp.responseparser;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mac on 11/07/18.
 */

public class BadgeResponse {

    @SerializedName("success")
    @Expose
    private String success;
    @SerializedName("code")
    @Expose
    private String code;
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("GetReceiveEmailCount")
    @Expose
    private Integer getReceiveEmailCount;
    @SerializedName("GetReceiveLocationCount")
    @Expose
    private Integer getReceiveLocationCount;
    @SerializedName("GetmessageCount")
    @Expose
    private Integer getmessageCount;
    @SerializedName("videoCount")
    @Expose
    private Integer videoCount;
    @SerializedName("otherCount")
    @Expose
    private Integer otherCount;
    @SerializedName("audioCount")
    @Expose
    private Integer audioCount;

    public String getSuccess() {
        return success;
    }

    public void setSuccess(String success) {
        this.success = success;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Integer getGetReceiveEmailCount() {
        return getReceiveEmailCount;
    }

    public void setGetReceiveEmailCount(Integer getReceiveEmailCount) {
        this.getReceiveEmailCount = getReceiveEmailCount;
    }

    public Integer getGetReceiveLocationCount() {
        return getReceiveLocationCount;
    }

    public void setGetReceiveLocationCount(Integer getReceiveLocationCount) {
        this.getReceiveLocationCount = getReceiveLocationCount;
    }

    public Integer getGetmessageCount() {
        return getmessageCount;
    }

    public void setGetmessageCount(Integer getmessageCount) {
        this.getmessageCount = getmessageCount;
    }

    public Integer getVideoCount() {
        return videoCount;
    }

    public void setVideoCount(Integer videoCount) {
        this.videoCount = videoCount;
    }

    public Integer getOtherCount() {
        return otherCount;
    }

    public void setOtherCount(Integer otherCount) {
        this.otherCount = otherCount;
    }

    public Integer getAudioCount() {
        return audioCount;
    }

    public void setAudioCount(Integer audioCount) {
        this.audioCount = audioCount;
    }

}
