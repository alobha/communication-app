package com.communicationapp;


import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.communicationapp.comman.CommonMethod;
import com.communicationapp.comman.Constants;
import com.communicationapp.comman.MyPreferences;
import com.communicationapp.comman.ProgressBarDialog;
import com.communicationapp.interfaces.ProgressClickListener;
import com.communicationapp.responseparser.NewMailResponse;
import com.google.gson.Gson;

import org.json.JSONObject;

public class EmailDetailActivity extends AppCompatActivity implements View.OnClickListener,ProgressClickListener {

    private boolean isChecked=false;
    private ImageView mivLock;
    private  Toolbar toolbar;
    private EditText metMessage;
    private Button mbtnSend;
    private TextView mtvEmail;
    private TextView mtvSubject;
    private TextView mtvMessage;
    private String email;
    private String subject;
    private String content;
    private String receiver_id;
    private String isSecure;
    private LinearLayout container;
    private ProgressBarDialog mProgressDialog;
    private String mailType;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email_details);
        getRefercnceId();
        setToolbarWidget();
        email=getIntent().getStringExtra("email");
        subject=getIntent().getStringExtra("subject");
        content=getIntent().getStringExtra("content");
        receiver_id=getIntent().getStringExtra("receiver_id");
        mailType=getIntent().getStringExtra("mailType");
        if(MyPreferences.getActiveInstance(EmailDetailActivity.this).getIsSecure()){
            mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_red));
        }else {
            mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_green));
        }

        if(mailType.equalsIgnoreCase("Send Mail"))
        {
            mbtnSend.setVisibility(View.GONE);
            metMessage.setVisibility(View.GONE);
        }else {
            mbtnSend.setVisibility(View.VISIBLE);
            metMessage.setVisibility(View.VISIBLE);
        }

        mtvEmail.setText(email);
        mtvSubject.setText(subject);
        mtvMessage.setText(content);

    }

    public void getRefercnceId(){
        mProgressDialog = new ProgressBarDialog(EmailDetailActivity.this, null);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        mivLock = (ImageView) findViewById(R.id.ivLock);
        mtvEmail = (TextView) findViewById(R.id.tvEmail);
        mtvSubject = (TextView) findViewById(R.id.tvSubject);
        mtvMessage = (TextView) findViewById(R.id.tvMessage);
        metMessage = (EditText) findViewById(R.id.etMessage);
        mbtnSend = (Button) findViewById(R.id.btnSend);
        container = (LinearLayout) findViewById(R.id.container);
        mbtnSend.setOnClickListener(this);
        mivLock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isChecked==false){
                    MyPreferences.getActiveInstance(EmailDetailActivity.this).setIsSecure(true);
                    mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_red));
                    isChecked=true;
                }else {
                    MyPreferences.getActiveInstance(EmailDetailActivity.this).setIsSecure(false);
                    mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_green));
                    isChecked=false;
                }

            }
        });
    }
    private void setToolbarWidget() {

        toolbar.setNavigationIcon(R.drawable.ic_back_arrow_24dp);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        getSupportActionBar().setTitle("");

    }
    @Override
    public void onClick(View v) {
        switch (v.getId()){

            case R.id.btnSend:
                if(MyPreferences.getActiveInstance(EmailDetailActivity.this).getIsSecure()){
                    mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_red));
                    isSecure ="1";

                }else {
                    mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_green));
                    isSecure ="0";
                }
                if(CommonMethod.isOnline(EmailDetailActivity.this)) {
                    if(validationReplay(v)){

                        validateSendMail();

                    }
                }
                break;
        }
    }


    private boolean validationReplay(View view) {
      if (metMessage.getText().toString().trim().length() == 0) {
            CommonMethod.showSnackbar(EmailDetailActivity.this,view,"Please enter your message");
            return false;
        }
        return true;

    }

    @Override
    public void onRetryClick() {
        mProgressDialog.showContent();
    }

    private void validateSendMail() {

        mProgressDialog.showLoading();
        AndroidNetworking.post(Constants.SEND_MAIL)
                .addBodyParameter("sender_id", MyPreferences.getActiveInstance(EmailDetailActivity.this).getUserId())
                .addBodyParameter("receiver_id", receiver_id)
                .addBodyParameter("subject", subject)
                .addBodyParameter("content",metMessage.getText().toString().trim())
                .addBodyParameter("isSecure", isSecure)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        mProgressDialog.showContent();
                        Gson gson = new Gson();
                        NewMailResponse mNewMailResponse = gson.fromJson(response.toString(), NewMailResponse.class);
                        if (mNewMailResponse != null) {
                            if (mNewMailResponse.code.equals("200")) {

                                Toast.makeText(EmailDetailActivity.this,mNewMailResponse.message,Toast.LENGTH_SHORT).show();
                                finish();

                            } else {
                                CommonMethod.showSnackbar(EmailDetailActivity.this,container, mNewMailResponse.message);

                            }
                        } else {

                            CommonMethod.showSnackbar(EmailDetailActivity.this,container, getString(R.string.connection_error));
                        }

                    }

                    @Override
                    public void onError(ANError anError) {

                        mProgressDialog.showRetry();
                        CommonMethod.showNetworkSnack(container,getResources().getString(R.string.error_server),"Ok");
                    }

                });
    }

}