package com.communicationapp;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.VideoView;


/**
 * Created by MMAD on 11-03-2018.
 */

public class ActivityVideoView extends AppCompatActivity {
    private static final String TAG = "ActivityVideoView";

    /*****************Attributes name***************************/

    private Toolbar toolbar;
    protected ImageView btnconti, btnstop, btnplay;
    private ProgressBar progressBar;
    private RelativeLayout llvideioview;
    private VideoView videoview;
    private String videoUrl;
    private MediaController mediacontroller;
    private Uri uri;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_videoview);
        mediacontroller = new MediaController(ActivityVideoView.this);


        getRefrenceId();
        setToolbarWidget();

        mediacontroller.setAnchorView(videoview);

        videoUrl = "https://www.youtube.com/watch?v=K7o2hTkeTNQ";
        videoview.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                videoview.pause();
            }
        });
        videoview.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            // Close the progress bar and play the video
            public void onPrepared(MediaPlayer mp) {
                progressBar.setVisibility(View.GONE);
                videoview.setBackgroundColor(0);

            }
        });
        btnconti.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnconti.setVisibility(View.GONE);
                btnstop.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.VISIBLE);
                uri = Uri.parse(videoUrl);
                videoview.setMediaController(null);
                videoview.setVideoURI(uri);
                videoview.start();
                videoview.setVisibility(View.VISIBLE);


            }
        });

        btnstop.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoview.pause();
                progressBar.setVisibility(View.GONE);
                btnplay.setVisibility(View.VISIBLE);
                btnstop.setVisibility(View.GONE);

            }
        });
        btnplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                btnplay.setVisibility(View.GONE);
                btnstop.setVisibility(View.VISIBLE);
                videoview.setVisibility(View.VISIBLE);
                videoview.start();

            }
        });


    }
    /*******************Method for get Attributes Id*********************************/

    private void getRefrenceId(){
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        btnconti = (ImageView) findViewById(R.id.btnconti);
        btnstop = (ImageView)findViewById(R.id.btnstop);
        btnplay = (ImageView) findViewById(R.id.btnplay);
        progressBar = (ProgressBar)findViewById(R.id.progrss);
        llvideioview = (RelativeLayout) findViewById(R.id.llvideioview);
        videoview = (VideoView) findViewById(R.id.videoview);

    }

    /*******************Method for Action bar Setting*********************************/


    private void setToolbarWidget() {
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow_24dp);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();

            }
        });
        getSupportActionBar().setTitle("");
        getSupportActionBar().setDisplayShowTitleEnabled(false);


    }
}
