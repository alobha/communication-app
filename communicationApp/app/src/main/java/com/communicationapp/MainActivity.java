package com.communicationapp;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.communicationapp.adapter.DrawerItemCustomAdapter;
import com.communicationapp.fragment.HistoryFragment;
import com.communicationapp.fragment.HomeFragment;
import com.communicationapp.fragment.ContactFragment;
import com.communicationapp.fragment.ProfileFragment;
import com.communicationapp.comman.MyPreferences;
import com.communicationapp.model.DataModel;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";

    /*****************Attributes name***************************/
    private String[] mNavigationDrawerItemTitles;
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private Toolbar toolbar;
    private ImageView mivLock;
    public ImageView mivEdit;
    private ImageView mdirect;
    private TextView mtvHeadrTitle;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    android.support.v7.app.ActionBarDrawerToggle mDrawerToggle;
    Fragment fragment = null;
    private boolean isChecked = false;
    private Boolean exit = false;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mTitle = mDrawerTitle = getTitle();

        getRefrenceId();
        setupToolbar();
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        getSupportActionBar().setHomeButtonEnabled(true);

        /*******************Model Create For Side Menu List *********************************/
        DataModel[] drawerItem = new DataModel[5];
        drawerItem[0] = new DataModel("HOME");
        drawerItem[1] = new DataModel("CONTACTS");
        drawerItem[2] = new DataModel("PROFILE");
        drawerItem[3] = new DataModel("HISTORY");
        drawerItem[4] = new DataModel("LOGOUT");

        DrawerItemCustomAdapter adapter = new DrawerItemCustomAdapter(this, R.layout.list_view_item_row, drawerItem);
        mDrawerList.setAdapter(adapter);
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        setupDrawerToggle();

        fragmentReplace(new HomeFragment());

    }

    /*******************Method for get Attributes Id*********************************/

    private void getRefrenceId() {

        mNavigationDrawerItemTitles = getResources().getStringArray(R.array.navigation_drawer_items_array);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

    }

    /*******************Action perform on side menu list *********************************/

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }

    }

    /*******************Method for fragment call from side menu list *********************************/

    private void selectItem(int position) {

        switch (position) {
            case 0:
                fragment = new HomeFragment();
                mdirect.setVisibility(View.GONE);
                mivLock.setVisibility(View.VISIBLE);
                mivEdit.setVisibility(View.GONE);
                fragmentReplace(fragment);
                break;
            case 1:
                fragment = new ContactFragment();
                mdirect.setVisibility(View.GONE);
                mivLock.setVisibility(View.GONE);
                mivEdit.setVisibility(View.GONE);
                break;
            case 2:
                fragment = new ProfileFragment();
                mdirect.setVisibility(View.GONE);
                mivLock.setVisibility(View.GONE);
                mivEdit.setVisibility(View.VISIBLE);

                break;
            case 3:
                fragment = new HistoryFragment();
                mdirect.setVisibility(View.GONE);
                mivLock.setVisibility(View.GONE);
                mivEdit.setVisibility(View.GONE);
                break;
            case 4:
                gotoLoginActivity();
                break;

            default:
                break;
        }

        if (fragment != null) {
            FragmentManager fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();

            mDrawerList.setItemChecked(position, true);
            mDrawerList.setSelection(position);
            setTitle(mNavigationDrawerItemTitles[position]);
            mDrawerLayout.closeDrawer(mDrawerList);

        } else {
            Log.e("MainActivity", "Error in creating fragment");
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        if (title.equals("HOME")) {
            mtvHeadrTitle.setText("NVISILINK");
        } else {
            mtvHeadrTitle.setText(mTitle);
        }
        getSupportActionBar().setTitle("");
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    /*******************Method for Action bar Setting*********************************/
    void setupToolbar() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
        mivLock = (ImageView) findViewById(R.id.ivLock);

        mivEdit = (ImageView) findViewById(R.id.ivEdit);
        mdirect = (ImageView) findViewById(R.id.direct);
        mtvHeadrTitle = (TextView) findViewById(R.id.tvHeadrTitle);
        mtvHeadrTitle.setText("NVISILINK");
        getSupportActionBar().setTitle("");

        mivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ProfileFragment profileFragment = new ProfileFragment();
                profileFragment.getVisiableonOrOff();

            }
        });
        mivLock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isChecked == false) {
                    MyPreferences.getActiveInstance(MainActivity.this).setIsSecure(true);
                    mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_red));
                    isChecked = true;
                } else {
                    MyPreferences.getActiveInstance(MainActivity.this).setIsSecure(false);
                    mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_green));
                    isChecked = false;
                }

            }
        });

    }

    /*******************Method for naviagation Button *********************************/

    void setupDrawerToggle() {
        mDrawerToggle = new android.support.v7.app.ActionBarDrawerToggle(this, mDrawerLayout, toolbar, R.string.app_name, R.string.app_name);
        //This is necessary to change the icon of the Drawer Toggle upon state change.
        mDrawerToggle.syncState();
    }

    /*******************Method for fragment replace *********************************/

    private void fragmentReplace(Fragment fragment) {

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.content_frame, fragment);
        transaction.commit();
    }

    /*******************Method for navigate other Activity *********************************/

    private void gotoLoginActivity() {
        MyPreferences.getActiveInstance(MainActivity.this).setIsLoggedIn(false);
        Intent in = new Intent(MainActivity.this, LoginActivity.class);
        in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(in);
        finish();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (MyPreferences.getActiveInstance(MainActivity.this).getIsSecure()) {
            mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_red));
        } else {
            mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_green));
        }

    }

    @Override
    public void onBackPressed() {
        if (exit) {
            finish();
        } else {
            Toast.makeText(this, "Press Back again to Exit.", Toast.LENGTH_SHORT).show();
            exit = true;
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    exit = false;
                }
            }, 3 * 1000);

        }

    }




}
