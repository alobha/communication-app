package com.communicationapp;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.communicationapp.adapter.RecevDataAdapter;
import com.communicationapp.adapter.SentDataAdapter;
import com.communicationapp.comman.CommonMethod;
import com.communicationapp.comman.Constants;
import com.communicationapp.comman.MyPreferences;
import com.communicationapp.comman.ProgressBarDialog;
import com.communicationapp.interfaces.ProgressClickListener;
import com.communicationapp.model.ReceiveLocationuserList;
import com.communicationapp.model.SendedLocationuserList;
import com.communicationapp.responseparser.RecevedResponse;
import com.communicationapp.responseparser.ReciveLocationUserResponse;
import com.communicationapp.responseparser.SendLocationResponse;
import com.communicationapp.responseparser.SentLocationResponse;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class RecevedActivity extends AppCompatActivity implements ProgressClickListener {
    private static final String TAG = "RecevedActivity";

    /*****************Attributes name***************************/

    private ImageView mivLock;
    private boolean isChecked=false;
    private TextView tvSent;
    private TextView tvCurrntLocation;
    private TextView tvDateTime;
    private SwipeMenuListView mListView;
    private RecevDataAdapter mListDataAdapter;
    private LinearLayout container;
    private ProgressBarDialog mProgressDialog;
    private List<ReceiveLocationuserList> sendedLocationuserList;
    private String latitude;
    private String langtitude;
    private   List<Address> mAddress;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reseve);

        mAddress=getIntent().getParcelableArrayListExtra("address");
        latitude=getIntent().getStringExtra("latitude");
        langtitude=getIntent().getStringExtra("longitude");

        tvCurrntLocation = (TextView) findViewById( R.id.tvCurrntLocation );
        tvDateTime = (TextView) findViewById( R.id.tvDateTime );
        tvSent = (TextView) findViewById( R.id.tvSent );
        tvSent.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RecevedActivity.this,SentActivity.class);
                intent.putExtra("address", (Serializable) mAddress);
                intent.putExtra("latitude", String.valueOf(latitude));
                intent.putExtra("longitude", String.valueOf(langtitude));
                startActivity(intent);
                finish();

            }
        });
        initControls();
        setToolbarWidget();
        if(CommonMethod.isOnline(RecevedActivity.this)){
            validategetSentLocation();
            tvDateTime.setText(CommonMethod.date());
            tvCurrntLocation.setText(mAddress.get(0).getAdminArea()+", "+mAddress.get(0).getCountryCode());
        }else {
            CommonMethod.showSnackbar(RecevedActivity.this,container,getResources().getString(R.string.error_no_internet));
        }

        if(MyPreferences.getActiveInstance(RecevedActivity.this).getIsSecure()){
            mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_red));
        }else {
            mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_green));
        }


    }
    /*******************Method for Action bar Setting*********************************/

    private void setToolbarWidget() {

        LinearLayout llCointer =(LinearLayout)findViewById(R.id.llCointer);
        mProgressDialog = new ProgressBarDialog(RecevedActivity.this, llCointer);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        container = (LinearLayout) findViewById(R.id.container);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow_24dp);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();

            }
        });
        getSupportActionBar().setTitle("");
        mivLock = (ImageView) findViewById(R.id.ivLock);

        mivLock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isChecked==false){
                    MyPreferences.getActiveInstance(RecevedActivity.this).setIsSecure(true);
                    mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_red));
                    isChecked=true;
                }else {
                    MyPreferences.getActiveInstance(RecevedActivity.this).setIsSecure(false);
                    mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_green));
                    isChecked=false;
                }

            }
        });
    }
    /*****************Method for swipe list riht to left ***************************/

    private void initControls() {

        mListView=(SwipeMenuListView)findViewById(R.id.listView);
        mListView.setSwipeDirection(SwipeMenuListView.DIRECTION_LEFT);


        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                // Create different menus depending on the view type
                // create "open" item
                SwipeMenuItem openItem = new SwipeMenuItem(
                        getApplicationContext());

                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                        0x3F, 0x25)));
                // set item width
                deleteItem.setWidth(CommonMethod.dp2px(RecevedActivity.this,90));
                // set item title
                deleteItem.setTitle("Clear");
                // set item title fontsize
                deleteItem.setTitleSize(18);
                // set item title font color
                deleteItem.setTitleColor(Color.WHITE);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };

        // set creator
        mListView.setMenuCreator(creator);

        mListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {

                switch (index) {
                    case 0:
                        if(CommonMethod.isOnline(RecevedActivity.this)){
                            validateDelet(sendedLocationuserList.get(position).getRecv_id(),position);
                        }else {
                            CommonMethod.showSnackbar(RecevedActivity.this,container,getResources().getString(R.string.error_no_internet));
                        }
                        //Toast.makeText(RecevedActivity.this,"Like button press",Toast.LENGTH_SHORT).show();
                        break;
                    case 1:

                        Toast.makeText(RecevedActivity.this,"Item deleted",Toast.LENGTH_SHORT).show();

                        break;
                }
                return true;
            }
        });

        //mListView

        mListView.setOnMenuStateChangeListener(new SwipeMenuListView.OnMenuStateChangeListener() {
            @Override
            public void onMenuOpen(int position) {

            }
            @Override
            public void onMenuClose(int position) {

            }
        });

        mListView.setOnSwipeListener(new SwipeMenuListView.OnSwipeListener() {
            @Override
            public void onSwipeStart(int position) {

            }

            @Override
            public void onSwipeEnd(int position) {

            }
        });
    }


    @Override
    public void onRetryClick() {
        mProgressDialog.showContent();
        if(CommonMethod.isOnline(RecevedActivity.this)){
            validategetSentLocation();
        }else {
            CommonMethod.showSnackbar(RecevedActivity.this,container,getResources().getString(R.string.error_no_internet));
        }
    }
    /*******************method network connection*********************************/

    private void validategetSentLocation() {

        mProgressDialog.showLoading();
        AndroidNetworking.post(Constants.ReceiveLocationbyuser)
                .addBodyParameter("user_id", MyPreferences.getActiveInstance(RecevedActivity.this).getUserId())
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        mProgressDialog.showContent();
                        Gson gson = new Gson();
                        ReciveLocationUserResponse mProfileResponse = gson.fromJson(response.toString(), ReciveLocationUserResponse.class);
                        if (mProfileResponse != null) {
                            if (mProfileResponse.getSuccess().equals("1")) {

                                parseData(mProfileResponse.getreceiveLocationuserList());

                            } else {
                                mProgressDialog.showEmpty();

                            }
                        } else {

                            CommonMethod.showSnackbar(RecevedActivity.this,container, getString(R.string.connection_error));
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        mProgressDialog.showRetry();
                        CommonMethod.showNetworkSnack(container,getResources().getString(R.string.error_server),"Ok");
                    }

                });
    }

    private void parseData(List<ReceiveLocationuserList> sendedLocationuserList ){
        this.sendedLocationuserList =sendedLocationuserList;
        mListDataAdapter=new RecevDataAdapter(RecevedActivity.this,sendedLocationuserList);
        mListView.setAdapter(mListDataAdapter);

    }


    private void validateDelet(String recv_id, final int position) {
        mProgressDialog.showLoading();
        AndroidNetworking.post(Constants.delete_receive)
                .addBodyParameter("user_id", MyPreferences.getActiveInstance(RecevedActivity.this).getUserId())
                .addBodyParameter("id",recv_id)
                .addBodyParameter("type","location")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        mProgressDialog.showContent();
                        Gson gson = new Gson();
                        SendLocationResponse mSendLocationResponse = gson.fromJson(response.toString(), SendLocationResponse.class);
                        if (mSendLocationResponse != null) {
                            if (mSendLocationResponse.getSuccess().equals("1")) {

                               mListDataAdapter.reemove(position);

                            } else {
                                mProgressDialog.showEmpty();
                            }
                        } else {

                            CommonMethod.showSnackbar(RecevedActivity.this,container, getString(R.string.connection_error));
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        mProgressDialog.showRetry();
                        CommonMethod.showNetworkSnack(container,getResources().getString(R.string.error_server),"Ok");
                    }

                });
    }


}
