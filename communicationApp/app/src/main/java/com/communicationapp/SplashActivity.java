package com.communicationapp;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.communicationapp.comman.MyPreferences;

public class SplashActivity extends AppCompatActivity {

    Handler handler;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        /*****************Declare handle for stay splash view 3 second **************************/
        handler=new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                gotoLoginActivity();
            }
        },3000);

    }
    /*******************Method for navigate other Activity *********************************/
    private void gotoLoginActivity() {
        if(MyPreferences.getActiveInstance(SplashActivity.this).getIsLoggedIn()){
            Intent in = new Intent(SplashActivity.this, MainActivity.class);
            in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(in);
            finish();
        }else {
            Intent in = new Intent(SplashActivity.this, LoginActivity.class);
            in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(in);
            finish();
        }
    }
}
