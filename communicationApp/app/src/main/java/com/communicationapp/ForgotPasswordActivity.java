package com.communicationapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.communicationapp.interfaces.ProgressClickListener;
import com.communicationapp.responseparser.ForgotPasswordResponse;
import com.communicationapp.responseparser.LoginResponse;
import com.communicationapp.comman.CommonMethod;
import com.communicationapp.comman.Constants;
import com.communicationapp.comman.ProgressBarDialog;
import com.google.gson.Gson;

import org.json.JSONObject;

public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener,ProgressClickListener {
    private static final String TAG = "ForgotPasswordActivity";

    /*****************Attributes name***************************/
    private EditText metEmail;
    private Button mbtnSubmint;
    private Toolbar toolbar;
    private CoordinatorLayout container;
    private ProgressBarDialog mProgressDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        getReferenceId();
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        setToolbarWidget();

    }

    /*******************Method for get Attributes Id*********************************/

    public void getReferenceId(){
        metEmail = (EditText) findViewById(R.id.etEmail);
        mbtnSubmint = (Button) findViewById(R.id.btnSubmint);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        container = (CoordinatorLayout) findViewById(R.id.container);
        mbtnSubmint.setOnClickListener(this);
        mProgressDialog = new ProgressBarDialog(ForgotPasswordActivity.this, null);

    }
    /*******************Method for Action bar Setting*********************************/

    private void setToolbarWidget() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow_24dp);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                CommonMethod.closeKeyboard(ForgotPasswordActivity.this,v);
            }
        });
        getSupportActionBar().setTitle("");

    }
    /*******************Action perform on Attributes *********************************/

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.btnSubmint:

                if (CommonMethod.isOnline(ForgotPasswordActivity.this)) {
                    if (validationForgotpassword(view)) {
                        validateForgotPassword();
                    }
                } else {
                    CommonMethod.showSnack(container, getResources().getString(R.string.error_no_internet));
                }

                break;
            case R.id.container:
                CommonMethod.hideKeyboard(ForgotPasswordActivity.this,view);
                break;

        }

    }
    /*******************Check Validation on Attributes *********************************/

    private boolean validationForgotpassword(View view) {

       if (metEmail.getText().toString().trim().length() == 0) {
            CommonMethod.showSnackbar(ForgotPasswordActivity.this,view,"Please enter your email id");
            return false;
        } else if (!CommonMethod.isEmailValid(metEmail.getText().toString().trim())) {
           CommonMethod.showSnackbar(ForgotPasswordActivity.this,view,"Please enter valid email id");
            return false;
        }
        return true;

    }

    /*******************Method for navigate other Activity *********************************/

    private void gotoLoginActivity() {
        Intent in = new Intent(ForgotPasswordActivity.this, LoginActivity.class);
        in.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(in);
        finish();
    }


    @Override
    public void onRetryClick() {
        mProgressDialog.showContent();
    }
    /*******************method network connection*********************************/

    private void validateForgotPassword() {
        mProgressDialog.showLoading();
        AndroidNetworking.post(Constants.forgot_password)
                .addBodyParameter("user_email", metEmail.getText().toString())
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        mProgressDialog.showContent();
                        Gson gson = new Gson();
                        ForgotPasswordResponse mForgotPasswordResponse = gson.fromJson(response.toString(), ForgotPasswordResponse.class);
                        if (mForgotPasswordResponse != null) {
                            if (mForgotPasswordResponse.getCode().equals("200")) {

                             if(mForgotPasswordResponse.getSuccess().equals("1")){

                                 Toast.makeText(ForgotPasswordActivity.this,mForgotPasswordResponse.getMsg(),Toast.LENGTH_SHORT).show();
                                 gotoLoginActivity();
                             }else {
                                 Toast.makeText(ForgotPasswordActivity.this,mForgotPasswordResponse.getMsg(),Toast.LENGTH_SHORT).show();

                             }

                            } else {
                                CommonMethod.showSnackbar(ForgotPasswordActivity.this,container, mForgotPasswordResponse.getMsg());

                            }
                        } else {

                            CommonMethod.showSnackbar(ForgotPasswordActivity.this,container, getString(R.string.connection_error));
                        }

                    }

                    @Override
                    public void onError(ANError anError) {

                        mProgressDialog.showRetry();
                        CommonMethod.showNetworkSnack(container,getResources().getString(R.string.error_server),"Ok");
                    }

                });
    }


}
