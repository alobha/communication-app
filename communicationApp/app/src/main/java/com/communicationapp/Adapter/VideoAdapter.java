package com.communicationapp.Adapter;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.communicationapp.ActivityVideoView;
import com.communicationapp.R;


public class VideoAdapter extends RecyclerView.Adapter<VideoAdapter.ListViewHolder> {
    public String[] data;
    public Activity mcontent;
    public VideoAdapter(Activity mcontent,String[] Data) {
        this.data = Data;
        this.mcontent = mcontent;
    }

    @Override
    public VideoAdapter.ListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from( parent.getContext() );
        View view = inflater.inflate( R.layout.list_row_video, parent, false );
        return new VideoAdapter.ListViewHolder( view );
    }
    @Override
    public void onBindViewHolder(VideoAdapter.ListViewHolder holder, int position) {
        String title = data[position];
        holder.mtvDate.setText( title );


    }
    @Override
    public int getItemCount() {
        return data.length;
    }
    public class ListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView mtvName;
        private TextView mtvDate;
        private TextView mtvVideo;

        public ListViewHolder(View itemView) {
            super( itemView );
            itemView.setOnClickListener(this);
            mtvName = (TextView) itemView.findViewById( R.id.tvName );
            mtvDate = (TextView) itemView.findViewById( R.id.tvDate );
            mtvVideo = (TextView) itemView.findViewById( R.id.tvVideo );

        }

        @Override
        public void onClick(View v) {
            Intent intent =new Intent(mcontent, ActivityVideoView.class);
            mcontent.startActivity(intent);

        }
    }

}


