package com.communicationapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.communicationapp.R;
import com.communicationapp.model.ContactList;

import java.util.List;


public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ListViewHolder> {
    public List<ContactList> contactList ;
    private Context mContext;
    public ContactAdapter(Context mContext,List<ContactList> contactList ) {
        this.contactList = contactList;
        this.mContext = mContext;
    }

    @Override
    public ContactAdapter.ListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from( parent.getContext() );

        View view = inflater.inflate( R.layout.list_row_contact, parent, false );
        return new ContactAdapter.ListViewHolder( view );
    }

    @Override
    public void onBindViewHolder(ContactAdapter.ListViewHolder holder, int position) {
        holder.text.setText(contactList.get(position).getFirstName() +" " + contactList.get(position).getLastName());


    }


    @Override
    public int getItemCount() {
        return contactList.size();
    }


    public class ListViewHolder extends RecyclerView.ViewHolder {
        ImageView imgIcon;
        TextView text;

        public ListViewHolder(View itemView) {
            super( itemView );
            text = (TextView) itemView.findViewById( R.id.tvName );

        }
    }

}


