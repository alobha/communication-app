package com.communicationapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.communicationapp.R;
import com.communicationapp.model.ReceivedFilesList;

import java.util.List;

/**
 * Created by mac on 17/09/18.
 */

public class RecivedFileAdapterAudio extends BaseAdapter {
    private LayoutInflater mInflater;
    public List<ReceivedFilesList> receivedFilesList;
    private Context mContext;

    public RecivedFileAdapterAudio(Context mcontext, List<ReceivedFilesList> receivedFilesList) {
        this.mContext = mcontext;
        this.receivedFilesList = receivedFilesList;
        mInflater = (LayoutInflater) mcontext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return receivedFilesList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final RecivedFileAdapterAudio.ViewHolder holder;
        if(convertView==null){
            convertView=mInflater.inflate(R.layout.list_row_file, null);
            holder = new RecivedFileAdapterAudio.ViewHolder();
            holder.mtvName = (TextView) convertView.findViewById( R.id.tvName );
            holder.mtvDate = (TextView) convertView.findViewById( R.id.tvDate );
            holder.mtvVideo = (TextView) convertView.findViewById( R.id.tvVideo );
            holder.mllCounter = (LinearLayout) convertView.findViewById( R.id.llCounter );

            convertView.setTag(holder);
        }else {

            holder= (RecivedFileAdapterAudio.ViewHolder) convertView.getTag();
        }

        holder.mtvDate.setText(receivedFilesList.get(position).getReceiveDate());
        String mFileName =  receivedFilesList.get(position).getFile().replace("http://alobhatechnology.com/staging/communicationapp_api/assets/files/", "");
        mFileName =mFileName.replaceAll("[0-9]","");
        holder.mtvVideo.setText(mFileName);
        holder.mtvName.setText(receivedFilesList.get(position).getSenderName() + " "+receivedFilesList.get(position).getSenderLastName());



        return convertView;
    }

    class ViewHolder {

        private TextView mtvName;
        private TextView mtvDate;
        private TextView mtvVideo;
        private LinearLayout mllCounter;
    }

    public void remove( int position) {

        receivedFilesList.remove(position);
        // notify item added by position
        notifyDataSetChanged();
    }

}
