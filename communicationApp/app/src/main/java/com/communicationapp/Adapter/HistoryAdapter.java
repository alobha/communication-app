package com.communicationapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.communicationapp.R;
import com.communicationapp.model.HistoryList;

import java.util.List;

public class HistoryAdapter extends RecyclerView.Adapter<HistoryAdapter.ListViewHolder> {
    private List<HistoryList> historyList;
    private Context mContext;

    public HistoryAdapter(Context mContext,List<HistoryList> historyList) {
        this.historyList = historyList;
    }

    @Override
    public HistoryAdapter.ListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from( parent.getContext() );

        View view = inflater.inflate( R.layout.singlelist_history, parent, false );
        return new HistoryAdapter.ListViewHolder( view );
    }

    @Override
    public void onBindViewHolder(HistoryAdapter.ListViewHolder holder, int position) {

        holder.tvDescription.setText(historyList.get(position).getType()+" Send To "+ historyList.get(position).getSendTo());
        holder.tvDate.setText(historyList.get(position).getSendDate());
    }


    @Override
    public int getItemCount() {
        return historyList.size();
    }


    public class ListViewHolder extends RecyclerView.ViewHolder {
        TextView tvDescription;
        TextView tvDate;

        public ListViewHolder(View itemView) {
            super( itemView );
            tvDescription = (TextView) itemView.findViewById( R.id.tvDescription );
            tvDate = (TextView) itemView.findViewById( R.id.tvDate );

        }
    }

}


