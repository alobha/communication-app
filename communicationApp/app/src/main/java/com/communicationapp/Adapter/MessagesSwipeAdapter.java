package com.communicationapp.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.communicationapp.ChatActivity;
import com.communicationapp.R;
import com.communicationapp.comman.MyPreferences;
import com.communicationapp.model.MessageList;

import java.util.ArrayList;
import java.util.List;

public class MessagesSwipeAdapter extends RecyclerView.Adapter<MessagesSwipeAdapter.MyHolder> {

    List<MessageList> mData;
    private LayoutInflater inflater;
    private Context ctx;


    public MessagesSwipeAdapter(Context ctx, List<MessageList> mData) {
        this.ctx = ctx;
        this.mData = mData;

    }


    public void removeItem(int position) {
        mData.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, mData.size());
    }
    public void restoreItem(MessageList model, int position) {

        mData.add(position, model);
        // notify item added by position
        notifyItemInserted(position);
    }




    @NonNull
    @Override
    public MyHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {


        View tempView = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.single_row_messages, viewGroup, false);


        MyHolder holder = new MyHolder(tempView);


        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull final MyHolder holder, final int position) {
        String groupName="";
        if(mData.get(position).getGroupUser().contains(MyPreferences.getActiveInstance(ctx).getFirstName())){
            groupName =  mData.get(position).getGroupUser();
            groupName = groupName.replaceAll(MyPreferences.getActiveInstance(ctx).getFirstName(), "");
            if(groupName.startsWith(",")){
                groupName=groupName.startsWith(",") ? groupName.substring(1) : groupName;
            }else {
                groupName =groupName.replaceAll(",", "");
            }
            holder.tvUser.setText(groupName);
        }


        holder.mtvDate.setText(mData.get(position).getDate());
        holder.tvSubject.setText(mData.get(position).getReply());
        holder.llContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(ctx, ChatActivity.class);
                intent.putExtra("group_id",mData.get(position).getGroupId());
                intent.putExtra("userName",mData.get(position).getGroupUser());
                intent.putExtra("recever_id",mData.get(position).getSenderId());
                ctx.startActivity(intent);

            }
        });



       /* myHolder.btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mData.remove(i);
                notifyItemRemoved(i);
                notifyItemRangeChanged(i, mData.size());

            }
        });*/

    }


    @Override
    public int getItemCount() {
        return mData.size();
    }

    class MyHolder extends RecyclerView.ViewHolder {

        private final TextView tvUser,mtvDate,tvSubject;
        LinearLayout llContainer;

        TextView uName, uMobile;
        protected CheckBox checkBox;

        public MyHolder(@NonNull View itemView) {
            super(itemView);
          tvUser=(TextView)itemView.findViewById(R.id.tvUser);
           mtvDate=(TextView)itemView.findViewById(R.id.tvDate);
           tvSubject=(TextView)itemView.findViewById(R.id.tvSubject);
           llContainer=(LinearLayout) itemView.findViewById(R.id.llContainer);

            //  btnDelete = itemView.findViewById(R.id.btnDelete);
        }



    }
}