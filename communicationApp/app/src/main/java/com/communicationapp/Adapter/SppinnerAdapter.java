package com.communicationapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.TextView;

import com.communicationapp.NewMessageActivity;
import com.communicationapp.R;
import com.communicationapp.model.ContactList;

import java.util.ArrayList;
import java.util.List;

public class SppinnerAdapter  extends BaseAdapter {

    private List<ContactList> contactList;
    private LayoutInflater mInflater;
    private TextView mSelectedItems;
    private static int selectedCount = 0;
    private static String firstSelected = "";
    private SppinnerAdapter.ViewHolder holder;
    private static String selected = "";	//shortened selected values representation

    public static String getSelected() {
        return selected;
    }

    public void setSelected(String selected) {
        SppinnerAdapter.selected = selected;
    }

    public SppinnerAdapter(Context context, List<ContactList> contactList,TextView tv) {
        mInflater = LayoutInflater.from(context);
        this.mSelectedItems = tv;
        this.contactList = contactList;
        selectedCount =0;

    }

    @Override
    public int getCount() {
        // TODO Auto-generated method stub
        return contactList.size();
    }

    @Override
    public Object getItem(int arg0) {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public long getItemId(int arg0) {
        // TODO Auto-generated method stub
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub

        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.drop_down_list_row, null);
            holder = new SppinnerAdapter.ViewHolder();
            holder.tvTextview = (TextView) convertView.findViewById(R.id.tvTextview);
            holder.chkbox = (CheckBox) convertView.findViewById(R.id.checkbox);
            convertView.setTag(holder);
        } else {
            holder = (SppinnerAdapter.ViewHolder) convertView.getTag();
        }

        holder.tvTextview.setText(contactList.get(position).getFirstName()+" "+contactList.get(position).getLastName());

        final int position1 = position;

        //whenever the checkbox is clicked the selected values textview is updated with new selected values
        holder.chkbox.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                setText(position1);
            }
        });

        if(NewMessageActivity.contactList.get(position1).isChecked())
            holder.chkbox.setChecked(true);
        else
            holder.chkbox.setChecked(false);
        return convertView;
    }


    /*
     * Function which updates the selected values display and information(checkSelected[])
     * */
    private void setText(int position1){
        if (!NewMessageActivity.contactList.get(position1).isChecked()) {
            NewMessageActivity.contactList.get(position1).setChecked(true);
            selectedCount =selectedCount+1;
        } else {
            NewMessageActivity.contactList.get(position1).setChecked(false);
            selectedCount=selectedCount-1;
        }

        if (selectedCount == 0) {
            mSelectedItems.setText(R.string.select_string);
        } else if (selectedCount == 1) {
            for (int i = 0; i <  NewMessageActivity.contactList.size(); i++) {
                if ( NewMessageActivity.contactList.get(i).isChecked() == true) {
                    firstSelected = contactList.get(i).getFirstName();
                    break;
                }
            }
            mSelectedItems.setText(firstSelected);
            setSelected(firstSelected);
        } else if (selectedCount > 1) {
            for (int i = 0; i <  NewMessageActivity.contactList.size(); i++) {
                if ( NewMessageActivity.contactList.get(i).isChecked() == true) {
                    firstSelected = contactList.get(i).getFirstName();
                    break;
                }
            }
            mSelectedItems.setText(firstSelected + " & "+ (selectedCount - 1) + " more");
            setSelected(firstSelected + " & "+ (selectedCount - 1) + " more");
        }
    }

    private class ViewHolder {
        TextView tvTextview;
        CheckBox chkbox;
    }
}