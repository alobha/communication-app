package com.communicationapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.communicationapp.R;
import com.communicationapp.model.SendFilesList;

import java.util.List;

/**
 * Created by mac on 10/07/18.
 */

public class SendFileAdapter extends RecyclerView.Adapter<SendFileAdapter.ListViewHolder> {
    public List<SendFilesList> sendFilesList;
    private Context mContext;
    private onClieck callBack;
    public SendFileAdapter(Context mContext, List<SendFilesList> sendFilesList,onClieck callBack) {
        this.mContext = mContext;
        this.sendFilesList = sendFilesList;
        this.callBack = callBack;
    }

    @Override
    public SendFileAdapter.ListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from( parent.getContext() );
        View view = inflater.inflate( R.layout.list_row_file, parent, false );
        return new SendFileAdapter.ListViewHolder( view );
    }
    @Override
    public void onBindViewHolder(SendFileAdapter.ListViewHolder holder, final int position) {
        holder.mtvDate.setText(sendFilesList.get(position).getReceiveDate());
        String mFileName =  sendFilesList.get(position).getFile().replace("http://alobhatechnology.com/staging/communicationapp_api/assets/files/", "");
        mFileName =mFileName.replaceAll("[0-9]","");
        holder.mtvVideo.setText(mFileName);
        holder.mtvName.setText(sendFilesList.get(position).getReceiverName());
        holder.mllCounter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

               callBack.getUrl(sendFilesList.get(position).getFile());
            }
        });
    }
    @Override
    public int getItemCount() {
        return sendFilesList.size();
    }
    public class ListViewHolder extends RecyclerView.ViewHolder {
        private TextView mtvName;
        private TextView mtvDate;
        private TextView mtvVideo;
        private LinearLayout mllCounter;

        public ListViewHolder(View itemView) {
            super( itemView );
            mtvName = (TextView) itemView.findViewById( R.id.tvName );
            mtvDate = (TextView) itemView.findViewById( R.id.tvDate );
            mtvVideo = (TextView) itemView.findViewById( R.id.tvVideo );
            mllCounter = (LinearLayout) itemView.findViewById( R.id.llCounter );

        }
    }

    public interface onClieck{

        public void getUrl(String url);
    }

}