package com.communicationapp.Adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.communicationapp.R;


public class AudioAdapter extends RecyclerView.Adapter<AudioAdapter.ListViewHolder> {
    public String[] data;

    public AudioAdapter(String[] Data) {
        this.data = Data;
    }

    @Override
    public AudioAdapter.ListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from( parent.getContext() );
        View view = inflater.inflate( R.layout.list_row_audio, parent, false );
        return new AudioAdapter.ListViewHolder( view );
    }
    @Override
    public void onBindViewHolder(AudioAdapter.ListViewHolder holder, int position) {
        String title = data[position];
        holder.mtvDate.setText( title );
    }
    @Override
    public int getItemCount() {
        return data.length;
    }
    public class ListViewHolder extends RecyclerView.ViewHolder {
        private TextView mtvName;
        private TextView mtvDate;
        private TextView mtvVideo;

        public ListViewHolder(View itemView) {
            super( itemView );
            mtvName = (TextView) itemView.findViewById( R.id.tvName );
            mtvDate = (TextView) itemView.findViewById( R.id.tvDate );
            mtvVideo = (TextView) itemView.findViewById( R.id.tvVideo );

        }
    }

}


