package com.communicationapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.communicationapp.ChatActivity;
import com.communicationapp.comman.CommonMethod;
import com.communicationapp.R;
import com.communicationapp.comman.MyPreferences;
import com.communicationapp.model.LastConversation;
import com.communicationapp.model.MessageList;

import java.util.ArrayList;
import java.util.List;

public class MessageAdapter extends BaseAdapter {
    private LayoutInflater mInflater;
    private List<MessageList> messageList;
    private Activity mContext;

    public MessageAdapter(Activity mcontext, List<MessageList> messageList) {
        this.mContext = mcontext;
        this.messageList = messageList;
        mInflater = (LayoutInflater) mcontext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return messageList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final MessageAdapter.ViewHolder holder;
        if(convertView==null){
            convertView=mInflater.inflate(R.layout.list_row_message, null);
            holder = new MessageAdapter.ViewHolder();
            holder.tvUser=(TextView)convertView.findViewById(R.id.tvUser);
            holder.mtvDate=(TextView)convertView.findViewById(R.id.tvDate);
            holder.tvSubject=(TextView)convertView.findViewById(R.id.tvSubject);
            holder.llContainer=(LinearLayout) convertView.findViewById(R.id.llContainer);

            convertView.setTag(holder);
        }else {

            holder= (MessageAdapter.ViewHolder) convertView.getTag();
        }

            String groupName="";
            if(messageList.get(position).getGroupUser().contains(MyPreferences.getActiveInstance(mContext).getFirstName())){
                groupName =  messageList.get(position).getGroupUser();
                groupName = groupName.replaceAll(MyPreferences.getActiveInstance(mContext).getFirstName(), "");
                if(groupName.startsWith(",")){
                    groupName=groupName.startsWith(",") ? groupName.substring(1) : groupName;
                }else {
                    groupName =groupName.replaceAll(",", "");
                }
                holder.tvUser.setText(groupName);
            }


        holder.mtvDate.setText(messageList.get(position).getDate());
        holder.tvSubject.setText(messageList.get(position).getReply());
        /*holder.llContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(mContext, ChatActivity.class);
                intent.putExtra("group_id",messageList.get(position).getGroupId());
                intent.putExtra("userName",messageList.get(position).getGroupUser());
                intent.putExtra("recever_id",messageList.get(position).getSenderId());
                mContext.startActivity(intent);

            }
        });*/
        return convertView;
    }

    class ViewHolder {

        TextView tvUser;
        TextView tvSubject;
        TextView mtvDate;
        LinearLayout llContainer;

    }

    public void remove( int position) {

        messageList.remove(position);
        // notify item added by position
        notifyDataSetChanged();
    }

}
