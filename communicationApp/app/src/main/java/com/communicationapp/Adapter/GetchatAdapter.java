package com.communicationapp.adapter;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.communicationapp.R;
import com.communicationapp.comman.CommonMethod;
import com.communicationapp.comman.MyPreferences;
import com.communicationapp.model.ChatList;
import com.communicationapp.model.ConversationList;

import java.util.List;


/**
 * Created by Admn on 4/10/2017.
 */

public class GetchatAdapter extends RecyclerView.Adapter<GetchatAdapter.MyViewHolder>
{
    List<ChatList> chatList ;
    Activity activity;
      public class MyViewHolder extends RecyclerView.ViewHolder {
          TextView tv_message_other, tv_message_mine,singleName,senderName,tvTime,tvTime1;
          LinearLayout layout_chat_others,layout_chat_mine;
          public MyViewHolder(View view)
          {
              super(view);
              tv_message_other = (TextView) view.findViewById(R.id.tv_message_other);
              singleName = (TextView) view.findViewById(R.id.singleName);
              senderName = (TextView) view.findViewById(R.id.senderName);
              tvTime = (TextView) view.findViewById(R.id.tvTime);
              tvTime1 = (TextView) view.findViewById(R.id.tvTime1);
              tv_message_mine = (TextView) view.findViewById(R.id.tv_message_mine);
              layout_chat_others=(LinearLayout)view.findViewById(R.id.layout_chat_others);
              layout_chat_mine=(LinearLayout)view.findViewById(R.id.layout_chat_mine);
          }
    }
    public GetchatAdapter(List<ChatList> chatList , Activity activity) {
        this.chatList = chatList;
        this.activity=activity;
        notifyDataSetChanged();
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, final int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.list_row_chat, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position)
    {

        final ChatList mData = chatList.get(position);
        String MyChat= mData.getSenderId();

        if(MyChat.equalsIgnoreCase(MyPreferences.getActiveInstance(activity).getUserId()))
        {
            holder.layout_chat_mine.setVisibility(View.VISIBLE);
            holder.layout_chat_others.setVisibility(View.GONE);
            holder.tv_message_mine.setText(mData.getReply());
            holder.senderName.setText(mData.getSenderName());
            if(CommonMethod.date1().endsWith(mData.getDate())){
                holder.tvTime1.setText(mData.getTime());

            }else {
                holder.tvTime1.setText(mData.getDate());

            }
        }
        else
        {
            holder.layout_chat_mine.setVisibility(View.GONE);
            holder.layout_chat_others.setVisibility(View.VISIBLE);
            holder.tv_message_other.setText(mData.getReply());
            holder.singleName.setText(mData.getSenderName());
            if(CommonMethod.date1().endsWith(mData.getDate())){
                holder.tvTime.setText(mData.getTime());

            }else {
                holder.tvTime.setText(mData.getDate());


            }
        }

    }

    @Override
    public int getItemCount() {
        return chatList.size();
    }
}
