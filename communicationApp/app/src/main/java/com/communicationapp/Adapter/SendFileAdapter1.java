package com.communicationapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.communicationapp.R;
import com.communicationapp.model.ReceivedFilesList;
import com.communicationapp.model.SendFilesList;

import java.util.List;

/**
 * Created by mac on 19/09/18.
 */

public class SendFileAdapter1 extends BaseAdapter {
    private LayoutInflater mInflater;
    public List<SendFilesList> sendFilesList;
    private Context mContext;
    public SendFileAdapter1(Context mcontext,List<SendFilesList> sendFilesList) {
        this.mContext = mcontext;
        this.sendFilesList = sendFilesList;
        mInflater = (LayoutInflater) mcontext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return sendFilesList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final SendFileAdapter1.ViewHolder holder;
        if(convertView==null){
            convertView=mInflater.inflate(R.layout.list_row_file, null);
            holder = new SendFileAdapter1.ViewHolder();
            holder.mtvName = (TextView) convertView.findViewById( R.id.tvName );
            holder.mtvDate = (TextView) convertView.findViewById( R.id.tvDate );
            holder.mtvVideo = (TextView) convertView.findViewById( R.id.tvVideo );
            holder.mllCounter = (LinearLayout) convertView.findViewById( R.id.llCounter );

            convertView.setTag(holder);
        }else {

            holder= (SendFileAdapter1.ViewHolder) convertView.getTag();
        }

        holder.mtvDate.setText(sendFilesList.get(position).getReceiveDate());
        String mFileName =  sendFilesList.get(position).getFile().replace("http://alobhatechnology.com/staging/communicationapp_api/assets/files/", "");
        mFileName =mFileName.replaceAll("[0-9]","");
        holder.mtvVideo.setText(mFileName);
        holder.mtvName.setText(sendFilesList.get(position).getReceiverName() + " "+sendFilesList.get(position).getReceiverLastName());



        return convertView;
    }

    class ViewHolder {

        private TextView mtvName;
        private TextView mtvDate;
        private TextView mtvVideo;
        private LinearLayout mllCounter;
    }

    public void remove( int position) {

        sendFilesList.remove(position);
        // notify item added by position
        notifyDataSetChanged();
    }

}
