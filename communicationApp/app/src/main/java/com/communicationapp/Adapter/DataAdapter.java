package com.communicationapp.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.communicationapp.R;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ListViewHolder> {
    public String[] data;

    public DataAdapter(String[] Data) {
        this.data = Data;
    }

    @Override
    public DataAdapter.ListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from( parent.getContext() );

        View view = inflater.inflate( R.layout.row_layout, parent, false );
        return new DataAdapter.ListViewHolder( view );
    }

    @Override
    public void onBindViewHolder(DataAdapter.ListViewHolder holder, int position) {
        String title = data[position];
        holder.txtTitle.setText( title );


    }


    @Override
    public int getItemCount() {
        return data.length;
    }


    public class ListViewHolder extends RecyclerView.ViewHolder {
        ImageView imgIcon;
        TextView txtTitle;

        public ListViewHolder(View itemView) {
            super( itemView );
            ImageView imgIcon = (ImageView) itemView.findViewById( R.id.img );


        }
    }

}
