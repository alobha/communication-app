package com.communicationapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.communicationapp.model.ContactList;
import com.communicationapp.R;

import java.util.List;


public class ContactListAdapter extends BaseAdapter {
    private LayoutInflater mInflater;
    public List<ContactList> contactList ;
    private Activity mContext;
    public ContactListAdapter(Activity mcontext, List<ContactList> contactList) {
        this.mContext = mcontext;
        this.contactList = contactList;
        mInflater = (LayoutInflater) mcontext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }
    @Override
    public int getCount() {
        return contactList.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public Object getItem(int position) {

        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (convertView == null) {
            convertView = mInflater.inflate(R.layout.contactlist, null);
            holder = new ViewHolder();

            holder.mtvName=(TextView)convertView.findViewById(R.id.tvName);
           holder.ivImageView= (ImageView) convertView.findViewById(R.id.ivImageView);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }

        if(contactList.get(position).isChecked()){
            holder.ivImageView.setVisibility(View.VISIBLE);
        }else {
            holder.ivImageView.setVisibility(View.GONE);
        }
        holder.mtvName.setText(contactList.get(position).getFirstName() +" "+ contactList.get(position).getLastName());

        return convertView;
    }

    public class ViewHolder {
        public TextView mtvName,mtvEmailId;
        public ImageView ivImageView;

    }

}
