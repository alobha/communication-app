package com.communicationapp.adapter;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.communicationapp.R;
import com.communicationapp.comman.MyPreferences;
import com.communicationapp.model.AllmailList;
import com.communicationapp.model.MessageList;

import java.util.List;

/**
 * Created by mac on 17/09/18.
 */

public class NewEmailAdapter extends BaseAdapter {
    private LayoutInflater mInflater;
    public List<AllmailList> emailList ;
    private Activity mContext;

    public NewEmailAdapter(Activity mcontext, List<AllmailList> emailList) {
        this.mContext = mcontext;
        this.emailList = emailList;
        mInflater = (LayoutInflater) mcontext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return emailList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final NewEmailAdapter.ViewHolder holder;
        if(convertView==null){
            convertView=mInflater.inflate(R.layout.list_rowemail, null);
            holder = new NewEmailAdapter.ViewHolder();
            holder.tvEmailId = (TextView) convertView.findViewById( R.id.tvEmailId );
            holder.tvDate = (TextView) convertView.findViewById( R.id.tvDate );
            holder.tvSubject = (TextView) convertView.findViewById( R.id.tvSubject );
            holder.tvType = (TextView) convertView.findViewById( R.id.tvType );
            convertView.setTag(holder);
        }else {

            holder= (NewEmailAdapter.ViewHolder) convertView.getTag();
        }

        holder.tvDate.setText(emailList.get(position).getReceiveMailDate());
        holder.tvEmailId.setText(emailList.get(position).getName()+ " "+emailList.get(position).getLastName());
        holder.tvSubject.setText(emailList.get(position).getContent());
        holder.tvType.setText(emailList.get(position).getMailType());
        if(emailList.get(position).getMailType().equalsIgnoreCase("Send Mail")){
            holder.tvType.setTextColor(Color.RED);
        }else {
            holder.tvType.setTextColor(Color.BLUE);

        }

        return convertView;
    }

    class ViewHolder {

        private TextView tvEmailId;
        private TextView tvDate;
        private TextView tvSubject;
        private TextView tvType;

    }

    public void remove( int position) {

        emailList.remove(position);
        // notify item added by position
        notifyDataSetChanged();
    }

}
