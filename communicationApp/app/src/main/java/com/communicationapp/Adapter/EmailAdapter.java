package com.communicationapp.adapter;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.communicationapp.EmailDetailActivity;
import com.communicationapp.R;
import com.communicationapp.model.AllmailList;
import com.communicationapp.model.MessageList;

import java.util.List;

public class EmailAdapter extends RecyclerView.Adapter<EmailAdapter.ListViewHolder> {
    public List<AllmailList> emailList ;
    public Activity mcontext;
    public EmailAdapter(Activity context, List<AllmailList> emailList ) {
        this.emailList = emailList;
        this.mcontext = context;
    }

    @Override
    public EmailAdapter.ListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from( parent.getContext() );

        View view = inflater.inflate( R.layout.list_rowemail, parent, false );
        return new EmailAdapter.ListViewHolder( view );
    }

    @Override
    public void onBindViewHolder(EmailAdapter.ListViewHolder holder, int position) {

        holder.tvDate.setText(emailList.get(position).getReceiveMailDate());
        holder.tvEmailId.setText(emailList.get(position).getName()+ " "+emailList.get(position).getLastName());
        holder.tvSubject.setText(emailList.get(position).getContent());
        holder.tvType.setText(emailList.get(position).getMailType());
        if(emailList.get(position).getMailType().equalsIgnoreCase("Send Mail")){
            holder.tvType.setTextColor(Color.RED);
        }else {
            holder.tvType.setTextColor(Color.BLUE);

        }

    }


    @Override
    public int getItemCount() {
        return emailList.size();
    }


    public class ListViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        private TextView tvEmailId;
        private TextView tvDate;
        private TextView tvSubject;
        private TextView tvType;

        public ListViewHolder(View itemView) {
            super( itemView );
            itemView.setOnClickListener(this);
            tvEmailId = (TextView) itemView.findViewById( R.id.tvEmailId );
            tvDate = (TextView) itemView.findViewById( R.id.tvDate );
            tvSubject = (TextView) itemView.findViewById( R.id.tvSubject );
            tvType = (TextView) itemView.findViewById( R.id.tvType );

        }

        @Override
        public void onClick(View v) {
                Intent intent = new Intent(mcontext, EmailDetailActivity.class);
                intent.putExtra("email", emailList.get(getAdapterPosition()).getName() + " " + emailList.get(getAdapterPosition()).getLastName());
                intent.putExtra("subject", emailList.get(getAdapterPosition()).getSubject());
                intent.putExtra("content", emailList.get(getAdapterPosition()).getContent());
                intent.putExtra("receiver_id", emailList.get(getAdapterPosition()).getSenderId());
                intent.putExtra("mailType", emailList.get(getAdapterPosition()).getMailType());
                mcontext.startActivity(intent);


        }
    }



    public void removeItem(int position) {
        emailList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, emailList.size());
    }
    public void restoreItem(AllmailList model, int position) {

        emailList.add(position, model);
        // notify item added by position
        notifyItemInserted(position);
    }

}
