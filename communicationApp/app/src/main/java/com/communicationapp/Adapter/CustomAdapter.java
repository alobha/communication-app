package com.communicationapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.communicationapp.R;
import com.communicationapp.model.Arraylist;
import com.communicationapp.responseparser.BadgeResponse;

import java.util.ArrayList;

public class CustomAdapter extends RecyclerView.Adapter<CustomAdapter.MyViewHolder> {

    private ArrayList<Arraylist> dataSet;
    private Context mcontext;
    private gridClicListner callback;
    private BadgeResponse mBadgeResponse;
    public CustomAdapter(Context mcontext,ArrayList<Arraylist> data,gridClicListner callback,BadgeResponse mBadgeResponse) {
        this.dataSet = data;
        this.mcontext = mcontext;
        this.callback = callback;
        this.mBadgeResponse = mBadgeResponse;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent,
                                           int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.products, parent, false);


        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int listPosition) {

        ImageView imageView = holder.imageViewIcon;
        imageView.setImageResource(dataSet.get(listPosition).getImage());

        holder.imageViewIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.getPosition(listPosition);
            }
        });

        if(listPosition==0){
            if(mBadgeResponse!=null&&!mBadgeResponse.getGetReceiveLocationCount().equals(0)) {
                holder.count_tv.setText(String.valueOf(mBadgeResponse.getGetReceiveLocationCount()));
                holder.count_tv.setVisibility(View.VISIBLE);
            }else {
                holder.count_tv.setVisibility(View.GONE);
            }
        }
        if(listPosition==1){
            if(mBadgeResponse!=null&&!mBadgeResponse.getGetmessageCount().equals(0)) {
                holder.count_tv.setText(String.valueOf(mBadgeResponse.getGetmessageCount()));
                holder.count_tv.setVisibility(View.VISIBLE);
            }else {
                holder.count_tv.setVisibility(View.GONE);
            }
        }

        if(listPosition==2){
            if(mBadgeResponse!=null&&!mBadgeResponse.getOtherCount().equals(0)) {
                holder.count_tv.setText(String.valueOf(mBadgeResponse.getOtherCount()));
                holder.count_tv.setVisibility(View.VISIBLE);
            }else {
                holder.count_tv.setVisibility(View.GONE);
            }
        }

        if(listPosition==3){
            if(mBadgeResponse!=null&&!mBadgeResponse.getGetReceiveEmailCount().equals(0)) {
                holder.count_tv.setText(String.valueOf(mBadgeResponse.getGetReceiveEmailCount()));
                holder.count_tv.setVisibility(View.VISIBLE);
            }else {
                holder.count_tv.setVisibility(View.GONE);
            }
        }

        if(listPosition==4){
            if(mBadgeResponse!=null&&!mBadgeResponse.getVideoCount().equals(0)) {
                holder.count_tv.setText(String.valueOf(mBadgeResponse.getVideoCount()));
                holder.count_tv.setVisibility(View.VISIBLE);
            }else {
                holder.count_tv.setVisibility(View.GONE);
            }
        }

        if(listPosition==5){
            if(mBadgeResponse!=null&&!mBadgeResponse.getAudioCount().equals(0)) {
                holder.count_tv.setText(String.valueOf(mBadgeResponse.getAudioCount()));
                holder.count_tv.setVisibility(View.VISIBLE);
            }else {
                holder.count_tv.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return dataSet.size();
    }
    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        private ImageView imageViewIcon;
        private TextView count_tv;

        public MyViewHolder(View itemView)  {
            super(itemView);
            final Context context = itemView.getContext();
            this.imageViewIcon = (ImageView) itemView.findViewById(R.id.image);
            this.count_tv = (TextView) itemView.findViewById(R.id.count_tv);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {

        }
    }


    public interface gridClicListner{

        public void getPosition(int position);

    }


}

