package com.communicationapp.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.communicationapp.R;
import com.communicationapp.model.AllmailList;
import com.communicationapp.model.ReceivedFilesList;

import java.util.List;

public class RecivedFileAdapter extends RecyclerView.Adapter<RecivedFileAdapter.ListViewHolder> {
    public List<ReceivedFilesList> receivedFilesList;
    private Context mContext;
    private onClieck callBack;
    public RecivedFileAdapter(Context mContext, List<ReceivedFilesList> receivedFilesList,onClieck callBack) {
        this.mContext = mContext;
        this.callBack = callBack;
        this.receivedFilesList = receivedFilesList;
    }

    @Override
    public RecivedFileAdapter.ListViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from( parent.getContext() );
        View view = inflater.inflate( R.layout.list_row_file, parent, false );
        return new RecivedFileAdapter.ListViewHolder( view );
    }
    @Override
    public void onBindViewHolder(RecivedFileAdapter.ListViewHolder holder, final int position) {
        holder.mtvDate.setText(receivedFilesList.get(position).getReceiveDate());
        String mFileName =  receivedFilesList.get(position).getFile().replace("http://alobhatechnology.com/staging/communicationapp_api/assets/files/", "");
        mFileName =mFileName.replaceAll("[0-9]","");
        holder.mtvVideo.setText(mFileName);
        holder.mtvName.setText(receivedFilesList.get(position).getSenderName() + " "+receivedFilesList.get(position).getSenderLastName());

        holder.mllCounter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                callBack.getUrlReceved(receivedFilesList.get(position).getFile());
            }
        });
    }
    @Override
    public int getItemCount() {
        return receivedFilesList.size();
    }
    public class ListViewHolder extends RecyclerView.ViewHolder {
        private TextView mtvName;
        private TextView mtvDate;
        private TextView mtvVideo;
        private LinearLayout mllCounter;
        public ListViewHolder(View itemView) {
            super( itemView );
            mtvName = (TextView) itemView.findViewById( R.id.tvName );
            mtvDate = (TextView) itemView.findViewById( R.id.tvDate );
            mtvVideo = (TextView) itemView.findViewById( R.id.tvVideo );
            mllCounter = (LinearLayout) itemView.findViewById( R.id.llCounter );

        }
    }

    public interface onClieck{

        public void getUrlReceved(String url);
    }
    public void removeItem(int position) {
        receivedFilesList.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, receivedFilesList.size());
    }
    public void restoreItem(ReceivedFilesList model, int position) {

        receivedFilesList.add(position, model);
        // notify item added by position
        notifyItemInserted(position);
    }

}


