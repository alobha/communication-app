package com.communicationapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.communicationapp.R;
import com.communicationapp.model.ReceiveLocationuserList;
import com.communicationapp.model.SendedLocationuserList;
import com.communicationapp.responseparser.ReciveLocationUserResponse;

import java.util.List;

/**
 * Created by mac on 07/07/18.
 */

public class RecevDataAdapter  extends BaseAdapter {

    private LayoutInflater mInflater;
    private List<ReceiveLocationuserList> sendedLocationuserList;
    private Context mContext;

    public RecevDataAdapter(Context mcontext, List<ReceiveLocationuserList> sendedLocationuserList) {
        this.mContext = mcontext;
        this.sendedLocationuserList = sendedLocationuserList;
        mInflater = (LayoutInflater) mcontext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

    }

    @Override
    public int getCount() {
        return sendedLocationuserList.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final RecevDataAdapter.ViewHolder holder;
        if(convertView==null){
            convertView=mInflater.inflate(R.layout.list_item, null);
            holder = new RecevDataAdapter.ViewHolder();
            holder.mtvName=(TextView)convertView.findViewById(R.id.tvName);
            holder.mtvlocation=(TextView)convertView.findViewById(R.id.tvlocation);

            convertView.setTag(holder);
        }else {

            holder= (RecevDataAdapter.ViewHolder) convertView.getTag();
        }

        holder.mtvName.setText(sendedLocationuserList.get(position).getFirstName() + sendedLocationuserList.get(position).getLastName());
        holder.mtvlocation.setText(sendedLocationuserList.get(position).getLocation());

        return convertView;
    }

    class ViewHolder {

        TextView mtvName;
        TextView mtvlocation;

    }
    public void reemove(int position){
        sendedLocationuserList.remove(position);
        notifyDataSetChanged();
    }

}

