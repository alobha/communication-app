package com.communicationapp;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.communicationapp.adapter.RecivedFileAdapter;
import com.communicationapp.adapter.RecivedFileAdapter1;
import com.communicationapp.adapter.SendFileAdapter;
import com.communicationapp.comman.CommonMethod;
import com.communicationapp.comman.Constants;
import com.communicationapp.comman.FileOpen;
import com.communicationapp.comman.FilePath;
import com.communicationapp.comman.MyPreferences;
import com.communicationapp.comman.ProgressBarDialog;
import com.communicationapp.fragment.FragmentSentAudio;
import com.communicationapp.fragment.FrangmentRecevieAudio;
import com.communicationapp.interfaces.ProgressClickListener;
import com.communicationapp.model.AllmailList;
import com.communicationapp.model.MessageList;
import com.communicationapp.model.ReceivedFilesList;
import com.communicationapp.model.SendFilesList;
import com.communicationapp.responseparser.FileResponse;
import com.communicationapp.responseparser.SendLocationResponse;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.List;

public class CloudeActivity extends AppCompatActivity implements View.OnClickListener, ProgressClickListener {
    private static final String TAG = "CloudeActivity";

    /*****************Attributes name***************************/

    private ImageView mivLock;
    private boolean isChecked = false;
    private ImageView mAddbutton;
    private TextView mtvSent;
    private TextView mtvRECEIVED;
    private CoordinatorLayout container;
    private ProgressBarDialog mProgressDialog;
    private List<ReceivedFilesList> receivedFilesList;
    private List<SendFilesList> sendFilesList;
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cloude);
        getRefrenceId();
        setToolbarWidget();

        if (MyPreferences.getActiveInstance(CloudeActivity.this).getIsSecure()) {
            mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_red));
        } else {
            mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_green));
        }


    }

    /*******************Method for get Attributes Id*********************************/

    private void getRefrenceId() {
        FrameLayout llCointer = (FrameLayout) findViewById(R.id.llCointer);
        mProgressDialog = new ProgressBarDialog(CloudeActivity.this, llCointer);
        mtvSent = (TextView) findViewById(R.id.tvSent);
        mtvRECEIVED = (TextView) findViewById(R.id.tvRECEIVED);
        mAddbutton = (ImageView) findViewById(R.id.ivAdd);
        container = (CoordinatorLayout) findViewById(R.id.container);
        mAddbutton.setOnClickListener(this);
        mtvSent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mtvSent.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                mtvSent.setTextColor(getResources().getColor(R.color.colorWhite));

                mtvRECEIVED.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                mtvRECEIVED.setTextColor(getResources().getColor(R.color.colorPrimary));
                fragmentReplace(new FragmentSentAudio(sendFilesList));
                mAddbutton.setVisibility(View.VISIBLE);
                CommonMethod.isReceved=false;


            }
        });
        mtvRECEIVED.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mtvSent.setBackgroundColor(getResources().getColor(R.color.colorWhite));
                mtvSent.setTextColor(getResources().getColor(R.color.colorPrimary));

                mtvRECEIVED.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
                mtvRECEIVED.setTextColor(getResources().getColor(R.color.colorWhite));

                fragmentReplace(new FrangmentRecevieAudio(receivedFilesList));
                mAddbutton.setVisibility(View.GONE);
                CommonMethod.isReceved=true;

            }
        });

        mivLock = (ImageView) findViewById(R.id.ivLock);
        mivLock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isChecked == false) {
                    MyPreferences.getActiveInstance(CloudeActivity.this).setIsSecure(true);
                    mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_red));
                    isChecked = true;
                } else {
                    MyPreferences.getActiveInstance(CloudeActivity.this).setIsSecure(false);
                    mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_green));
                    isChecked = false;
                }

            }
        });
    }/*******************Method for Action bar Setting*********************************/

    private void setToolbarWidget() {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow_24dp);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();

            }
        });
        getSupportActionBar().setTitle("");

    }

    /*******************Action perform on Attributes *********************************/

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivAdd:
                CommonMethod.flag=true;
                selectDialogue();
                break;

        }
    }

    /******************Retrive file here **********************/
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {

            if (resultCode == RESULT_OK) {

                //the selected audio.
                Uri uri = data.getData();
                String path = FilePath.getPath(CloudeActivity.this, uri);
                Intent shareintent = new Intent(CloudeActivity.this, ContactListActivity.class);
                shareintent.putExtra("type", "other");
                shareintent.putExtra("navigate", "Cloude");
                shareintent.putExtra("path", path);
                startActivity(shareintent);
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


    /******************method for dialogue box choose file **********************/

    private void selectDialogue() {

        final CharSequence[] items = {"Choose File",
                "Cancel"};

        AlertDialog.Builder builder = new AlertDialog.Builder(CloudeActivity.this);
        builder.setTitle("Add File");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (items[item].equals("Choose File")) {

                    browseDocuments();

                } else if (items[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    /******************method for call implicit intent for choose docment **********************/

    private void browseDocuments() {

        String[] mimeTypes =
                {"application/msword", "application/vnd.openxmlformats-officedocument.wordprocessingml.document", // .doc & .docx
                        "application/vnd.ms-powerpoint", "application/vnd.openxmlformats-officedocument.presentationml.presentation", // .ppt & .pptx
                        "application/vnd.ms-excel", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", // .xls & .xlsx
                        "text/plain",
                        "application/pdf",
                        "application/zip"};

        Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            intent.setType(mimeTypes.length == 1 ? mimeTypes[0] : "*/*");
            if (mimeTypes.length > 0) {
                intent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes);
            }
        } else {
            String mimeTypesStr = "";
            for (String mimeType : mimeTypes) {
                mimeTypesStr += mimeType + "|";
            }
            intent.setType(mimeTypesStr.substring(0, mimeTypesStr.length() - 1));
        }


        startActivityForResult(Intent.createChooser(intent, "ChooseFile"), 1);

    }

    @Override
    public void onRetryClick() {
        mProgressDialog.showContent();
        if (CommonMethod.isOnline(CloudeActivity.this)) {
            validateGetFileList();
        } else {
            CommonMethod.showSnackbar(CloudeActivity.this, container, getResources().getString(R.string.error_no_internet));
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(!CommonMethod.flag) {
            callFetchALlFilesAPI();
            callUpdateCountAPI();
        }
    }

    private void callUpdateCountAPI() {
        if (CommonMethod.isOnline(this)) {
            updateBadgeCountAPI();
        }
    }

    private void updateBadgeCountAPI() {
        AndroidNetworking.post(Constants.UPDATE_BADGE)
                .addBodyParameter("type", "other")
                .addBodyParameter("receiver_id", MyPreferences.getActiveInstance(this).getUserId())
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });

    }

    private void callFetchALlFilesAPI() {
        if (CommonMethod.isOnline(CloudeActivity.this)) {
            validateGetFileList();
        } else {
            CommonMethod.showSnack(container, getResources().getString(R.string.error_no_internet));
        }

    }

    private void validateGetFileList() {

        mProgressDialog.showLoading();
        AndroidNetworking.post(Constants.ReceiveFiles)
                .addBodyParameter("user_id", MyPreferences.getActiveInstance(CloudeActivity.this).getUserId())
                .addBodyParameter("type", "other")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        mProgressDialog.showContent();
                        Gson gson = new Gson();
                        FileResponse mFileResponse = gson.fromJson(response.toString(), FileResponse.class);
                        if (mFileResponse != null) {
                            if (mFileResponse.getCode().equals("200")) {

                                parseData(mFileResponse.getReceivedFilesList(), mFileResponse.getSendFilesList());

                            }

                        } else {

                            mProgressDialog.showRetry();
                        }


                    }

                    @Override
                    public void onError(ANError anError) {
                        mProgressDialog.showRetry();
                        CommonMethod.showNetworkSnack(container, getResources().getString(R.string.error_server), "Ok");
                    }

                });
    }

    private void parseData(List<ReceivedFilesList> receivedFilesList, List<SendFilesList> sendFilesList) {
        this.receivedFilesList = receivedFilesList;
        this.sendFilesList = sendFilesList;
        if(CommonMethod.isReceved){
            fragmentReplace(new FrangmentRecevieAudio(receivedFilesList));
        }else {
            fragmentReplace(new FragmentSentAudio(sendFilesList));

        }
    }

    private void fragmentReplace(Fragment fragment) {

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.llCointer, fragment);
        transaction.commit();
    }


}
