package com.communicationapp;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.communicationapp.adapter.SentDataAdapter;
import com.communicationapp.comman.CommonMethod;
import com.communicationapp.comman.Constants;
import com.communicationapp.comman.MyPreferences;
import com.communicationapp.comman.ProgressBarDialog;
import com.communicationapp.interfaces.ProgressClickListener;
import com.communicationapp.model.SendedLocationuserList;
import com.communicationapp.responseparser.SendLocationResponse;
import com.communicationapp.responseparser.SentLocationResponse;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class SentActivity extends AppCompatActivity implements ProgressClickListener {
    private static final String TAG = "SentActivity";

    /*****************Attributes name***************************/
    private ArrayList<String> mArrayList = new ArrayList<>();
    private ImageView mivLock;
    private boolean isChecked = false;
    private TextView tvRECEIVED;
    private TextView tvCurrntLocation;
    private TextView tvDateTime;
    private SwipeMenuListView mListView;
    private SentDataAdapter mListDataAdapter;
    private LinearLayout container;
    private ProgressBarDialog mProgressDialog;
    private List<SendedLocationuserList> sendedLocationuserList;
    private List<Address> mAddress;
    private String location;
    private String isSecure;
    private String latitude;
    private String langtitude;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sent);

        mAddress = getIntent().getParcelableArrayListExtra("address");
        latitude = getIntent().getStringExtra("latitude");
        langtitude = getIntent().getStringExtra("longitude");

        tvRECEIVED = (TextView) findViewById(R.id.tvRECEIVED);
        tvCurrntLocation = (TextView) findViewById(R.id.tvCurrntLocation);
        tvDateTime = (TextView) findViewById(R.id.tvDateTime);
        tvRECEIVED.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SentActivity.this, RecevedActivity.class);
                intent.putExtra("address", (Serializable) mAddress);
                intent.putExtra("latitude", String.valueOf(latitude));
                intent.putExtra("longitude", String.valueOf(langtitude));
                startActivity(intent);
                finish();

            }
        });
        initControls();
        setToolbarWidget();

        if (CommonMethod.isOnline(SentActivity.this)) {
            validategetSentLocation();
            tvDateTime.setText(CommonMethod.date());
            tvCurrntLocation.setText(mAddress.get(0).getAdminArea() + ", " + mAddress.get(0).getCountryCode());


            StringBuilder builder = new StringBuilder();
            for (int i = 0; i < mAddress.size(); i++) {
                builder.append(mAddress.get(i).getSubLocality());
                builder.append(" " + mAddress.get(i).getFeatureName());
                builder.append(" " + mAddress.get(i).getLocality());
                builder.append(" " + mAddress.get(i).getAdminArea());
                builder.append(" " + mAddress.get(i).getCountryName());
                builder.append(" " + mAddress.get(i).getPostalCode());

            }

            location = builder.toString();
        } else {
            CommonMethod.showSnackbar(SentActivity.this, container, getResources().getString(R.string.error_no_internet));
        }

        if (MyPreferences.getActiveInstance(SentActivity.this).getIsSecure()) {
            mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_red));
        } else {
            mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_green));
        }

    }

    /*******************Method for Action bar Setting*********************************/

    private void setToolbarWidget() {
        LinearLayout llCointer = (LinearLayout) findViewById(R.id.llCointer);

        mProgressDialog = new ProgressBarDialog(SentActivity.this, llCointer);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        container = (LinearLayout) findViewById(R.id.container);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow_24dp);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
        getSupportActionBar().setTitle("");
        mivLock = (ImageView) findViewById(R.id.ivLock);
        mivLock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isChecked == false) {
                    MyPreferences.getActiveInstance(SentActivity.this).setIsSecure(true);
                    mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_red));
                    isChecked = true;
                } else {
                    MyPreferences.getActiveInstance(SentActivity.this).setIsSecure(false);
                    mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_green));
                    isChecked = false;
                }

            }
        });

    }

    /*****************Method for swipe list riht to left ***************************/

    private void initControls() {

        mListView = (SwipeMenuListView) findViewById(R.id.listView);
        mListView.setSwipeDirection(SwipeMenuListView.DIRECTION_LEFT);

        // mListView.setCloseInterpolator(new BounceInterpolator());


        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                // Create different menus depending on the view type
                // create "open" item
                SwipeMenuItem openItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                openItem.setBackground(new ColorDrawable(Color.rgb(0, 128,
                        0)));
                // set item width
                openItem.setWidth(CommonMethod.dp2px(SentActivity.this, 90));
                // set item title
                openItem.setTitle("Send Again");
                // set item title fontsize
                openItem.setTitleSize(18);
                // set item title font color
                openItem.setTitleColor(Color.WHITE);
                // add to menu
                menu.addMenuItem(openItem);
                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                        0x3F, 0x25)));
                // set item width
                deleteItem.setWidth(CommonMethod.dp2px(SentActivity.this, 90));
                // set item title
                deleteItem.setTitle("Clear");
                // set item title fontsize
                deleteItem.setTitleSize(18);
                // set item title font color
                deleteItem.setTitleColor(Color.WHITE);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };

        // set creator
        mListView.setMenuCreator(creator);

        mListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {

                switch (index) {
                    case 0:
                        if (MyPreferences.getActiveInstance(SentActivity.this).getIsSecure()) {
                            mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_red));
                            isSecure = "1";

                        } else {
                            mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_green));
                            isSecure = "0";
                        }
                        if (CommonMethod.isOnline(SentActivity.this)) {
                            validateSendLocation(sendedLocationuserList.get(position).getReceiverId());
                        } else {
                            CommonMethod.showSnackbar(SentActivity.this, container, getResources().getString(R.string.error_no_internet));
                        }
                        break;
                    case 1:
                        if (CommonMethod.isOnline(SentActivity.this)) {
                            validateDelet(sendedLocationuserList.get(position).getRecv_id(),position);
                        } else {
                            CommonMethod.showSnackbar(SentActivity.this, container, getResources().getString(R.string.error_no_internet));
                        }
                        break;
                }
                return true;
            }
        });

        //mListView

        mListView.setOnMenuStateChangeListener(new SwipeMenuListView.OnMenuStateChangeListener() {
            @Override
            public void onMenuOpen(int position) {

            }

            @Override
            public void onMenuClose(int position) {

            }
        });

        mListView.setOnSwipeListener(new SwipeMenuListView.OnSwipeListener() {
            @Override
            public void onSwipeStart(int position) {

            }

            @Override
            public void onSwipeEnd(int position) {

            }
        });
    }

    @Override
    public void onRetryClick() {
        mProgressDialog.showContent();
        if (CommonMethod.isOnline(SentActivity.this)) {
            validategetSentLocation();
        } else {
            CommonMethod.showSnackbar(SentActivity.this, container, getResources().getString(R.string.error_no_internet));
        }
    }

    private void validategetSentLocation() {

        mProgressDialog.showLoading();
        AndroidNetworking.post(Constants.GETSEND_LOCATIONLIST)
                .addBodyParameter("user_id", MyPreferences.getActiveInstance(SentActivity.this).getUserId())
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        mProgressDialog.showContent();
                        try {
                            Log.e("SentResponse",">>>>>>>"+response.toString(2));

                        }catch (Exception ex){
                            ex.printStackTrace();
                        }
                        Gson gson = new Gson();
                        SentLocationResponse mSentLocationResponse = gson.fromJson(response.toString(), SentLocationResponse.class);
                        if (mSentLocationResponse != null) {
                            if (mSentLocationResponse.getSuccess().equals("1")) {

                                parseData(mSentLocationResponse.getSendedLocationuserList());

                            } else {
                                mProgressDialog.showEmpty();

                            }
                        } else {

                            CommonMethod.showSnackbar(SentActivity.this, container, getString(R.string.connection_error));
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        mProgressDialog.showRetry();
                        CommonMethod.showNetworkSnack(container, getResources().getString(R.string.error_server), "Ok");
                    }

                });
    }

    private void parseData(List<SendedLocationuserList> sendedLocationuserList) {
        if(sendedLocationuserList!=null) {
            this.sendedLocationuserList = sendedLocationuserList;
            mListDataAdapter = new SentDataAdapter(SentActivity.this, sendedLocationuserList);
            mListView.setAdapter(mListDataAdapter);
        }
    }

    /*******************method network connection*********************************/

    private void validateSendLocation(String mUserId) {
        mProgressDialog.showLoading();
        AndroidNetworking.post(Constants.SEND_LOCATION)
                .addBodyParameter("sender_id", MyPreferences.getActiveInstance(SentActivity.this).getUserId())
                .addBodyParameter("receiver_id", mUserId)
                .addBodyParameter("lat", latitude)
                .addBodyParameter("long", langtitude)
                .addBodyParameter("location", location)
                .addBodyParameter("isSecure", isSecure)
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        mProgressDialog.showContent();
                        Gson gson = new Gson();
                        SendLocationResponse mSendLocationResponse = gson.fromJson(response.toString(), SendLocationResponse.class);
                        if (mSendLocationResponse != null) {
                            if (mSendLocationResponse.getSuccess().equals("1")) {

                                Toast.makeText(SentActivity.this, mSendLocationResponse.getMessage(), Toast.LENGTH_SHORT).show();
                                validategetSentLocation();
                            } else {
                                mProgressDialog.showEmpty();
                            }
                        } else {

                            CommonMethod.showSnackbar(SentActivity.this, container, getString(R.string.connection_error));
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        mProgressDialog.showRetry();
                        CommonMethod.showNetworkSnack(container, getResources().getString(R.string.error_server), "Ok");
                    }

                });
    }

    /*******************method network connection*********************************/


    private void validateDelet(String mLocationId,final int position) {
        mProgressDialog.showLoading();
        AndroidNetworking.post(Constants.delete_receive)
                .addBodyParameter("user_id", MyPreferences.getActiveInstance(SentActivity.this).getUserId())
                .addBodyParameter("id", mLocationId)
                .addBodyParameter("type", "location")
                .addBodyParameter("deleted_type", "sended")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        mProgressDialog.showContent();
                        Gson gson = new Gson();
                        SendLocationResponse mSendLocationResponse = gson.fromJson(response.toString(), SendLocationResponse.class);
                        if (mSendLocationResponse != null) {
                            if (mSendLocationResponse.getSuccess().equals("1")) {
                                mListDataAdapter.reemove(position);

                            } else {
                                mProgressDialog.showEmpty();
                            }
                        } else {

                            CommonMethod.showSnackbar(SentActivity.this, container, getString(R.string.connection_error));
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        mProgressDialog.showRetry();
                        CommonMethod.showNetworkSnack(container, getResources().getString(R.string.error_server), "Ok");
                    }

                });
    }

}