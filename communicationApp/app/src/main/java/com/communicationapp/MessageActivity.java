package com.communicationapp;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.communicationapp.adapter.MessageAdapter;
import com.communicationapp.adapter.MessagesSwipeAdapter;
import com.communicationapp.comman.MyPreferences;
import com.communicationapp.interfaces.ProgressClickListener;
import com.communicationapp.model.LastConversation;
import com.communicationapp.model.MessageList;
import com.communicationapp.responseparser.MessageResponse;
import com.communicationapp.comman.CommonMethod;
import com.communicationapp.comman.Constants;
import com.communicationapp.comman.ProgressBarDialog;
import com.communicationapp.responseparser.SendLocationResponse;
import com.google.gson.Gson;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class MessageActivity extends AppCompatActivity implements View.OnClickListener, ProgressClickListener {
    private static final String TAG = "MessageActivity";

    /*****************Attributes name***************************/
    private ImageView mAddbutton;
    private SwipeMenuListView mListView;
    private MessageAdapter mListDataAdapter;
    private ImageView mivLock;
    private boolean isChecked = false;
    private CoordinatorLayout container;
    private LinearLayout llCointer;
    private ProgressBarDialog mProgressDialog;
    private RecyclerView rvMessages;
    private List<MessageList> allDataList;
    private MessagesSwipeAdapter mSwipeMessagesAdapter;
    private Paint p;
    //private SwipeRefreshLayout messagesSwipe;
    private boolean isClicked = false;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);
        setToolbarWidget();
        initControls();
        p = new Paint();

        //messagesSwipe = findViewById(R.id.messagesSwipe);

       /* messagesSwipe.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                callGetMessageAPI();

            }
        });*/


        rvMessages = findViewById(R.id.rvMessages);
        rvMessages.setLayoutManager(new LinearLayoutManager(MessageActivity.this));
        rvMessages.setHasFixedSize(true);

        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1,int position, long arg3)
            {

                Intent intent=new Intent(MessageActivity.this, ChatActivity.class);
                intent.putExtra("group_id",allDataList.get(position).getGroupId());
                intent.putExtra("userName",allDataList.get(position).getGroupUser());
                intent.putExtra("recever_id",allDataList.get(position).getSenderId());
                startActivity(intent);
            }
        });


    }

    /*******************Method for Action bar Setting*********************************/

    private void setToolbarWidget() {

        llCointer = (LinearLayout) findViewById(R.id.llCointer);
        mProgressDialog = new ProgressBarDialog(MessageActivity.this, llCointer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow_24dp);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                CommonMethod.closeKeyboard(MessageActivity.this, v);
            }
        });
        getSupportActionBar().setTitle("");
        mAddbutton = (ImageView) findViewById(R.id.ivAdd);
        container = (CoordinatorLayout) findViewById(R.id.container);
        mAddbutton.setOnClickListener(this);
        mivLock = (ImageView) findViewById(R.id.ivLock);
        mivLock.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isChecked == false) {
                    MyPreferences.getActiveInstance(MessageActivity.this).setIsSecure(true);
                    mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_red));
                    isChecked = true;
                } else {
                    MyPreferences.getActiveInstance(MessageActivity.this).setIsSecure(false);
                    mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_green));
                    isChecked = false;
                }

            }
        });

    }

    /*****************Method for swipe list riht to left ***************************/

    private void initControls() {

        mListView = (SwipeMenuListView) findViewById(R.id.listView);
        mListView.setSwipeDirection(SwipeMenuListView.DIRECTION_LEFT);
        // mListView.setCloseInterpolator(new BounceInterpolator());
        SwipeMenuCreator creator = new SwipeMenuCreator() {
            @Override
            public void create(SwipeMenu menu) {
                // Create different menus depending on the view type
                // create "open" item
                /*SwipeMenuItem openItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                openItem.setBackground(new ColorDrawable(Color.rgb(0, 128,
                        0)));
                // set item width
                openItem.setWidth(CommonMethod.dp2px(MessageActivity.this, 90));
                // set item title
                openItem.setTitle("Clear");
                // set item title fontsize
                openItem.setTitleSize(18);
                // set item title font color
                openItem.setTitleColor(Color.WHITE);
                // add to menu
                menu.addMenuItem(openItem);*/
                // create "delete" item
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getApplicationContext());
                // set item background
                deleteItem.setBackground(new ColorDrawable(Color.rgb(0xF9,
                        0x3F, 0x25)));
                // set item width
                deleteItem.setWidth(CommonMethod.dp2px(MessageActivity.this, 90));
                // set item title
                deleteItem.setTitle("Clear");
                // set item title fontsize
                deleteItem.setTitleSize(18);
                // set item title font color
                deleteItem.setTitleColor(Color.WHITE);
                // add to menu
                menu.addMenuItem(deleteItem);
            }
        };

        // set creator
        mListView.setMenuCreator(creator);

        mListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {

                switch (index) {
                    case 0:
                        //Toast.makeText(MessageActivity.this, "Item deleted0", Toast.LENGTH_SHORT).show();
                        validateDelet(allDataList.get(position).getCr_id(),position);

                        break;
                    case 1:
                        Toast.makeText(MessageActivity.this, "Item deleted", Toast.LENGTH_SHORT).show();

                        break;
                }
                return true;
            }
        });

        //mListView

        mListView.setOnMenuStateChangeListener(new SwipeMenuListView.OnMenuStateChangeListener() {
            @Override
            public void onMenuOpen(int position) {

            }

            @Override
            public void onMenuClose(int position) {

            }
        });

        mListView.setOnSwipeListener(new SwipeMenuListView.OnSwipeListener() {
            @Override
            public void onSwipeStart(int position) {

            }

            @Override
            public void onSwipeEnd(int position) {

            }
        });
    }
    /*******************Action perform on Attributes *********************************/

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.ivAdd:
                Intent intent = new Intent(MessageActivity.this, NewMessageActivity.class);
                startActivity(intent);
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (MyPreferences.getActiveInstance(MessageActivity.this).getIsSecure()) {
            mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_red));
        } else {
            mivLock.setImageDrawable(getResources().getDrawable(R.drawable.lock_green));
        }

        callGetMessageAPI();
        callUpdateCountAPI();
    }

    private void callUpdateCountAPI() {
        if (CommonMethod.isOnline(this)) {
            updateBadgeCountAPI();
        }
    }

    private void updateBadgeCountAPI() {
        AndroidNetworking.post(Constants.UPDATE_BADGE)
                .addBodyParameter("type", "message")
                .addBodyParameter("receiver_id", MyPreferences.getActiveInstance(this).getUserId())
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                    }

                    @Override
                    public void onError(ANError anError) {

                    }
                });

    }


    private void callGetMessageAPI() {
        //messagesSwipe.setRefreshing(false);

        if (CommonMethod.isOnline(MessageActivity.this)) {
            validateGetMessage();
        } else {
            CommonMethod.showSnack(container, getResources().getString(R.string.error_no_internet));
        }

    }

    @Override
    public void onRetryClick() {
        mProgressDialog.showContent();
    }

    /*******************method network connection*********************************/

    private void validateGetMessage() {
        mProgressDialog.showLoading();
        AndroidNetworking.post(Constants.get_receivemessage_list)
                .addBodyParameter("user_id", MyPreferences.getActiveInstance(MessageActivity.this).getUserId())
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("ALL_MESSAGES", "-----------" + response);
                        mProgressDialog.showContent();
                        Gson gson = new Gson();
                        MessageResponse mMessageResponse = gson.fromJson(response.toString(), MessageResponse.class);
                        if (mMessageResponse != null) {
                            if (mMessageResponse.getCode().equals("200")) {

                                if (!mMessageResponse.getMessageList().isEmpty()) {

                                    processLastConversation(mMessageResponse.getMessageList());


                                } else {
                                    mProgressDialog.showEmpty();
                                }

                            }
                        } else {

                            mProgressDialog.showRetry();
                        }

                    }

                    @Override
                    public void onError(ANError anError) {

                        mProgressDialog.showRetry();
                        CommonMethod.showNetworkSnack(container, getResources().getString(R.string.error_server), "Ok");
                    }

                });
    }

    private void processLastConversation(List<MessageList> messageList) {
        if (messageList != null) {
            mListDataAdapter = new MessageAdapter(MessageActivity.this, messageList);
            mListView.setAdapter(mListDataAdapter);
            this.allDataList = messageList;
        }
    }



    private void validateDelet(String receiveMessgeId, final int position) {
        mProgressDialog.showLoading();
        AndroidNetworking.post(Constants.delete_receive)
                .addBodyParameter("user_id", MyPreferences.getActiveInstance(MessageActivity.this).getUserId())
                .addBodyParameter("id", receiveMessgeId)
                .addBodyParameter("type", "message")
                .setPriority(Priority.MEDIUM)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        mProgressDialog.showContent();
                        Gson gson = new Gson();
                        SendLocationResponse mSendLocationResponse = gson.fromJson(response.toString(), SendLocationResponse.class);
                        if (mSendLocationResponse != null) {
                            if (mSendLocationResponse.getSuccess().equals("1")) {


                                CommonMethod.showSnackbar(MessageActivity.this, container, mSendLocationResponse.getMessage());
                                mListDataAdapter.remove(position);
                            } else {
                                mProgressDialog.showEmpty();
                            }
                        } else {

                            CommonMethod.showSnackbar(MessageActivity.this, container, getString(R.string.connection_error));
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        mProgressDialog.showRetry();
                        CommonMethod.showNetworkSnack(container, getResources().getString(R.string.error_server), "Ok");
                    }

                });
    }
}