package com.communicationapp;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Address;
import android.location.Location;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.communicationapp.comman.CommonMethod;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;
import java.util.List;

import static com.communicationapp.comman.CommonMethod.showNetworkSnack;

public class AnotherLocationActivity extends FragmentActivity implements
        OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        GoogleMap.OnMarkerDragListener,
        GoogleMap.OnMapLongClickListener,
        View.OnClickListener{

    //Our Map
    private GoogleMap mMap;

    //To store longitude and latitude from map
    private double longitude;
    private double latitude;
    //Google ApiClient
    private GoogleApiClient googleApiClient;
    private ImageView ivBackArrow;

    private AlertDialog mOtpDialog;
    /****************Atributes name************************/

    private RelativeLayout rlContainer;
    private LinearLayout container;
    private TextView tvLangtitude;
    private TextView tvLatitude;
    private ImageView ivSendLocation;
    String City;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_anthore_location);
        if(CommonMethod.isLocationServiceEnabled(AnotherLocationActivity.this)){

        }else {
            showNetworkSnack(container,"Please enable locaiton","Ok");
        }

        setToolbarWidget();
        showVerifyOtpDialog();

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        //Initializing googleapi client
        googleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();


    }
    private void setToolbarWidget() {
        rlContainer =(RelativeLayout)findViewById(R.id.rlContainer);
        container =(LinearLayout) findViewById(R.id.container);
        tvLangtitude =(TextView)findViewById(R.id.tvLangtitude);
        tvLatitude =(TextView)findViewById(R.id.tvLatitude);
        ivSendLocation =(ImageView) findViewById(R.id.ivSendLocation);
        ivSendLocation.setOnClickListener(this);
        ivBackArrow =(ImageView) findViewById(R.id.ivBackArrow);
        ivBackArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }


    private void showVerifyOtpDialog() {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(AnotherLocationActivity.this);
        View view = LayoutInflater.from(AnotherLocationActivity.this).inflate(R.layout.verify_otp_dialog, null);


        alertDialogBuilder.setView(view);
        TextView tvOk = view.findViewById(R.id.tvOk);
        TextView tvCancel = view.findViewById(R.id.tvCancel);
        final EditText etlongitude = view.findViewById(R.id.etlongitude);
        final EditText etlatitude = view.findViewById(R.id.etlatitude);

        tvCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                mOtpDialog.dismiss();
            }
        });  tvOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(etlatitude.getText().toString().trim())) {
                    CommonMethod.showSnackbar(AnotherLocationActivity.this, v, "Please enter latitude");
                }else if (TextUtils.isEmpty(etlongitude.getText().toString().trim())) {
                    CommonMethod.showSnackbar(AnotherLocationActivity.this, v, "Please enter longitude");
                } else if (CommonMethod.isOnline(AnotherLocationActivity.this)) {
                    mOtpDialog.dismiss();

                    StringBuilder builder =new StringBuilder();
                    List<Address> addresses = CommonMethod.getAddress(AnotherLocationActivity.this,latitude,longitude);
                    for(int i=0;i<addresses.size();i++){
                        builder.append(addresses.get(i).getSubLocality());
                        builder.append(" "+addresses.get(i).getFeatureName());
                        builder.append(" "+addresses.get(i).getLocality());
                        builder.append(" "+addresses.get(i).getAdminArea());
                        builder.append(" "+addresses.get(i).getCountryName());
                        builder.append(" "+addresses.get(i).getPostalCode());
                        City = addresses.get(i).getLocality();

                    }
                    Intent intent =new Intent(AnotherLocationActivity.this,ContactListActivity.class);
                    intent.putExtra("navigate","Location");
                    intent.putExtra("latitude",tvLatitude.getText().toString().trim());
                    intent.putExtra("langtitude",tvLangtitude.getText().toString().trim());
                    intent.putExtra("location",builder.toString());
                    intent.putExtra("City",City);
                    startActivity(intent);


                } else {
                    CommonMethod.showSnackbar(AnotherLocationActivity.this, v, "No Connection Avaliable");
                }
            }
        });

        //alertDialogBuilder.create();
        mOtpDialog = alertDialogBuilder.create();
        mOtpDialog = alertDialogBuilder.show();
    }

    @Override
    protected void onStart() {
        googleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onStop() {
        googleApiClient.disconnect();
        super.onStop();
    }

    //Getting current location
    private void getCurrentLocation() {
        mMap.clear();
        //Creating a location object
        Location location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);
        if (location != null) {
            //Getting longitude and latitude
            longitude = location.getLongitude();
            latitude = location.getLatitude();

            //moving the map to location
            moveMap();
        }
    }

    //Function to move the map
    private void moveMap() {
        //String to display current latitude and longitude
        String msg = latitude + ", "+longitude;

        //Creating a LatLng Object to store Coordinates
        LatLng latLng = new LatLng(latitude, longitude);

        //Adding marker to map
        mMap.addMarker(new MarkerOptions()
                .position(latLng) //setting position
                .draggable(true) //Making the marker draggable
                .title("Current Location")); //Adding a title

        //Moving the camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));

        //Animating the camera
        mMap.animateCamera(CameraUpdateFactory.zoomTo(15));

        //Displaying current coordinates in toast
        //Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
        tvLangtitude.setText(String.valueOf(longitude));
        tvLatitude.setText(String.valueOf(latitude));
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng latLng = new LatLng(-34, 151);
        mMap.addMarker(new MarkerOptions().position(latLng).draggable(true));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.setOnMarkerDragListener(this);
        mMap.setOnMapLongClickListener(this);

        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng latLng) {
                mMap.clear();

                //Adding a new marker to the current pressed position
                mMap.addMarker(new MarkerOptions()
                        .position(latLng)
                        .draggable(true));
                tvLangtitude.setText(String.valueOf(latLng.longitude));
                tvLatitude.setText(String.valueOf(latLng.latitude));
            }
        });
    }

    @Override
    public void onConnected(Bundle bundle) {
        getCurrentLocation();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {

    }

    @Override
    public void onMapLongClick(LatLng latLng) {
        //Clearing all the markers
        /*mMap.clear();

        //Adding a new marker to the current pressed position
        mMap.addMarker(new MarkerOptions()
                .position(latLng)
                .draggable(true));
        tvLangtitude.setText(String.valueOf(latLng.longitude));
        tvLatitude.setText(String.valueOf(latLng.latitude));*/
    }



    @Override
    public void onMarkerDragStart(Marker marker) {

    }

    @Override
    public void onMarkerDrag(Marker marker) {

    }

    @Override
    public void onMarkerDragEnd(Marker marker) {
        //Getting the coordinates
        latitude = marker.getPosition().latitude;
        longitude = marker.getPosition().longitude;
        //Moving the map
        moveMap();
    }

    @Override
    public void onClick(View v) {
        if(v == rlContainer){
            getCurrentLocation();
            moveMap();
        }else if(v == ivSendLocation)
        {

            if(CommonMethod.isLocationServiceEnabled(AnotherLocationActivity.this)){
                StringBuilder builder =new StringBuilder();
                List<Address> addresses = CommonMethod.getAddress(AnotherLocationActivity.this,latitude,longitude);
                for(int i=0;i<addresses.size();i++){
                    builder.append(addresses.get(i).getSubLocality());
                    builder.append(" "+addresses.get(i).getFeatureName());
                    builder.append(" "+addresses.get(i).getLocality());
                    builder.append(" "+addresses.get(i).getAdminArea());
                    builder.append(" "+addresses.get(i).getCountryName());
                    builder.append(" "+addresses.get(i).getPostalCode());
                    City = addresses.get(i).getLocality();
                }
                Intent intent =new Intent(AnotherLocationActivity.this,ContactListActivity.class);
                intent.putExtra("navigate","Location");
                intent.putExtra("latitude",tvLatitude.getText().toString().trim());
                intent.putExtra("langtitude",tvLangtitude.getText().toString().trim());
                intent.putExtra("location",builder.toString());
                intent.putExtra("City",City);
                startActivity(intent);
            }else {
                showNetworkSnack(container,"Please enable locaiton","Ok");
            }


        }
    }

    public  void showNetworkSnack(View view, String message, String action){
        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG) .setAction(action, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent myIntent = new Intent( Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                startActivity(myIntent);
            }
        });
        snackbar.setActionTextColor(Color.RED);

        View sbView = snackbar.getView();
        TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
        textView.setTextColor(Color.YELLOW);
        snackbar.show();
    }
}
