package com.communicationapp;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.baoyz.widget.PullRefreshLayout;
import com.communicationapp.adapter.GetchatAdapter;
import com.communicationapp.comman.Constants;
import com.communicationapp.comman.MyPreferences;
import com.communicationapp.model.ChatList;
import com.communicationapp.model.ConversationList;
import com.communicationapp.responseparser.GetChatResponse;
import com.communicationapp.comman.CommonMethod;
import com.communicationapp.comman.HttpClient;
import com.communicationapp.responseparser.NewMessageResponse;
import com.github.nukc.stateview.StateView;
import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import java.util.List;

public class ChatActivity extends AppCompatActivity {
    private TextView mToolbarCustomTitle;
    private Toolbar toolbar;
    private RecyclerView mRecyclerViewGet;
    private GetchatAdapter mAdapter;
    PullRefreshLayout layout;

    RelativeLayout frame_container;
    EditText type_message;
    ImageView iv_send_message;
    private String group_id;
    private String recever_id;
    private String userName;
    private Runnable runnable2;
    private Handler handler2 = new Handler();
    private boolean keepupdating = true;
    private int checkArraylistSize;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);

        initializeVariables();

        mRecyclerViewGet.setAdapter(null);

        iv_send_message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(type_message.getText().toString())) {
                    Toast.makeText(ChatActivity.this, "Please Enter Your Message", Toast.LENGTH_SHORT).show();
                } else {
                    if (CommonMethod.isOnline(ChatActivity.this)) {
                        new AddchatMessageAsyntask().execute();
                    }
                }
            }
        });

        runnable2 = new Runnable() {
            public void run() {
                new GetChatAsynTask().execute();
                if (keepupdating)
                    handler2.postDelayed(runnable2, 3000);
            }
        };

    }

    private void initializeVariables() {

        //sessionManager = new SessionManager(Chat_Level3.this);

        Intent intent = getIntent();

        group_id = intent.getStringExtra("group_id");
        recever_id = intent.getStringExtra("recever_id");
        if(MyPreferences.getActiveInstance(ChatActivity.this).getUserId().equalsIgnoreCase(recever_id)){
            recever_id="";
        }
        userName = intent.getStringExtra("userName");
        type_message = (EditText) findViewById(R.id.type_message);
        iv_send_message = (ImageView) findViewById(R.id.iv_send_message);

        mRecyclerViewGet = (RecyclerView) findViewById(R.id.RV_chatlist);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ChatActivity.this, LinearLayoutManager.VERTICAL, false);
        mRecyclerViewGet.setLayoutManager(mLayoutManager);
        mRecyclerViewGet.setItemAnimator(new DefaultItemAnimator());

        frame_container = (RelativeLayout) findViewById(R.id.activity_chat);


        setToolbarWidget();
    }



    @Override
    protected void onResume() {
        super.onResume();
        keepupdating = true;
        handler2.postDelayed(runnable2, 3000);

    }

    @Override
    public void onPause() {
        super.onPause();
        keepupdating = false;
    }

    private class AddchatMessageAsyntask extends AsyncTask<String, Void, String> {

        private NewMessageResponse mNewMessageResponse;
        private String response = "";

        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... params) {
            response = AddmessageCall();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                Gson gson = new Gson();
                 mNewMessageResponse = gson.fromJson(response, NewMessageResponse.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            }
            if (mNewMessageResponse != null) {
                if (mNewMessageResponse.getCode().trim().equals("200")) {

                    group_id = mNewMessageResponse.getGroupId();
                    type_message.setText("");
                } else {
                    CommonMethod.showAlert(mNewMessageResponse.getMessage(), ChatActivity.this);
                }
            }
        }

        @SuppressLint("DefaultLocale")
        private String AddmessageCall() {
            try {
                String url = Constants.post_chat;
                HttpClient httpClient = new HttpClient(url);
                httpClient.connectForMultipart();
                httpClient.addFormPart("sender_id", MyPreferences.getActiveInstance(ChatActivity.this).getUserId());
                httpClient.addFormPart("group_id", group_id);
                httpClient.addFormPart("recever_id", recever_id);
                httpClient.addFormPart("reply", type_message.getText().toString());
                httpClient.finishMultipart();
                response = httpClient.getResponse();
                Log.e("ADDCHATMESSAGE--", response);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }
    }

    private class GetChatAsynTask extends AsyncTask<String, Void, String> {

        private GetChatResponse mGetChatResponse;
        private String response = "";

        protected void onPreExecute() {
            super.onPreExecute();

        }

        @Override
        protected String doInBackground(String... params) {
            response = GetchatCall();
            return response;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            try {
                Gson gson = new Gson();
                mGetChatResponse = gson.fromJson(response, GetChatResponse.class);
            } catch (JsonSyntaxException e) {
                e.printStackTrace();
            }
            if (mGetChatResponse != null) {
                if (mGetChatResponse.getCode().trim().equals("200")) {

                    if (mGetChatResponse.getChatList().size()>checkArraylistSize||mGetChatResponse.getChatList().size()<checkArraylistSize) {
                        checkArraylistSize = mGetChatResponse.getChatList().size();

                        mAdapter = new GetchatAdapter(mGetChatResponse.getChatList(), ChatActivity.this);
                        mRecyclerViewGet.setAdapter(mAdapter);
                        mRecyclerViewGet.scrollToPosition(mAdapter.getItemCount() - 1);
                    }


                } else {

                }
            } else {

            }
        }

        @SuppressLint("DefaultLocale")
        private String GetchatCall() {
            try {
                String url = Constants.get_Chatlis;
                HttpClient httpClient = new HttpClient(url);
                httpClient.connectForMultipart();
                httpClient.addFormPart("group_id",group_id);
                httpClient.finishMultipart();
                response = httpClient.getResponse();

            } catch (Exception e) {
                e.printStackTrace();
            }
            return response;
        }
    }

    private void setToolbarWidget() {
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_back_arrow_24dp);
        setSupportActionBar(toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
                CommonMethod.closeKeyboard(ChatActivity.this,v);
            }
        });
        mToolbarCustomTitle = (TextView) findViewById(R.id.toolbar_title);
        mToolbarCustomTitle.setText(userName);
        getSupportActionBar().setTitle("");

    }

    @Override
    public void onBackPressed() {
        CommonMethod.overridePendingTransitionslide(ChatActivity.this);
        finish();
    }
}
