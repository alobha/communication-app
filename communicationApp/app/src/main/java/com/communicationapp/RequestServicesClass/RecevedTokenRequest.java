package com.communicationapp.RequestServicesClass;

public class RecevedTokenRequest {

    private String userId;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
