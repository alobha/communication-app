package com.communicationapp.RequestServicesClass;

public class NewMessageTokenRequest {
    private String access_token;
    private String message;
    private String receiver_ids;

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getReceiver_ids() {
        return receiver_ids;
    }

    public void setReceiver_ids(String receiver_ids) {
        this.receiver_ids = receiver_ids;
    }
}
