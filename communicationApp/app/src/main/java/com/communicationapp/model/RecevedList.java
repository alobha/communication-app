package com.communicationapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RecevedList {

    @SerializedName("ZZCUSTOMERID")
    @Expose
    public String zZCUSTOMERID;
    @SerializedName("ZZEMAILID")
    @Expose
    public String zZEMAILID;
    @SerializedName("ZZPWD")
    @Expose
    public String zZPWD;
    @SerializedName("ZZDOMAIN")
    @Expose
    public String zZDOMAIN;
    @SerializedName("ZZGEOGRAPHY")
    @Expose
    public String zZGEOGRAPHY;
    @SerializedName("ZZCUSTTYPE")
    @Expose
    public String zZCUSTTYPE;
    @SerializedName("ZZCUSTIMG")
    @Expose
    public String zZCUSTIMG;
    @SerializedName("ZZLANGUANE")
    @Expose
    public String zZLANGUANE;
    @SerializedName("ZZCUSTOTHRID")
    @Expose
    public String zZCUSTOTHRID;
    @SerializedName("ZZCUSTNAME")
    @Expose
    public String zZCUSTNAME;
    @SerializedName("ZZCUSTABUT")
    @Expose
    public String zZCUSTABUT;
    @SerializedName("ZZCUSTBRTH")
    @Expose
    public String zZCUSTBRTH;
    @SerializedName("ZZCUSTGNDR")
    @Expose
    public String zZCUSTGNDR;
    @SerializedName("ZZCUSTCONTACT")
    @Expose
    public String zZCUSTCONTACT;
    @SerializedName("ZZCUSTALTCONTACT")
    @Expose
    public String zZCUSTALTCONTACT;
}
