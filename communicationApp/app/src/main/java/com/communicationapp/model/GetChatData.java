package com.communicationapp.model;

/**
 * Created by Admn on 4/10/2017.
 */

public class GetChatData
{
    String c_id,level_two, level_three,time, status, level1_id, cr_id, reply, media, media_type, sender_id, sender_type;

    public String getC_id() {
        return c_id;
    }

    public void setC_id(String c_id) {
        this.c_id = c_id;
    }

    public String getLevel_two() {
        return level_two;
    }

    public void setLevel_two(String level_two) {
        this.level_two = level_two;
    }

    public String getLevel_three() {
        return level_three;
    }

    public void setLevel_three(String level_three) {
        this.level_three = level_three;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getLevel1_id() {
        return level1_id;
    }

    public void setLevel1_id(String level1_id) {
        this.level1_id = level1_id;
    }

    public String getCr_id() {
        return cr_id;
    }

    public void setCr_id(String cr_id) {
        this.cr_id = cr_id;
    }

    public String getReply() {
        return reply;
    }

    public void setReply(String reply) {
        this.reply = reply;
    }

    public String getMedia() {
        return media;
    }

    public void setMedia(String media) {
        this.media = media;
    }

    public String getMedia_type() {
        return media_type;
    }

    public void setMedia_type(String media_type) {
        this.media_type = media_type;
    }

    public String getSender_id() {
        return sender_id;
    }

    public void setSender_id(String sender_id) {
        this.sender_id = sender_id;
    }

    public String getSender_type() {
        return sender_type;
    }

    public void setSender_type(String sender_type) {
        this.sender_type = sender_type;
    }
}
