package com.communicationapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MessageList {

    public String getCr_id() {
        return cr_id;
    }

    public void setCr_id(String cr_id) {
        this.cr_id = cr_id;
    }

    public String getcIdFk() {
        return cIdFk;
    }

    public void setcIdFk(String cIdFk) {
        this.cIdFk = cIdFk;
    }

    @SerializedName("cr_id")
    @Expose
    private String cr_id;


    @SerializedName("receive_messge_id")
    @Expose
    private String receiveMessgeId;
    @SerializedName("receive_user_id")
    @Expose
    private String receiveUserId;
    @SerializedName("group_id")
    @Expose
    private String groupId;
    @SerializedName("reply")
    @Expose
    private String reply;
    @SerializedName("sender_id")
    @Expose
    private String senderId;
    @SerializedName("sender_name")
    @Expose
    private String senderName;
    @SerializedName("c_id_fk")
    @Expose
    private String cIdFk;
    @SerializedName("time")
    @Expose
    private String time;
    @SerializedName("date")
    @Expose
    private String date;
    @SerializedName("user_group")
    @Expose
    private String userGroup;
    @SerializedName("group_user")
    @Expose
    private String groupUser;
    @SerializedName("Receive_name")
    @Expose
    private String Receive_name;

    public String getReceive_name() {
        return Receive_name;
    }

    public void setReceive_name(String receive_name) {
        Receive_name = receive_name;
    }

    public String getReceiveMessgeId() {
        return receiveMessgeId;
    }

    public void setReceiveMessgeId(String receiveMessgeId) {
        this.receiveMessgeId = receiveMessgeId;
    }

    public String getReceiveUserId() {
        return receiveUserId;
    }

    public void setReceiveUserId(String receiveUserId) {
        this.receiveUserId = receiveUserId;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getReply() {
        return reply;
    }

    public void setReply(String reply) {
        this.reply = reply;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getCIdFk() {
        return cIdFk;
    }

    public void setCIdFk(String cIdFk) {
        this.cIdFk = cIdFk;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getUserGroup() {
        return userGroup;
    }

    public void setUserGroup(String userGroup) {
        this.userGroup = userGroup;
    }

    public String getGroupUser() {
        return groupUser;
    }

    public void setGroupUser(String groupUser) {
        this.groupUser = groupUser;
    }
}
