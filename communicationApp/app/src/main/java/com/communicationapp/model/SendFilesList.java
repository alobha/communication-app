package com.communicationapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mac on 10/07/18.
 */

public class SendFilesList {

    @SerializedName("receive_file_id")
    @Expose
    private String receive_file_id;

    @SerializedName("isSecure")
    @Expose
    private String isSecure;
    @SerializedName("sender_file_id")
    @Expose
    private String senderFileId;
    @SerializedName("sender_id")
    @Expose
    private String senderId;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("file")
    @Expose
    private String file;
    @SerializedName("send_date")
    @Expose
    private String sendDate;
    @SerializedName("receiver_name")
    @Expose
    private String receiverName;
    @SerializedName("receiver_last_name")
    @Expose
    private String receiverLastName;
    @SerializedName("receiver_id")
    @Expose
    private String receiverId;
    @SerializedName("receiver_phone")
    @Expose
    private String receiverPhone;
    @SerializedName("receiver_email")
    @Expose
    private String receiverEmail;
    @SerializedName("receive_date")
    @Expose
    private String receiveDate;

    public String getIsSecure() {
        return isSecure;
    }

    public void setIsSecure(String isSecure) {
        this.isSecure = isSecure;
    }

    public String getSenderFileId() {
        return senderFileId;
    }

    public void setSenderFileId(String senderFileId) {
        this.senderFileId = senderFileId;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getSendDate() {
        return sendDate;
    }

    public void setSendDate(String sendDate) {
        this.sendDate = sendDate;
    }

    public String getReceiverName() {
        return receiverName;
    }

    public void setReceiverName(String receiverName) {
        this.receiverName = receiverName;
    }

    public String getReceiverLastName() {
        return receiverLastName;
    }

    public void setReceiverLastName(String receiverLastName) {
        this.receiverLastName = receiverLastName;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getReceiverPhone() {
        return receiverPhone;
    }

    public void setReceiverPhone(String receiverPhone) {
        this.receiverPhone = receiverPhone;
    }

    public String getReceiverEmail() {
        return receiverEmail;
    }

    public void setReceiverEmail(String receiverEmail) {
        this.receiverEmail = receiverEmail;
    }

    public String getReceiveDate() {
        return receiveDate;
    }

    public void setReceiveDate(String receiveDate) {
        this.receiveDate = receiveDate;
    }

    public String getReceive_file_id() {
        return receive_file_id;
    }

    public void setReceive_file_id(String receive_file_id) {
        this.receive_file_id = receive_file_id;
    }
}
