package com.communicationapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by mac on 10/07/18.
 */

public class ReceivedFilesList {


    public String getReceive_file_id() {
        return receive_file_id;
    }

    public void setReceive_file_id(String receive_file_id) {
        this.receive_file_id = receive_file_id;
    }

    @SerializedName("receive_file_id")
    @Expose
    private String receive_file_id;

    @SerializedName("isSecure")
    @Expose
    private String isSecure;
    @SerializedName("sender_file_id")
    @Expose
    private String senderFileId;
    @SerializedName("sender_id")
    @Expose
    private String senderId;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("file")
    @Expose
    private String file;
    @SerializedName("sender_name")
    @Expose
    private String senderName;
    @SerializedName("sender_last_name")
    @Expose
    private String senderLastName;
    @SerializedName("receiver_id")
    @Expose
    private String receiverId;
    @SerializedName("sender_phone")
    @Expose
    private String senderPhone;
    @SerializedName("sender_email")
    @Expose
    private String senderEmail;
    @SerializedName("receive_date")
    @Expose
    private String receiveDate;

    public String getIsSecure() {
        return isSecure;
    }

    public void setIsSecure(String isSecure) {
        this.isSecure = isSecure;
    }

    public String getSenderFileId() {
        return senderFileId;
    }

    public void setSenderFileId(String senderFileId) {
        this.senderFileId = senderFileId;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getSenderName() {
        return senderName;
    }

    public void setSenderName(String senderName) {
        this.senderName = senderName;
    }

    public String getSenderLastName() {
        return senderLastName;
    }

    public void setSenderLastName(String senderLastName) {
        this.senderLastName = senderLastName;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getSenderPhone() {
        return senderPhone;
    }

    public void setSenderPhone(String senderPhone) {
        this.senderPhone = senderPhone;
    }

    public String getSenderEmail() {
        return senderEmail;
    }

    public void setSenderEmail(String senderEmail) {
        this.senderEmail = senderEmail;
    }

    public String getReceiveDate() {
        return receiveDate;
    }

    public void setReceiveDate(String receiveDate) {
        this.receiveDate = receiveDate;
    }
}
