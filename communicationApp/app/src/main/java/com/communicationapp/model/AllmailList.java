package com.communicationapp.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AllmailList {


    public String getReceive_mail_id() {
        return receive_mail_id;
    }

    public void setReceive_mail_id(String receive_mail_id) {
        this.receive_mail_id = receive_mail_id;
    }

    @SerializedName("receive_mail_id")
    @Expose
    private String receive_mail_id;
    @SerializedName("isSecure")
    @Expose
    private String isSecure;
    @SerializedName("mail_type")
    @Expose
    private String mailType;
    @SerializedName("mail_id")
    @Expose
    private String mailId;
    @SerializedName("sender_id")
    @Expose
    private String senderId;
    @SerializedName("subject")
    @Expose
    private String subject;
    @SerializedName("receive_mail_date")
    @Expose
    private String receiveMailDate;
    @SerializedName("content")
    @Expose
    private String content;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("last_name")
    @Expose
    private String lastName;
    @SerializedName("receiver_id")
    @Expose
    private String receiverId;
    @SerializedName("phone")
    @Expose
    private String phone;
    @SerializedName("email")
    @Expose
    private String email;

    public String getIsSecure() {
        return isSecure;
    }

    public void setIsSecure(String isSecure) {
        this.isSecure = isSecure;
    }

    public String getMailType() {
        return mailType;
    }

    public void setMailType(String mailType) {
        this.mailType = mailType;
    }

    public String getMailId() {
        return mailId;
    }

    public void setMailId(String mailId) {
        this.mailId = mailId;
    }

    public String getSenderId() {
        return senderId;
    }

    public void setSenderId(String senderId) {
        this.senderId = senderId;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getReceiveMailDate() {
        return receiveMailDate;
    }

    public void setReceiveMailDate(String receiveMailDate) {
        this.receiveMailDate = receiveMailDate;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(String receiverId) {
        this.receiverId = receiverId;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
