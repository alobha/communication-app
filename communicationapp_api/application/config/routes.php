<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'welcome';

$route['login'] = 'Manage_user/login';
$route['reset_password'] = 'Manage_user/reset_password';
$route['forgot_password'] = 'Manage_user/Forgotpassword';
$route['register'] = 'Manage_user/registration';
$route['get_profile'] = 'Manage_user/get_profile';
$route['update_profile'] = 'Manage_user/update_profile';
$route['get_contact_list'] = 'Manage_user/get_contact_list';
$route['sendLocation'] = 'Manage_user/sendLocation';
$route['getSendedLocationList'] = 'Manage_user/getSendedLocationList';
$route['ReceiveLocationbyuser'] = 'Manage_user/ReceiveLocationbyuser';
$route['sendMail'] = 'Manage_user/sendMail';
$route['ReceiveMail'] = 'Manage_user/ReceiveMail';
$route['sendFile'] = 'Manage_user/sendFile';
$route['ReceiveFiles'] = 'Manage_user/ReceiveFiles';
$route['Creating_Conversation'] = 'Manage_user/Creating_Conversation';
$route['getLastConversation'] = 'Manage_user/getLastConversation';
$route['get_conversation'] = 'Manage_user/get_conversation';
$route['get_veg_count'] = 'Manage_user/get_veg_count';
$route['Gethistory'] = 'Manage_user/Gethistory';
$route['delete_receive'] = 'Manage_user/delete_receive';
$route['create_group'] = 'Manage_user/create_group';
$route['get_receivemessage_list'] = 'Manage_user/get_receivemessage_list';
$route['post_chat'] = 'Manage_user/post_chat';
$route['get_Chatlist'] = 'Manage_user/get_Chatlist';
$route['RemoveVegCount'] = 'Manage_user/RemoveVegCount';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
