<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
class Manage_user extends CI_Controller
{
    
/*Define Variable */

         var $user_id='';
         var $username='';
         var $surname='';
         var $email='';
         var $phone=''; 
		 var $mobile='';
         var $address1='';
         var $address2='';
         var $city='';
         var $zip='';
         var $langlat='';
         var $password='';
         var $fb_id='';
         var $g_id='';
         var $g_photo='';
         var $google_plus='';
         var $skype='';
         var $facebook='';
         var $user_type='';
         var $country='';
         var $state='';
         var $pincode='';
         var $old_password='';   
         var $new_password='';
		 var $user_email='';
		 var $sender_id='';
         var $receiver_id='';
         var $lat='';
         var $long='';   
         var $location='';
		 var $isSecure='';
		 
	
		 
    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->model('Apimodel');
        //$this->crud_model->ip_data();
		//$this->config->cache_query();
        date_default_timezone_set('Asia/Kolkata');  
    }
    
  /* Start Api Function  */
    /*  user registration function */
	
public function registration()
    { 
		if($this->input->post('mobile'))
            $this->mobile = $this->input->post('mobile');

             if($this->input->post('password'))
            $this->password = $this->input->post('password');
		    if($this->input->post('first_name'))
            $this->first_name = $this->input->post('first_name');

         if($this->input->post('last_name'))
            $this->last_name = $this->input->post('last_name');

             if($this->input->post('email'))
            $this->email = $this->input->post('email');


        if($this->mobile=='')
        
        {
            
           log_message('error', ''.__METHOD__);
             return jsonResponse('200', '0','Mobile number  is Required');    
        }
    
    elseif($this->password=='')
          {   
             log_message('error', 'Password is Required'.__METHOD__);
             return jsonResponse('200', '0','Password is Required'); 
             
               }
			   
	   
			   
        elseif($this->first_name=='')
          {   
           log_message('error', 'Name is Required'.__METHOD__);
             return jsonResponse('200', '0','Name is Required'); 
             
           }
	elseif($this->last_name=='')
          {   
           log_message('error', 'Name is Required'.__METHOD__);
             return jsonResponse('200', '0','Last name is Required'); 
             
           }

        elseif($this->email=='')
       {   
           log_message('error', 'Email is Required '.__METHOD__);
             return jsonResponse('200', '0','Email is Required'); 
             
            }


      $validate = $this->Apimodel->validate_mobile($this->email);

      if( $validate){

           log_message('error', ''.__METHOD__);
             return jsonResponse('200', '0','This email address already registered.');

      }else{
		  
		  
		   //$password=  sha1($this->password);
		   //$password = $this->password;
		   $password = encrypt_string(trim($this->password));
		         
        $Register = $this->Apimodel->user_registration($this->mobile,$password,$this->first_name,$this->last_name,$this->email);
     
     if($Register){

     
               //$datass['access_token']= $Register[0]->access_token;
               $datass['first_name']= $Register[0]->first_name;
               $datass['last_name']= $Register[0]->last_name;
               $datass['user_id']= $Register[0]->user_id;             
               $datass['phone']= $Register[0]->phone;
               $datass['email']= $Register[0]->email;
              
            $response = array(
            "success"=>'1',
            "code"=>'200',
            "message"=>"Your registration successfully.",
           "registerDetails"=>$datass,
               
           );  
                                              
            echo json_encode($response);
         
          
        }else{
                 log_message('error', ''.__METHOD__);
             return jsonResponse('200', '0','error somthing went wrong');  

        }   

    }
}


public function RemoveVegCount()
    {
		if($this->input->post('type'))
            $this->type = $this->input->post('type');

             if($this->input->post('receiver_id'))
            $this->receiver_id = $this->input->post('receiver_id');

        if($this->type=='') {
            
           log_message('error', ''.__METHOD__);
             return jsonResponse('200', '0','Type is Required');    
        }
    
    elseif($this->receiver_id=='')
    {   
           log_message('error', 'Invalid parameter or value passed in '.__METHOD__);
             return jsonResponse('200', '0','Receiver id is Required'); 
             
        }
		
	$where=array('type'=>$this->type,'receiveId'=>$this->receiver_id);
 $this->db->where($where);
 $delete = $this->db->delete('veg_count'); 
		
			   
    
     if($delete){
            $response = array(
            "success"=>'1',
            "code"=>'200',
            "message"=>"Data deleted.",
               
           );  
                                              
            echo json_encode($response);
         
          
        }else{
                 log_message('error', ''.__METHOD__);
             return jsonResponse('200', '0','Something Went wrong');  

        }           
        
    }











public function login()
    {
		if($this->input->post('email'))
            $this->email = $this->input->post('email');

             if($this->input->post('password'))
            $this->password = $this->input->post('password');

        if($this->email=='') {
            
           log_message('error', ''.__METHOD__);
             return jsonResponse('200', '0','Email is Required');    
        }
    
    elseif($this->password=='')
    {   
           log_message('error', 'Invalid parameter or value passed in '.__METHOD__);
             return jsonResponse('200', '0','Password is Required'); 
             
        }
    
	
	//$password = encrypt_string($this->password);
	//$password=  sha1($this->password);	
	$password = (trim($this->password));
        $login = $this->Apimodel->login_user($this->email,$password);
     
     if($login){

     
               //$datass['access_token']= $login[0]->access_token;
               $datass['first_name']= $login[0]->first_name;
               $datass['last_name']= $login[0]->last_name;
               $datass['user_id']= $login[0]->user_id;             
               $datass['phone']= $login[0]->phone;
               $datass['email']= $login[0]->email;
              
        
            $response = array(
            "success"=>'1',
            "code"=>'200',
            "message"=>"Login successfully.",
           "user_details"=>$datass,
               
           );  
                                              
            echo json_encode($response);
         
          
        }else{
                 log_message('error', ''.__METHOD__);
             return jsonResponse('200', '0','Email or Password Not Matched');  

        }           
        
    }

public function get_profile()
    {
		if($this->input->post('user_id'))
            $this->user_id = $this->input->post('user_id');

        if($this->user_id=='')
        
        {
            
           log_message('error', ''.__METHOD__);
             return jsonResponse('200', '0','user id  is Required');    
        }
		
        $profile = $this->Apimodel->get_profile($this->user_id);
     
     if($profile){

     
               //$datass['access_token']= $profile[0]->access_token;
               $datass['first_name']= $profile[0]->first_name;
               $datass['last_name']= $profile[0]->last_name;
               $datass['user_id']= $profile[0]->user_id;             
               $datass['phone']= $profile[0]->phone;
               $datass['email']= $profile[0]->email;
			   $datass['password']= $profile[0]->password;
              
        
            $response = array(
            "success"=>'1',
            "code"=>'200',
            "message"=>"get profile successfully.",
           "user_profile"=>$datass,
               
           );  
                                              
            echo json_encode($response);
         
          
        }else{
                 log_message('error', ''.__METHOD__);
             return jsonResponse('200', '0','Data not found');  

        }           
        
    }

public function update_profile()
    { 
	   if($this->input->post('user_id'))
            $this->user_id = $this->input->post('user_id');
		if($this->input->post('mobile'))
            $this->mobile = $this->input->post('mobile');
		    if($this->input->post('first_name'))
            $this->first_name = $this->input->post('first_name');
         if($this->input->post('last_name'))
            $this->last_name = $this->input->post('last_name');
             if($this->input->post('email'))
            $this->email = $this->input->post('email');
        
       if($this->user_id=='')
       {   
           log_message('error', 'user id is Required '.__METHOD__);
             return jsonResponse('200', '0','user id is Required'); 
             
            }

		         
        $update_profile = $this->Apimodel->update_profile($this->mobile,$this->user_id,$this->first_name,$this->last_name,$this->email);
     
     if($update_profile){

     
               //$datass['access_token']= $update_profile[0]->access_token;
               $datass['first_name']= $update_profile[0]->first_name;
               $datass['last_name']= $update_profile[0]->last_name;
               $datass['user_id']= $update_profile[0]->user_id;
			   $datass['phone']= $update_profile[0]->phone;
               $datass['email']= $update_profile[0]->email;
              
            $response = array(
            "success"=>'1',
            "code"=>'200',
            "message"=>"profile update successfully.",
           "profile_details"=>$datass,
               
           );  
                                              
            echo json_encode($response);
         
          
        }else{
                 log_message('error', ''.__METHOD__);
             return jsonResponse('200', '0','profile not update');  

        }   

    
}

public function reset_password()
    {
       
             if($this->input->post('user_id'))
            $this->user_id = $this->input->post('user_id');
		
       
           if($this->input->post('old_password'))
            $this->old_password = $this->input->post('old_password');

             if($this->input->post('new_password'))
            $this->new_password = $this->input->post('new_password');

       

        
    
        if($this->old_password=='')
        
        {
            
           log_message('error', ''.__METHOD__);
             return jsonResponse('200', '0','Old password is Required');    
        }
    
    elseif($this->new_password=='')
    {   
           log_message('error', 'Invalid parameter or value passed in '.__METHOD__);
             return jsonResponse('200', '0','New Password is Required'); 
             
        }
	elseif($this->user_id=='')
    {   
           log_message('error', 'Invalid parameter or value passed in '.__METHOD__);
             return jsonResponse('200', '0','User id is Required'); 
             
        }
		//$old_password=sha1($this->old_password);
		$old_password = $this->old_password;
		
 $check_old_pass = $this->Apimodel->check_old_password($old_password,$this->user_id);
     
     if($check_old_pass){

	       //$new_password=sha1($this->new_password);
             $new_password = encrypt_string(trim($this->new_password)); 
		$formdata=array("password"=>$new_password);
		
	 $reset_success = $this->Apimodel->reset_password($formdata,$this->user_id);	  
		if($reset_success){
         log_message('error', ''.__METHOD__);   
		 return jsonResponse('200', '1','Password reset successfully.');	
		
		}else{
			log_message('error', ''.__METHOD__);
             return jsonResponse('200', '0','Password Not reset');  
		
		}
          
        }else{
                 log_message('error', ''.__METHOD__);
             return jsonResponse('200', '0','Old  Password Not Match');  

        }           
        
    }



	

public function Forgotpassword()
  {

    if($this->input->post('user_email'))
            $this->user_email = $this->input->post('user_email'); 
             
  if($this->user_email=='')
    
    {
            
           log_message('error', ''.__METHOD__);
           return jsonResponse('200', '0','Email is Required');    
        }
  $forgot = $this->Apimodel->Forgot_pass($this->user_email);
   /* print_r($login);
   exit; */
     if($forgot=='sent'){
      $response = array(
            "code"=>'200',
            "success"=>'1',
      "msg"=>"Password  sent successfully on your Email id",     
       );  
                                        
            echo json_encode($response);
     
          
        }
    elseif($forgot=='notemail'){
      
          $code = "200";
          $success = "0"; 
          $message = "This Email Id Not Registered";  
      
          return jsonResponse($code, $success, $message);
      
    }
    
    else{

         
          $code = "200";
          $success = "0"; 
          $message = "Password Not Sent";  
      
          return jsonResponse($code, $success, $message);

        }

  } 


/******************** Get Contact list **********************/

public function get_contact_list()
    {
		if($this->input->post('user_id'))
            $this->user_id = $this->input->post('user_id');

        if($this->user_id=='')
        
        {
            
           log_message('error', ''.__METHOD__);
             return jsonResponse('200', '0','user id  is Required');    
        }
		
        $contactlist = $this->Apimodel->getContactList($this->user_id);
     
     if($contactlist){

     foreach($contactlist as $contact)
	 { 
              $datass['first_name']= $contact->first_name;
               $datass['last_name']= $contact->last_name;
               $datass['user_id']= $contact->user_id;             
               $datass['phone']= $contact->phone;
               $datass['email']= $contact->email;
			   $data[] = $datass;
	 }	   
              
        
            $response = array(
            "success"=>'1',
            "code"=>'200',
            "message"=>"Contact list fetched",
           "contactList"=>$data,
               
           );  
                                              
            echo json_encode($response);
         
          
        }else{
                 log_message('error', ''.__METHOD__);
             return jsonResponse('200', '0','Data not found');  

        }           
        
    }


	//*********************** Send Location api ***********************//

public function sendLocation()
    { 
		if($this->input->post('sender_id'))
            $this->sender_id = $this->input->post('sender_id');
		if($this->input->post('receiver_id'))
            $this->receiver_id = $this->input->post('receiver_id');

        if($this->input->post('lat'))
            $this->lat = $this->input->post('lat');
		if($this->input->post('long'))
            $this->long = $this->input->post('long');

        if($this->input->post('location'))
            $this->location = $this->input->post('location');

        if($this->input->post('isSecure'))
            $this->isSecure = $this->input->post('isSecure');
		
    if($this->sender_id=='')
             {
           log_message('error', ''.__METHOD__);
             return jsonResponse('200', '0','sender id is Required');    
        }
    elseif($this->receiver_id=='')
          {   
             
             return jsonResponse('200', '0','receiver id is Required'); 
             
               }
    elseif($this->lat=='')
          {   
             
             return jsonResponse('200', '0','Latitute is Required'); 
             
               }
    elseif($this->long=='')
          {   
          
             return jsonResponse('200', '0','Longitute is Required'); 
             
           }
	elseif($this->location=='')
          {   
             return jsonResponse('200', '0','Location is Required'); 
             
           }

    
    else{
		$sender_id=$this->sender_id;
		if($this->isSecure=='1') 
        {

		$sender_id=$this->sender_id;
		$receiver_id=$this->receiver_id;
		$lat=encrypt_string($this->lat);
		$long=encrypt_string($this->long);
		$location=encrypt_string($this->location);
		$isSecure=$this->isSecure;
		
		$formData= array(
		               'sender_id'=>$sender_id,
		               'lat'=>$lat,
		               'long'=>$long,
		               'location'=>$location,
		               'isSecure'=>$isSecure,
		);
		
		//print_r($formData);die();
	
		
		
		}else{
			
			$formData=array(
						'sender_id'=>$this->sender_id,
						'lat'=>$this->lat,
						'long'=>$this->long,
						'location'=>$this->location,
						'isSecure'=>$this->isSecure
		);
		
		}

		 
    $sendLocation_id = $this->Apimodel->sendLocation($formData);
    if($sendLocation_id){
		             $receiver_ids=explode(",",$this->receiver_id);
		
		          $receiver_idcount = count($receiver_ids);
				  
				  
				  
		  
		for($i=0; $i<$receiver_idcount;$i++)
		{
		$receiveData=array(
						'receiver_id'=>$receiver_ids[$i],
						'send_location_id'=>$sendLocation_id,
						  'sender_id'=>$sender_id,
						
		);	
		
		$vegcountData=array(
						'receiveId'=>$receiver_ids[$i],
						'type'=>'location',						
		);
$vegcount = $this->Apimodel->StoreVegCount($table='veg_count',$vegcountData);
			
	   $receiveLocation = $this->Apimodel->receiveLocation($receiveData);
			
		}
            $response = array(
            "success"=>'1',
            "code"=>'200',
            "message"=>"Location sent successfully",
               
           );  
                                              
    echo json_encode($response);
         
          
   }else{
                 log_message('error', ''.__METHOD__);
             return jsonResponse('200', '0','error somthing went wrong');  

        }   

    }
}

//**************** Get Location ****************//
	
public function getSendedLocationList()
    {
		if($this->input->post('user_id'))
            $this->user_id = $this->input->post('user_id');

        if($this->user_id=='')
        
        {
            
           log_message('error', ''.__METHOD__);
             return jsonResponse('200', '0','user id  is Required');    
        }
		
        $getSendedLocationList = $this->Apimodel->getSendedLocationList($this->user_id);
     
     if($getSendedLocationList){

     foreach($getSendedLocationList as $row)
	 { 
              $datass['isSecure']= $row->isSecure;
			  if($datass['isSecure']=='1'){
			   $datass['recv_id']= $row->recv_id;  
			   $datass['location_id']= $row->location_id;
               $datass['sender_id']= $row->sender_id;             
               $datass['lat']= trim(decrypt_string($row->lat));
               $datass['long']= trim(decrypt_string($row->long));
			   $datass['location']= trim(decrypt_string($row->location));
			   //$datass['send_date']= $row->send_date;
			   $timestamp = strtotime($row->send_date);
			   $datass['send_date']= date('F j, Y | g:i', $timestamp);
			   
			   
			   
	$receive_location = $this->Apimodel->getSendeduserdetailsLocationList($datass['location_id']);  

foreach($receive_location as $row1)
	 {    

     $datass['recv_id']= $row1->recv_id; 

     $datass['first_name']= $row1->first_name;
		       $datass['last_name']= $row1->last_name;
               $datass['receiver_id']= $row1->user_id;             
               $datass['phone']= $row1->phone;
               $datass['email']= $row1->email;
			   $datass['receive_date']= $row1->receive_date;
             
		$newdatass[]=$datass;
	 }		
			   
				  
	}else{
		  
		   $datass['location_id']= $row->location_id;
               $datass['sender_id']= $row->sender_id;             
               $datass['lat']= trim(($row->lat));
               $datass['long']= trim(($row->long));
			   $datass['location']= trim(($row->location));
			   //$datass['send_date']= $row->send_date;
			   $timestamp = strtotime($row->send_date);
			   $datass['send_date']= date('F j, Y | g:i', $timestamp);
			   
	$receive_location = $this->Apimodel->getSendeduserdetailsLocationList($datass['location_id']);  

foreach($receive_location as $row1)
	 {    

         $datass['recv_id']= $row1->recv_id; 
           $datass['first_name']= $row1->first_name;
		       $datass['last_name']= $row1->last_name;
               $datass['receiver_id']= $row1->user_id;             
               $datass['phone']= $row1->phone;
               $datass['email']= $row1->email;
			   //$datass['receive_date']= $row1->receive_date;
			   $timestamp = strtotime($row1->receive_date);
			   $datass['receive_date']= date('F j, Y | g:i', $timestamp);
             
		$newdatass[]=$datass;
	 }		
		
	} 

	
			 
	 }	 

	 
    function date_compare($a, $b)
{
    $t1 = $a['recv_id'];
    $t2 = $b['recv_id'];
    return $t1 - $t2;
}    
usort($newdatass, 'date_compare');  

//print_r($newdatass);	




$newdatass=array_reverse($newdatass);

//echo '<pre>';
//print_r($newdatass);
        
            $response = array(
            "success"=>'1',
            "code"=>'200',
            "message"=>"Send location list fetched",
           "sendedLocationuserList"=>$newdatass,
               
           );  
                                              
          echo json_encode($response);
         
          
	 }else{
                 log_message('error', ''.__METHOD__);
             return jsonResponse('200', '0','Data not found');  

        }           
        
    }
	
	
	//**************** Get Location ****************//
	
public function ReceiveLocationbyuser()
    {
		if($this->input->post('user_id'))
            $this->user_id = $this->input->post('user_id');

        if($this->user_id=='')
        
        {
            
           log_message('error', ''.__METHOD__);
             return jsonResponse('200', '0','user id  is Required');    
        }
		
        $GetReceiveLocation = $this->Apimodel->GetReceiveLocation($this->user_id);
     
     if($GetReceiveLocation){

     foreach($GetReceiveLocation as $row)
	 { 
              $datass['isSecure']= $row->isSecure;
			  if($datass['isSecure']=='1'){
				  
			   $datass['location_id']= $row->location_id;
               $datass['sender_id']= $row->sender_id;             
               $datass['lat']= trim(decrypt_string($row->lat));
               $datass['long']= trim(decrypt_string($row->long));
			   $datass['location']= trim(decrypt_string($row->location));
			   $datass['send_date']= $row->send_date;
	           $datass['first_name']= $row->first_name;
		       $datass['last_name']= $row->last_name;
               $datass['receiver_id']= $row->user_id;             
               $datass['phone']= $row->phone;
               $datass['email']= $row->email;
			   $datass['recv_id']= $row->recv_id;
			   //$datass['receive_date']= $row->receive_date;
			   $timestamp = strtotime($row->receive_date);
			   $datass['receive_date']= date('F j, Y | g:i', $timestamp);
             
		$newdatass[]=$datass;
		
			   
				  
	} else{
		
		$datass['location_id']= $row->location_id;
               $datass['sender_id']= $row->sender_id;             
               $datass['lat']= trim(($row->lat));
               $datass['long']= trim(($row->long));
			   $datass['location']= trim(($row->location));
			   $datass['send_date']= $row->send_date;
	           $datass['first_name']= $row->first_name;
		       $datass['last_name']= $row->last_name;
               $datass['receiver_id']= $row->user_id;             
               $datass['phone']= $row->phone;
               $datass['email']= $row->email;
			   $datass['recv_id']= $row->recv_id;
			   //$datass['receive_date']= $row->receive_date;
			   $timestamp = strtotime($row->receive_date);
			   $datass['receive_date']= date('F j, Y | g:i', $timestamp);
             
		$newdatass[]=$datass;
		
		
	}

	
			 
	 }	
	 
function date_compare($a, $b)
{
    $t1 = strtotime($a['receive_date']);
    $t2 = strtotime($b['receive_date']);
    return $t1 - $t2;
}    
usort($newdatass, 'date_compare');	 
	 
	 
            $response = array(
            "success"=>'1',
            "code"=>'200',
            "message"=>"Receive Location list fetched",
           "ReceiveLocationuserList"=>$newdatass,
               
           );  
                                              
            echo json_encode($response);
         
          
	 }else{
                 log_message('error', ''.__METHOD__);
             return jsonResponse('200', '0','Data not found');  

        }           
        
    }

	
	
	
public function sendMail()
    { 
		if($this->input->post('sender_id'))
            $this->sender_id = $this->input->post('sender_id');
		if($this->input->post('receiver_id'))
            $this->receiver_id = $this->input->post('receiver_id');

        if($this->input->post('subject'))
            $this->subject = $this->input->post('subject');
		if($this->input->post('content'))
            $this->content = $this->input->post('content');

        if($this->input->post('isSecure'))
            $this->isSecure = $this->input->post('isSecure');

        
		
    if($this->sender_id=='')
             {
           log_message('error', ''.__METHOD__);
             return jsonResponse('200', '0','sender id is Required');    
        }
    elseif($this->receiver_id=='')
          {   
             
             return jsonResponse('200', '0','receiver id is Required'); 
             
               }
    elseif($this->subject=='')
          {   
             
             return jsonResponse('200', '0','Subject is Required'); 
             
               }
    elseif($this->content=='')
          {   
          
             return jsonResponse('200', '0','Content is Required'); 
             
           }
    
    else{
		if($this->isSecure=='1') 
        {

		$sender_id=$this->sender_id;
		$receiver_id=$this->receiver_id;
		$subject=encrypt_string($this->subject);
		$content=encrypt_string($this->content);
		
		$isSecure=$this->isSecure;
		
		$formData= array(
		               'sender_id'=>$sender_id,
		               'subject'=>$subject,
		               'content'=>$content,
		               'isSecure'=>$isSecure,
		);
		
		//print_r($formData);die();
	
		
		
		}else{
			
			$formData=array(
						'sender_id'=>$this->sender_id,
						'subject'=>$this->subject,
						'content'=>$this->content,
						'isSecure'=>'0'
		);
		
		}

		 
    $sendMail_id = $this->Apimodel->sendMail($formData);
    if($sendMail_id){
		             $receiver_ids=explode(",",$this->receiver_id);
		
		          $receiver_idcount = count($receiver_ids);
				  
				  
				  
		  
		for($i=0; $i<$receiver_idcount;$i++)
		{
			
			 $profile = $this->Apimodel->get_profile($receiver_ids[$i]);
     
               $first_name= $profile[0]->first_name;
               $last_name= $profile[0]->last_name;
               $email= $profile[0]->email;
			   
			   $content = $this->content;
			   
			   
			   //$message = 'Hi '.$first_name.' ,'.'</br>'.$content;
			    $message = 'Dear '.$first_name.',';
                $message .= '<p>'.$content.'</p>';
			   
			   
			   
			 $subject = $this->subject;

          $header = "From:Comunication<sales@alobhatechnologies.com>\r\n";

         $header .= "MIME-Version: 1.0\r\n";
         $header .= "Content-type: text/html\r\n";

         $mail = mail($email,$subject,$message,$header);
		
		$receiveData=array(
						'receiver_user_id'=>$receiver_ids[$i],
						'mail_id'=>$sendMail_id,
						'receiver_mail'=>$email,
						'sender_id'=>$this->sender_id,
		);	
		
		//print_r($receiveData);
		$vegcountData=array(
						'receiveId'=>$receiver_ids[$i],
						'type'=>'email',						
		);
$vegcount = $this->Apimodel->StoreVegCount($table='veg_count',$vegcountData);

		
	   $receiveLocation = $this->Apimodel->receiverMail($receiveData);
			
		}
            $response = array(
            "success"=>'1',
            "code"=>'200',
            "message"=>"Mail sent successfully",
               
           );  
                                              
    echo json_encode($response);
         
          
   }else{
                 log_message('error', ''.__METHOD__);
             return jsonResponse('200', '0','error somthing went wrong');  

        }   

    }
}

	
	
	
  public function ReceiveMail()
    {
		if($this->input->post('user_id'))
            $this->user_id = $this->input->post('user_id');

        if($this->user_id=='')
        
        {
            
           log_message('error', ''.__METHOD__);
             return jsonResponse('200', '0','user id  is Required');    
        }
		$newdatass =array();
		$newdatass1=array();
		
		
		
        $GetReceiveEmail = $this->Apimodel->GetReceiveEmail($this->user_id);
		
	//print_r($GetReceiveEmail);
		
    
     

     foreach($GetReceiveEmail as $row)
	 { 
              $datass['isSecure']= $row->isSecure;
			  if($datass['isSecure']=='1'){
			   
			   $datass['mail_type']= 'Receive Mail';  
			   $datass['receive_mail_id']= $row->receive_mail_id;
			   $datass['mail_id']= $row->mail_id;
               $datass['sender_id']= $row->sender_id;             
               $datass['subject']= trim(decrypt_string($row->subject));
               $datass['content']= trim(decrypt_string($row->content));
	           $datass['name']= $row->first_name;
		       $datass['last_name']= $row->last_name;
               $datass['receiver_id']= $row->receiver_user_id;             
               $datass['phone']= $row->phone;
               $datass['email']= $row->email;
			   $timestamp = strtotime($row->receive_mail_date);
			   $datass['receive_mail_date']= date('F j, Y | g:i', $timestamp);
			  
             
		$newdatass[]=$datass;
		
			   
				  
	} else{
		
		
		       $datass['mail_type']= 'Receive Mail';
			   $datass['receive_mail_id']= $row->receive_mail_id;
		       $datass['mail_id']= $row->mail_id;
               $datass['sender_id']= $row->sender_id;             
               $datass['subject']= trim(($row->subject));
               $datass['content']= trim(($row->content));
	           $datass['name']= $row->first_name;
		       $datass['last_name']= $row->last_name;
               $datass['receiver_id']= $row->receiver_user_id;             
               $datass['phone']= $row->phone;
               $datass['email']= $row->email;
			  // $datass['receive_mail_date']= $row->receive_mail_date;
               $timestamp = strtotime($row->receive_mail_date);
			   $datass['receive_mail_date']= date('F j, Y | g:i', $timestamp);
		$newdatass[]=$datass;
		
		
	}
	 }
	 
	//print_r($newdatass); exit;
	 
	 
	
//******************** Sended mail List *******************//

$GetSendEmail = $this->Apimodel->getSendedEmailList($this->user_id);

//print_r($GetSendEmail) ;exit;
     
    

     foreach($GetSendEmail as $row1)
	 { 
              $datass1['isSecure']= $row1->isSecure;
			  if($datass1['isSecure']=='1'){
			$datass1['receive_mail_id']= $row1->receive_mail_id;
			   $datass1['mail_type']= 'Send Mail';  
			   $datass1['mail_id']= $row1->mail_id;
               $datass1['sender_id']= $row1->sender_id;             
               $datass1['subject']= trim(decrypt_string($row1->subject));
               $datass1['content']= trim(decrypt_string($row1->content));
$send_mail_details = $this->Apimodel->getSendeduserdetailsEmailList11($datass1['mail_id']);

//print_r($send_mail_details);  			   
			   
foreach($send_mail_details as $rowss)
	 {         
	        $datass1['receive_mail_id']= $rowss->receive_mail_id;
	          $datass1['name']= $rowss->first_name;
		       $datass1['last_name']= $rowss->last_name;
               $datass1['receiver_id']= $rowss->user_id;             
               $datass1['phone']= $rowss->phone;
               $datass1['email']= $rowss->email;
			  $timestamp = strtotime($rowss->receive_mail_date);
			   $datass1['receive_mail_date']= date('F j, Y | g:i', $timestamp);
             
		$newdatass1[]=$datass1;
		// print_r($newdatass1) ;
		
	 }	   
			   
	} else{
		$datass1['receive_mail_id']= $row1->receive_mail_id;
		       $datass1['mail_type']= 'Send Mail'; 
		       $datass1['mail_id']= $row1->mail_id;
               $datass1['sender_id']= $row1->sender_id;             
               $datass1['subject']= trim(($row1->subject));
               $datass1['content']= trim(($row1->content));
	$send_mail_details = $this->Apimodel->getSendeduserdetailsEmailList11($datass1['mail_id']); 

	
	 foreach($send_mail_details as $rowss)
	 {    
         $datass1['receive_mail_id']= $rowss->receive_mail_id;
	         $datass1['name']= $rowss->first_name;
		       $datass1['last_name']= $rowss->last_name;
               $datass1['receiver_id']= $rowss->user_id;             
               $datass1['phone']= $rowss->phone;
               $datass1['email']= $rowss->email;
			  $timestamp = strtotime($rowss->receive_mail_date);
			   $datass1['receive_mail_date']= date('F j, Y | g:i', $timestamp);
             
		$newdatass1[]=$datass1;
		
		 //print_r($newdatass1) ;
	 }	 
		
		
		
	}	
	 }
	
//exit;
	
	 
	 
	  //print_r($newdatass1); exit;
	$mail_list = array_merge($newdatass,$newdatass1);
	//print_r($mail_list);exit;
	 
	 
	foreach($mail_list as $mail_lists)
	 {        
$datass13['isSecure']= $mail_lists['isSecure'];
$datass13['receive_mail_id']= $mail_lists['receive_mail_id'];
$datass13['mail_type']= $mail_lists['mail_type'];
$datass13['mail_id']= $mail_lists['mail_id'];             
$datass13['sender_id']= $mail_lists['sender_id'];
$datass13['subject']= $mail_lists['subject'];
$datass13['receive_mail_date']= $mail_lists['receive_mail_date'];
$datass13['content']= $mail_lists['content'];

$datass13['name']= $mail_lists['name'];
$datass13['last_name']= $mail_lists['last_name'];        
$datass13['receiver_id']= $mail_lists['receiver_id'];
$datass13['phone']= $mail_lists['phone'];
$datass13['email']= $mail_lists['email'];


             
		$finalmaillist[]=$datass13;
	 }	 
function date_compare($a, $b)
{
    $t1 = $a['receive_mail_id'];
    $t2 = $b['receive_mail_id'];
    return $t1 - $t2;
}    
usort($finalmaillist, 'date_compare');
	 
$finalmaillist=array_reverse($finalmaillist);	 
	 
	 
	if($GetReceiveEmail OR $GetSendEmail){ 
	 
            $response = array(
            "success"=>'1',
            "code"=>'200',
            "message"=>"Email list fetched",
          // "emailList"=>$newdatass,
		   "AllmailList"=>$finalmaillist,
               
           );  
                                              
            echo json_encode($response);
         
          
	  }else{
                 log_message('error', ''.__METHOD__);
             return jsonResponse('200', '0','Data not found');  

        }           
         
    }

	
  
public function sendFile()
    { 
		if($this->input->post('sender_id'))
            $this->sender_id = $this->input->post('sender_id');
		if($this->input->post('receiver_id'))
            $this->receiver_id = $this->input->post('receiver_id');

        if($this->input->post('type'))
            $this->type = $this->input->post('type');
		

        if($this->input->post('isSecure'))
            $this->isSecure = $this->input->post('isSecure');

        
		
    if($this->sender_id=='')
             {
           log_message('error', ''.__METHOD__);
             return jsonResponse('200', '0','sender id is Required');    
        }
    elseif($this->receiver_id=='')
          {   
             
             return jsonResponse('200', '0','receiver id is Required'); 
             
               }
    elseif($this->type=='')
          {   
             
             return jsonResponse('200', '0','type is Required'); 
             
               }
			   
elseif($_FILES['file']['name']=='')
          {   
             
             return jsonResponse('200', '0','file is Required'); 
             
               }
			   
			   

    else{
		
		$pic = time().$_FILES['file']['name']; 
		$pic = str_replace(" ", "_", $pic);
        $pic_loc = $_FILES['file']['tmp_name'];
          $folder=BASEPATH."/../assets/files/";
        move_uploaded_file($pic_loc,$folder.$pic);
		
		
		
		if($this->isSecure=='1') 
        {

		$sender_id=$this->sender_id;
		$receiver_id=$this->receiver_id;
		$file=encrypt_string($pic);
		
		
		$isSecure=$this->isSecure;
		
		$formData= array(
		               'sender_id'=>$sender_id,
		               'file'=>$file,
					   'type'=>$this->type,
		               'isSecure'=>$isSecure,
		);
		
		//print_r($formData);die();
	
		
		
		}else{
			
			$formData=array(
						'sender_id'=>$this->sender_id,
						'file'=>$pic,
						'type'=>$this->type,
						'isSecure'=>'0'
		);
		
		}

		 
    $sendFile = $this->Apimodel->sendFile($formData);
    if($sendFile){
		//echo $sendFile;exit;
		
		             $receiver_ids=explode(",",$this->receiver_id);
		            
		          $receiver_idcount = count($receiver_ids);
				  
				  
				  
		  
		for($i=0; $i<$receiver_idcount;$i++)
		{
			
			 //$profile = $this->Apimodel->receiveFile($receiver_ids[$i]);
     
               /* $first_name= $profile[0]->first_name;
               $last_name= $profile[0]->last_name;
               $email= $profile[0]->email;
			   
			   $content = $this->content;
			   
			   
			   //$message = 'Hi '.$first_name.' ,'.'</br>'.$content;
			    $message = 'Dear '.$first_name.',';
                $message .= '<p>'.$content.'</p>';
			   
			   
			   
			 $subject = $this->subject;

          $header = "From:Comunication<sales@alobhatechnologies.com>\r\n";

         $header .= "MIME-Version: 1.0\r\n";
         $header .= "Content-type: text/html\r\n";

         $mail = mail($email,$subject,$message,$header); */
		
		$receiveData=array(
						'receiver_id'=>$receiver_ids[$i],
						'file_id'=>$sendFile,
						'sender_id'=>$this->sender_id,
		);	
		
		$vegcountData=array(
						'receiveId'=>$receiver_ids[$i],
						'type'=>$this->type,
						
		);
		
		
		//print_r($receiveData);
$vegcount = $this->Apimodel->StoreVegCount($table='veg_count',$vegcountData);	
	   $receiveLocation = $this->Apimodel->receiveFile($receiveData);
			
		}
            $response = array(
            "success"=>'1',
            "code"=>'200',
            "message"=>"File sent successfully",
               
           );  
                                              
    echo json_encode($response);
         
          
   }else{
                 log_message('error', ''.__METHOD__);
             return jsonResponse('200', '0','error somthing went wrong');  

        }   

    }
}

	
  
  public function ReceiveFiles()
    {
		
            $this->user_id = $this->input->post('user_id');
            $requestType = $this->input->post('type');
		
		
		
        if($this->user_id=='')
        
        {
            
           log_message('error', ''.__METHOD__);
             return jsonResponse('200', '0','user id  is Required');    
        }
		
        $GetAllReceiveFiles = $this->Apimodel->GetAllReceiveFiles($this->user_id);
     
    //echo '<pre>';
//print_r($GetAllReceiveFiles);
     foreach($GetAllReceiveFiles as $row)
	 { 
              $datass['isSecure']= $row->isSecure;
			   $type = $row->type;	
			  if($type==$requestType){
			  if($datass['isSecure']=='1'){
				  
			   $datass['receive_file_id']= $row->receive_file_id;
			   $datass['sender_file_id']= $row->sender_file_id;
               $datass['sender_id']= $row->sender_id;
               $datass['type']= $row->type;			   
               $datass['file']= base_url().'assets/files/'.trim(decrypt_string($row->file));
	           $datass['sender_name']= $row->first_name;
		       $datass['sender_last_name']= $row->last_name;
               $datass['receiver_id']= $row->receiver_id;             
               $datass['sender_phone']= $row->phone;
               $datass['sender_email']= $row->email;
			   $timestamp = strtotime($row->receive_date);
			   $datass['receive_date']= date('F j, Y | g:i',$timestamp);//$row->receive_date;
             
		$newdatass[]=$datass;
		
			   
				  
	} else{
		
		$datass['receive_file_id']= $row->receive_file_id;
               $datass['sender_file_id']= $row->sender_file_id;
               $datass['sender_id']= $row->sender_id;
               $datass['type']= $row->type;			   
              $datass['file']= base_url().'assets/files/'.trim(($row->file));
	           $datass['sender_name']= $row->first_name;
		       $datass['sender_last_name']= $row->last_name;
               $datass['receiver_id']= $row->receiver_id;             
               $datass['sender_phone']= $row->phone;
               $datass['sender_email']= $row->email;
			    $timestamp = strtotime($row->receive_date);
			   $datass['receive_date']=  date('F j, Y | g:i', $timestamp);
             
		$newdatass[]=$datass;
		
		
	}

	 }
			 
	 }	

 /* function date_compare($a, $b)
{
    $t1 = $a['sender_file_id'];
    $t2 = $b['sender_file_id'];
    return $t1 - $t2;
}    
usort($newdatass, 'date_compare');   */

//print_r($newdatass);	




$newdatass=array_reverse($newdatass);
//echo '<pre>';
//print_r($newdatass); 
	 
	 
 $getSendedFileList = $this->Apimodel->getSendedFileList($this->user_id);
  
//print_r($getSendedFileList); exit;
     

     foreach($getSendedFileList as $row1)
	 { 
	     
              $datass1['isSecure']= $row1->isSecure;
			  
			  $types = $row1->type;	
			  if($types==$requestType){
			  
			  if($datass1['isSecure']=='1'){
				  
			   $datass1['sender_file_id']= $row1->sender_file_id;
               $datass1['sender_id']= $row1->sender_id;
               $datass1['type']= $row1->type;			   
               $datass1['file']= base_url().'assets/files/'.trim(decrypt_string($row1->file));
			   $timestamp = strtotime($row1->send_date);
			   $datass1['send_date']= date('F j, Y | g:i', $timestamp);
			    
			   
	$receive_file = $this->Apimodel->getSendeduserdetailsFileList($datass1['sender_file_id']);  

foreach($receive_file as $row2)
	 {         $datass1['receive_file_id']= $row2->receive_file_id;
	           $datass1['receiver_name']= $row2->first_name;
		       $datass1['receiver_last_name']= $row2->last_name;
               $datass1['receiver_id']= $row2->receiver_id;             
               $datass1['receiver_phone']= $row2->phone;
               $datass1['receiver_email']= $row2->email;
			          
			    $timestamp = strtotime($row2->receive_date);
			   $datass1['receive_date']= date('F j, Y | g:i',$timestamp);//$row2->receive_date;
             
			 
			 
		$newdatass1[]=$datass1;
	 }		
	
	
				  
	}else{
		
		$datass1['sender_file_id']= $row1->sender_file_id;
               $datass1['sender_id']= $row1->sender_id;
               $datass1['type']= $row1->type;			   
               $datass1['file']= base_url().'assets/files/'.trim(($row1->file));
			   //echo $row1->send_date;die();
			   
			   $timestamp1 = strtotime($row1->send_date);
			   $datass1['send_date']= date('F j, Y | g:i', $timestamp1);//$row1->send_date;
	$receive_file = $this->Apimodel->getSendeduserdetailsFileList($datass1['sender_file_id']);  

foreach($receive_file as $row2)
	 {  
               $datass1['receive_file_id']= $row2->receive_file_id;
	           $datass1['receiver_name']= $row2->first_name;
		       $datass1['receiver_last_name']= $row2->last_name;
               $datass1['receiver_id']= $row2->receiver_id;             
               $datass1['receiver_phone']= $row2->phone;
               $datass1['receiver_email']= $row2->email;
			   $timestamp1 = strtotime($row2->receive_date);
			   $datass1['receive_date']= date('F j, Y | g:i',$timestamp1);//$row2->receive_date;
             
		$newdatass1[]=$datass1;
	 }		
		
	} 

	
			 
	}	}  

function date_compare($a, $b)
{
    $t1 = $a['receive_file_id'];
    $t2 = $b['receive_file_id'];
    return $t1 - $t2;
}    
usort($newdatass1, 'date_compare');  

//print_r($newdatass);	




$newdatass1=array_reverse($newdatass1);
	
  
	 
	 $emty = array();
	 
            $response = array(
            "success"=>'1',
            "code"=>'200',
            "message"=>"File list fetched",
           "receivedFilesList"=>$newdatass?$newdatass:$emty,
		   "sendFilesList"=>$newdatass1?$newdatass1:$emty,
               
           );  
                                              
            echo json_encode($response);
         
          
	      
        
    }

//************************* Craete group chate functionality*******//

public function create_group()
    { 
		
       $group_admin_id = $this->input->post('user_id');
	   $group_name = $this->input->post('group_name');
		
		
    if($group_admin_id=='')
             {
           log_message('error', ''.__METHOD__);
             return jsonResponse('200', '0','user id is Required');    
        }
    elseif($group_name=='')
          {   
             
             return jsonResponse('200', '0','group name is Required'); 
             
               }
    
    
    else{
			$time=time();				
			$formData=array(
						'group_admin_id'=>$group_admin_id,
						'group_name'=>$group_name,
						'time'=>$time,
						
		);
		

}
	}




	
//******************* Get Group message *****************//	
public function get_Chatlist()
    { 
		
       $group_id = $this->input->post('group_id');
	  
    if($group_id=='')
             {
           log_message('error', ''.__METHOD__);
             return jsonResponse('200', '0','group id is Required');    
        }
   		 
    $data = $this->Apimodel->Getchat_list($group_id);
	
foreach($data as $row)	
	{ 
	          
		$database['sender_name'] = $row->first_name;
		$database['reply'] = $row->reply;
		$database['sender_id'] = $row->user_id_fk;
		$database['group_id'] = $row->c_id_fk;
		$database['user_group'] = $row->user_two;
		$database['time'] = date('G.i', $row->time);
              $date = date('d-m-Y', $row->time);
		      $timestamp = strtotime($date);
			  $database['date']= date('F j, Y', $timestamp);
		
	
		$newarray[]=$database;
		
	}
	
		
/* $reversedata = array_reverse($newarray);
	 
$details = $this->Apimodel->unique_multidim_array($reversedata,'c_id_fk');	

$last_conversation=array();
			foreach($details as $row)
			{
			$datas['receive_messge_id']=$row['receive_messge_id'];	
			$datas['receive_user_id']=$row['receive_user_id'];
			$datas['group_id']=$row['group_id'];
			$datas['reply']=$row['reply'];
			$datas['sender_id']=$row['sender_id'];					
			$datas['c_id_fk']=$row['c_id_fk'];
			$datas['time']=$row['time'];
			$datas['date']=$row['date'];
			$datas['user_group']=$row['user_group'];
			
			
$datas['group_user']= $this->Apimodel->getuser_name($datas['user_group']);
	
         $last_conversation[]=$datas;
} */



 
//echo '<pre>';
   //print_r($newarray);
  // exit;
 
            $response = array(
            "success"=>'1',
            "code"=>'200',
            "message"=>"chat message get successfully",
			"chat_list"=>$newarray,
               
           );  
                                              
    echo json_encode($response);
         
          
   

    }	
	






	
public function get_receivemessage_list()
    { 
		
       $user_id = $this->input->post('user_id');
	  
		
		
    if($user_id=='')
             {
           log_message('error', ''.__METHOD__);
             return jsonResponse('200', '0','user id is Required');    
        }
   		 
    $data = $this->Apimodel->Getrecievemessage_send_message($user_id);
	//print_r($data);
foreach($data as $row)	
	{ 
	          
		$database['cr_id'] = $row->cr_id;
		$database['receive_messge_id'] = $row->receive_messge_id;
		$database['receive_user_id'] = $row->receive_user_id;
		$database['group_id'] = $row->group_id;
		$database['receiveId'] = $row->receiveId;
		$database['reply'] = $row->reply;
		$database['sender_id'] = $row->user_id_fk;
		$database['c_id_fk'] = $row->c_id_fk;
		$database['user_group'] = $row->user_two;
		$database['time'] = date('G.i', $row->time);
        $date = date('d-m-Y', $row->time);
		$timestamp = strtotime($date);
	    $database['date']= date('F j, Y', $timestamp);
		
		
		
	
		$newarray[]=$database;
		
	}
	
		
$reversedata = array_reverse($newarray);
	 
$details = $this->Apimodel->unique_multidim_array($reversedata,'c_id_fk');	

$last_conversation=array();
			foreach($details as $row)
			{
			$datas['cr_id']=$row['cr_id'];		
			$datas['receive_messge_id']=$row['receive_messge_id'];	
			$datas['receiveId']=$row['receiveId'];	
			$datas['receive_user_id']=$row['receive_user_id'];
			$datas['group_id']=$row['group_id'];
			$datas['reply']=$row['reply'];
			$datas['sender_id']=$row['sender_id'];
$datas['sender_name']= $this->Apimodel->getuser_name($datas['sender_id']);
$datas['Receive_name']= $this->Apimodel->getuser_name($datas['receiveId']);	

			
			$datas['c_id_fk']=$row['c_id_fk'];
			$datas['time']=$row['time'];
			$datas['date']=$row['date'];
			$datas['user_group']=$row['user_group'];
			
			
$datas['group_user']= $this->Apimodel->getuser_name($datas['user_group']);

if($datas['Receive_name']!=$datas['sender_name']){
$datas['Receive_name']= $this->Apimodel->getuser_name($datas['receiveId']);			
}else{
	
	$datas['Receive_name']= $datas['group_user']?$datas['group_user']:'';
}


	
         $last_conversation[]=$datas;
}



 
//echo '<pre>';
   //print_r($last_conversation);
  // exit;
 
            $response = array(
            "success"=>'1',
            "code"=>'200',
            "message"=>"message get successfully",
			"message_list"=>$last_conversation,
               
           );  
                                              
    echo json_encode($response);
         
          
   

    }	
	
//*******************************************************//

public function Creating_Conversation()
    { 
	   
            $user_one = $this->input->post('user_one');
            $user_two = $this->input->post('user_two');
		    $reply = $this->input->post('reply');
       if($user_one=='')
        {
             return jsonResponse('200', '0','User one id  is Required');    
        }elseif($user_two=='')
        {
             return jsonResponse('200', '0','User two id  is Required');    
        }
		elseif($reply=='')
        {
             return jsonResponse('200', '0','text is Required');    
        }
	
         $user_twos=explode(",",$user_two);
		            
		          $user_twoscount = count($user_twos);
				  
				  
				  
		  
		for($i=0; $i<$user_twoscount;$i++)
		{  

$vegcountData=array(
						'receiveId'=>$user_twos[$i],
						'type'=>'message',						
		);
$vegcount = $this->Apimodel->StoreVegCount($table='veg_count',$vegcountData);
		}   
		         
  $c_id = $this->Apimodel->Creating_Conversation($user_one,$user_two);
     
     if($c_id){

	$data = array(
                "user_id_fk" => $user_one ,
                "reply" => $reply,
				"ip" => $_SERVER['REMOTE_ADDR'],
                "time" => time(), 
                "c_id_fk" => $c_id,				
                );
	 
 $data = $this->Apimodel->conversation_reply($data);
               
		} 		
		//}
		


for($i=0; $i<$user_twoscount;$i++)
		{
			
$this->Apimodel->user_receive_message($c_id,$user_twos[$i],$user_one);

		}


		
            $response = array(
            "success"=>'1',
            "code"=>'200',
			"message"=>'message post successfully',
            "c_id"=>$c_id,
           
               
           );  
                                              
            echo json_encode($response);
         
          
       

    
}






public function post_chat()
    { 
	   
            $sender_id = $this->input->post('sender_id');
            $group_id = $this->input->post('group_id');
			$recever_id = $this->input->post('recever_id');
			$reply = $this->input->post('reply');
			
		    
       if($sender_id=='')
        {
             return jsonResponse('200', '0','Sendor id  is Required');    
        }elseif($group_id=='')
        {
             return jsonResponse('200', '0','Group id  is Required');    
        }
		elseif($reply=='')
        {
             return jsonResponse('200', '0','reply text is Required');    
        }
		
	if($recever_id==''){





	
	$data = array(
                "user_id_fk" => $sender_id ,
				
                "reply" => $reply,
				"ip" => $_SERVER['REMOTE_ADDR'],
                "time" => time(), 
                "c_id_fk" => $group_id,				
                );
	}else{
		
		$data = array(
                "user_id_fk" => $sender_id ,
				"receiveId" => $recever_id ,
                "reply" => $reply,
				"ip" => $_SERVER['REMOTE_ADDR'],
                "time" => time(), 
                "c_id_fk" => $group_id,				
                );
		
	}
	
	
$user_twos=explode(",",$recever_id);

$user_twoscount = count($user_twos);
for($i=0; $i<$user_twoscount;$i++)
{  
$vegcountData=array(
'receiveId'=>$user_twos[$i],
'type'=>'message',						
);
$vegcount = $this->Apimodel->StoreVegCount($table='veg_count',$vegcountData);
}   	

	
	
 $data = $this->Apimodel->conversation_reply($data);
               
		 		
            $response = array(
            "success"=>'1',
            "code"=>'200',
			"message"=>'message post successfully',
            "group_id"=>$group_id,
           
               
           );  
                                              
            echo json_encode($response);
         
 
}


	
	
 
  
  
 //**************** Chat functionality ***************//



public function get_conversation()
    {

 $c_id = $this->input->post('c_id');

if($c_id=='')
        {
         return jsonResponse('200', '0','chat id is Required');    
        }else{
			
$aa = $this->db->query("SELECT R.cr_id,R.time,R.reply,U.user_id,U.first_name,U.email FROM users U, conversation_reply R WHERE R.user_id_fk=U.user_id and R.c_id_fk='$c_id' ORDER BY R.cr_id ASC LIMIT 20");
     $num_rows = $aa->num_rows();
      
        $data =$aa->result();
		$conversation=array();
			foreach($data as $row)
			{
			$datas['cr_id']=$row->cr_id;
			//$datas['time']=$row->time;
			
			  $datas['time'] = date('G.i.s', $row->time);
              $date = date('d-m-Y', $row->time);
		      $timestamp = strtotime($date);
			  $datas['date']= date('F j, Y', $timestamp);
			
			
			
			$datas['reply']=$row->reply;
			$datas['user_id']=$row->user_id;
			$datas['username']=$row->first_name;
			$datas['email']=$row->email;
			
         $conversation[]=$datas;
}
     $emty = array();
	 
            $response = array(
            "success"=>'1',
            "code"=>'200',
            "message"=>"conversation list fetched",
           "conversation_list"=>$conversation?$conversation:$emty,
           );  
                                              
            echo json_encode($response);	
		
		
		
	}


}	
 
 
 
 
 
  
  
  
 public function getLastConversation()
    {
       $user_one = $this->input->post('user_one');	   
       if($user_one=='')
        {
             return jsonResponse('200', '0','User one id  is Required');    
        }
		
    
$aa = $this->db->query("SELECT  R.reply,R.time , U.user_id,C.c_id,U.first_name,U.email 
FROM users U,conversation C, conversation_reply R
WHERE 
(CASE

WHEN C.user_one = '$user_one'
THEN C.user_two = U.user_id
WHEN C.user_two = '$user_one'
THEN C.user_one= U.user_id
END
)
AND 
C.c_id=R.c_id_fk
AND
(C.user_one ='$user_one' OR C.user_two ='$user_one') ORDER BY C.c_id DESC");

    $dda =$aa->result();
    $newarray=array();	
	foreach($dda as $row)	
	{ 
	          $database['time'] = date('G.i.s', $row->time);
              $date = date('d-m-Y', $row->time);
		      $timestamp = strtotime($date);
			  $database['date']= date('F j, Y', $timestamp);
		
		
		
		$database['user_id'] = $row->user_id;
		$database['reply'] = $row->reply;
		$database['c_id'] = $row->c_id;
		$database['first_name'] = $row->first_name;
		
		$newarray[]=$database;
		
	}
	
//********* Remove duplicate value and create new array **********//

$reversedata = array_reverse($newarray);
	 
$details = $this->Apimodel->unique_multidim_array($reversedata,'user_id');		 


$last_conversation=array();
			foreach($details as $row)
			{
			$datas['c_id']=$row['c_id'];	
			$datas['date']=$row['date'];
			$datas['time']=$row['time'];
			$datas['reply']=$row['reply'];
			$datas['user_id']=$row['user_id'];
			$datas['username']=$row['first_name'];
			
			
         $last_conversation[]=$datas;
}
     $emty = array();
	 
            $response = array(
            "success"=>'1',
            "code"=>'200',
            "message"=>"conversation list fetched",
           "last_conversation"=>$last_conversation?$last_conversation:$emty,
           );  
                                              
            echo json_encode($response);	
	
}
 
  

/******************** Get veg count **********************/

public function get_veg_count()
    {
		if($this->input->post('user_id'))
            $this->user_id = $this->input->post('user_id');

        if($this->user_id=='')
        
        {
            
           log_message('error', ''.__METHOD__);
             return jsonResponse('200', '0','user id is Required');    
        }
		 //$GetReceiveEmail = 0;
		
		$where=array(
						'receiveId'=>$this->user_id,
						'type'=>'email',						
		);
		
		
		
		
		 $GetReceiveEmail = $this->Apimodel->Getvegcount($where);		
		if($GetReceiveEmail!=''){
		 $GetReceiveEmailCount = $GetReceiveEmail;
		}else{
			$GetReceiveEmailCount=0;
			
		}
		 
		 
		/************** Receive Count email *****************/
		$where=array(
						'receiveId'=>$this->user_id,
						'type'=>'location',						
		);
		 $GetReceiveLocation = $this->Apimodel->Getvegcount($where);//exit;	
		 
		 if($GetReceiveLocation!=''){
		 $GetReceiveLocationCount = $GetReceiveLocation;
		}else{
			$GetReceiveLocationCount=0;
			
		}
		
		/************** Receive Count location *****************/
		
		
		
		
	/************** Receive Count message *****************/
		$where=array(
						'receiveId'=>$this->user_id,
						'type'=>'message',						
		);
		 $GetlastmessageCount = $this->Apimodel->Getvegcount($where);//exit;	
		 
		 if($GetlastmessageCount!=''){
		 $GetlastmessageCount = $GetlastmessageCount;
		}else{
			$GetlastmessageCount=0;
			
		}	
	
	
/* $data = $this->Apimodel->Getrecievemessage_send_message($this->user_id);
	
foreach($data as $row)	
	{ 
	          
		$database['receive_messge_id'] = $row->receive_messge_id;
		$database['receive_user_id'] = $row->receive_user_id;
		$database['group_id'] = $row->group_id;
		$database['reply'] = $row->reply;
		$database['sender_id'] = $row->user_id_fk;
		$database['c_id_fk'] = $row->c_id_fk;
		$database['user_group'] = $row->user_two;
		$database['time'] = date('G.i.s', $row->time);
              $date = date('d-m-Y', $row->time);
		      $timestamp = strtotime($date);
			  $database['date']= date('F j, Y', $timestamp);
		
		
		
	
		$newarray[]=$database;
		
	}
	
		
$reversedata = array_reverse($newarray);
	 
$details = $this->Apimodel->unique_multidim_array($reversedata,'c_id_fk');	

$GetlastmessageCount = count($details); */





/************** Receive Count message *****************/	


/************** Receive fies Count message *****************/	

       $videoCount =    0;
	   $otherCount =   0;
       $audioCount =   0; 
$where=array(
'receiveId'=>$this->user_id,
'type'=>'audio',						
);
$audio = $this->Apimodel->Getvegcount($where); 



$where=array(
'receiveId'=>$this->user_id,
'type'=>'other',						
);
$other = $this->Apimodel->Getvegcount($where);


   
$where=array(
'receiveId'=>$this->user_id,
'type'=>'video/image',						
);
$video = $this->Apimodel->Getvegcount($where);
     
	   $videoCount =    $video;
	   $otherCount =    $other;
       $audioCount =   $audio;
	   
	   
	
$emty=0;		
		
		
	
$response = array(
            "success"=>'1',
            "code"=>'200',
            "message"=>"vegcount list fetched",
            "GetReceiveEmailCount"=>$GetReceiveEmailCount?$GetReceiveEmailCount:$emty,
			"GetReceiveLocationCount"=>$GetReceiveLocationCount?$GetReceiveLocationCount:$emty,
			"GetmessageCount"=>$GetlastmessageCount?$GetlastmessageCount:$emty,
			"videoCount"=>$videoCount?$videoCount:$emty,
			"otherCount"=>$otherCount?$otherCount:$emty,
			"audioCount"=>$audioCount?$audioCount:$emty,
			
           );  
                                              
            echo json_encode($response);


	
                  
        
    }

  
  
  
 //**************** Get History data****************//
	
public function Gethistory()
    {
		if($this->input->post('user_id'))
            $this->user_id = $this->input->post('user_id');

        if($this->user_id=='')
        
        {
            
           log_message('error', ''.__METHOD__);
             return jsonResponse('200', '0','user id  is Required');    
        }
		
		$location =array();
		
		
		
		
		//***************** Location history ***********************//
		
		
        $getSendedLocationList = $this->Apimodel->getSendedLocationList($this->user_id);

     foreach($getSendedLocationList as $row)
	 { 
              $datass['isSecure']= $row->isSecure;
			  if($datass['isSecure']=='1'){
				  
			   $location_id= $row->location_id;
                $datass['type']= 'location';			   
			   $datass['name']= trim(decrypt_string($row->location));
			   $datass['send_date']= $row->send_date;
			   
	$receive_location = $this->Apimodel->getSendeduserdetailsLocationList($location_id);  

foreach($receive_location as $row1)
	 {         $datass['send_to']= $row1->first_name;
	                      
     
             
		$location[]=$datass;
	 }		
			   
				  
	}else{
		
		       $location_id= $row->location_id; 
			     $datass['type']= 'location';	
                $datass['name']= trim(($row->location));			   
			   $timestamp = strtotime($row->send_date);
			   $datass['send_date']= date('F j, Y | g:i', $timestamp);
			   
	$receive_location = $this->Apimodel->getSendeduserdetailsLocationList($location_id);  

foreach($receive_location as $row1)
	 {         $datass['send_to']= $row1->first_name;
              
             
		$location[]=$datass;
	 }		
		
	}
	 }
//**************************** Send file history **********************************//

$fileHistory=array();


 $getSendedFileList = $this->Apimodel->getSendedFileList($this->user_id);
  
//print_r($getSendedFileList); exit;
     

     foreach($getSendedFileList as $row1)
	 { 
	     
              $datass1['isSecure']= $row1->isSecure;
			  
			  $types = $row1->type;	
			 
			  
			  if($datass1['isSecure']=='1'){
				 $sender_file_id = $row1->sender_file_id;
               $datass1['type']= $row1->type;			   
               $datass1['name']= base_url().'assets/files/'.trim(decrypt_string($row1->file));
			   $timestamp = strtotime($row1->send_date);
			   $datass1['send_date']= date('F j, Y | g:i', $timestamp);
			    
			   
	$receive_file = $this->Apimodel->getSendeduserdetailsFileList($sender_file_id);  

foreach($receive_file as $row2)
	 {         $datass1['send_to']= $row2->first_name;
                     
            
		$fileHistory[]=$datass1;
	 }		
			   
				  
	}else{
		
		       $sender_file_id = $row1->sender_file_id;
               $datass1['type']= $row1->type;			   
               $datass1['name']= base_url().'assets/files/'.trim(($row1->file));
			   $timestamp1 = strtotime($row1->send_date);
			   $datass1['send_date']= date('F j, Y | g:i', $timestamp1);//$row1->send_date;
	$receive_file = $this->Apimodel->getSendeduserdetailsFileList($sender_file_id);  

foreach($receive_file as $row2)
	 {         $datass1['send_to']= $row2->first_name;
		       $fileHistory[]=$datass1;
		
	 }		
		
	} 

	
			 }   
			 
			 
			 
//**************************** Send Email history **********************************//

$emailHistory=array();


 $getSendedEmailList = $this->Apimodel->getSendedEmailList($this->user_id);
  
//print_r($getSendedFileList); exit;
     

     foreach($getSendedEmailList as $row3)
	 { 
	     
              $datass5['isSecure']= $row3->isSecure;
			  if($datass5['isSecure']=='1'){
			   $mail_id = $row3->mail_id;
               $datass5['type']= 'email';				   
               $datass5['name']= trim(decrypt_string($row3->subject));
			   $timestamp = strtotime($row3->mail_date);
			   $datass5['send_date']= date('F j, Y | g:i', $timestamp);
			    
			   
	$receive_file = $this->Apimodel->getSendeduserdetailsEmailList($datass5['mail_id']);  

foreach($receive_file as $row4)
	 {         $datass5['send_to']= $row4->first_name;
		$emailHistory[]=$datass5;
	 }		
			   
				  
	}else{
		
		        $mail_id = $row3->mail_id;
                 $datass5['type']= 'email';				
                $datass5['name']= trim(($row3->subject));
			   $timestamp1 = strtotime($row3->mail_date);
			   $datass5['send_date']= date('F j, Y | g:i', $timestamp1);//$row1->send_date;
	$receive_file = $this->Apimodel->getSendeduserdetailsEmailList($mail_id);  

foreach($receive_file as $row4)
	 {         $datass5['send_to']= $row4->first_name;
		                      
		$emailHistory[]=$datass5;
		
	 }		
		
	} 
			 }			 
			 


//*********************** Message history ****************//


$user_one = $this->input->post('user_id');	 
    
$aa = $this->db->query("SELECT  R.reply,R.time , U.user_id,C.c_id,U.first_name,U.email 
FROM users U,conversation C, conversation_reply R
WHERE 
(CASE

WHEN C.user_one = '$user_one'
THEN C.user_two = U.user_id
WHEN C.user_two = '$user_one'
THEN C.user_one= U.user_id
END
)
AND 
C.c_id=R.c_id_fk
AND
(C.user_one ='$user_one' OR C.user_two ='$user_one') ORDER BY C.c_id DESC");

    $dda =$aa->result();
    $newarray=array();	
	foreach($dda as $row)	
	{ 
	           $database['user_id'] = $row->user_id;
		       $database['type']= 'message';				
               $database['name']= trim(($row->reply));
			   $date = date('d-m-Y', $row->time);
		       $timestamp = strtotime($date);
			   $database['send_date']= date('F j, Y', $timestamp);
		       $database['send_to'] = $row->first_name;
			   $database['time'] = date('G.i', $row->time);
			   
		$newarray[]=$database;
		
	}
	
	//print_r($newarray);
//********* Remove duplicate value and create new array **********//

//$reversedata = array_reverse($newarray);
	 
$details = $this->Apimodel->unique_multidim_array($newarray,'user_id');		 


$last_conversation=array();
			foreach($details as $row)
			{
			$datas['isSecure']='0';	
			$datas['type']=$row['type'];	
			$datas['name']=$row['name'];
			$datas['send_date']=$row['send_date'].'| '.$row['time'];
			$datas['send_to']=$row['send_to'];
		
			
         $last_conversation[]=$datas;
}



$HISTORY=array();			 
			 
			 
$HISTORY = array_merge($location,$fileHistory,$emailHistory,$last_conversation);
 

	 
              
        
            $response = array(
            "success"=>'1',
            "code"=>'200',
           "message"=>"Send location list fetched",
          // "LocationHistory"=>$location,
		  // "fileHistory"=>$fileHistory,
		  // "emailHistory"=>$emailHistory,
		  //  "MessageHistory"=>$last_conversation,
		    "History_List"=>$HISTORY,
			
               
           );  
                                              
            echo json_encode($response);
         
          
	         
        
    }
	
	
public function delete_receive()
    {
		
             $user_id = $this->input->post('user_id');
			 $id = $this->input->post('id');
			 $type = $this->input->post('type');
			 
			 $deleted_type = $this->input->post('deleted_type');
			
			 
			 
			 
			 
        if($user_id=='')
        {
           log_message('error', ''.__METHOD__);
             return jsonResponse('200', '0','user id  is Required');    
        }
	elseif($id=='')
        {
           log_message('error', ''.__METHOD__);
             return jsonResponse('200', '0',' id  is Required');    
        }
	elseif($type=='')
        {
           log_message('error', ''.__METHOD__);
             return jsonResponse('200', '0','type  is Required');    
        }
		
if($deleted_type=='sended')
   {
	
//die('testing sended');	

 if($type=='email')
	  {
		$table_name ='receive_mail';  
		 $column_name = 'receive_mail_id'; 
         $data=array('remove_sended'=>'1');		 
	  }elseif($type=='location')
	  {
		  $table_name ='receive_locations';
		   $column_name = 'recv_id';
		   $data=array('remove_sended'=>'1');
	  }elseif($type=='file')
	  {
		 $table_name ='receive_files'; 
		 $column_name = 'receive_file_id';
		 $data=array('remove_sended'=>'1');
        		 
	  }
	  elseif($type=='message')
	  {
		 $table_name ='conversation_reply'; 
		 $column_name = 'cr_id';
		 $data=array('c_id_fk'=>'0');
		//*********This part in process****// 
		 
		  $response = array(
            "success"=>'1',
            "code"=>'200',
           "message"=>"Data deleted",
           );  
                                              
            echo json_encode($response);
		 exit;
		
        		 
	  }

	  
	  
	  if($table_name!='')
	  {
		  
		$this->db->where($column_name, $id);
        //$delete = $this->db->delete($table_name);    
		$this->db->update($table_name,$data);
      $response = array(
            "success"=>'1',
            "code"=>'200',
           "message"=>"Data deleted",
           );  
                                              
            echo json_encode($response);

	  }else{
		  $response = array(
            "success"=>'0',
            "code"=>'200',
           "message"=>"Somthing went wrong",
           );  
                                              
            echo json_encode($response); 
		  
	  }

	 
 }else{	
		
	//die('testing');
      if($type=='email')
	  {
		$table_name ='receive_mail';  
		 $column_name = 'receive_mail_id'; 
         $data=array('rece_delete_status'=>'1');		 
	  }elseif($type=='location')
	  {
		  $table_name ='receive_locations';
		   $column_name = 'recv_id';
		   $data=array('rece_delete_status'=>'1');
		   
	  }elseif($type=='file')
	  {
		 $table_name ='receive_files'; 
		 $column_name = 'receive_file_id';
		 $data=array('rece_delete_status'=>'1');
        		 
	  }
	  elseif($type=='message')
	  {
		 $table_name ='conversation_reply'; 
		 $column_name = 'cr_id';
		 $data=array('c_id_fk'=>'0');
        		 
	  }

	  if($table_name!='')
	  {
		  
		$this->db->where($column_name, $id);
        //$delete = $this->db->delete($table_name);    
		$this->db->update($table_name,$data);
		//echo $this->db->last_query();
		
      $response = array(
            "success"=>'1',
            "code"=>'200',
           "message"=>"Data deleted",
           );  
                                              
            echo json_encode($response);

	  }else{
		  $response = array(
            "success"=>'0',
            "code"=>'200',
           "message"=>"Somthing went wrong",
           );  
                                              
            echo json_encode($response); 
		  
	  }

	}
	
}
	
  


 /* End  Api Function  */
}
/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */