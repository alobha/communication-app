-- phpMyAdmin SQL Dump
-- version 2.8.0.1
-- http://www.phpmyadmin.net
-- 
-- Host: custsql-ipg107.eigbox.net
-- Generation Time: Oct 03, 2018 at 01:51 AM
-- Server version: 5.6.37
-- PHP Version: 4.4.9
-- 
-- Database: `communication_app`
-- 

-- --------------------------------------------------------

-- 
-- Table structure for table `conversation`
-- 

CREATE TABLE `conversation` (
  `c_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_one` varchar(50) NOT NULL,
  `user_two` varchar(100) NOT NULL,
  `ip` varchar(100) NOT NULL,
  `time` varchar(100) NOT NULL,
  PRIMARY KEY (`c_id`)
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1 AUTO_INCREMENT=45 ;

-- 
-- Dumping data for table `conversation`
-- 

INSERT INTO `conversation` VALUES (1, '7', '7,9', '182.77.50.88', '1532960196');
INSERT INTO `conversation` VALUES (2, '9', '9,1', '182.77.50.88', '1532962515');
INSERT INTO `conversation` VALUES (3, '1', '1,3,7', '182.77.50.88', '1533013794');
INSERT INTO `conversation` VALUES (4, '1', '1,4,8,9,10,11', '182.77.50.88', '1533014279');
INSERT INTO `conversation` VALUES (5, '19', '19,1,2,3', '182.77.50.88', '1533029125');
INSERT INTO `conversation` VALUES (6, '21', '21,1', '182.77.50.88', '1533032983');
INSERT INTO `conversation` VALUES (7, '21', '21,19', '182.77.50.88', '1533033007');
INSERT INTO `conversation` VALUES (8, '22', '22,5,11,16', '182.77.127.11', '1533042933');
INSERT INTO `conversation` VALUES (9, '13', '13,12', '73.54.163.206', '1533245670');
INSERT INTO `conversation` VALUES (10, '13', '13,12,16', '73.54.163.206', '1533245698');
INSERT INTO `conversation` VALUES (11, '23', '23,5,6,11', '223.190.69.170', '1533633203');
INSERT INTO `conversation` VALUES (12, '23', '23,11,13', '223.190.103.163', '1535563503');
INSERT INTO `conversation` VALUES (13, '16', '16,12,13,23', '223.190.103.163', '1535563873');
INSERT INTO `conversation` VALUES (14, '13', '13,11', '73.54.163.206', '1535565908');
INSERT INTO `conversation` VALUES (15, '16', '16,13', '157.38.239.103', '1535647487');
INSERT INTO `conversation` VALUES (16, '8', '8,1', '103.70.203.204', '1536341403');
INSERT INTO `conversation` VALUES (17, '8', '8,1', '103.70.203.230', '1536469381');
INSERT INTO `conversation` VALUES (18, '1', '1,2,3,4,8', '103.70.203.230', '1536470431');
INSERT INTO `conversation` VALUES (19, '1', '1,2,3,4', '103.70.203.230', '1536471475');
INSERT INTO `conversation` VALUES (20, '27', '27,26', '103.70.203.230', '1536472121');
INSERT INTO `conversation` VALUES (21, '28', '28,12', '108.28.7.235', '1536558482');
INSERT INTO `conversation` VALUES (22, '26', '26,27', '182.77.50.88', '1536652054');
INSERT INTO `conversation` VALUES (23, '10', '10,1,2', '182.77.50.88', '1536755229');
INSERT INTO `conversation` VALUES (24, '27', '27,1', '182.77.50.88', '1536756082');
INSERT INTO `conversation` VALUES (25, '27', '27,1', '182.77.50.88', '1536756115');
INSERT INTO `conversation` VALUES (26, '27', '27,26', '182.77.50.88', '1536756836');
INSERT INTO `conversation` VALUES (27, '27', '27,26', '182.77.50.88', '1536756848');
INSERT INTO `conversation` VALUES (28, '27', '27,26', '182.77.50.88', '1536757632');
INSERT INTO `conversation` VALUES (29, '27', '27,26', '182.77.50.88', '1536757646');
INSERT INTO `conversation` VALUES (30, '27', '27,26', '182.77.50.88', '1536757739');
INSERT INTO `conversation` VALUES (31, '27', '27,26', '182.77.50.88', '1536757752');
INSERT INTO `conversation` VALUES (32, '1', '1,10', '47.30.190.58', '1537251847');
INSERT INTO `conversation` VALUES (33, '26', '26,1', '182.77.50.88', '1537357075');
INSERT INTO `conversation` VALUES (34, '10', '10,1,6,7', '182.77.50.88', '1537365188');
INSERT INTO `conversation` VALUES (35, '10', '10,1,2,4,5', '182.77.50.88', '1537365269');
INSERT INTO `conversation` VALUES (36, '13', '13,16', '96.241.238.178', '1537384031');
INSERT INTO `conversation` VALUES (37, '11', '11,13', '47.30.161.73', '1537386175');
INSERT INTO `conversation` VALUES (38, '29', '29,12', '108.28.7.235', '1537402328');
INSERT INTO `conversation` VALUES (39, '6', '6,1,4', '47.31.65.197', '1537797608');
INSERT INTO `conversation` VALUES (40, '6', '6,1,2,3,4', '223.190.121.117', '1537857014');
INSERT INTO `conversation` VALUES (41, '10', '10,1,2', '182.77.50.88', '1537858533');
INSERT INTO `conversation` VALUES (42, '6', '6,3,4', '223.190.121.117', '1537884888');
INSERT INTO `conversation` VALUES (43, '31', '31,2', '45.251.51.243', '1538242944');
INSERT INTO `conversation` VALUES (44, '32', '32,1', '45.251.51.243', '1538260648');

-- --------------------------------------------------------

-- 
-- Table structure for table `conversation_reply`
-- 

CREATE TABLE `conversation_reply` (
  `cr_id` int(12) NOT NULL AUTO_INCREMENT,
  `reply` varchar(500) NOT NULL,
  `user_id_fk` varchar(100) NOT NULL,
  `receiveId` varchar(50) NOT NULL,
  `ip` varchar(100) NOT NULL,
  `time` varchar(100) NOT NULL,
  `c_id_fk` varchar(100) NOT NULL,
  PRIMARY KEY (`cr_id`)
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=latin1 AUTO_INCREMENT=107 ;

-- 
-- Dumping data for table `conversation_reply`
-- 

INSERT INTO `conversation_reply` VALUES (1, 'Hiiii', '7', '', '182.77.50.88', '1532960196', '1');
INSERT INTO `conversation_reply` VALUES (2, 'yyy', '7', '', '182.77.50.88', '1532961089', '1');
INSERT INTO `conversation_reply` VALUES (3, 'hello', '9', '7', '182.77.50.88', '1532962288', '1');
INSERT INTO `conversation_reply` VALUES (4, 'Chh', '9', '', '182.77.50.88', '1532962515', '2');
INSERT INTO `conversation_reply` VALUES (5, 'hj', '9', '', '182.77.50.88', '1532962524', '2');
INSERT INTO `conversation_reply` VALUES (6, 'hj', '9', '', '182.77.50.88', '1532962524', '2');
INSERT INTO `conversation_reply` VALUES (7, 'hu', '9', '', '182.77.50.88', '1532962534', '2');
INSERT INTO `conversation_reply` VALUES (8, 'hi', '9', '', '182.77.50.88', '1532962542', '2');
INSERT INTO `conversation_reply` VALUES (9, 'hi', '9', '', '182.77.50.88', '1532962543', '2');
INSERT INTO `conversation_reply` VALUES (10, 'gu', '9', '', '182.77.50.88', '1532962580', '2');
INSERT INTO `conversation_reply` VALUES (11, 'ji', '9', '', '182.77.50.88', '1532962636', '2');
INSERT INTO `conversation_reply` VALUES (12, 'Hello every one', '1', '', '182.77.50.88', '1533013794', '3');
INSERT INTO `conversation_reply` VALUES (13, 'Say hello', '1', '', '182.77.50.88', '1533014279', '0');
INSERT INTO `conversation_reply` VALUES (14, 'hello', '1', '9', '182.77.50.88', '1533015711', '2');
INSERT INTO `conversation_reply` VALUES (15, 'hhhhh', '9', '1', '182.77.50.88', '1533015781', '2');
INSERT INTO `conversation_reply` VALUES (16, 'hello', '9', '1', '182.77.50.88', '1533015786', '2');
INSERT INTO `conversation_reply` VALUES (17, 'hello', '9', '1', '182.77.50.88', '1533015789', '2');
INSERT INTO `conversation_reply` VALUES (18, 'hh', '9', '1', '182.77.50.88', '1533015796', '2');
INSERT INTO `conversation_reply` VALUES (19, 'h', '9', '1', '182.77.50.88', '1533015798', '2');
INSERT INTO `conversation_reply` VALUES (20, 'good morning', '1', '', '182.77.50.88', '1533015836', '0');
INSERT INTO `conversation_reply` VALUES (21, 'going to home', '1', '', '182.77.50.88', '1533015861', '0');
INSERT INTO `conversation_reply` VALUES (22, 'Hi', '19', '', '182.77.50.88', '1533029125', '5');
INSERT INTO `conversation_reply` VALUES (23, 'hello', '19', '', '182.77.50.88', '1533029294', '5');
INSERT INTO `conversation_reply` VALUES (24, 'new', '1', '19', '182.77.50.88', '1533029355', '5');
INSERT INTO `conversation_reply` VALUES (25, 'Of f', '21', '', '182.77.50.88', '1533032983', '6');
INSERT INTO `conversation_reply` VALUES (26, 'gudud', '21', '', '182.77.50.88', '1533032989', '6');
INSERT INTO `conversation_reply` VALUES (27, 'gxhjd', '21', '', '182.77.50.88', '1533032993', '6');
INSERT INTO `conversation_reply` VALUES (28, 'Ufu', '21', '', '182.77.50.88', '1533033007', '7');
INSERT INTO `conversation_reply` VALUES (29, 'gh', '21', '', '182.77.50.88', '1533033015', '7');
INSERT INTO `conversation_reply` VALUES (30, 'Hi', '22', '', '182.77.127.11', '1533042933', '8');
INSERT INTO `conversation_reply` VALUES (31, 'yes', '22', '', '182.77.127.11', '1533042960', '8');
INSERT INTO `conversation_reply` VALUES (32, 'now', '5', '22', '182.77.127.11', '1533043145', '8');
INSERT INTO `conversation_reply` VALUES (33, 'how are you?', '22', '5', '182.77.127.11', '1533043171', '0');
INSERT INTO `conversation_reply` VALUES (34, 'I m good', '5', '', '182.77.127.11', '1533043188', '0');
INSERT INTO `conversation_reply` VALUES (35, 'grt', '22', '5', '182.77.127.11', '1533043208', '0');
INSERT INTO `conversation_reply` VALUES (36, 'how are you frnds 2', '2', '', '182.77.50.88', '1533102870', '1');
INSERT INTO `conversation_reply` VALUES (37, 'Test 8/2', '13', '', '73.54.163.206', '1533245670', '0');
INSERT INTO `conversation_reply` VALUES (38, 'Test 2 8/2', '13', '', '73.54.163.206', '1533245698', '10');
INSERT INTO `conversation_reply` VALUES (39, 'test with zANNTI to deteermine packet vilume', '13', '', '73.54.163.206', '1533268026', '10');
INSERT INTO `conversation_reply` VALUES (40, 'test 2', '13', '', '73.54.163.206', '1533268344', '0');
INSERT INTO `conversation_reply` VALUES (41, 'test with csploit', '13', '', '73.54.163.206', '1533268562', '0');
INSERT INTO `conversation_reply` VALUES (42, 'Hi', '23', '', '223.190.69.170', '1533633203', '11');
INSERT INTO `conversation_reply` VALUES (43, 'Hi lets test it now', '23', '', '223.190.103.163', '1535563503', '0');
INSERT INTO `conversation_reply` VALUES (44, 'Hii test', '16', '', '223.190.103.163', '1535563873', '13');
INSERT INTO `conversation_reply` VALUES (45, 'Sachin. Tried to call. Just call me when you are ready', '13', '', '73.54.163.206', '1535565908', '0');
INSERT INTO `conversation_reply` VALUES (46, 'hii', '16', '', '157.38.239.103', '1535646977', '13');
INSERT INTO `conversation_reply` VALUES (47, 'how are you Erik', '16', '', '157.38.239.103', '1535646991', '13');
INSERT INTO `conversation_reply` VALUES (48, 'this is 10 pm IST here on 30 Aug 2018', '16', '', '157.38.239.103', '1535647019', '13');
INSERT INTO `conversation_reply` VALUES (49, 'great here!!!', '13', '16', '73.54.163.206', '1535647436', '13');
INSERT INTO `conversation_reply` VALUES (50, 'ok', '16', '', '157.38.239.103', '1535647446', '0');
INSERT INTO `conversation_reply` VALUES (51, 'One to message', '16', '', '157.38.239.103', '1535647487', '15');
INSERT INTO `conversation_reply` VALUES (52, 'perfect', '13', '16', '73.54.163.206', '1535647511', '0');
INSERT INTO `conversation_reply` VALUES (53, 'hello', '1', '21', '182.77.50.88', '1536313319', '6');
INSERT INTO `conversation_reply` VALUES (54, 'Hello', '8', '', '103.70.203.204', '1536341403', '16');
INSERT INTO `conversation_reply` VALUES (55, 'Hey', '8', '', '103.70.203.230', '1536469381', '16');
INSERT INTO `conversation_reply` VALUES (56, 'hhhhhhh', '1', '8', '103.70.203.230', '1536470322', '16');
INSERT INTO `conversation_reply` VALUES (57, 'Namste', '1', '', '103.70.203.230', '1536470431', '0');
INSERT INTO `conversation_reply` VALUES (58, 'Hello', '1', '', '103.70.203.230', '1536471475', '0');
INSERT INTO `conversation_reply` VALUES (59, 'Hello p1', '27', '', '103.70.203.230', '1536472121', '20');
INSERT INTO `conversation_reply` VALUES (60, 'how are you p1', '27', '', '103.70.203.230', '1536472143', '20');
INSERT INTO `conversation_reply` VALUES (61, 'what are you doing write now', '27', '', '103.70.203.230', '1536472166', '20');
INSERT INTO `conversation_reply` VALUES (62, 'hiiiii', '26', '27', '103.70.203.230', '1536472804', '20');
INSERT INTO `conversation_reply` VALUES (63, 'hello', '26', '27', '103.70.203.230', '1536472833', '20');
INSERT INTO `conversation_reply` VALUES (64, 'hiiiii', '26', '27', '103.70.203.230', '1536472846', '20');
INSERT INTO `conversation_reply` VALUES (65, 'abc', '26', '', '103.70.203.230', '1536472995', '0');
INSERT INTO `conversation_reply` VALUES (66, 'hello abc', '27', '26', '103.70.203.230', '1536473157', '0');
INSERT INTO `conversation_reply` VALUES (67, 'hello abc2', '27', '', '103.70.203.230', '1536473288', '0');
INSERT INTO `conversation_reply` VALUES (68, 'hello ABC 3', '27', '', '103.70.203.230', '1536473306', '0');
INSERT INTO `conversation_reply` VALUES (69, 'hellp222', '26', '27', '103.70.203.230', '1536474134', '0');
INSERT INTO `conversation_reply` VALUES (70, 'how are you', '26', '27', '103.70.203.230', '1536474144', '0');
INSERT INTO `conversation_reply` VALUES (71, 'hiiii', '27', '26', '103.70.203.230', '1536474338', '0');
INSERT INTO `conversation_reply` VALUES (72, 'heeeee', '27', '26', '103.70.203.230', '1536474344', '0');
INSERT INTO `conversation_reply` VALUES (73, 'gggg', '27', '26', '103.70.203.230', '1536474349', '0');
INSERT INTO `conversation_reply` VALUES (74, 'hii', '6', '23', '117.96.254.232', '1536527065', '0');
INSERT INTO `conversation_reply` VALUES (75, 'Wassup', '28', '', '108.28.7.235', '1536558482', '21');
INSERT INTO `conversation_reply` VALUES (76, 'Chilling and thrilling, man. you know the drill', '12', '28', '108.28.7.235', '1536559331', '21');
INSERT INTO `conversation_reply` VALUES (77, 'Hi I am p1', '26', '', '182.77.50.88', '1536652054', '0');
INSERT INTO `conversation_reply` VALUES (78, 'Dgshsh', '10', '', '182.77.50.88', '1536755229', '0');
INSERT INTO `conversation_reply` VALUES (79, 'Hello', '27', '', '182.77.50.88', '1536756082', '0');
INSERT INTO `conversation_reply` VALUES (80, 'What''s up', '27', '', '182.77.50.88', '1536756115', '0');
INSERT INTO `conversation_reply` VALUES (81, 'Hello', '27', '', '182.77.50.88', '1536756836', '0');
INSERT INTO `conversation_reply` VALUES (82, 'What''s up', '27', '', '182.77.50.88', '1536756848', '0');
INSERT INTO `conversation_reply` VALUES (83, 'Hello', '27', '', '182.77.50.88', '1536757632', '0');
INSERT INTO `conversation_reply` VALUES (84, 'What''s up', '27', '', '182.77.50.88', '1536757646', '0');
INSERT INTO `conversation_reply` VALUES (85, 'Hello', '27', '', '182.77.50.88', '1536757739', '0');
INSERT INTO `conversation_reply` VALUES (86, 'What''s up', '27', '', '182.77.50.88', '1536757752', '0');
INSERT INTO `conversation_reply` VALUES (87, 'Test', '1', '', '47.30.190.58', '1537251847', '0');
INSERT INTO `conversation_reply` VALUES (88, 'fyfc', '11', '23', '47.30.243.61', '1537349479', '0');
INSERT INTO `conversation_reply` VALUES (89, 'Gdydy', '26', '', '182.77.50.88', '1537357075', '33');
INSERT INTO `conversation_reply` VALUES (90, 'Ehhssjsj', '10', '', '182.77.50.88', '1537365188', '0');
INSERT INTO `conversation_reply` VALUES (91, 'hshsh', '10', '', '182.77.50.88', '1537365197', '0');
INSERT INTO `conversation_reply` VALUES (92, 'Ghui', '10', '', '182.77.50.88', '1537365269', '0');
INSERT INTO `conversation_reply` VALUES (93, 'Text test 1', '13', '', '96.241.238.178', '1537384031', '36');
INSERT INTO `conversation_reply` VALUES (94, 'Is message working fine ?', '11', '', '47.30.161.73', '1537386175', '37');
INSERT INTO `conversation_reply` VALUES (95, 'yes', '13', '11', '96.241.238.178', '1537386924', '37');
INSERT INTO `conversation_reply` VALUES (96, 'did you get mine', '13', '11', '96.241.238.178', '1537386941', '37');
INSERT INTO `conversation_reply` VALUES (97, 'In bathroom', '29', '', '108.28.7.235', '1537402328', '38');
INSERT INTO `conversation_reply` VALUES (98, 'Hi this is communication app.', '6', '', '47.31.65.197', '1537797608', '39');
INSERT INTO `conversation_reply` VALUES (99, 'Hi, this is testing message .', '6', '', '223.190.121.117', '1537857014', '40');
INSERT INTO `conversation_reply` VALUES (100, 'Yssu', '10', '', '182.77.50.88', '1537858533', '41');
INSERT INTO `conversation_reply` VALUES (101, 'hsysujzzj', '10', '', '182.77.50.88', '1537858547', '41');
INSERT INTO `conversation_reply` VALUES (102, 'Gdhddhdhdhhdhdhdhdh', '6', '', '223.190.121.117', '1537884888', '42');
INSERT INTO `conversation_reply` VALUES (103, 'test', '11', '13', '47.30.184.201', '1537971024', '37');
INSERT INTO `conversation_reply` VALUES (104, 'jist downloaded. i will send some data and test tomorrow am', '13', '11', '96.241.238.178', '1537983703', '37');
INSERT INTO `conversation_reply` VALUES (105, 'ghhf', '31', '', '45.251.51.243', '1538242944', '43');
INSERT INTO `conversation_reply` VALUES (106, 'rrcrrcrt', '32', '', '45.251.51.243', '1538260648', '44');

-- --------------------------------------------------------

-- 
-- Table structure for table `group_conversation`
-- 

CREATE TABLE `group_conversation` (
  `group_id` int(11) NOT NULL AUTO_INCREMENT,
  `group_admin_id` varchar(50) NOT NULL,
  `group_name` varchar(100) NOT NULL,
  `time` varchar(100) NOT NULL,
  PRIMARY KEY (`group_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

-- 
-- Dumping data for table `group_conversation`
-- 

INSERT INTO `group_conversation` VALUES (1, '1', 'group1', '1532512561');
INSERT INTO `group_conversation` VALUES (2, '1', 'group2', '1532512988');

-- --------------------------------------------------------

-- 
-- Table structure for table `group_conversation_reply`
-- 

CREATE TABLE `group_conversation_reply` (
  `group_reply_id` int(12) NOT NULL AUTO_INCREMENT,
  `group_message` varchar(500) NOT NULL,
  `send_id` varchar(100) NOT NULL,
  `ip` varchar(100) NOT NULL,
  `time` varchar(100) NOT NULL,
  `group_id` varchar(100) NOT NULL,
  PRIMARY KEY (`group_reply_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `group_conversation_reply`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `messages`
-- 

CREATE TABLE `messages` (
  `message_id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `message` text NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- 
-- Dumping data for table `messages`
-- 


-- --------------------------------------------------------

-- 
-- Table structure for table `receive_files`
-- 

CREATE TABLE `receive_files` (
  `receive_file_id` int(11) NOT NULL AUTO_INCREMENT,
  `receiver_id` varchar(50) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `file_id` int(11) NOT NULL,
  `receive_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `staus` tinyint(5) NOT NULL,
  `rece_delete_status` enum('0','1') NOT NULL DEFAULT '0',
  `remove_sended` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`receive_file_id`)
) ENGINE=InnoDB AUTO_INCREMENT=272 DEFAULT CHARSET=latin1 AUTO_INCREMENT=272 ;

-- 
-- Dumping data for table `receive_files`
-- 

INSERT INTO `receive_files` VALUES (2, '3', 1, 1, '2018-07-11 04:07:14', 0, '0', '0');
INSERT INTO `receive_files` VALUES (3, '2', 1, 2, '2018-07-11 04:08:00', 0, '0', '0');
INSERT INTO `receive_files` VALUES (4, '3', 1, 2, '2018-07-11 04:08:00', 0, '0', '0');
INSERT INTO `receive_files` VALUES (5, '2', 1, 3, '2018-07-11 04:08:27', 0, '0', '0');
INSERT INTO `receive_files` VALUES (6, '3', 1, 3, '2018-07-11 04:08:27', 0, '0', '0');
INSERT INTO `receive_files` VALUES (7, '1', 3, 4, '2018-07-11 04:13:34', 0, '1', '0');
INSERT INTO `receive_files` VALUES (8, '2', 3, 4, '2018-07-11 04:13:34', 0, '0', '0');
INSERT INTO `receive_files` VALUES (9, '1', 3, 5, '2018-07-11 04:14:37', 0, '1', '0');
INSERT INTO `receive_files` VALUES (10, '2', 3, 5, '2018-07-11 04:14:37', 0, '0', '0');
INSERT INTO `receive_files` VALUES (11, '2', 3, 6, '2018-07-11 04:15:59', 0, '0', '0');
INSERT INTO `receive_files` VALUES (12, '1', 3, 6, '2018-07-11 04:15:59', 0, '0', '0');
INSERT INTO `receive_files` VALUES (13, '1', 3, 7, '2018-07-11 04:26:15', 0, '1', '0');
INSERT INTO `receive_files` VALUES (14, '2', 3, 7, '2018-07-11 04:26:15', 0, '0', '0');
INSERT INTO `receive_files` VALUES (15, '3', 1, 8, '2018-07-11 05:23:56', 0, '0', '0');
INSERT INTO `receive_files` VALUES (16, '1', 3, 9, '2018-07-11 07:50:04', 0, '1', '0');
INSERT INTO `receive_files` VALUES (17, '2', 3, 9, '2018-07-11 07:50:04', 0, '0', '0');
INSERT INTO `receive_files` VALUES (18, '1', 3, 10, '2018-07-11 07:52:12', 0, '1', '0');
INSERT INTO `receive_files` VALUES (19, '2', 3, 10, '2018-07-11 07:52:12', 0, '0', '0');
INSERT INTO `receive_files` VALUES (20, '1', 3, 11, '2018-07-11 10:16:11', 0, '1', '0');
INSERT INTO `receive_files` VALUES (21, '6', 5, 12, '2018-07-11 10:39:44', 0, '0', '0');
INSERT INTO `receive_files` VALUES (22, '5', 6, 13, '2018-07-11 11:04:14', 0, '0', '0');
INSERT INTO `receive_files` VALUES (23, '7', 8, 14, '2018-07-11 11:59:51', 0, '0', '0');
INSERT INTO `receive_files` VALUES (24, '7', 8, 15, '2018-07-11 12:00:15', 0, '0', '0');
INSERT INTO `receive_files` VALUES (25, '7', 8, 16, '2018-07-11 12:01:41', 0, '0', '0');
INSERT INTO `receive_files` VALUES (26, '6', 8, 16, '2018-07-11 12:01:41', 0, '1', '0');
INSERT INTO `receive_files` VALUES (27, '5', 8, 16, '2018-07-11 12:01:41', 0, '0', '0');
INSERT INTO `receive_files` VALUES (28, '4', 8, 16, '2018-07-11 12:01:41', 0, '0', '0');
INSERT INTO `receive_files` VALUES (29, '3', 8, 16, '2018-07-11 12:01:41', 0, '0', '0');
INSERT INTO `receive_files` VALUES (30, '2', 8, 16, '2018-07-11 12:01:41', 0, '0', '0');
INSERT INTO `receive_files` VALUES (31, '1', 8, 16, '2018-07-11 12:01:41', 0, '1', '0');
INSERT INTO `receive_files` VALUES (32, '1', 8, 17, '2018-07-11 12:02:34', 0, '1', '0');
INSERT INTO `receive_files` VALUES (33, '2', 8, 17, '2018-07-11 12:02:34', 0, '0', '0');
INSERT INTO `receive_files` VALUES (34, '3', 8, 17, '2018-07-11 12:02:34', 0, '0', '0');
INSERT INTO `receive_files` VALUES (35, '4', 8, 17, '2018-07-11 12:02:34', 0, '0', '0');
INSERT INTO `receive_files` VALUES (36, '5', 8, 17, '2018-07-11 12:02:34', 0, '0', '0');
INSERT INTO `receive_files` VALUES (37, '6', 8, 17, '2018-07-11 12:02:34', 0, '1', '0');
INSERT INTO `receive_files` VALUES (38, '7', 8, 17, '2018-07-11 12:02:34', 0, '0', '0');
INSERT INTO `receive_files` VALUES (39, '1', 7, 18, '2018-07-12 01:23:57', 0, '1', '0');
INSERT INTO `receive_files` VALUES (40, '2', 7, 18, '2018-07-12 01:23:57', 0, '0', '0');
INSERT INTO `receive_files` VALUES (41, '1', 7, 19, '2018-07-12 02:14:01', 0, '1', '0');
INSERT INTO `receive_files` VALUES (42, '2', 7, 19, '2018-07-12 02:14:01', 0, '0', '0');
INSERT INTO `receive_files` VALUES (43, '3', 7, 19, '2018-07-12 02:14:01', 0, '0', '0');
INSERT INTO `receive_files` VALUES (44, '7', 10, 20, '2018-07-12 02:43:38', 0, '0', '0');
INSERT INTO `receive_files` VALUES (45, '10', 7, 21, '2018-07-12 02:46:12', 0, '1', '0');
INSERT INTO `receive_files` VALUES (46, '2', 7, 22, '2018-07-12 04:02:45', 0, '0', '0');
INSERT INTO `receive_files` VALUES (47, '9', 7, 23, '2018-07-12 04:03:17', 0, '0', '0');
INSERT INTO `receive_files` VALUES (48, '2', 7, 23, '2018-07-12 04:03:17', 0, '0', '0');
INSERT INTO `receive_files` VALUES (49, '6', 7, 24, '2018-07-12 04:03:45', 0, '0', '0');
INSERT INTO `receive_files` VALUES (50, '5', 7, 24, '2018-07-12 04:03:45', 0, '0', '0');
INSERT INTO `receive_files` VALUES (51, '4', 7, 24, '2018-07-12 04:03:45', 0, '0', '0');
INSERT INTO `receive_files` VALUES (52, '11', 6, 25, '2018-07-12 12:43:50', 0, '1', '0');
INSERT INTO `receive_files` VALUES (53, '11', 13, 26, '2018-07-12 20:41:58', 0, '1', '1');
INSERT INTO `receive_files` VALUES (54, '12', 13, 26, '2018-07-12 20:41:58', 0, '0', '1');
INSERT INTO `receive_files` VALUES (55, '1', 4, 27, '2018-07-16 03:56:44', 0, '1', '0');
INSERT INTO `receive_files` VALUES (56, '3', 4, 27, '2018-07-16 03:56:44', 0, '0', '0');
INSERT INTO `receive_files` VALUES (57, '2', 4, 27, '2018-07-16 03:56:44', 0, '0', '0');
INSERT INTO `receive_files` VALUES (58, '13', 16, 28, '2018-08-30 12:51:06', 0, '0', '0');
INSERT INTO `receive_files` VALUES (59, '7', 1, 29, '2018-09-04 01:48:28', 0, '0', '0');
INSERT INTO `receive_files` VALUES (60, '8', 1, 29, '2018-09-04 01:48:28', 0, '0', '0');
INSERT INTO `receive_files` VALUES (61, '7', 1, 30, '2018-09-04 01:56:44', 0, '0', '0');
INSERT INTO `receive_files` VALUES (62, '8', 1, 30, '2018-09-04 01:56:44', 0, '0', '0');
INSERT INTO `receive_files` VALUES (63, '7', 1, 31, '2018-09-04 01:57:38', 0, '0', '0');
INSERT INTO `receive_files` VALUES (64, '8', 1, 31, '2018-09-04 01:57:38', 0, '0', '0');
INSERT INTO `receive_files` VALUES (65, '10', 1, 32, '2018-09-04 02:02:26', 0, '1', '0');
INSERT INTO `receive_files` VALUES (66, '7', 1, 32, '2018-09-04 02:02:26', 0, '1', '0');
INSERT INTO `receive_files` VALUES (67, '8', 1, 33, '2018-09-04 02:03:04', 0, '0', '0');
INSERT INTO `receive_files` VALUES (68, '7', 1, 33, '2018-09-04 02:03:04', 0, '0', '0');
INSERT INTO `receive_files` VALUES (69, '2', 1, 34, '2018-09-04 02:03:39', 0, '0', '0');
INSERT INTO `receive_files` VALUES (70, '3', 1, 34, '2018-09-04 02:03:39', 0, '0', '0');
INSERT INTO `receive_files` VALUES (71, '2', 1, 35, '2018-09-04 02:56:17', 0, '0', '0');
INSERT INTO `receive_files` VALUES (72, '3', 1, 35, '2018-09-04 02:56:17', 0, '0', '0');
INSERT INTO `receive_files` VALUES (73, '4', 1, 35, '2018-09-04 02:56:17', 0, '0', '0');
INSERT INTO `receive_files` VALUES (74, '7', 1, 35, '2018-09-04 02:56:17', 0, '0', '0');
INSERT INTO `receive_files` VALUES (75, '4', 1, 36, '2018-09-04 02:56:41', 0, '0', '0');
INSERT INTO `receive_files` VALUES (76, '2', 1, 36, '2018-09-04 02:56:41', 0, '0', '0');
INSERT INTO `receive_files` VALUES (77, '6', 1, 36, '2018-09-04 02:56:41', 0, '1', '0');
INSERT INTO `receive_files` VALUES (78, '8', 1, 36, '2018-09-04 02:56:41', 0, '0', '0');
INSERT INTO `receive_files` VALUES (79, '10', 1, 36, '2018-09-04 02:56:41', 0, '1', '0');
INSERT INTO `receive_files` VALUES (80, '11', 1, 36, '2018-09-04 02:56:41', 0, '1', '0');
INSERT INTO `receive_files` VALUES (81, '6', 1, 37, '2018-09-04 02:57:21', 0, '0', '0');
INSERT INTO `receive_files` VALUES (82, '11', 1, 37, '2018-09-04 02:57:21', 0, '1', '0');
INSERT INTO `receive_files` VALUES (83, '3', 1, 37, '2018-09-04 02:57:21', 0, '0', '0');
INSERT INTO `receive_files` VALUES (84, '2', 1, 37, '2018-09-04 02:57:21', 0, '0', '0');
INSERT INTO `receive_files` VALUES (85, '11', 23, 38, '2018-09-05 10:20:15', 0, '1', '0');
INSERT INTO `receive_files` VALUES (86, '11', 23, 39, '2018-09-05 10:21:01', 0, '1', '0');
INSERT INTO `receive_files` VALUES (87, '25', 1, 40, '2018-09-07 07:39:47', 0, '0', '0');
INSERT INTO `receive_files` VALUES (88, '2', 1, 41, '2018-09-07 07:42:00', 0, '0', '0');
INSERT INTO `receive_files` VALUES (89, '3', 1, 41, '2018-09-07 07:42:00', 0, '0', '0');
INSERT INTO `receive_files` VALUES (90, '4', 1, 41, '2018-09-07 07:42:00', 0, '0', '0');
INSERT INTO `receive_files` VALUES (91, '7', 1, 41, '2018-09-07 07:42:00', 0, '0', '0');
INSERT INTO `receive_files` VALUES (92, '8', 1, 41, '2018-09-07 07:42:00', 0, '0', '0');
INSERT INTO `receive_files` VALUES (93, '25', 1, 41, '2018-09-07 07:42:00', 0, '0', '0');
INSERT INTO `receive_files` VALUES (94, '2', 1, 42, '2018-09-07 09:28:09', 0, '0', '0');
INSERT INTO `receive_files` VALUES (95, '26', 27, 43, '2018-09-11 03:58:41', 0, '1', '0');
INSERT INTO `receive_files` VALUES (96, '26', 27, 44, '2018-09-11 04:00:38', 0, '1', '0');
INSERT INTO `receive_files` VALUES (97, '23', 11, 45, '2018-09-11 12:42:21', 0, '0', '0');
INSERT INTO `receive_files` VALUES (98, '1', 10, 46, '2018-09-12 08:28:18', 0, '1', '1');
INSERT INTO `receive_files` VALUES (99, '3', 10, 46, '2018-09-12 08:28:18', 0, '0', '1');
INSERT INTO `receive_files` VALUES (100, '26', 27, 47, '2018-09-12 08:59:09', 0, '1', '0');
INSERT INTO `receive_files` VALUES (101, '26', 27, 48, '2018-09-12 09:00:27', 0, '1', '0');
INSERT INTO `receive_files` VALUES (102, '27', 26, 49, '2018-09-12 09:05:46', 0, '1', '0');
INSERT INTO `receive_files` VALUES (103, '27', 26, 50, '2018-09-12 09:06:32', 0, '1', '0');
INSERT INTO `receive_files` VALUES (104, '23', 11, 51, '2018-09-14 11:35:11', 0, '0', '0');
INSERT INTO `receive_files` VALUES (105, '1', 11, 52, '2018-09-14 11:35:37', 0, '1', '0');
INSERT INTO `receive_files` VALUES (106, '2', 11, 52, '2018-09-14 11:35:37', 0, '0', '0');
INSERT INTO `receive_files` VALUES (107, '3', 11, 52, '2018-09-14 11:35:37', 0, '0', '0');
INSERT INTO `receive_files` VALUES (108, '4', 11, 52, '2018-09-14 11:35:37', 0, '0', '0');
INSERT INTO `receive_files` VALUES (109, '5', 11, 52, '2018-09-14 11:35:37', 0, '0', '0');
INSERT INTO `receive_files` VALUES (110, '6', 11, 52, '2018-09-14 11:35:37', 0, '0', '0');
INSERT INTO `receive_files` VALUES (111, '7', 11, 52, '2018-09-14 11:35:37', 0, '0', '0');
INSERT INTO `receive_files` VALUES (112, '1', 10, 53, '2018-09-17 10:41:08', 0, '1', '0');
INSERT INTO `receive_files` VALUES (113, '1', 10, 54, '2018-09-17 10:42:54', 0, '1', '0');
INSERT INTO `receive_files` VALUES (114, '1', 10, 55, '2018-09-17 11:00:08', 0, '1', '0');
INSERT INTO `receive_files` VALUES (115, '2', 10, 55, '2018-09-17 11:00:08', 0, '0', '0');
INSERT INTO `receive_files` VALUES (116, '3', 10, 55, '2018-09-17 11:00:08', 0, '0', '0');
INSERT INTO `receive_files` VALUES (117, '5', 10, 55, '2018-09-17 11:00:08', 0, '0', '0');
INSERT INTO `receive_files` VALUES (118, '6', 10, 55, '2018-09-17 11:00:08', 0, '0', '0');
INSERT INTO `receive_files` VALUES (119, '10', 1, 56, '2018-09-17 11:00:42', 0, '1', '0');
INSERT INTO `receive_files` VALUES (120, '9', 1, 56, '2018-09-17 11:00:42', 0, '0', '0');
INSERT INTO `receive_files` VALUES (121, '8', 1, 56, '2018-09-17 11:00:42', 0, '0', '0');
INSERT INTO `receive_files` VALUES (122, '10', 1, 57, '2018-09-17 11:03:25', 0, '1', '0');
INSERT INTO `receive_files` VALUES (123, '9', 1, 57, '2018-09-17 11:03:25', 0, '0', '0');
INSERT INTO `receive_files` VALUES (124, '8', 1, 57, '2018-09-17 11:03:25', 0, '0', '0');
INSERT INTO `receive_files` VALUES (125, '7', 1, 57, '2018-09-17 11:03:25', 0, '0', '0');
INSERT INTO `receive_files` VALUES (126, '6', 1, 57, '2018-09-17 11:03:25', 0, '0', '0');
INSERT INTO `receive_files` VALUES (127, '2', 1, 58, '2018-09-18 02:20:38', 0, '0', '0');
INSERT INTO `receive_files` VALUES (128, '3', 1, 58, '2018-09-18 02:20:38', 0, '0', '0');
INSERT INTO `receive_files` VALUES (129, '10', 1, 58, '2018-09-18 02:20:38', 0, '1', '0');
INSERT INTO `receive_files` VALUES (130, '4', 1, 59, '2018-09-18 02:21:47', 0, '0', '0');
INSERT INTO `receive_files` VALUES (131, '6', 1, 59, '2018-09-18 02:21:47', 0, '0', '0');
INSERT INTO `receive_files` VALUES (132, '9', 1, 59, '2018-09-18 02:21:47', 0, '0', '0');
INSERT INTO `receive_files` VALUES (133, '10', 1, 60, '2018-09-18 02:22:53', 0, '1', '0');
INSERT INTO `receive_files` VALUES (134, '10', 1, 61, '2018-09-18 02:24:20', 0, '1', '0');
INSERT INTO `receive_files` VALUES (135, '10', 1, 62, '2018-09-18 02:25:10', 0, '1', '0');
INSERT INTO `receive_files` VALUES (136, '10', 1, 63, '2018-09-18 02:25:40', 0, '1', '0');
INSERT INTO `receive_files` VALUES (137, '2', 1, 64, '2018-09-18 09:18:18', 0, '0', '0');
INSERT INTO `receive_files` VALUES (138, '10', 1, 64, '2018-09-18 09:18:18', 0, '1', '0');
INSERT INTO `receive_files` VALUES (139, '26', 1, 64, '2018-09-18 09:18:18', 0, '1', '0');
INSERT INTO `receive_files` VALUES (140, '27', 1, 64, '2018-09-18 09:18:18', 0, '0', '0');
INSERT INTO `receive_files` VALUES (141, '2', 1, 65, '2018-09-18 09:19:05', 0, '0', '0');
INSERT INTO `receive_files` VALUES (142, '10', 1, 65, '2018-09-18 09:19:05', 0, '1', '0');
INSERT INTO `receive_files` VALUES (143, '27', 1, 65, '2018-09-18 09:19:05', 0, '0', '0');
INSERT INTO `receive_files` VALUES (144, '4', 1, 66, '2018-09-18 09:25:02', 0, '0', '0');
INSERT INTO `receive_files` VALUES (145, '6', 1, 66, '2018-09-18 09:25:02', 0, '0', '0');
INSERT INTO `receive_files` VALUES (146, '9', 1, 66, '2018-09-18 09:25:02', 0, '0', '0');
INSERT INTO `receive_files` VALUES (147, '1', 10, 67, '2018-09-18 09:37:52', 0, '1', '1');
INSERT INTO `receive_files` VALUES (148, '2', 10, 67, '2018-09-18 09:37:52', 0, '0', '1');
INSERT INTO `receive_files` VALUES (149, '11', 10, 67, '2018-09-18 09:37:52', 0, '1', '1');
INSERT INTO `receive_files` VALUES (150, '25', 10, 67, '2018-09-18 09:37:52', 0, '0', '0');
INSERT INTO `receive_files` VALUES (151, '23', 11, 68, '2018-09-18 10:15:55', 0, '0', '0');
INSERT INTO `receive_files` VALUES (152, '16', 10, 69, '2018-09-18 10:46:40', 0, '0', '1');
INSERT INTO `receive_files` VALUES (153, '1', 10, 70, '2018-09-18 10:49:18', 0, '1', '0');
INSERT INTO `receive_files` VALUES (154, '2', 10, 70, '2018-09-18 10:49:18', 0, '0', '0');
INSERT INTO `receive_files` VALUES (155, '3', 10, 70, '2018-09-18 10:49:18', 0, '0', '0');
INSERT INTO `receive_files` VALUES (156, '1', 26, 71, '2018-09-19 03:35:21', 0, '1', '0');
INSERT INTO `receive_files` VALUES (157, '3', 26, 71, '2018-09-19 03:35:21', 0, '0', '0');
INSERT INTO `receive_files` VALUES (158, '1', 26, 72, '2018-09-19 03:36:40', 0, '1', '0');
INSERT INTO `receive_files` VALUES (159, '3', 26, 72, '2018-09-19 03:36:40', 0, '0', '0');
INSERT INTO `receive_files` VALUES (160, '1', 26, 73, '2018-09-19 04:28:03', 0, '1', '0');
INSERT INTO `receive_files` VALUES (161, '1', 26, 74, '2018-09-19 04:31:30', 0, '1', '0');
INSERT INTO `receive_files` VALUES (162, '1', 26, 75, '2018-09-19 04:31:39', 0, '1', '0');
INSERT INTO `receive_files` VALUES (163, '1', 26, 76, '2018-09-19 04:31:48', 0, '1', '0');
INSERT INTO `receive_files` VALUES (164, '1', 26, 77, '2018-09-19 04:34:29', 0, '1', '0');
INSERT INTO `receive_files` VALUES (165, '1', 26, 78, '2018-09-19 04:36:36', 0, '1', '0');
INSERT INTO `receive_files` VALUES (166, '1', 26, 79, '2018-09-19 04:37:02', 0, '1', '0');
INSERT INTO `receive_files` VALUES (167, '1', 26, 80, '2018-09-19 04:38:13', 0, '1', '0');
INSERT INTO `receive_files` VALUES (168, '1', 26, 81, '2018-09-19 04:38:23', 0, '1', '0');
INSERT INTO `receive_files` VALUES (169, '1', 26, 82, '2018-09-19 04:38:33', 0, '1', '0');
INSERT INTO `receive_files` VALUES (170, '2', 1, 83, '2018-09-19 06:56:48', 0, '0', '1');
INSERT INTO `receive_files` VALUES (171, '3', 1, 83, '2018-09-19 06:56:48', 0, '0', '1');
INSERT INTO `receive_files` VALUES (172, '4', 1, 83, '2018-09-19 06:56:48', 0, '0', '1');
INSERT INTO `receive_files` VALUES (173, '5', 1, 83, '2018-09-19 06:56:48', 0, '0', '1');
INSERT INTO `receive_files` VALUES (174, '26', 1, 84, '2018-09-19 07:03:00', 0, '0', '1');
INSERT INTO `receive_files` VALUES (175, '2', 1, 85, '2018-09-19 07:12:11', 0, '0', '1');
INSERT INTO `receive_files` VALUES (176, '3', 1, 85, '2018-09-19 07:12:11', 0, '0', '1');
INSERT INTO `receive_files` VALUES (177, '20', 1, 85, '2018-09-19 07:12:11', 0, '0', '1');
INSERT INTO `receive_files` VALUES (178, '22', 1, 85, '2018-09-19 07:12:11', 0, '0', '1');
INSERT INTO `receive_files` VALUES (179, '26', 1, 85, '2018-09-19 07:12:11', 0, '1', '1');
INSERT INTO `receive_files` VALUES (180, '1', 26, 86, '2018-09-19 07:16:06', 0, '1', '1');
INSERT INTO `receive_files` VALUES (181, '2', 26, 86, '2018-09-19 07:16:06', 0, '0', '1');
INSERT INTO `receive_files` VALUES (182, '3', 26, 86, '2018-09-19 07:16:06', 0, '0', '1');
INSERT INTO `receive_files` VALUES (183, '4', 26, 86, '2018-09-19 07:16:06', 0, '0', '1');
INSERT INTO `receive_files` VALUES (184, '7', 26, 86, '2018-09-19 07:16:06', 0, '0', '1');
INSERT INTO `receive_files` VALUES (185, '8', 26, 86, '2018-09-19 07:16:06', 0, '0', '1');
INSERT INTO `receive_files` VALUES (186, '1', 26, 87, '2018-09-19 07:22:24', 0, '1', '1');
INSERT INTO `receive_files` VALUES (187, '2', 26, 87, '2018-09-19 07:22:24', 0, '0', '1');
INSERT INTO `receive_files` VALUES (188, '3', 26, 87, '2018-09-19 07:22:24', 0, '0', '1');
INSERT INTO `receive_files` VALUES (189, '5', 26, 87, '2018-09-19 07:22:24', 0, '0', '1');
INSERT INTO `receive_files` VALUES (190, '6', 26, 87, '2018-09-19 07:22:24', 0, '0', '1');
INSERT INTO `receive_files` VALUES (191, '7', 26, 87, '2018-09-19 07:22:24', 0, '0', '1');
INSERT INTO `receive_files` VALUES (192, '8', 26, 87, '2018-09-19 07:22:24', 0, '0', '1');
INSERT INTO `receive_files` VALUES (193, '1', 26, 88, '2018-09-19 07:23:28', 0, '1', '1');
INSERT INTO `receive_files` VALUES (194, '2', 26, 88, '2018-09-19 07:23:28', 0, '0', '1');
INSERT INTO `receive_files` VALUES (195, '3', 26, 88, '2018-09-19 07:23:28', 0, '0', '1');
INSERT INTO `receive_files` VALUES (196, '5', 26, 88, '2018-09-19 07:23:28', 0, '0', '1');
INSERT INTO `receive_files` VALUES (197, '1', 26, 89, '2018-09-19 07:47:38', 0, '0', '0');
INSERT INTO `receive_files` VALUES (198, '2', 26, 89, '2018-09-19 07:47:38', 0, '0', '0');
INSERT INTO `receive_files` VALUES (199, '3', 26, 89, '2018-09-19 07:47:38', 0, '0', '0');
INSERT INTO `receive_files` VALUES (200, '1', 26, 90, '2018-09-19 07:51:13', 0, '0', '0');
INSERT INTO `receive_files` VALUES (201, '1', 26, 91, '2018-09-19 07:52:06', 0, '0', '0');
INSERT INTO `receive_files` VALUES (202, '1', 26, 92, '2018-09-19 07:59:37', 0, '0', '0');
INSERT INTO `receive_files` VALUES (203, '1', 10, 93, '2018-09-19 09:56:26', 0, '0', '1');
INSERT INTO `receive_files` VALUES (204, '2', 10, 93, '2018-09-19 09:56:26', 0, '0', '0');
INSERT INTO `receive_files` VALUES (205, '3', 10, 93, '2018-09-19 09:56:26', 0, '0', '0');
INSERT INTO `receive_files` VALUES (206, '4', 10, 93, '2018-09-19 09:56:26', 0, '0', '1');
INSERT INTO `receive_files` VALUES (207, '1', 10, 94, '2018-09-19 09:57:22', 0, '0', '1');
INSERT INTO `receive_files` VALUES (208, '2', 10, 94, '2018-09-19 09:57:22', 0, '0', '0');
INSERT INTO `receive_files` VALUES (209, '3', 10, 94, '2018-09-19 09:57:22', 0, '0', '0');
INSERT INTO `receive_files` VALUES (210, '4', 10, 94, '2018-09-19 09:57:22', 0, '0', '1');
INSERT INTO `receive_files` VALUES (211, '2', 1, 95, '2018-09-19 10:00:42', 0, '0', '0');
INSERT INTO `receive_files` VALUES (212, '4', 1, 95, '2018-09-19 10:00:42', 0, '0', '0');
INSERT INTO `receive_files` VALUES (213, '5', 1, 95, '2018-09-19 10:00:42', 0, '0', '0');
INSERT INTO `receive_files` VALUES (214, '7', 1, 95, '2018-09-19 10:00:42', 0, '0', '0');
INSERT INTO `receive_files` VALUES (215, '9', 1, 95, '2018-09-19 10:00:42', 0, '0', '0');
INSERT INTO `receive_files` VALUES (216, '11', 1, 95, '2018-09-19 10:00:42', 0, '0', '0');
INSERT INTO `receive_files` VALUES (217, '2', 1, 96, '2018-09-19 10:18:58', 0, '0', '0');
INSERT INTO `receive_files` VALUES (218, '3', 1, 96, '2018-09-19 10:18:58', 0, '0', '0');
INSERT INTO `receive_files` VALUES (219, '5', 1, 96, '2018-09-19 10:18:58', 0, '0', '0');
INSERT INTO `receive_files` VALUES (220, '1', 10, 97, '2018-09-19 11:20:40', 0, '0', '0');
INSERT INTO `receive_files` VALUES (221, '2', 10, 97, '2018-09-19 11:20:40', 0, '0', '0');
INSERT INTO `receive_files` VALUES (222, '3', 10, 97, '2018-09-19 11:20:40', 0, '0', '0');
INSERT INTO `receive_files` VALUES (223, '3', 10, 98, '2018-09-19 11:23:41', 0, '0', '0');
INSERT INTO `receive_files` VALUES (224, '4', 10, 98, '2018-09-19 11:23:41', 0, '0', '0');
INSERT INTO `receive_files` VALUES (225, '7', 10, 98, '2018-09-19 11:23:41', 0, '0', '0');
INSERT INTO `receive_files` VALUES (226, '3', 10, 99, '2018-09-19 11:40:57', 0, '0', '1');
INSERT INTO `receive_files` VALUES (227, '6', 10, 99, '2018-09-19 11:40:57', 0, '0', '0');
INSERT INTO `receive_files` VALUES (228, '1', 10, 100, '2018-09-19 11:42:53', 0, '0', '0');
INSERT INTO `receive_files` VALUES (229, '2', 10, 100, '2018-09-19 11:42:53', 0, '0', '0');
INSERT INTO `receive_files` VALUES (230, '4', 10, 100, '2018-09-19 11:42:53', 0, '0', '0');
INSERT INTO `receive_files` VALUES (231, '1', 10, 101, '2018-09-19 11:43:24', 0, '0', '1');
INSERT INTO `receive_files` VALUES (232, '2', 10, 101, '2018-09-19 11:43:24', 0, '0', '0');
INSERT INTO `receive_files` VALUES (233, '3', 10, 101, '2018-09-19 11:43:24', 0, '0', '0');
INSERT INTO `receive_files` VALUES (234, '5', 10, 101, '2018-09-19 11:43:24', 0, '0', '0');
INSERT INTO `receive_files` VALUES (235, '16', 13, 102, '2018-09-19 15:13:55', 0, '0', '0');
INSERT INTO `receive_files` VALUES (236, '1', 10, 103, '2018-09-21 06:22:05', 0, '0', '1');
INSERT INTO `receive_files` VALUES (237, '2', 10, 103, '2018-09-21 06:22:05', 0, '0', '0');
INSERT INTO `receive_files` VALUES (238, '3', 10, 103, '2018-09-21 06:22:05', 0, '0', '0');
INSERT INTO `receive_files` VALUES (239, '1', 10, 104, '2018-09-21 06:23:26', 0, '0', '0');
INSERT INTO `receive_files` VALUES (240, '2', 10, 104, '2018-09-21 06:23:26', 0, '0', '0');
INSERT INTO `receive_files` VALUES (241, '3', 10, 104, '2018-09-21 06:23:26', 0, '0', '0');
INSERT INTO `receive_files` VALUES (242, '1', 10, 105, '2018-09-21 06:24:02', 0, '0', '0');
INSERT INTO `receive_files` VALUES (243, '2', 10, 105, '2018-09-21 06:24:02', 0, '0', '0');
INSERT INTO `receive_files` VALUES (244, '3', 10, 105, '2018-09-21 06:24:02', 0, '0', '0');
INSERT INTO `receive_files` VALUES (245, '5', 10, 105, '2018-09-21 06:24:02', 0, '0', '0');
INSERT INTO `receive_files` VALUES (246, '4', 6, 106, '2018-09-24 08:45:34', 0, '0', '0');
INSERT INTO `receive_files` VALUES (247, '5', 6, 106, '2018-09-24 08:45:34', 0, '0', '0');
INSERT INTO `receive_files` VALUES (248, '8', 6, 106, '2018-09-24 08:45:34', 0, '0', '0');
INSERT INTO `receive_files` VALUES (249, '9', 6, 106, '2018-09-24 08:45:34', 0, '0', '0');
INSERT INTO `receive_files` VALUES (250, '3', 6, 107, '2018-09-24 09:25:04', 0, '0', '0');
INSERT INTO `receive_files` VALUES (251, '4', 6, 107, '2018-09-24 09:25:04', 0, '0', '0');
INSERT INTO `receive_files` VALUES (252, '7', 6, 107, '2018-09-24 09:25:04', 0, '0', '0');
INSERT INTO `receive_files` VALUES (253, '8', 6, 107, '2018-09-24 09:25:04', 0, '0', '0');
INSERT INTO `receive_files` VALUES (254, '5', 6, 108, '2018-09-24 09:41:34', 0, '0', '1');
INSERT INTO `receive_files` VALUES (255, '7', 6, 108, '2018-09-24 09:41:34', 0, '0', '1');
INSERT INTO `receive_files` VALUES (256, '8', 6, 108, '2018-09-24 09:41:34', 0, '0', '1');
INSERT INTO `receive_files` VALUES (257, '10', 6, 108, '2018-09-24 09:41:34', 0, '0', '1');
INSERT INTO `receive_files` VALUES (258, '3', 6, 109, '2018-09-24 09:42:33', 0, '0', '0');
INSERT INTO `receive_files` VALUES (259, '7', 6, 109, '2018-09-24 09:42:33', 0, '0', '0');
INSERT INTO `receive_files` VALUES (260, '8', 6, 109, '2018-09-24 09:42:33', 0, '0', '0');
INSERT INTO `receive_files` VALUES (261, '9', 6, 109, '2018-09-24 09:42:33', 0, '0', '0');
INSERT INTO `receive_files` VALUES (262, '11', 6, 110, '2018-09-24 10:36:27', 0, '0', '1');
INSERT INTO `receive_files` VALUES (263, '23', 6, 110, '2018-09-24 10:36:27', 0, '0', '1');
INSERT INTO `receive_files` VALUES (264, '10', 1, 111, '2018-09-25 04:18:37', 0, '0', '0');
INSERT INTO `receive_files` VALUES (265, '2', 1, 112, '2018-09-25 04:18:52', 0, '0', '0');
INSERT INTO `receive_files` VALUES (266, '3', 1, 112, '2018-09-25 04:18:52', 0, '0', '0');
INSERT INTO `receive_files` VALUES (267, '7', 1, 113, '2018-09-25 04:22:26', 0, '0', '0');
INSERT INTO `receive_files` VALUES (268, '1', 27, 114, '2018-09-25 04:27:55', 0, '0', '0');
INSERT INTO `receive_files` VALUES (269, '2', 27, 114, '2018-09-25 04:27:55', 0, '0', '0');
INSERT INTO `receive_files` VALUES (270, '16', 30, 115, '2018-09-26 13:15:42', 0, '0', '0');
INSERT INTO `receive_files` VALUES (271, '2', 1, 116, '2018-10-03 01:38:30', 0, '0', '0');

-- --------------------------------------------------------

-- 
-- Table structure for table `receive_locations`
-- 

CREATE TABLE `receive_locations` (
  `recv_id` int(11) NOT NULL AUTO_INCREMENT,
  `receiver_id` int(11) NOT NULL,
  `send_location_id` varchar(255) NOT NULL,
  `receive_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sender_id` int(11) NOT NULL,
  `rece_delete_status` enum('0','1') NOT NULL DEFAULT '0',
  `remove_sended` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`recv_id`)
) ENGINE=InnoDB AUTO_INCREMENT=173 DEFAULT CHARSET=latin1 AUTO_INCREMENT=173 ;

-- 
-- Dumping data for table `receive_locations`
-- 

INSERT INTO `receive_locations` VALUES (1, 2, '1', '2018-07-11 03:28:08', 1, '0', '0');
INSERT INTO `receive_locations` VALUES (2, 3, '2', '2018-07-11 03:29:09', 1, '0', '1');
INSERT INTO `receive_locations` VALUES (3, 3, '3', '2018-07-11 03:59:52', 1, '0', '1');
INSERT INTO `receive_locations` VALUES (4, 2, '4', '2018-07-11 04:02:40', 3, '0', '0');
INSERT INTO `receive_locations` VALUES (6, 2, '5', '2018-07-11 04:04:54', 3, '0', '0');
INSERT INTO `receive_locations` VALUES (7, 3, '6', '2018-07-11 08:14:16', 1, '0', '1');
INSERT INTO `receive_locations` VALUES (8, 6, '7', '2018-07-11 10:32:50', 5, '1', '0');
INSERT INTO `receive_locations` VALUES (9, 6, '8', '2018-07-11 10:34:38', 5, '1', '0');
INSERT INTO `receive_locations` VALUES (10, 5, '9', '2018-07-11 11:02:07', 6, '0', '1');
INSERT INTO `receive_locations` VALUES (11, 5, '10', '2018-07-11 11:02:31', 6, '0', '1');
INSERT INTO `receive_locations` VALUES (14, 5, '12', '2018-07-12 06:00:52', 6, '0', '1');
INSERT INTO `receive_locations` VALUES (15, 6, '13', '2018-07-12 10:27:47', 10, '1', '0');
INSERT INTO `receive_locations` VALUES (16, 6, '14', '2018-07-12 12:25:51', 11, '0', '0');
INSERT INTO `receive_locations` VALUES (17, 11, '15', '2018-07-12 12:26:47', 6, '1', '1');
INSERT INTO `receive_locations` VALUES (18, 11, '16', '2018-07-12 20:43:44', 13, '1', '0');
INSERT INTO `receive_locations` VALUES (19, 12, '16', '2018-07-12 20:43:44', 13, '0', '0');
INSERT INTO `receive_locations` VALUES (20, 13, '17', '2018-07-12 20:53:19', 12, '0', '0');
INSERT INTO `receive_locations` VALUES (21, 8, '18', '2018-07-13 06:46:03', 7, '1', '0');
INSERT INTO `receive_locations` VALUES (24, 6, '20', '2018-07-13 12:12:14', 3, '0', '0');
INSERT INTO `receive_locations` VALUES (25, 3, '21', '2018-07-13 12:15:41', 7, '0', '0');
INSERT INTO `receive_locations` VALUES (26, 4, '21', '2018-07-13 12:15:41', 7, '0', '0');
INSERT INTO `receive_locations` VALUES (27, 5, '21', '2018-07-13 12:15:41', 7, '0', '0');
INSERT INTO `receive_locations` VALUES (28, 11, '22', '2018-07-19 12:36:22', 13, '1', '0');
INSERT INTO `receive_locations` VALUES (29, 11, '23', '2018-07-20 17:13:07', 13, '1', '0');
INSERT INTO `receive_locations` VALUES (30, 12, '23', '2018-07-20 17:13:07', 13, '0', '0');
INSERT INTO `receive_locations` VALUES (31, 2, '24', '2018-07-25 11:39:07', 1, '0', '1');
INSERT INTO `receive_locations` VALUES (32, 3, '24', '2018-07-25 11:39:07', 1, '0', '1');
INSERT INTO `receive_locations` VALUES (33, 4, '24', '2018-07-25 11:39:07', 1, '0', '1');
INSERT INTO `receive_locations` VALUES (34, 3, '25', '2018-07-25 11:39:39', 1, '0', '1');
INSERT INTO `receive_locations` VALUES (35, 5, '25', '2018-07-25 11:39:39', 1, '0', '1');
INSERT INTO `receive_locations` VALUES (36, 4, '26', '2018-07-25 12:29:05', 1, '0', '1');
INSERT INTO `receive_locations` VALUES (37, 11, '27', '2018-07-25 13:25:10', 5, '0', '0');
INSERT INTO `receive_locations` VALUES (38, 11, '28', '2018-07-27 15:13:13', 17, '0', '0');
INSERT INTO `receive_locations` VALUES (39, 5, '29', '2018-07-27 15:13:26', 17, '0', '0');
INSERT INTO `receive_locations` VALUES (40, 5, '30', '2018-07-27 15:13:30', 16, '0', '0');
INSERT INTO `receive_locations` VALUES (41, 6, '30', '2018-07-27 15:13:30', 16, '0', '0');
INSERT INTO `receive_locations` VALUES (42, 11, '31', '2018-07-27 15:13:56', 17, '0', '0');
INSERT INTO `receive_locations` VALUES (43, 16, '32', '2018-07-27 15:14:47', 17, '0', '0');
INSERT INTO `receive_locations` VALUES (44, 11, '33', '2018-07-27 15:15:02', 17, '0', '0');
INSERT INTO `receive_locations` VALUES (45, 17, '34', '2018-07-27 15:16:25', 5, '0', '0');
INSERT INTO `receive_locations` VALUES (46, 5, '35', '2018-07-27 15:18:19', 16, '0', '0');
INSERT INTO `receive_locations` VALUES (47, 6, '35', '2018-07-27 15:18:19', 16, '0', '0');
INSERT INTO `receive_locations` VALUES (48, 22, '36', '2018-07-31 09:21:12', 5, '0', '0');
INSERT INTO `receive_locations` VALUES (49, 12, '37', '2018-08-02 17:31:12', 13, '0', '0');
INSERT INTO `receive_locations` VALUES (50, 12, '38', '2018-08-02 17:31:49', 13, '0', '0');
INSERT INTO `receive_locations` VALUES (51, 2, '39', '2018-08-07 02:45:59', 1, '0', '1');
INSERT INTO `receive_locations` VALUES (52, 3, '39', '2018-08-07 02:45:59', 1, '0', '1');
INSERT INTO `receive_locations` VALUES (54, 2, '40', '2018-08-07 02:55:56', 10, '0', '0');
INSERT INTO `receive_locations` VALUES (55, 4, '40', '2018-08-07 02:55:56', 10, '0', '0');
INSERT INTO `receive_locations` VALUES (56, 5, '40', '2018-08-07 02:55:56', 10, '0', '0');
INSERT INTO `receive_locations` VALUES (57, 6, '40', '2018-08-07 02:55:56', 10, '0', '0');
INSERT INTO `receive_locations` VALUES (58, 7, '40', '2018-08-07 02:55:56', 10, '0', '0');
INSERT INTO `receive_locations` VALUES (59, 8, '40', '2018-08-07 02:55:56', 10, '1', '0');
INSERT INTO `receive_locations` VALUES (60, 3, '40', '2018-08-07 02:55:56', 10, '0', '0');
INSERT INTO `receive_locations` VALUES (61, 5, '41', '2018-08-07 05:12:24', 23, '0', '0');
INSERT INTO `receive_locations` VALUES (62, 6, '42', '2018-08-07 05:12:47', 23, '0', '0');
INSERT INTO `receive_locations` VALUES (63, 11, '43', '2018-08-30 12:45:28', 13, '0', '0');
INSERT INTO `receive_locations` VALUES (64, 11, '44', '2018-08-30 12:45:52', 13, '0', '0');
INSERT INTO `receive_locations` VALUES (65, 16, '45', '2018-08-30 12:47:07', 13, '0', '0');
INSERT INTO `receive_locations` VALUES (66, 16, '46', '2018-08-30 12:47:57', 13, '0', '0');
INSERT INTO `receive_locations` VALUES (67, 2, '47', '2018-09-06 04:30:55', 24, '0', '0');
INSERT INTO `receive_locations` VALUES (68, 2, '48', '2018-09-07 03:38:50', 1, '0', '1');
INSERT INTO `receive_locations` VALUES (69, 2, '49', '2018-09-07 04:01:21', 1, '0', '1');
INSERT INTO `receive_locations` VALUES (72, 1, '52', '2018-09-07 04:09:38', 8, '1', '0');
INSERT INTO `receive_locations` VALUES (73, 1, '53', '2018-09-07 04:30:12', 8, '1', '0');
INSERT INTO `receive_locations` VALUES (74, 1, '54', '2018-09-07 04:30:25', 8, '1', '0');
INSERT INTO `receive_locations` VALUES (75, 2, '55', '2018-09-07 08:05:54', 1, '0', '1');
INSERT INTO `receive_locations` VALUES (76, 3, '55', '2018-09-07 08:05:54', 1, '0', '1');
INSERT INTO `receive_locations` VALUES (77, 4, '55', '2018-09-07 08:05:54', 1, '0', '1');
INSERT INTO `receive_locations` VALUES (78, 25, '55', '2018-09-07 08:05:54', 1, '0', '1');
INSERT INTO `receive_locations` VALUES (79, 2, '56', '2018-09-07 08:13:13', 1, '0', '1');
INSERT INTO `receive_locations` VALUES (80, 8, '57', '2018-09-07 09:30:07', 1, '0', '1');
INSERT INTO `receive_locations` VALUES (81, 8, '58', '2018-09-07 09:31:44', 1, '0', '1');
INSERT INTO `receive_locations` VALUES (82, 11, '59', '2018-09-09 17:03:41', 6, '0', '1');
INSERT INTO `receive_locations` VALUES (83, 6, '60', '2018-09-10 06:56:29', 11, '0', '0');
INSERT INTO `receive_locations` VALUES (84, 1, '61', '2018-09-12 08:26:27', 10, '1', '1');
INSERT INTO `receive_locations` VALUES (85, 6, '61', '2018-09-12 08:26:27', 10, '0', '1');
INSERT INTO `receive_locations` VALUES (86, 1, '62', '2018-09-12 08:26:53', 10, '1', '1');
INSERT INTO `receive_locations` VALUES (87, 2, '62', '2018-09-12 08:26:53', 10, '0', '1');
INSERT INTO `receive_locations` VALUES (88, 11, '63', '2018-09-13 05:05:02', 6, '0', '1');
INSERT INTO `receive_locations` VALUES (89, 5, '64', '2018-09-13 05:05:19', 6, '0', '1');
INSERT INTO `receive_locations` VALUES (90, 5, '65', '2018-09-13 05:31:38', 6, '0', '1');
INSERT INTO `receive_locations` VALUES (91, 6, '66', '2018-09-13 14:11:24', 11, '0', '0');
INSERT INTO `receive_locations` VALUES (92, 10, '67', '2018-09-18 02:23:50', 1, '1', '0');
INSERT INTO `receive_locations` VALUES (93, 27, '68', '2018-09-19 02:24:40', 26, '0', '1');
INSERT INTO `receive_locations` VALUES (94, 1, '69', '2018-09-19 04:00:08', 26, '1', '1');
INSERT INTO `receive_locations` VALUES (95, 1, '70', '2018-09-19 04:00:22', 26, '1', '1');
INSERT INTO `receive_locations` VALUES (96, 1, '71', '2018-09-19 04:00:34', 26, '1', '1');
INSERT INTO `receive_locations` VALUES (97, 10, '72', '2018-09-19 04:03:25', 1, '0', '0');
INSERT INTO `receive_locations` VALUES (98, 1, '73', '2018-09-19 06:12:52', 26, '1', '1');
INSERT INTO `receive_locations` VALUES (99, 2, '73', '2018-09-19 06:12:52', 26, '0', '1');
INSERT INTO `receive_locations` VALUES (100, 3, '73', '2018-09-19 06:12:52', 26, '0', '1');
INSERT INTO `receive_locations` VALUES (101, 1, '74', '2018-09-19 06:38:41', 26, '1', '1');
INSERT INTO `receive_locations` VALUES (102, 2, '74', '2018-09-19 06:38:41', 26, '0', '1');
INSERT INTO `receive_locations` VALUES (103, 3, '74', '2018-09-19 06:38:41', 26, '0', '1');
INSERT INTO `receive_locations` VALUES (104, 4, '74', '2018-09-19 06:38:41', 26, '0', '1');
INSERT INTO `receive_locations` VALUES (105, 2, '75', '2018-09-19 06:40:47', 1, '0', '1');
INSERT INTO `receive_locations` VALUES (106, 3, '75', '2018-09-19 06:40:47', 1, '0', '1');
INSERT INTO `receive_locations` VALUES (107, 4, '75', '2018-09-19 06:40:47', 1, '0', '1');
INSERT INTO `receive_locations` VALUES (108, 7, '75', '2018-09-19 06:40:47', 1, '0', '1');
INSERT INTO `receive_locations` VALUES (109, 8, '75', '2018-09-19 06:40:47', 1, '0', '1');
INSERT INTO `receive_locations` VALUES (110, 1, '76', '2018-09-19 06:43:57', 26, '1', '0');
INSERT INTO `receive_locations` VALUES (111, 2, '76', '2018-09-19 06:43:57', 26, '0', '0');
INSERT INTO `receive_locations` VALUES (112, 1, '77', '2018-09-19 06:44:02', 26, '1', '0');
INSERT INTO `receive_locations` VALUES (113, 2, '77', '2018-09-19 06:44:02', 26, '0', '0');
INSERT INTO `receive_locations` VALUES (114, 3, '77', '2018-09-19 06:44:02', 26, '0', '0');
INSERT INTO `receive_locations` VALUES (115, 1, '78', '2018-09-19 06:44:08', 26, '1', '0');
INSERT INTO `receive_locations` VALUES (116, 2, '78', '2018-09-19 06:44:08', 26, '0', '0');
INSERT INTO `receive_locations` VALUES (117, 3, '78', '2018-09-19 06:44:08', 26, '0', '0');
INSERT INTO `receive_locations` VALUES (118, 1, '79', '2018-09-19 06:44:13', 26, '1', '0');
INSERT INTO `receive_locations` VALUES (119, 2, '79', '2018-09-19 06:44:13', 26, '0', '0');
INSERT INTO `receive_locations` VALUES (120, 3, '79', '2018-09-19 06:44:13', 26, '0', '0');
INSERT INTO `receive_locations` VALUES (121, 1, '80', '2018-09-19 07:34:48', 26, '1', '0');
INSERT INTO `receive_locations` VALUES (122, 1, '81', '2018-09-19 07:35:07', 26, '0', '0');
INSERT INTO `receive_locations` VALUES (123, 1, '82', '2018-09-19 07:35:36', 26, '1', '0');
INSERT INTO `receive_locations` VALUES (124, 1, '83', '2018-09-19 09:55:32', 10, '1', '1');
INSERT INTO `receive_locations` VALUES (125, 2, '83', '2018-09-19 09:55:32', 10, '0', '1');
INSERT INTO `receive_locations` VALUES (126, 3, '83', '2018-09-19 09:55:32', 10, '0', '1');
INSERT INTO `receive_locations` VALUES (127, 5, '83', '2018-09-19 09:55:32', 10, '0', '1');
INSERT INTO `receive_locations` VALUES (128, 16, '84', '2018-09-19 15:06:01', 13, '0', '0');
INSERT INTO `receive_locations` VALUES (129, 16, '85', '2018-09-19 15:06:21', 13, '0', '0');
INSERT INTO `receive_locations` VALUES (130, 5, '86', '2018-09-20 01:59:42', 6, '0', '1');
INSERT INTO `receive_locations` VALUES (131, 10, '86', '2018-09-20 01:59:42', 6, '0', '1');
INSERT INTO `receive_locations` VALUES (132, 11, '86', '2018-09-20 01:59:42', 6, '0', '1');
INSERT INTO `receive_locations` VALUES (133, 13, '87', '2018-09-20 02:09:45', 11, '1', '0');
INSERT INTO `receive_locations` VALUES (134, 16, '87', '2018-09-20 02:09:45', 11, '0', '0');
INSERT INTO `receive_locations` VALUES (135, 23, '87', '2018-09-20 02:09:45', 11, '0', '0');
INSERT INTO `receive_locations` VALUES (136, 1, '88', '2018-09-20 02:16:12', 10, '0', '1');
INSERT INTO `receive_locations` VALUES (137, 2, '88', '2018-09-20 02:16:12', 10, '0', '1');
INSERT INTO `receive_locations` VALUES (138, 3, '88', '2018-09-20 02:16:12', 10, '0', '1');
INSERT INTO `receive_locations` VALUES (139, 4, '88', '2018-09-20 02:16:12', 10, '0', '1');
INSERT INTO `receive_locations` VALUES (140, 12, '89', '2018-09-20 11:14:37', 13, '0', '0');
INSERT INTO `receive_locations` VALUES (141, 3, '90', '2018-09-24 09:42:53', 6, '0', '1');
INSERT INTO `receive_locations` VALUES (142, 4, '90', '2018-09-24 09:42:53', 6, '0', '1');
INSERT INTO `receive_locations` VALUES (143, 5, '90', '2018-09-24 09:42:53', 6, '0', '1');
INSERT INTO `receive_locations` VALUES (144, 8, '90', '2018-09-24 09:42:53', 6, '0', '1');
INSERT INTO `receive_locations` VALUES (145, 4, '91', '2018-09-24 09:46:09', 6, '0', '0');
INSERT INTO `receive_locations` VALUES (146, 3, '92', '2018-09-24 09:56:52', 6, '0', '1');
INSERT INTO `receive_locations` VALUES (147, 4, '92', '2018-09-24 09:56:52', 6, '0', '0');
INSERT INTO `receive_locations` VALUES (148, 5, '92', '2018-09-24 09:56:52', 6, '0', '0');
INSERT INTO `receive_locations` VALUES (149, 7, '92', '2018-09-24 09:56:52', 6, '0', '0');
INSERT INTO `receive_locations` VALUES (150, 3, '93', '2018-09-24 09:57:03', 6, '0', '1');
INSERT INTO `receive_locations` VALUES (151, 3, '94', '2018-09-24 09:57:18', 6, '0', '0');
INSERT INTO `receive_locations` VALUES (152, 3, '95', '2018-09-24 09:57:39', 6, '0', '1');
INSERT INTO `receive_locations` VALUES (153, 3, '96', '2018-09-24 09:57:46', 6, '0', '1');
INSERT INTO `receive_locations` VALUES (154, 3, '97', '2018-09-24 09:58:25', 6, '0', '0');
INSERT INTO `receive_locations` VALUES (155, 4, '98', '2018-09-24 10:05:40', 1, '0', '0');
INSERT INTO `receive_locations` VALUES (156, 8, '98', '2018-09-24 10:05:40', 1, '0', '0');
INSERT INTO `receive_locations` VALUES (157, 4, '99', '2018-09-24 10:05:44', 1, '0', '0');
INSERT INTO `receive_locations` VALUES (158, 4, '100', '2018-09-24 10:05:49', 1, '0', '0');
INSERT INTO `receive_locations` VALUES (159, 8, '101', '2018-09-24 10:06:43', 1, '0', '0');
INSERT INTO `receive_locations` VALUES (160, 5, '102', '2018-09-24 10:16:57', 6, '0', '1');
INSERT INTO `receive_locations` VALUES (161, 11, '103', '2018-09-24 10:35:21', 6, '0', '0');
INSERT INTO `receive_locations` VALUES (162, 12, '103', '2018-09-24 10:35:21', 6, '0', '0');
INSERT INTO `receive_locations` VALUES (163, 13, '103', '2018-09-24 10:35:21', 6, '0', '0');
INSERT INTO `receive_locations` VALUES (164, 14, '103', '2018-09-24 10:35:21', 6, '0', '0');
INSERT INTO `receive_locations` VALUES (165, 23, '103', '2018-09-24 10:35:21', 6, '0', '0');
INSERT INTO `receive_locations` VALUES (166, 11, '104', '2018-09-24 10:35:53', 6, '0', '0');
INSERT INTO `receive_locations` VALUES (167, 10, '105', '2018-09-25 03:48:26', 1, '0', '0');
INSERT INTO `receive_locations` VALUES (168, 10, '106', '2018-09-25 03:57:07', 1, '0', '0');
INSERT INTO `receive_locations` VALUES (169, 10, '107', '2018-09-25 04:14:23', 1, '0', '0');
INSERT INTO `receive_locations` VALUES (170, 1, '108', '2018-09-26 10:11:06', 11, '1', '0');
INSERT INTO `receive_locations` VALUES (171, 3, '108', '2018-09-26 10:11:06', 11, '0', '0');
INSERT INTO `receive_locations` VALUES (172, 5, '108', '2018-09-26 10:11:06', 11, '0', '0');

-- --------------------------------------------------------

-- 
-- Table structure for table `receive_mail`
-- 

CREATE TABLE `receive_mail` (
  `receive_mail_id` int(11) NOT NULL AUTO_INCREMENT,
  `receiver_user_id` varchar(50) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `mail_id` int(11) NOT NULL,
  `receive_mail_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `staus` tinyint(5) NOT NULL,
  `receiver_mail` varchar(100) NOT NULL,
  `rece_delete_status` enum('0','1') NOT NULL DEFAULT '0',
  `remove_sended` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`receive_mail_id`)
) ENGINE=InnoDB AUTO_INCREMENT=138 DEFAULT CHARSET=latin1 AUTO_INCREMENT=138 ;

-- 
-- Dumping data for table `receive_mail`
-- 

INSERT INTO `receive_mail` VALUES (1, '7', 1, 1, '2018-07-30 01:19:18', 0, 'raj1@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (2, '1', 7, 2, '2018-07-30 01:49:29', 0, 'pankaj.alobha@gmail.com', '1', '0');
INSERT INTO `receive_mail` VALUES (3, '7', 7, 3, '2018-07-30 03:44:21', 0, 'raj1@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (4, '7', 7, 4, '2018-07-30 03:48:57', 0, 'raj1@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (5, '7', 7, 5, '2018-07-30 03:58:49', 0, 'raj1@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (6, '2', 21, 6, '2018-07-31 06:31:11', 0, 'pankaj.sharma@alobhatechnologies.com', '0', '0');
INSERT INTO `receive_mail` VALUES (7, '1', 21, 7, '2018-07-31 06:31:46', 0, 'pankaj.alobha@gmail.com', '1', '0');
INSERT INTO `receive_mail` VALUES (8, '3', 21, 8, '2018-07-31 06:32:26', 0, 'raj@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (9, '1', 21, 9, '2018-07-31 06:32:44', 0, 'pankaj.alobha@gmail.com', '1', '0');
INSERT INTO `receive_mail` VALUES (10, '1', 20, 10, '2018-07-31 07:38:19', 0, 'pankaj.alobha@gmail.com', '1', '0');
INSERT INTO `receive_mail` VALUES (11, '20', 21, 11, '2018-07-31 07:39:05', 0, 'kumar@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (12, '21', 20, 12, '2018-07-31 07:39:21', 0, 'a1@a.com', '0', '0');
INSERT INTO `receive_mail` VALUES (13, '20', 21, 13, '2018-07-31 07:39:50', 0, 'kumar@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (14, '20', 21, 14, '2018-07-31 07:40:25', 0, 'kumar@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (15, '12', 13, 15, '2018-08-02 17:36:06', 0, 'john@espretech.com', '0', '0');
INSERT INTO `receive_mail` VALUES (16, '1', 10, 16, '2018-08-07 02:55:07', 0, 'pankaj.alobha@gmail.com', '1', '0');
INSERT INTO `receive_mail` VALUES (17, '13', 12, 17, '2018-08-08 00:04:35', 0, 'ev.intlrail@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (18, '11', 13, 18, '2018-08-30 12:44:39', 0, 'sachinsiwal@gmail.com', '1', '0');
INSERT INTO `receive_mail` VALUES (19, '13', 16, 19, '2018-08-30 12:45:51', 0, 'ev.intlrail@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (20, '2', 1, 20, '2018-09-04 02:50:28', 0, 'pankaj.sharma@alobhatechnologies.com', '0', '0');
INSERT INTO `receive_mail` VALUES (21, '3', 1, 21, '2018-09-04 02:50:52', 0, 'raj@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (22, '10', 1, 21, '2018-09-04 02:50:52', 0, 'roHit.alobha@gmail.com', '1', '0');
INSERT INTO `receive_mail` VALUES (23, '2', 1, 22, '2018-09-07 08:01:33', 0, 'pankaj.sharma@alobhatechnologies.com', '0', '0');
INSERT INTO `receive_mail` VALUES (24, '3', 1, 22, '2018-09-07 08:01:34', 0, 'raj@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (25, '4', 1, 22, '2018-09-07 08:01:34', 0, 'ra@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (26, '25', 1, 22, '2018-09-07 08:01:34', 0, 'ather.rashid@alobhatechnologies.com', '0', '0');
INSERT INTO `receive_mail` VALUES (27, '1', 10, 23, '2018-09-12 08:27:35', 0, 'pankaj.alobha@gmail.com', '1', '0');
INSERT INTO `receive_mail` VALUES (28, '5', 10, 23, '2018-09-12 08:27:35', 0, 'ankita.dhaka09@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (29, '1', 27, 24, '2018-09-12 08:47:49', 0, 'pankaj.alobha@gmail.com', '1', '0');
INSERT INTO `receive_mail` VALUES (30, '1', 27, 25, '2018-09-12 08:50:47', 0, 'pankaj.alobha@gmail.com', '1', '0');
INSERT INTO `receive_mail` VALUES (31, '26', 27, 26, '2018-09-12 08:55:14', 0, 'p1@gmail.com', '1', '0');
INSERT INTO `receive_mail` VALUES (32, '26', 27, 27, '2018-09-12 08:56:06', 0, 'p1@gmail.com', '1', '0');
INSERT INTO `receive_mail` VALUES (33, '27', 26, 28, '2018-09-12 09:04:39', 0, 'p2@gmail.com', '1', '0');
INSERT INTO `receive_mail` VALUES (34, '27', 26, 29, '2018-09-12 09:05:00', 0, 'p2@gmail.com', '1', '0');
INSERT INTO `receive_mail` VALUES (35, '27', 26, 30, '2018-09-12 09:09:42', 0, 'p2@gmail.com', '1', '0');
INSERT INTO `receive_mail` VALUES (36, '12', 11, 31, '2018-09-18 02:08:07', 0, 'john@espretech.com', '0', '0');
INSERT INTO `receive_mail` VALUES (37, '13', 11, 31, '2018-09-18 02:08:08', 0, 'ev.intlrail@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (38, '16', 11, 31, '2018-09-18 02:08:08', 0, 'sachinsiwa@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (39, '23', 11, 31, '2018-09-18 02:08:08', 0, 'rashi@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (40, '10', 1, 32, '2018-09-18 02:19:06', 0, 'roHit.alobha@gmail.com', '1', '0');
INSERT INTO `receive_mail` VALUES (41, '1', 10, 33, '2018-09-18 09:28:42', 0, 'pankaj.alobha@gmail.com', '1', '0');
INSERT INTO `receive_mail` VALUES (42, '1', 10, 34, '2018-09-18 09:31:07', 0, 'pankaj.alobha@gmail.com', '1', '0');
INSERT INTO `receive_mail` VALUES (43, '2', 10, 34, '2018-09-18 09:31:07', 0, 'pankaj.sharma@alobhatechnologies.com', '0', '0');
INSERT INTO `receive_mail` VALUES (44, '23', 11, 35, '2018-09-18 10:10:16', 0, 'rashi@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (45, '1', 26, 36, '2018-09-19 04:26:42', 0, 'pankaj.alobha@gmail.com', '1', '0');
INSERT INTO `receive_mail` VALUES (46, '2', 10, 37, '2018-09-19 05:28:59', 0, 'pankaj.sharma@alobhatechnologies.com', '0', '0');
INSERT INTO `receive_mail` VALUES (47, '5', 10, 37, '2018-09-19 05:28:59', 0, 'ankita.dhaka09@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (48, '6', 10, 37, '2018-09-19 05:29:00', 0, 'ankita.siwal09@gmail.com', '1', '0');
INSERT INTO `receive_mail` VALUES (49, '4', 11, 38, '2018-09-19 05:32:12', 0, 'ra@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (50, '5', 11, 38, '2018-09-19 05:32:12', 0, 'ankita.dhaka09@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (51, '23', 11, 38, '2018-09-19 05:32:13', 0, 'rashi@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (52, '2', 11, 39, '2018-09-19 05:32:56', 0, 'pankaj.sharma@alobhatechnologies.com', '0', '0');
INSERT INTO `receive_mail` VALUES (53, '3', 11, 39, '2018-09-19 05:32:56', 0, 'raj@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (54, '4', 11, 39, '2018-09-19 05:32:56', 0, 'ra@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (55, '5', 11, 39, '2018-09-19 05:32:56', 0, 'ankita.dhaka09@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (56, '13', 11, 39, '2018-09-19 05:32:56', 0, 'ev.intlrail@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (57, '16', 11, 39, '2018-09-19 05:32:56', 0, 'sachinsiwa@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (58, '23', 11, 39, '2018-09-19 05:32:57', 0, 'rashi@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (59, '1', 10, 40, '2018-09-19 05:33:28', 0, 'pankaj.alobha@gmail.com', '1', '0');
INSERT INTO `receive_mail` VALUES (60, '2', 10, 40, '2018-09-19 05:33:29', 0, 'pankaj.sharma@alobhatechnologies.com', '0', '0');
INSERT INTO `receive_mail` VALUES (61, '3', 10, 40, '2018-09-19 05:33:29', 0, 'raj@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (62, '4', 10, 40, '2018-09-19 05:33:29', 0, 'ra@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (63, '1', 10, 41, '2018-09-19 05:34:48', 0, 'pankaj.alobha@gmail.com', '1', '0');
INSERT INTO `receive_mail` VALUES (64, '2', 10, 41, '2018-09-19 05:34:48', 0, 'pankaj.sharma@alobhatechnologies.com', '0', '0');
INSERT INTO `receive_mail` VALUES (65, '3', 10, 41, '2018-09-19 05:34:48', 0, 'raj@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (66, '4', 10, 41, '2018-09-19 05:34:49', 0, 'ra@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (67, '1', 26, 42, '2018-09-19 05:36:32', 0, 'pankaj.alobha@gmail.com', '1', '0');
INSERT INTO `receive_mail` VALUES (68, '2', 26, 42, '2018-09-19 05:36:32', 0, 'pankaj.sharma@alobhatechnologies.com', '0', '0');
INSERT INTO `receive_mail` VALUES (69, '3', 26, 42, '2018-09-19 05:36:32', 0, 'raj@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (70, '4', 26, 42, '2018-09-19 05:36:32', 0, 'ra@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (71, '1', 11, 43, '2018-09-19 05:37:11', 0, 'pankaj.alobha@gmail.com', '1', '0');
INSERT INTO `receive_mail` VALUES (72, '2', 11, 43, '2018-09-19 05:37:11', 0, 'pankaj.sharma@alobhatechnologies.com', '0', '0');
INSERT INTO `receive_mail` VALUES (73, '3', 11, 43, '2018-09-19 05:37:11', 0, 'raj@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (74, '16', 11, 43, '2018-09-19 05:37:11', 0, 'sachinsiwa@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (75, '23', 11, 43, '2018-09-19 05:37:11', 0, 'rashi@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (76, '1', 26, 44, '2018-09-19 05:39:46', 0, 'pankaj.alobha@gmail.com', '1', '0');
INSERT INTO `receive_mail` VALUES (77, '1', 26, 45, '2018-09-19 05:40:05', 0, 'pankaj.alobha@gmail.com', '1', '0');
INSERT INTO `receive_mail` VALUES (78, '2', 26, 45, '2018-09-19 05:40:06', 0, 'pankaj.sharma@alobhatechnologies.com', '0', '0');
INSERT INTO `receive_mail` VALUES (79, '2', 1, 46, '2018-09-19 07:28:53', 0, 'pankaj.sharma@alobhatechnologies.com', '0', '1');
INSERT INTO `receive_mail` VALUES (80, '3', 1, 46, '2018-09-19 07:28:53', 0, 'raj@gmail.com', '0', '1');
INSERT INTO `receive_mail` VALUES (81, '4', 1, 46, '2018-09-19 07:28:53', 0, 'ra@gmail.com', '0', '1');
INSERT INTO `receive_mail` VALUES (82, '5', 1, 46, '2018-09-19 07:28:53', 0, 'ankita.dhaka09@gmail.com', '0', '1');
INSERT INTO `receive_mail` VALUES (83, '1', 26, 47, '2018-09-19 07:31:59', 0, 'pankaj.alobha@gmail.com', '1', '1');
INSERT INTO `receive_mail` VALUES (84, '2', 26, 47, '2018-09-19 07:31:59', 0, 'pankaj.sharma@alobhatechnologies.com', '0', '1');
INSERT INTO `receive_mail` VALUES (85, '3', 26, 47, '2018-09-19 07:31:59', 0, 'raj@gmail.com', '0', '1');
INSERT INTO `receive_mail` VALUES (86, '4', 26, 47, '2018-09-19 07:31:59', 0, 'ra@gmail.com', '0', '1');
INSERT INTO `receive_mail` VALUES (87, '1', 26, 48, '2018-09-19 07:38:48', 0, 'pankaj.alobha@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (88, '1', 26, 49, '2018-09-19 07:39:06', 0, 'pankaj.alobha@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (89, '2', 26, 49, '2018-09-19 07:39:07', 0, 'pankaj.sharma@alobhatechnologies.com', '0', '0');
INSERT INTO `receive_mail` VALUES (90, '3', 26, 49, '2018-09-19 07:39:07', 0, 'raj@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (91, '1', 10, 50, '2018-09-19 09:53:38', 0, 'pankaj.alobha@gmail.com', '0', '1');
INSERT INTO `receive_mail` VALUES (92, '2', 10, 50, '2018-09-19 09:53:38', 0, 'pankaj.sharma@alobhatechnologies.com', '0', '1');
INSERT INTO `receive_mail` VALUES (93, '3', 10, 50, '2018-09-19 09:53:38', 0, 'raj@gmail.com', '0', '1');
INSERT INTO `receive_mail` VALUES (94, '6', 10, 50, '2018-09-19 09:53:38', 0, 'ankita.siwal09@gmail.com', '0', '1');
INSERT INTO `receive_mail` VALUES (95, '7', 10, 50, '2018-09-19 09:53:39', 0, 'raj1@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (96, '1', 11, 51, '2018-09-19 12:04:08', 0, 'pankaj.alobha@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (97, '2', 11, 51, '2018-09-19 12:04:08', 0, 'pankaj.sharma@alobhatechnologies.com', '0', '0');
INSERT INTO `receive_mail` VALUES (98, '5', 11, 51, '2018-09-19 12:04:09', 0, 'ankita.dhaka09@gmail.com', '0', '1');
INSERT INTO `receive_mail` VALUES (99, '7', 11, 51, '2018-09-19 12:04:09', 0, 'raj1@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (100, '12', 11, 51, '2018-09-19 12:04:09', 0, 'john@espretech.com', '0', '0');
INSERT INTO `receive_mail` VALUES (101, '13', 11, 51, '2018-09-19 12:04:09', 0, 'ev.intlrail@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (102, '11', 13, 52, '2018-09-19 15:10:03', 0, 'sachinsiwal@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (103, '16', 13, 53, '2018-09-19 15:13:09', 0, 'sachinsiwa@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (104, '13', 11, 54, '2018-09-19 15:33:57', 0, 'ev.intlrail@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (105, '11', 13, 55, '2018-09-19 15:35:36', 0, 'sachinsiwal@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (106, '13', 11, 56, '2018-09-19 15:40:36', 0, 'ev.intlrail@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (107, '13', 11, 57, '2018-09-19 15:46:47', 0, 'ev.intlrail@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (108, '11', 13, 58, '2018-09-19 15:49:05', 0, 'sachinsiwal@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (109, '13', 11, 59, '2018-09-19 15:51:44', 0, 'ev.intlrail@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (110, '5', 6, 60, '2018-09-20 02:02:26', 0, 'ankita.dhaka09@gmail.com', '0', '1');
INSERT INTO `receive_mail` VALUES (111, '10', 6, 60, '2018-09-20 02:02:26', 0, 'roHit.alobha@gmail.com', '0', '1');
INSERT INTO `receive_mail` VALUES (112, '11', 6, 60, '2018-09-20 02:02:26', 0, 'sachinsiwal@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (113, '3', 6, 61, '2018-09-24 10:39:41', 0, 'raj@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (114, '4', 6, 61, '2018-09-24 10:39:41', 0, 'ra@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (115, '5', 6, 61, '2018-09-24 10:39:41', 0, 'ankita.dhaka09@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (116, '1', 6, 62, '2018-09-24 10:44:04', 0, 'pankaj.alobha@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (117, '2', 6, 62, '2018-09-24 10:44:04', 0, 'pankaj.sharma@alobhatechnologies.com', '0', '0');
INSERT INTO `receive_mail` VALUES (118, '3', 6, 62, '2018-09-24 10:44:04', 0, 'raj@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (119, '1', 6, 63, '2018-09-25 02:31:18', 0, 'pankaj.alobha@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (120, '3', 6, 63, '2018-09-25 02:31:18', 0, 'raj@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (121, '4', 6, 63, '2018-09-25 02:31:18', 0, 'ra@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (122, '1', 10, 64, '2018-09-25 03:25:01', 0, 'pankaj.alobha@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (123, '2', 10, 64, '2018-09-25 03:25:01', 0, 'pankaj.sharma@alobhatechnologies.com', '0', '0');
INSERT INTO `receive_mail` VALUES (124, '3', 1, 65, '2018-09-25 03:28:22', 0, 'raj@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (125, '5', 1, 65, '2018-09-25 03:28:22', 0, 'ankita.dhaka09@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (126, '10', 1, 65, '2018-09-25 03:28:22', 0, 'roHit.alobha@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (127, '2', 1, 66, '2018-09-25 03:30:41', 0, 'pankaj.sharma@alobhatechnologies.com', '0', '0');
INSERT INTO `receive_mail` VALUES (128, '1', 10, 67, '2018-09-25 03:38:41', 0, 'pankaj.alobha@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (129, '10', 1, 68, '2018-09-25 03:39:51', 0, 'roHit.alobha@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (130, '10', 1, 69, '2018-09-25 05:42:39', 0, 'roHit.alobha@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (131, '10', 1, 70, '2018-09-25 10:08:56', 0, 'roHit.alobha@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (132, '8', 10, 71, '2018-09-25 10:09:25', 0, 'raj2@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (133, '1', 6, 72, '2018-09-25 10:15:19', 0, 'pankaj.alobha@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (134, '4', 6, 72, '2018-09-25 10:15:20', 0, 'ra@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (135, '5', 6, 72, '2018-09-25 10:15:20', 0, 'ankita.dhaka09@gmail.com', '0', '1');
INSERT INTO `receive_mail` VALUES (136, '10', 6, 72, '2018-09-25 10:15:20', 0, 'roHit.alobha@gmail.com', '0', '0');
INSERT INTO `receive_mail` VALUES (137, '11', 6, 72, '2018-09-25 10:15:20', 0, 'sachinsiwal@gmail.com', '0', '1');

-- --------------------------------------------------------

-- 
-- Table structure for table `receive_message_user`
-- 

CREATE TABLE `receive_message_user` (
  `receive_messge_id` int(12) NOT NULL AUTO_INCREMENT,
  `receive_user_id` varchar(500) NOT NULL,
  `senduser_id` varchar(100) NOT NULL,
  `group_id` varchar(100) NOT NULL,
  `rece_delete_status` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`receive_messge_id`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=latin1 AUTO_INCREMENT=77 ;

-- 
-- Dumping data for table `receive_message_user`
-- 

INSERT INTO `receive_message_user` VALUES (1, '9', '7', '1', '0');
INSERT INTO `receive_message_user` VALUES (2, '1', '9', '2', '0');
INSERT INTO `receive_message_user` VALUES (3, '3', '1', '3', '0');
INSERT INTO `receive_message_user` VALUES (4, '7', '1', '3', '0');
INSERT INTO `receive_message_user` VALUES (5, '4', '1', '4', '0');
INSERT INTO `receive_message_user` VALUES (6, '8', '1', '4', '0');
INSERT INTO `receive_message_user` VALUES (7, '9', '1', '4', '0');
INSERT INTO `receive_message_user` VALUES (8, '10', '1', '4', '0');
INSERT INTO `receive_message_user` VALUES (9, '11', '1', '4', '0');
INSERT INTO `receive_message_user` VALUES (10, '1', '19', '5', '0');
INSERT INTO `receive_message_user` VALUES (11, '2', '19', '5', '0');
INSERT INTO `receive_message_user` VALUES (12, '3', '19', '5', '0');
INSERT INTO `receive_message_user` VALUES (13, '1', '21', '6', '0');
INSERT INTO `receive_message_user` VALUES (14, '19', '21', '7', '0');
INSERT INTO `receive_message_user` VALUES (15, '5', '22', '8', '0');
INSERT INTO `receive_message_user` VALUES (16, '11', '22', '8', '0');
INSERT INTO `receive_message_user` VALUES (17, '16', '22', '8', '0');
INSERT INTO `receive_message_user` VALUES (18, '12', '13', '9', '0');
INSERT INTO `receive_message_user` VALUES (19, '12', '13', '10', '0');
INSERT INTO `receive_message_user` VALUES (20, '16', '13', '10', '0');
INSERT INTO `receive_message_user` VALUES (21, '5', '23', '11', '0');
INSERT INTO `receive_message_user` VALUES (22, '6', '23', '11', '0');
INSERT INTO `receive_message_user` VALUES (23, '11', '23', '11', '0');
INSERT INTO `receive_message_user` VALUES (24, '11', '23', '12', '0');
INSERT INTO `receive_message_user` VALUES (25, '13', '23', '12', '0');
INSERT INTO `receive_message_user` VALUES (26, '12', '16', '13', '0');
INSERT INTO `receive_message_user` VALUES (27, '13', '16', '13', '0');
INSERT INTO `receive_message_user` VALUES (28, '23', '16', '13', '0');
INSERT INTO `receive_message_user` VALUES (29, '11', '13', '14', '0');
INSERT INTO `receive_message_user` VALUES (30, '13', '16', '15', '0');
INSERT INTO `receive_message_user` VALUES (31, '1', '8', '16', '0');
INSERT INTO `receive_message_user` VALUES (32, '1', '8', '17', '0');
INSERT INTO `receive_message_user` VALUES (33, '2', '1', '18', '0');
INSERT INTO `receive_message_user` VALUES (34, '3', '1', '18', '0');
INSERT INTO `receive_message_user` VALUES (35, '4', '1', '18', '0');
INSERT INTO `receive_message_user` VALUES (36, '8', '1', '18', '0');
INSERT INTO `receive_message_user` VALUES (37, '2', '1', '19', '0');
INSERT INTO `receive_message_user` VALUES (38, '3', '1', '19', '0');
INSERT INTO `receive_message_user` VALUES (39, '4', '1', '19', '0');
INSERT INTO `receive_message_user` VALUES (40, '26', '27', '20', '0');
INSERT INTO `receive_message_user` VALUES (41, '12', '28', '21', '0');
INSERT INTO `receive_message_user` VALUES (42, '27', '26', '22', '0');
INSERT INTO `receive_message_user` VALUES (43, '1', '10', '23', '0');
INSERT INTO `receive_message_user` VALUES (44, '2', '10', '23', '0');
INSERT INTO `receive_message_user` VALUES (45, '1', '27', '24', '0');
INSERT INTO `receive_message_user` VALUES (46, '1', '27', '25', '0');
INSERT INTO `receive_message_user` VALUES (47, '26', '27', '26', '0');
INSERT INTO `receive_message_user` VALUES (48, '26', '27', '27', '0');
INSERT INTO `receive_message_user` VALUES (49, '26', '27', '28', '0');
INSERT INTO `receive_message_user` VALUES (50, '26', '27', '29', '0');
INSERT INTO `receive_message_user` VALUES (51, '26', '27', '30', '0');
INSERT INTO `receive_message_user` VALUES (52, '26', '27', '31', '0');
INSERT INTO `receive_message_user` VALUES (53, '10', '1', '32', '0');
INSERT INTO `receive_message_user` VALUES (54, '1', '26', '33', '0');
INSERT INTO `receive_message_user` VALUES (55, '1', '10', '34', '0');
INSERT INTO `receive_message_user` VALUES (56, '6', '10', '34', '0');
INSERT INTO `receive_message_user` VALUES (57, '7', '10', '34', '0');
INSERT INTO `receive_message_user` VALUES (58, '1', '10', '35', '0');
INSERT INTO `receive_message_user` VALUES (59, '2', '10', '35', '0');
INSERT INTO `receive_message_user` VALUES (60, '4', '10', '35', '0');
INSERT INTO `receive_message_user` VALUES (61, '5', '10', '35', '0');
INSERT INTO `receive_message_user` VALUES (62, '16', '13', '36', '0');
INSERT INTO `receive_message_user` VALUES (63, '13', '11', '37', '0');
INSERT INTO `receive_message_user` VALUES (64, '12', '29', '38', '0');
INSERT INTO `receive_message_user` VALUES (65, '1', '6', '39', '0');
INSERT INTO `receive_message_user` VALUES (66, '4', '6', '39', '0');
INSERT INTO `receive_message_user` VALUES (67, '1', '6', '40', '0');
INSERT INTO `receive_message_user` VALUES (68, '2', '6', '40', '0');
INSERT INTO `receive_message_user` VALUES (69, '3', '6', '40', '0');
INSERT INTO `receive_message_user` VALUES (70, '4', '6', '40', '0');
INSERT INTO `receive_message_user` VALUES (71, '1', '10', '41', '0');
INSERT INTO `receive_message_user` VALUES (72, '2', '10', '41', '0');
INSERT INTO `receive_message_user` VALUES (73, '3', '6', '42', '0');
INSERT INTO `receive_message_user` VALUES (74, '4', '6', '42', '0');
INSERT INTO `receive_message_user` VALUES (75, '2', '31', '43', '0');
INSERT INTO `receive_message_user` VALUES (76, '1', '32', '44', '0');

-- --------------------------------------------------------

-- 
-- Table structure for table `send_files`
-- 

CREATE TABLE `send_files` (
  `sender_file_id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_id` int(11) NOT NULL,
  `type` varchar(50) NOT NULL,
  `file` varchar(300) NOT NULL,
  `send_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `staus` tinyint(5) NOT NULL,
  `isSecure` int(5) NOT NULL,
  `is_deleted` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`sender_file_id`)
) ENGINE=InnoDB AUTO_INCREMENT=117 DEFAULT CHARSET=latin1 AUTO_INCREMENT=117 ;

-- 
-- Dumping data for table `send_files`
-- 

INSERT INTO `send_files` VALUES (1, 1, 'video', '1531296434VID_20180710_163301.mp4', '2018-07-11 04:07:14', 0, 0, '0');
INSERT INTO `send_files` VALUES (2, 1, 'other', '1531296480pdf.pdf', '2018-07-11 04:08:00', 0, 0, '1');
INSERT INTO `send_files` VALUES (3, 1, 'audio', '1531296507DS_BassB140D-01.mp3', '2018-07-11 04:08:27', 0, 0, '1');
INSERT INTO `send_files` VALUES (4, 3, 'other', '1531296814pdf.pdf', '2018-07-11 04:13:34', 0, 0, '0');
INSERT INTO `send_files` VALUES (5, 3, 'other', '15312968771530885795852.jpg', '2018-07-11 04:14:37', 0, 0, '0');
INSERT INTO `send_files` VALUES (6, 3, 'video', '1531296959VID_20180710_163301.mp4', '2018-07-11 04:15:59', 0, 0, '0');
INSERT INTO `send_files` VALUES (7, 3, 'video/image', '15312975751530884152200.jpg', '2018-07-11 04:26:15', 0, 0, '0');
INSERT INTO `send_files` VALUES (8, 1, 'video/image', '1531301036VID_20180710_163301.mp4', '2018-07-11 05:23:56', 0, 0, '1');
INSERT INTO `send_files` VALUES (9, 3, 'video/image', '15313098041530275250953.mp4', '2018-07-11 07:50:04', 0, 0, '0');
INSERT INTO `send_files` VALUES (10, 3, 'audio', '15313099321523097821.mp3', '2018-07-11 07:52:12', 0, 0, '0');
INSERT INTO `send_files` VALUES (11, 3, 'other', '1531318571Screenshot_20180704-171446.png', '2018-07-11 10:16:11', 0, 0, '0');
INSERT INTO `send_files` VALUES (12, 5, 'other', '1531319984Screenshot_2018-07-11-19-57-19-89.png', '2018-07-11 10:39:44', 0, 0, '0');
INSERT INTO `send_files` VALUES (13, 6, 'other', '1531321454IMG_20180711_203311.jpg', '2018-07-11 11:04:14', 0, 0, '0');
INSERT INTO `send_files` VALUES (14, 8, 'other', '1531324791Screenshot_20180704-171446.png', '2018-07-11 11:59:51', 0, 0, '0');
INSERT INTO `send_files` VALUES (15, 8, 'video/image', '15313248151530893864330.jpg', '2018-07-11 12:00:15', 0, 0, '0');
INSERT INTO `send_files` VALUES (16, 8, 'video/image', '1531324901VID_20180710_163301.mp4', '2018-07-11 12:01:41', 0, 0, '0');
INSERT INTO `send_files` VALUES (17, 8, 'audio', '1531324954DS_BassB140D-01.mp3', '2018-07-11 12:02:34', 0, 0, '0');
INSERT INTO `send_files` VALUES (18, 7, 'video/image', '1531373037VID_20180710_163301.mp4', '2018-07-12 01:23:57', 0, 0, '0');
INSERT INTO `send_files` VALUES (19, 7, 'audio', '1531376041DS_BassB140D-01.mp3', '2018-07-12 02:14:01', 0, 0, '0');
INSERT INTO `send_files` VALUES (20, 10, 'video/image', '1531377818VID-20180711-WA0009.mp4', '2018-07-12 02:43:38', 0, 0, '0');
INSERT INTO `send_files` VALUES (21, 7, 'audio', '1531377972DS_BassB140D-01.mp3', '2018-07-12 02:46:12', 0, 0, '0');
INSERT INTO `send_files` VALUES (22, 7, 'other', '1531382565pdf.pdf', '2018-07-12 04:02:45', 0, 0, '0');
INSERT INTO `send_files` VALUES (23, 7, 'video/image', '15313825971530893864330.jpg', '2018-07-12 04:03:17', 0, 0, '0');
INSERT INTO `send_files` VALUES (24, 7, 'audio', '1531382625DS_BassB140D-01.mp3', '2018-07-12 04:03:45', 0, 0, '0');
INSERT INTO `send_files` VALUES (25, 6, 'video/image', '1531413829VID-20180712-WA0009.mp4', '2018-07-12 12:43:50', 0, 0, '0');
INSERT INTO `send_files` VALUES (26, 13, 'other', '1531442518Screenshot_2018-06-08-18-24-38.png', '2018-07-12 20:41:58', 0, 0, '0');
INSERT INTO `send_files` VALUES (27, 4, 'other', 'wmXk3Rt7845RwJzgHcMJOz0hheBYAcoT152QfMkvObWbaOW94ktgYPn7jzFsaFaE', '2018-07-16 03:56:44', 0, 1, '0');
INSERT INTO `send_files` VALUES (28, 16, 'audio', '1535647866hangouts_incoming_call.ogg', '2018-08-30 12:51:06', 0, 0, '0');
INSERT INTO `send_files` VALUES (29, 1, 'video/image', '1536040107VID-20180827-WA0000.mp4', '2018-09-04 01:48:28', 0, 0, '1');
INSERT INTO `send_files` VALUES (30, 1, 'other', '1536040604Sri_Hanuman_Chalisa_Hindi.pdf', '2018-09-04 01:56:44', 0, 0, '1');
INSERT INTO `send_files` VALUES (31, 1, 'audio', 'hgdsfvjdshfbvjdfb.amr', '2018-09-04 01:57:38', 0, 0, '1');
INSERT INTO `send_files` VALUES (32, 1, 'other', '1536040946Sri_Hanuman_Chalisa_Hindi.pdf', '2018-09-04 02:02:26', 0, 0, '1');
INSERT INTO `send_files` VALUES (33, 1, 'video/image', '1536040984VID-20180831-WA0001.mp4', '2018-09-04 02:03:04', 0, 0, '1');
INSERT INTO `send_files` VALUES (34, 1, 'audio', 'cxdfhgbfhgfjhgj.mp3', '2018-09-04 02:03:39', 0, 0, '1');
INSERT INTO `send_files` VALUES (35, 1, 'other', '1536044177Sri_Hanuman_Chalisa_Hindi.pdf', '2018-09-04 02:56:17', 0, 0, '1');
INSERT INTO `send_files` VALUES (36, 1, 'video/image', '1536044201IMG-20180904-WA0001.jpg', '2018-09-04 02:56:41', 0, 0, '1');
INSERT INTO `send_files` VALUES (37, 1, 'video/image', '1536044241VID-20180903-WA0000.mp4', '2018-09-04 02:57:21', 0, 0, '1');
INSERT INTO `send_files` VALUES (38, 23, 'other', '1536157215conditional-tense.pdf', '2018-09-05 10:20:15', 0, 0, '0');
INSERT INTO `send_files` VALUES (39, 23, 'video/image', '1536157261VID-20180902-WA0013.mp4', '2018-09-05 10:21:01', 0, 0, '0');
INSERT INTO `send_files` VALUES (40, 1, 'other', '1536320387pdf.pdf', '2018-09-07 07:39:47', 0, 0, '1');
INSERT INTO `send_files` VALUES (41, 1, 'other', '1536320520pdf.pdf', '2018-09-07 07:42:00', 0, 0, '1');
INSERT INTO `send_files` VALUES (42, 1, 'video/image', '1536326889images(24).jpg', '2018-09-07 09:28:09', 0, 0, '1');
INSERT INTO `send_files` VALUES (43, 27, 'other', '1536652721cachedtv_ALL_all.txt', '2018-09-11 03:58:41', 0, 0, '0');
INSERT INTO `send_files` VALUES (44, 27, 'other', '1536652838cachedtv_ALL_all.txt', '2018-09-11 04:00:38', 0, 0, '0');
INSERT INTO `send_files` VALUES (45, 11, 'audio', '1536684141hangouts_incoming_call.ogg', '2018-09-11 12:42:21', 0, 0, '0');
INSERT INTO `send_files` VALUES (46, 10, 'other', '1536755298Invoice_5488666867.pdf', '2018-09-12 08:28:18', 0, 0, '0');
INSERT INTO `send_files` VALUES (47, 27, 'audio', '1Mnwh9rvOU/yuhv3lG55YZZuGLJ7BMWbL6LyOqOR5Z3uN8q3JT7vzrFOcYzLPO58EavuS/GWAV3J9+ugc9ctFQ==', '2018-09-12 08:59:09', 0, 1, '0');
INSERT INTO `send_files` VALUES (48, 27, 'video/image', '77pdNiqafnI8X1koGileDU3VUzhZXel0uX+aWXNrpzlOWevNtZpivfMpErSa70qbZu1VlWs+KwgohJJWBnLjKg==', '2018-09-12 09:00:27', 0, 1, '0');
INSERT INTO `send_files` VALUES (49, 26, 'audio', '1536757546DS_BassB140D-01.mp3', '2018-09-12 09:05:46', 0, 0, '1');
INSERT INTO `send_files` VALUES (50, 26, 'video/image', '1536757591VID-20180908-WA0006.mp4', '2018-09-12 09:06:32', 0, 0, '1');
INSERT INTO `send_files` VALUES (51, 11, 'audio', '1536939311hangouts_incoming_call.ogg', '2018-09-14 11:35:11', 0, 0, '1');
INSERT INTO `send_files` VALUES (52, 11, 'audio', '1536939337hangouts_incoming_call.ogg', '2018-09-14 11:35:37', 0, 0, '1');
INSERT INTO `send_files` VALUES (53, 10, 'video/image', '1537195268VID-20180817-WA0000.mp4', '2018-09-17 10:41:08', 0, 0, '0');
INSERT INTO `send_files` VALUES (54, 10, 'video/image', '1537195374VID-20180817-WA0000.mp4', '2018-09-17 10:42:54', 0, 0, '0');
INSERT INTO `send_files` VALUES (55, 10, 'other', '1537196408ST-KY18000510__07-14.pdf', '2018-09-17 11:00:08', 0, 0, '1');
INSERT INTO `send_files` VALUES (56, 1, 'other', '1537196442Sri_Hanuman_Chalisa_Hindi.pdf', '2018-09-17 11:00:42', 0, 0, '1');
INSERT INTO `send_files` VALUES (57, 1, 'audio', '1537196605audiorecordtest.wav', '2018-09-17 11:03:25', 0, 0, '1');
INSERT INTO `send_files` VALUES (58, 1, 'audio', '1537251638audiorecordtest.wav', '2018-09-18 02:20:38', 0, 0, '1');
INSERT INTO `send_files` VALUES (59, 1, 'video/image', '1537251707VID-20180906-WA0001.mp4', '2018-09-18 02:21:47', 0, 0, '1');
INSERT INTO `send_files` VALUES (60, 1, 'video/image', '1537251773VID-20180906-WA0001.mp4', '2018-09-18 02:22:53', 0, 0, '1');
INSERT INTO `send_files` VALUES (61, 1, 'other', '1537251860Sri_Hanuman_Chalisa_Hindi.pdf', '2018-09-18 02:24:20', 0, 0, '1');
INSERT INTO `send_files` VALUES (62, 1, 'video/image', '1537251910VID-20180906-WA0001.mp4', '2018-09-18 02:25:10', 0, 0, '1');
INSERT INTO `send_files` VALUES (63, 1, 'audio', '1537251940audiorecordtest.wav', '2018-09-18 02:25:40', 0, 0, '1');
INSERT INTO `send_files` VALUES (64, 1, 'video/image', '1537276698VID-20180815-WA0016.mp4', '2018-09-18 09:18:18', 0, 0, '1');
INSERT INTO `send_files` VALUES (65, 1, 'video/image', '1537276745Screenshot_2018-09-14-19-09-27.png', '2018-09-18 09:19:05', 0, 0, '1');
INSERT INTO `send_files` VALUES (66, 1, 'audio', '1537277102audiorecordtest.wav', '2018-09-18 09:25:02', 0, 0, '1');
INSERT INTO `send_files` VALUES (67, 10, 'video/image', '1537277872Fruits_Drawing_Book_&_Coloring_Book_1536477569462.png', '2018-09-18 09:37:52', 0, 0, '0');
INSERT INTO `send_files` VALUES (68, 11, 'video/image', '153728015520180909_195452.mp4', '2018-09-18 10:15:55', 0, 0, '1');
INSERT INTO `send_files` VALUES (69, 10, 'video/image', '1537282000VID-20150903-WA0000.mp4', '2018-09-18 10:46:40', 0, 0, '0');
INSERT INTO `send_files` VALUES (70, 10, 'video/image', '1537282157VID-20180918-WA0008.mp4', '2018-09-18 10:49:18', 0, 0, '1');
INSERT INTO `send_files` VALUES (71, 26, 'other', 'b5zioSOTTiWPE+S2EQmV9s9djROAfsyTnWpKJLD2uzdkzWR4Hvs0XLn6knkKQgP3', '2018-09-19 03:35:21', 0, 1, '1');
INSERT INTO `send_files` VALUES (72, 26, 'audio', 'HaMULmgxk3gINJlIbrN4oKaeG+tt0nOki+LvvIVq8+C16kV7ojaBQAHMQMTkiM1g', '2018-09-19 03:36:40', 0, 1, '1');
INSERT INTO `send_files` VALUES (73, 26, 'audio', 'JgbZYnrVxcuUxIuHdAbG1jM05t9coWynMJOTYGQyo66GHalUoduF5oI52X0qsI3F', '2018-09-19 04:28:03', 0, 1, '1');
INSERT INTO `send_files` VALUES (74, 26, 'audio', '6lyVFlAEGUgO2sXge2OpaG8fIW6xn/6kbyff4Q2e1loMORmE9Ad5cf27IQXVLKsw', '2018-09-19 04:31:30', 0, 1, '0');
INSERT INTO `send_files` VALUES (75, 26, 'audio', '+5prG4YvQnD4rYqPHlOgiT8xTytLhzaRL1oPoBh9ZI5V97Oc9MlunGdiRhyqt3nG', '2018-09-19 04:31:38', 0, 1, '1');
INSERT INTO `send_files` VALUES (76, 26, 'audio', 'qJmDXtlXgv6J2iRodywFg73nh3TTQqfNiyOeGwO94n69FEczirlzwtwWsc7Urn7c', '2018-09-19 04:31:48', 0, 1, '1');
INSERT INTO `send_files` VALUES (77, 26, 'video/image', 'X6Rvnyx89sU5f6Rk43VL7bjVAbYNTX3Ly8xl3SdZ1O33OwsF9UaUCfczKcxGQnUbKqHtEMvCfq5Wif8D2kf+bg==', '2018-09-19 04:34:29', 0, 1, '1');
INSERT INTO `send_files` VALUES (78, 26, 'video/image', 'TSOvO41+ZsD2kPn1YaFLTWzXNbNdpESie4+ixdFaREUVZMdZBnYNjx2MkePyFdQoJTWZ/NbBeS+2LSDBMJmbXA==', '2018-09-19 04:36:36', 0, 1, '1');
INSERT INTO `send_files` VALUES (79, 26, 'video/image', 'I35Y+t5TSCzHCwBHqUBQ+0BhVRzFtqN2DJUoE2AuBE7x6sPlVBPF5BEizlZxK1icK7XzNOklraSanu4zK7WnRw==', '2018-09-19 04:37:02', 0, 1, '1');
INSERT INTO `send_files` VALUES (80, 26, 'other', 'GeKUvL6tmBrCYkWW527QlOlBNHWIJ2HXNl9hsyyjkUACtWLxR/dPSEsaD4MLi/RR', '2018-09-19 04:38:13', 0, 1, '1');
INSERT INTO `send_files` VALUES (81, 26, 'other', 'lZZNVK7jCBq2KRB1k+04lFwtc6ep0oWotVzcM7nGn34iZz3maHUuA6em7KGCZ0c6', '2018-09-19 04:38:23', 0, 1, '1');
INSERT INTO `send_files` VALUES (82, 26, 'other', '6kUsFapcDY6yNnSPA/74cndQPtzMqnlLmr6wHQbZHkPbDvRxOU7McXhKOEURqTfL', '2018-09-19 04:38:33', 0, 1, '1');
INSERT INTO `send_files` VALUES (83, 1, 'audio', 'AGDsHd0IO1VvKctoRQe734lDy3kHRXyYwv7DnjW1MPKfkC4+Ua2KTGfGV5UvY+uW', '2018-09-19 06:56:48', 0, 1, '0');
INSERT INTO `send_files` VALUES (84, 1, 'audio', '1537354980audiorecordtest.wav', '2018-09-19 07:03:00', 0, 0, '0');
INSERT INTO `send_files` VALUES (85, 1, 'video/image', '1537355531VID-20180906-WA0001.mp4', '2018-09-19 07:12:11', 0, 0, '0');
INSERT INTO `send_files` VALUES (86, 26, 'video/image', '7qC6LjypqsdfnelUxlrPMySdAYZCTebC6RijhkF64YGS69EAR7gIWKY41iLpNF31e8fQa2cOS6Y9pMBEPKxCTQ==', '2018-09-19 07:16:06', 0, 1, '0');
INSERT INTO `send_files` VALUES (87, 26, 'video/image', 'SmLe9hYPxFSMInNeFx0QtpdS9ilX/I8/1b11WYqC4/iinMkvj181A6F4lwsw0H+oQfGCk1nXQ5RIqqSsM21r3Q==', '2018-09-19 07:22:24', 0, 1, '0');
INSERT INTO `send_files` VALUES (88, 26, 'other', '25GAkQrN9B+ovZWywMZWJunzI+1sZSqumsTb/TIvoFsCH4BoV2vu5cknm1AMWpDT', '2018-09-19 07:23:28', 0, 1, '0');
INSERT INTO `send_files` VALUES (89, 26, 'video/image', 'zGuUu/Dym1W8rAZHO4dWHWJYZJcJQwmXohfD3uGUoGd586nNOBFmGlIs3KJczIVIjsV4IwrMANM0bz0TxpONIg==', '2018-09-19 07:47:38', 0, 1, '0');
INSERT INTO `send_files` VALUES (90, 26, 'video/image', 'Lj3ZBbr9ZmRLMPkBJEa1vGNtG2TRHuXtVMQK9sKTgE8BPJiH/2HywoA7Vjcz4JWUTEaL+tg62EdSQZl6bck9RQ==', '2018-09-19 07:51:13', 0, 1, '0');
INSERT INTO `send_files` VALUES (91, 26, 'other', 'PyWdg93mU8e7xaylh8o8RaW1oSM8j1s8GiuA9kHJydmbNpL6iRH2r5pffVsfOJy4', '2018-09-19 07:52:06', 0, 1, '0');
INSERT INTO `send_files` VALUES (92, 26, 'other', '9+3s3yzgX5m+KaHBBKZ/JV7MGN6BUxOkZ3Bx/AGqwXe39k2sg0VjatP6O4h8RfL+', '2018-09-19 07:59:37', 0, 1, '0');
INSERT INTO `send_files` VALUES (93, 10, 'other', '1537365386ST-KY18000510__07-14.pdf', '2018-09-19 09:56:26', 0, 0, '0');
INSERT INTO `send_files` VALUES (94, 10, 'video/image', '1537365442VID-20180903-WA0012.mp4', '2018-09-19 09:57:22', 0, 0, '0');
INSERT INTO `send_files` VALUES (95, 1, 'audio', '1537365642audiorecordtest.wav', '2018-09-19 10:00:42', 0, 0, '0');
INSERT INTO `send_files` VALUES (96, 1, 'audio', '1537366738audiorecordtest.wav', '2018-09-19 10:18:58', 0, 0, '0');
INSERT INTO `send_files` VALUES (97, 10, 'video/image', '1537370440Screenshot_20180919-200624.png', '2018-09-19 11:20:40', 0, 0, '0');
INSERT INTO `send_files` VALUES (98, 10, 'video/image', '1537370620VID-20180919-WA0003.mp4', '2018-09-19 11:23:41', 0, 0, '0');
INSERT INTO `send_files` VALUES (99, 10, 'audio', '15373716572011_nonstop_bollywood_mix_vol_1_dj_rdx_mp3_7725.mp3', '2018-09-19 11:40:57', 0, 0, '0');
INSERT INTO `send_files` VALUES (100, 10, 'other', '1537371773ST-KY18000510__07-14.pdf', '2018-09-19 11:42:53', 0, 0, '0');
INSERT INTO `send_files` VALUES (101, 10, 'video/image', '153737180420180919_093527.jpg', '2018-09-19 11:43:24', 0, 0, '0');
INSERT INTO `send_files` VALUES (102, 13, 'video/image', '1537384435Screenshot_2018-07-19-12-36-04.png', '2018-09-19 15:13:55', 0, 0, '0');
INSERT INTO `send_files` VALUES (103, 10, 'video/image', '1537525325VID-20180913-WA0022.mp4', '2018-09-21 06:22:05', 0, 0, '0');
INSERT INTO `send_files` VALUES (104, 10, 'audio', '1537525406AUD-20180817-WA0006.mp3', '2018-09-21 06:23:26', 0, 0, '0');
INSERT INTO `send_files` VALUES (105, 10, 'other', '1537525442thakur_prasad_calendar_2018_pdf.pdf', '2018-09-21 06:24:02', 0, 0, '0');
INSERT INTO `send_files` VALUES (106, 6, 'video/image', '1537793134VID-20180922-WA0005.mp4', '2018-09-24 08:45:34', 0, 0, '0');
INSERT INTO `send_files` VALUES (107, 6, 'video/image', '1537795504VID-20180922-WA0005.mp4', '2018-09-24 09:25:04', 0, 0, '0');
INSERT INTO `send_files` VALUES (108, 6, 'other', '1537796494388748346.pdf', '2018-09-24 09:41:34', 0, 0, '0');
INSERT INTO `send_files` VALUES (109, 6, 'audio', '1537796553REC20180924191204.mp3', '2018-09-24 09:42:33', 0, 0, '0');
INSERT INTO `send_files` VALUES (110, 6, 'other', '1537799787388748346.pdf', '2018-09-24 10:36:27', 0, 0, '0');
INSERT INTO `send_files` VALUES (111, 1, 'other', '1537863517pdf.pdf', '2018-09-25 04:18:37', 0, 0, '0');
INSERT INTO `send_files` VALUES (112, 1, 'other', '1537863532pdf.pdf', '2018-09-25 04:18:52', 0, 0, '0');
INSERT INTO `send_files` VALUES (113, 1, 'other', '1537863746pdf.pdf', '2018-09-25 04:22:26', 0, 0, '0');
INSERT INTO `send_files` VALUES (114, 27, 'other', '1537864075pdf.pdf', '2018-09-25 04:27:55', 0, 0, '0');
INSERT INTO `send_files` VALUES (115, 30, 'audio', '1537982142Over_the_Horizon.mp3', '2018-09-26 13:15:42', 0, 0, '0');
INSERT INTO `send_files` VALUES (116, 1, 'audio', '1538545110DS_BassB140D-01.mp3', '2018-10-03 01:38:30', 0, 0, '0');

-- --------------------------------------------------------

-- 
-- Table structure for table `send_locations`
-- 

CREATE TABLE `send_locations` (
  `location_id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_id` int(11) NOT NULL,
  `location` varchar(255) NOT NULL,
  `isSecure` varchar(10) NOT NULL,
  `lat` varchar(50) NOT NULL,
  `long` varchar(50) NOT NULL,
  `send_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_deleted` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`location_id`)
) ENGINE=InnoDB AUTO_INCREMENT=109 DEFAULT CHARSET=latin1 AUTO_INCREMENT=109 ;

-- 
-- Dumping data for table `send_locations`
-- 

INSERT INTO `send_locations` VALUES (1, 1, 'Sector 63 H-087 Noida Uttar Pradesh India 201301', '', '28.6284094', '77.3778739', '2018-07-11 03:28:08', '1');
INSERT INTO `send_locations` VALUES (2, 1, 'Sector 63 H081 Noida Uttar Pradesh India 201301', '', '28.628455', '77.3780532', '2018-07-11 03:29:09', '0');
INSERT INTO `send_locations` VALUES (3, 1, 'Sector 63 H-80 Noida Uttar Pradesh India 201301', '', '28.6286719', '77.3780356', '2018-07-11 03:59:52', '0');
INSERT INTO `send_locations` VALUES (4, 3, 'Sector 63 H085 Noida Uttar Pradesh India 201301', '', '28.6283436', '77.3781198', '2018-07-11 04:02:40', '0');
INSERT INTO `send_locations` VALUES (5, 3, 'Sector 63 H085 Noida Uttar Pradesh India 201301', '', '28.6283703', '77.3780459', '2018-07-11 04:04:54', '0');
INSERT INTO `send_locations` VALUES (6, 1, 'Sector 63 H124 Noida Uttar Pradesh India 201301', '', '28.6284926', '77.3784577', '2018-07-11 08:14:16', '0');
INSERT INTO `send_locations` VALUES (7, 5, 'Sector 1 Sector 1 Greater Noida Uttar Pradesh India 201306', '', '28.579531', '77.4377786', '2018-07-11 10:32:50', '0');
INSERT INTO `send_locations` VALUES (8, 5, 'Sector 1 Sector 1 Greater Noida Uttar Pradesh India 201306', '', '28.579531', '77.4377786', '2018-07-11 10:34:38', '0');
INSERT INTO `send_locations` VALUES (9, 6, 'Bisrakh Panchsheel Hynish Greater Noida Uttar Pradesh India 203207', '', '28.5794148', '77.4378844', '2018-07-11 11:02:07', '0');
INSERT INTO `send_locations` VALUES (10, 6, 'Bisrakh Panchsheel Hynish Greater Noida Uttar Pradesh India 203207', '', '28.5794149', '77.4378843', '2018-07-11 11:02:31', '0');
INSERT INTO `send_locations` VALUES (11, 10, 'Sector 63 H085 Noida Uttar Pradesh India 201301', '', '28.6282336', '77.3780939', '2018-07-12 02:46:19', '0');
INSERT INTO `send_locations` VALUES (12, 6, '/o/lMNgwcpu7c+RJT24fLgWiamHy1RY90HwN3U4Rb5lwxX0zFNvynqzHaSvmohbex9sdIWlVVTAweH/j/b5IaR0aiPE0HEE5cK6GM9N8L20=', '1', '1234567', '123456', '2018-07-12 06:00:52', '0');
INSERT INTO `send_locations` VALUES (13, 10, 'Sector 63 H-087 Noida Uttar Pradesh India 201301', '', '28.6284946', '77.3779122', '2018-07-12 10:27:47', '0');
INSERT INTO `send_locations` VALUES (14, 11, 'New Ashok Nagar B1/45 Noida Uttar Pradesh India 110096', '', '28.5891503', '77.3055323', '2018-07-12 12:25:51', '0');
INSERT INTO `send_locations` VALUES (15, 6, 'New Ashok Nagar B1/45 Noida Uttar Pradesh India 110096', '', '28.596903289326914', '77.31626473367214', '2018-07-12 12:26:47', '0');
INSERT INTO `send_locations` VALUES (16, 13, 'rNs6L9C/8ssUw5SWIcNBt9m664gFLOpQDFhugL4m8bCzok2CW75HodAyCR5a14933VuZ7zgUya2Ku9tI4xlORg==', '1', 'Cno7BaovNn2iz1RoC5vcMezuhh7SbK2udV3oSScCzx0=', 'uKTZhqDFWKR/pWshN9sBfOI3MartaZt0YnuwT7JrvuA=', '2018-07-12 20:43:44', '0');
INSERT INTO `send_locations` VALUES (17, 12, 'null 6832 Annandale Virginia United States 22003', '', '38.8310041', '-77.1820329', '2018-07-12 20:53:19', '0');
INSERT INTO `send_locations` VALUES (18, 7, 'PYhFy1SSLiuQ8oWIVN0242FqtHMa7lkuDg1zaQJ4oryFxWEHLT5tXdBJ8Lz14iDfiYYCigNeOaNvk9ODrG8o+A==', '1', 'M7KdKAE7pQiep1FwOtCyt6sdlYuom0i6AXHJOJa2MYQ=', 'omUx6Sx9G836pkTusP62cfXCEv0i78Dk68OJd6UZWB4=', '2018-07-13 06:46:03', '0');
INSERT INTO `send_locations` VALUES (19, 3, 'Sector 63 H-087 Noida Uttar Pradesh India 201301', '', '28.6284277', '77.3779032', '2018-07-13 12:11:25', '0');
INSERT INTO `send_locations` VALUES (20, 3, 'Sector 63 H-040 Noida Uttar Pradesh India 201301', '', '28.62980283128784', '77.37812150269747', '2018-07-13 12:12:14', '0');
INSERT INTO `send_locations` VALUES (21, 7, 'mctFGtIMzG68AphoeL3++cjy1r2ejVPFsSGa3qAGDpCcRh+uFliknQlJyrF7kL/0+TuZhcnSF5GVYACoRWSrvQ==', '1', '+DlNZE7J1VOJ2Wl9v5ru8p6GQVtaU/lo6N4h27Y7bNI=', '/GMLCcVC6fWd68TM349Af8esbJRgSPbuGhqH2+ls2zE=', '2018-07-13 12:15:41', '0');
INSERT INTO `send_locations` VALUES (22, 13, 'null 748 Atlanta Georgia United States 30306', '', '33.7749191', '-84.3591762', '2018-07-19 12:36:22', '0');
INSERT INTO `send_locations` VALUES (23, 13, 'null 740 Atlanta Georgia United States 30306', '', '33.7748171', '-84.3590014', '2018-07-20 17:13:07', '0');
INSERT INTO `send_locations` VALUES (24, 1, 'Sector 63 H-087 Noida Uttar Pradesh India 201301', '', '28.629672169461507', '77.38100755959749', '2018-07-25 11:39:07', '0');
INSERT INTO `send_locations` VALUES (25, 1, 'Sector 63 H-087 Noida Uttar Pradesh India 201301', '', '28.6284251', '77.3779301', '2018-07-25 11:39:39', '0');
INSERT INTO `send_locations` VALUES (26, 1, 'Sector 63 H-087 Noida Uttar Pradesh India 201301', '', '28.623701574472502', '77.37651720643044', '2018-07-25 12:29:05', '0');
INSERT INTO `send_locations` VALUES (27, 5, 'Jagrati Vihar 101/4 Meerut Uttar Pradesh India 250004', '', '28.966643143476794', '77.74950820952652', '2018-07-25 13:25:10', '0');
INSERT INTO `send_locations` VALUES (28, 17, 'Bisrakh Greater Noida Greater Noida Uttar Pradesh India 201306', '', '28.5793976', '77.4375697', '2018-07-27 15:13:13', '0');
INSERT INTO `send_locations` VALUES (29, 17, 'Bisrakh Greater Noida Greater Noida Uttar Pradesh India 201306', '', '28.5793976', '77.4375697', '2018-07-27 15:13:26', '0');
INSERT INTO `send_locations` VALUES (30, 16, 'Bisrakh Greater Noida Greater Noida Uttar Pradesh India 201306', '', '28.5794058', '77.437573', '2018-07-27 15:13:30', '0');
INSERT INTO `send_locations` VALUES (31, 17, 'Bisrakh Greater Noida Greater Noida Uttar Pradesh India 201306', '', '28.637318261072604', '77.4627985060215', '2018-07-27 15:13:56', '0');
INSERT INTO `send_locations` VALUES (32, 17, 'Bisrakh Greater Noida Greater Noida Uttar Pradesh India 201306', '', '28.637318261072604', '77.4627985060215', '2018-07-27 15:14:47', '0');
INSERT INTO `send_locations` VALUES (33, 17, 'Bisrakh Greater Noida Greater Noida Uttar Pradesh India 201306', '', '28.652751469002926', '77.45428282767534', '2018-07-27 15:15:02', '0');
INSERT INTO `send_locations` VALUES (34, 5, 'Bisrakh Greater Noida Greater Noida Uttar Pradesh India 201306', '', '28.5794835', '77.4375949', '2018-07-27 15:16:25', '0');
INSERT INTO `send_locations` VALUES (35, 16, 'Bisrakh Greater Noida Greater Noida Uttar Pradesh India 201306', '', '28.579550193287375', '77.43882156908512', '2018-07-27 15:18:19', '0');
INSERT INTO `send_locations` VALUES (36, 5, 'Bisrakh Panchsheel Hynish Greater Noida Uttar Pradesh India 203207', '', '28.57942', '77.4378466', '2018-07-31 09:21:12', '0');
INSERT INTO `send_locations` VALUES (37, 13, 'null 748 Atlanta Georgia United States 30306', '', '33.7748727', '-84.359022', '2018-08-02 17:31:12', '0');
INSERT INTO `send_locations` VALUES (38, 13, 'null 748 Atlanta Georgia United States 30306', '', '33.77503089268703', '-84.3674458935857', '2018-08-02 17:31:49', '0');
INSERT INTO `send_locations` VALUES (39, 1, 'Sector 62 Uttar Pradesh Noida Uttar Pradesh India 201301', '', '28.6303411', '77.3760466', '2018-08-07 02:45:59', '0');
INSERT INTO `send_locations` VALUES (40, 10, 'Sector 63 H-087 Noida Uttar Pradesh India 201301', '', '28.6284251', '77.3779301', '2018-08-07 02:55:56', '0');
INSERT INTO `send_locations` VALUES (41, 23, 'Amrapali Leisure Valley A-275 Greater Noida Uttar Pradesh India 201306', '', '28.5886386', '77.4347506', '2018-08-07 05:12:24', '0');
INSERT INTO `send_locations` VALUES (42, 23, 'Amrapali Leisure Valley A-275 Greater Noida Uttar Pradesh India 201306', '', '28.597387830127214', '77.44273625314236', '2018-08-07 05:12:47', '0');
INSERT INTO `send_locations` VALUES (43, 13, 'null 748 Atlanta Georgia United States 30306', '', '33.7748786', '-84.3590569', '2018-08-30 12:45:28', '0');
INSERT INTO `send_locations` VALUES (44, 13, 'null 748 Atlanta Georgia United States 30306', '', '33.76279940800299', '-84.3733923509717', '2018-08-30 12:45:52', '0');
INSERT INTO `send_locations` VALUES (45, 13, 'null 748 Atlanta Georgia United States 30306', '', '33.7748786', '-84.3590569', '2018-08-30 12:47:07', '0');
INSERT INTO `send_locations` VALUES (46, 13, 'null 748 Atlanta Georgia United States 30306', '', '33.76904134560122', '-84.37581337988377', '2018-08-30 12:47:57', '0');
INSERT INTO `send_locations` VALUES (47, 24, 'Sector 63 H-087 Noida Uttar Pradesh India 201301', '', '28.6284387', '77.3779438', '2018-09-06 04:30:55', '0');
INSERT INTO `send_locations` VALUES (48, 1, 'Sector 63 H-034 Noida Uttar Pradesh India 201301', '', '28.6287829', '77.3793516', '2018-09-07 03:38:50', '0');
INSERT INTO `send_locations` VALUES (49, 1, 'Sector 63 H-087 Noida Uttar Pradesh India 201301', '', '28.6284361', '77.3779432', '2018-09-07 04:01:21', '0');
INSERT INTO `send_locations` VALUES (50, 8, 'Sector 63 H-087 Noida Uttar Pradesh India 201301', '', '28.627392918419737', '77.37640991806984', '2018-09-07 04:06:48', '0');
INSERT INTO `send_locations` VALUES (51, 8, 'Sector 63 H-087 Noida Uttar Pradesh India 201301', '', '28.6284431', '77.3779437', '2018-09-07 04:09:31', '0');
INSERT INTO `send_locations` VALUES (52, 8, 'Sector 63 H-087 Noida Uttar Pradesh India 201301', '', '28.62284456845835', '77.37538296729326', '2018-09-07 04:09:38', '0');
INSERT INTO `send_locations` VALUES (53, 8, 'Sector 63 H-087 Noida Uttar Pradesh India 201301', '', '28.6284419', '77.3779412', '2018-09-07 04:30:12', '0');
INSERT INTO `send_locations` VALUES (54, 8, 'Sector 63 H-087 Noida Uttar Pradesh India 201301', '', '28.6284419', '77.3779412', '2018-09-07 04:30:25', '0');
INSERT INTO `send_locations` VALUES (55, 1, 'Sector 63 H-087 Noida Uttar Pradesh India 201301', '', '28.6282978', '77.377958', '2018-09-07 08:05:54', '0');
INSERT INTO `send_locations` VALUES (56, 1, 'Sector 63 H081 Noida Uttar Pradesh India 201301', '', '28.6284286', '77.3781003', '2018-09-07 08:13:13', '0');
INSERT INTO `send_locations` VALUES (57, 1, 'Sector 63 H081 Noida Uttar Pradesh India 201301', '', '28.6285352', '77.3779901', '2018-09-07 09:30:07', '0');
INSERT INTO `send_locations` VALUES (58, 1, 'Sector 63 H081 Noida Uttar Pradesh India 201301', '', '28.6285352', '77.3779901', '2018-09-07 09:31:44', '0');
INSERT INTO `send_locations` VALUES (59, 6, 'Bisrakh Greater Noida Greater Noida Uttar Pradesh India 201306', '', '28.5794171', '77.4376612', '2018-09-09 17:03:41', '0');
INSERT INTO `send_locations` VALUES (60, 11, 'Sector 62 Oshy Marg Noida Uttar Pradesh India 201309', '', '28.6222656', '77.3674655', '2018-09-10 06:56:29', '0');
INSERT INTO `send_locations` VALUES (61, 10, 'Sector 63 H-087 Noida Uttar Pradesh India 201301', '', '28.6284496', '77.3779536', '2018-09-12 08:26:27', '0');
INSERT INTO `send_locations` VALUES (62, 10, 'Sector 63 H-087 Noida Uttar Pradesh India 201301', '', '28.62531638787959', '77.37792871892452', '2018-09-12 08:26:53', '0');
INSERT INTO `send_locations` VALUES (63, 6, 'Bisrakh Sector 1 Greater Noida Uttar Pradesh India 203207', '', '28.5793819', '77.4375492', '2018-09-13 05:05:02', '0');
INSERT INTO `send_locations` VALUES (64, 6, 'Bisrakh Sector 1 Greater Noida Uttar Pradesh India 203207', '', '28.5793819', '77.4375492', '2018-09-13 05:05:19', '0');
INSERT INTO `send_locations` VALUES (65, 6, 'Bisrakh Sector 1 Greater Noida Uttar Pradesh India 203207', '', '28.5794814', '77.4377752', '2018-09-13 05:31:38', '0');
INSERT INTO `send_locations` VALUES (66, 11, 'Sector 2 D-34 Noida Uttar Pradesh India 201301', '', '28.5826464', '77.3157131', '2018-09-13 14:11:24', '0');
INSERT INTO `send_locations` VALUES (67, 1, 'Sector 63 H-087 Noida Uttar Pradesh India 201301', '', '28.6284469', '77.3779521', '2018-09-18 02:23:50', '1');
INSERT INTO `send_locations` VALUES (68, 26, 'Sector 64 A-4 Noida Uttar Pradesh India 201301', '', '28.6140018', '77.3781567', '2018-09-19 02:24:40', '0');
INSERT INTO `send_locations` VALUES (69, 26, 'Y2eivBiuE3Ui6Ae3O3JYR8RmK037LjBv/MH/TIacEyyrwbNohjtFeBAyS5W+lmJAEKP9oZoPmLvbDRxBlHqgMA==', '1', 'T/KooYwpbmqtk1/ylJ8HS4A5MvUjI1hLs6mNV6s1lX0=', '3LdAkaeShyw7ShEy3YLLpaqJCfeE3fVvdcuOrjeRfI8=', '2018-09-19 04:00:08', '0');
INSERT INTO `send_locations` VALUES (70, 26, 'ftuH1nvrtBLCDwRm66zjVvQAlPRei8CeXbIEodY67lQR+D0gDq+6TAKqtLHqzUV+miwMrUEd4xuAJf+lBzK6pA==', '1', 'X5ohaCmlDVsiV4KViifDkB/b24GMHcj3FZvsAyTfpzk=', 'IbySkB1GdI4jwLu7L9FgprORvEopYq9KYNBZY66ofwc=', '2018-09-19 04:00:22', '0');
INSERT INTO `send_locations` VALUES (71, 26, 'kdKtu8itayCs6CG1fQvWnkYYtATQDDxAmeBoKdVQp//rF4c2eKBldUzc6tl47uvK45evbcDuEqoaXEQAaSey0w==', '1', 'CgBQpwCB/vVqFd3eybtQBgYNnjiU2FkRG7o3oWLDBtE=', 'QBtNOjPDJfl0uJCzVAjL1VaB/zKNBIFOy2F/3pFt+NE=', '2018-09-19 04:00:34', '0');
INSERT INTO `send_locations` VALUES (72, 1, 'KNbwZmMeoYkXdBSYhfpMjYWSIG2XputZGOzB0LMJRaviSjo2gHqJ6VYV6i2rqvKP6mqSSAaM3fxO2+Z6M0sFYg==', '1', 'c9HE2AcsdX7tGEv3OOXb5vkJCU6xsbLtW1bZ9lVDFzU=', 'wmax28ldvxbhDhbU3owCwU36+l2ROMuwfsoW7C+dvtY=', '2018-09-19 04:03:25', '1');
INSERT INTO `send_locations` VALUES (73, 26, '6HEe9oD+pR/M9o0ZUBz8j8PznyvNmH3KReSucoDz3z+ygNqs2ltOACis5L1/HWn1cjc5AI1VF7+n7j9oIz+FTQ==', '1', '7x5QYTKukLKlcRy/eQl4ReQ8tTYCTirt4rsfJU8RMoA=', 'ovIXXaChURvAmwqihVdh8S6DjpI3uRKVZjdr3d2O/WQ=', '2018-09-19 06:12:52', '0');
INSERT INTO `send_locations` VALUES (74, 26, '8E2AtFT+gxuhg8rXMx7rjIH1gMiwyuHdL1OMdKSGKPbzGYDVIFsLAvd969J0TCzltQgo7pQV4cCxQWb2A9uZSQ==', '1', '+IExOVz+UMlPrOqm9QnrKQBT37vojn0qAfSINMmC0qA=', 'KOKf/cQkNmI0FuQjamxuOzykU07NaXELtA0rA3P8f3I=', '2018-09-19 06:38:41', '0');
INSERT INTO `send_locations` VALUES (75, 1, 'oIAygPvQJlUvNQ0+og4FeGQWfz0Jrdct+n6jyBRLHnWYTSU+MZoZo8Ni+92PNKZ+znJgDjtqE0fUOXLIgWHcRA==', '1', 'g2BZpqHwSO+R/TgqWAQrmylGPJpg2MQ/dXDLk6eiCiA=', 'tAFMGQRHy709E++O/qjfIIXNensfDyld5pK0Ph1XTZI=', '2018-09-19 06:40:47', '0');
INSERT INTO `send_locations` VALUES (76, 26, 'd3ZI6+gB30kBBhwtgxqkSVk1Y02FqAV5r5H4uxW5UFCXrtgpQqWn9qsVcd43m4ORxPos4iNojgUQG+3i1yr35Q==', '1', 'jUcLnv7ZLNKCAog5DIXiOC12MbDv6y+L9M5OZ+okXy8=', 'Dh5m1O12aLGUidMYtSaci2do61OWYLdGGDPDwZc6u8Q=', '2018-09-19 06:43:57', '0');
INSERT INTO `send_locations` VALUES (77, 26, 'FkPLrqaZFWsN92OtVulFo8m8rC8CjivnGOkPsh0wSHpU0td5jl07V5RizCwlm7aWbeNfic3NKK1d76S71l01Ig==', '1', 'nDGZOowcCF6heWix5g+bLb27jXjxD2CtHcjneZ2pWSI=', 'AjYH079fwfNYLYc5qhNdZrlqbQKIhTeLHfznA3FCteY=', '2018-09-19 06:44:02', '0');
INSERT INTO `send_locations` VALUES (78, 26, 'CmIxOrQMsVL8PBHNr1KPw93KEQujMX3sgRv6nx8qGQ8cZQK1Khftb9Iwc3ksQS28B3u7FAnU4FQiBgptDrlEsg==', '1', 'rCWHYg51QS6LvT3qvdeGJ5AIsj2rbG7VT5PSgKTG1i8=', 'lLY8bJnU9dIl+cAf0AYTzjgY76b5sVF4mzeVQSa8eK0=', '2018-09-19 06:44:08', '0');
INSERT INTO `send_locations` VALUES (79, 26, 'kk0kBwvWxKlFUfovAQii1YyUx55qJIGHgZhNVehkTaab5yZpwYatZMmXGd52pnBQyY6NsqfHSiBRJzPQ6wjQpQ==', '1', 'Qk1CYZByrRZQ8osqmyzMAtoLQvGqjUt63mFi2VDlN3Q=', 'bHUJaar5jBCwc8J4+kPzi09T8iiYuSgi111bAuhu3i8=', '2018-09-19 06:44:13', '0');
INSERT INTO `send_locations` VALUES (80, 26, 'pOvZ+5d1iKQm5QO9WkXeE+GgbKzMUTTDEzcCUSvAjvk5mVv+NwYJkdvGgQl03kAyRTUSHar0RvzpV7bzKD2yTw==', '1', 'VGNz8U6hoGeKvd5GGDrkVg8qLvXde3ymvVNsJUnEyhM=', 'LaMu1jJH6SZ3cqJcuMVqqMZXIHKIRF5cuFcaYw5fpQ8=', '2018-09-19 07:34:48', '0');
INSERT INTO `send_locations` VALUES (81, 26, '4FDngdNhd/kECK5F7Vth1LDTvYDXMZeXlO6OlPdW7uuGdqEHfZb+rcCbqp2rA9Q9LuNkRoYeQK4J9hQNuNSuoA==', '1', 'e4UJ2F1uS74aU+IYTsb6whktyth81yazJ8Ds4Vywk3I=', 'DJnYSMnkIdqFatbzyMMWX1shaJ7tygsHMPsDpZpX5dA=', '2018-09-19 07:35:07', '0');
INSERT INTO `send_locations` VALUES (82, 26, 'e8LQXNhVWb99IkeTPru7IJXKhGBIZz83CySdmZXcVmD5rhuW+SEoYseLUoiINqFsMg1TBEEitPH0EgFv5GabQg==', '1', '6HsDGNc3eOniitYdnI74CjkqukSKe7sogq4OUCRjlUY=', 'Hy0nFcbMHWGPTk6pAXLDHhzV4gnFzP8yQD2XNlRcmKY=', '2018-09-19 07:35:36', '0');
INSERT INTO `send_locations` VALUES (83, 10, 'Sector 63 H-087 Noida Uttar Pradesh India 201301', '', '28.628457', '77.377952', '2018-09-19 09:55:32', '0');
INSERT INTO `send_locations` VALUES (84, 13, 'null 7009 McLean Virginia United States 22101', '', '38.945447', '-77.1868814', '2018-09-19 15:06:01', '0');
INSERT INTO `send_locations` VALUES (85, 13, 'null 7009 McLean Virginia United States 22101', '', '38.92679820481902', '-77.19041213393211', '2018-09-19 15:06:21', '0');
INSERT INTO `send_locations` VALUES (86, 6, 'Bisrakh Sector 1 Greater Noida Uttar Pradesh India 203207', '', '28.580935154644102', '77.45163012295961', '2018-09-20 01:59:42', '0');
INSERT INTO `send_locations` VALUES (87, 11, 'null Noida Noida Uttar Pradesh India 201301', '', '28.6222968', '77.3672958', '2018-09-20 02:09:45', '0');
INSERT INTO `send_locations` VALUES (88, 10, 'Sector 63 H-087 Noida Uttar Pradesh India 201301', '', '28.571981457760927', '77.35678620636463', '2018-09-20 02:16:12', '0');
INSERT INTO `send_locations` VALUES (89, 13, 'null 7009 McLean Virginia United States 22101', '', '38.9453477', '-77.1867256', '2018-09-20 11:14:37', '0');
INSERT INTO `send_locations` VALUES (90, 6, 'Sector 63 H-087 Noida Uttar Pradesh India 201301', '', '28.6284746', '77.3779595', '2018-09-24 09:42:53', '0');
INSERT INTO `send_locations` VALUES (91, 6, 'Sector 63 H081 Noida Uttar Pradesh India 201301', '', '28.6285035', '77.3779758', '2018-09-24 09:46:09', '0');
INSERT INTO `send_locations` VALUES (92, 6, 'Sector 63 H-087 Noida Uttar Pradesh India 201301', '', '28.6284563', '77.3779524', '2018-09-24 09:56:52', '0');
INSERT INTO `send_locations` VALUES (93, 6, 'Sector 63 H-087 Noida Uttar Pradesh India 201301', '', '28.6284563', '77.3779524', '2018-09-24 09:57:03', '0');
INSERT INTO `send_locations` VALUES (94, 6, 'Sector 63 H-087 Noida Uttar Pradesh India 201301', '', '28.6284563', '77.3779524', '2018-09-24 09:57:18', '0');
INSERT INTO `send_locations` VALUES (95, 6, 'Sector 63 H-087 Noida Uttar Pradesh India 201301', '', '28.6284563', '77.3779524', '2018-09-24 09:57:39', '0');
INSERT INTO `send_locations` VALUES (96, 6, 'Sector 63 H-087 Noida Uttar Pradesh India 201301', '', '28.6284563', '77.3779524', '2018-09-24 09:57:46', '0');
INSERT INTO `send_locations` VALUES (97, 6, 'Sector 63 H-087 Noida Uttar Pradesh India 201301', '', '28.6284563', '77.3779524', '2018-09-24 09:58:25', '0');
INSERT INTO `send_locations` VALUES (98, 1, 'Sector 63 H-087 Noida Uttar Pradesh India 201301', '', '28.6284458', '77.3779514', '2018-09-24 10:05:40', '0');
INSERT INTO `send_locations` VALUES (99, 1, 'Sector 63 H-087 Noida Uttar Pradesh India 201301', '', '28.6284458', '77.3779514', '2018-09-24 10:05:44', '0');
INSERT INTO `send_locations` VALUES (100, 1, 'Sector 63 H-087 Noida Uttar Pradesh India 201301', '', '28.6284458', '77.3779514', '2018-09-24 10:05:49', '0');
INSERT INTO `send_locations` VALUES (101, 1, 'Sector 63 H-087 Noida Uttar Pradesh India 201301', '', '28.6284454', '77.3779515', '2018-09-24 10:06:43', '0');
INSERT INTO `send_locations` VALUES (102, 6, 'Sector 63 H-087 Noida Uttar Pradesh India 201301', '', '28.6284558', '77.3779527', '2018-09-24 10:16:57', '0');
INSERT INTO `send_locations` VALUES (103, 6, 'Sector 63 H-087 Noida Uttar Pradesh India 201301', '', '28.6284562', '77.3779521', '2018-09-24 10:35:21', '0');
INSERT INTO `send_locations` VALUES (104, 6, 'Sector 63 H-087 Noida Uttar Pradesh India 201301', '', '28.6284572', '77.3779515', '2018-09-24 10:35:53', '0');
INSERT INTO `send_locations` VALUES (105, 1, 'Sector 63 H-087 Noida Uttar Pradesh India 201301', '', '28.6284273', '77.3779523', '2018-09-25 03:48:26', '0');
INSERT INTO `send_locations` VALUES (106, 1, 'Sector 63 H-087 Noida Uttar Pradesh India 201301', '', '28.6284273', '77.3779523', '2018-09-25 03:57:07', '0');
INSERT INTO `send_locations` VALUES (107, 1, 'Sector 63 H-087 Noida Uttar Pradesh India 201301', '', '28.6284269', '77.3779527', '2018-09-25 04:14:23', '0');
INSERT INTO `send_locations` VALUES (108, 11, 'g9BB8O0vVGbGzOcNx6op5Bd53XGnOO+BccUQSkHGbSvL3T1o2muPWUgiEfVKcnYCi6c0hfFvZzPtA0pZXCtP4Q==', '1', '4hKbhmiHfrkFPOLh7WAcbFx+w0aa6sXKnUWmdNqgfME=', 'vMjIT7cVk6yf6V0Jj2F4kl+cxdoLBR3HYLuv1Fvtuxc=', '2018-09-26 10:11:06', '0');

-- --------------------------------------------------------

-- 
-- Table structure for table `send_mail`
-- 

CREATE TABLE `send_mail` (
  `mail_id` int(11) NOT NULL AUTO_INCREMENT,
  `sender_email` varchar(50) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `content` text NOT NULL,
  `mail_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `staus` tinyint(5) NOT NULL,
  `isSecure` int(5) NOT NULL,
  `is_deleted` enum('0','1') NOT NULL DEFAULT '0',
  PRIMARY KEY (`mail_id`)
) ENGINE=InnoDB AUTO_INCREMENT=73 DEFAULT CHARSET=latin1 AUTO_INCREMENT=73 ;

-- 
-- Dumping data for table `send_mail`
-- 

INSERT INTO `send_mail` VALUES (1, '', 1, 'Test', 'Bell', '2018-07-30 01:19:18', 0, 0, '1');
INSERT INTO `send_mail` VALUES (2, '', 7, '01QC0wTlkrbzMUX+uiwAbULKPj8TS5Z3dJNcuySTfVo=', 'el1eDOerxozo8UoGP9YuB3LbHKr6Dtbm7ggVFHAAJRc=', '2018-07-30 01:49:28', 0, 1, '0');
INSERT INTO `send_mail` VALUES (3, '', 7, 'Pankaj Test', 'Ufufuf', '2018-07-30 03:44:20', 0, 0, '0');
INSERT INTO `send_mail` VALUES (4, '', 7, 'Pankaj Test', 'Test', '2018-07-30 03:48:57', 0, 0, '0');
INSERT INTO `send_mail` VALUES (5, '', 7, 'Pankaj Test', 'Urufufuru', '2018-07-30 03:58:49', 0, 0, '0');
INSERT INTO `send_mail` VALUES (6, '', 21, 'Dgi', 'Y yet', '2018-07-31 06:31:11', 0, 0, '0');
INSERT INTO `send_mail` VALUES (7, '', 21, 'Hxhf', 'Udjfjfgi', '2018-07-31 06:31:46', 0, 0, '0');
INSERT INTO `send_mail` VALUES (8, '', 21, 'Ug', 'CCNY', '2018-07-31 06:32:25', 0, 0, '0');
INSERT INTO `send_mail` VALUES (9, '', 21, 'gXhpGoUTiNf0rFrLrTrTDGtWD7NADZhAkKG1l4tQVDw=', 'm4fLjt7JUmcHeObbDs9kpgz/rakFLXT2lmTgpLVpetk=', '2018-07-31 06:32:44', 0, 1, '0');
INSERT INTO `send_mail` VALUES (10, '', 20, 'Test', 'Heloo', '2018-07-31 07:38:19', 0, 0, '0');
INSERT INTO `send_mail` VALUES (11, '', 21, 'Xvhgxc', 'Fjjrhjy', '2018-07-31 07:39:05', 0, 0, '0');
INSERT INTO `send_mail` VALUES (12, '', 20, 'Xvhgxc', 'Gsgsgs', '2018-07-31 07:39:21', 0, 0, '0');
INSERT INTO `send_mail` VALUES (13, '', 21, 'Xvhgxc', 'Xhffk', '2018-07-31 07:39:50', 0, 0, '0');
INSERT INTO `send_mail` VALUES (14, '', 21, 'Xvhgxc', 'Asdfghjkl', '2018-07-31 07:40:25', 0, 0, '0');
INSERT INTO `send_mail` VALUES (15, '', 13, 'Email Test', 'Test 1 for email', '2018-08-02 17:36:05', 0, 0, '0');
INSERT INTO `send_mail` VALUES (16, '', 10, 'Test', 'Gshajak', '2018-08-07 02:55:07', 0, 0, '0');
INSERT INTO `send_mail` VALUES (17, '', 12, 'Email Test', 'Finally checking this', '2018-08-08 00:04:34', 0, 0, '0');
INSERT INTO `send_mail` VALUES (18, '', 13, 'Test 1', 'This is an email test', '2018-08-30 12:44:38', 0, 0, '0');
INSERT INTO `send_mail` VALUES (19, '', 16, 'Test 1', 'Test 1', '2018-08-30 12:45:51', 0, 0, '0');
INSERT INTO `send_mail` VALUES (20, '', 1, 'Test Mail Sb', 'Ji sjaj sbsnan', '2018-09-04 02:50:28', 0, 0, '1');
INSERT INTO `send_mail` VALUES (21, '', 1, 'Test2333', 'Zhai zbsjsj', '2018-09-04 02:50:52', 0, 0, '1');
INSERT INTO `send_mail` VALUES (22, '', 1, 'Test', 'Testtttttt', '2018-09-07 08:01:33', 0, 0, '1');
INSERT INTO `send_mail` VALUES (23, '', 10, 'Test', 'Gsshkssi', '2018-09-12 08:27:35', 0, 0, '1');
INSERT INTO `send_mail` VALUES (24, '', 27, 'Info', 'Tomorrow will be off you have to come on Tuesday.', '2018-09-12 08:47:49', 0, 0, '0');
INSERT INTO `send_mail` VALUES (25, '', 27, 'Leave Request For Sick', 'I am unable to come to work due to illness.', '2018-09-12 08:50:47', 0, 0, '0');
INSERT INTO `send_mail` VALUES (26, '', 27, 'qqCXCxpDPNdd987CbNU8uUsA54w9UN6o/6tKA5EDfY0=', 'z0XZFvluf5CJ0yJKwC7Gr5ydeMPtHwrQNrlNc4rndxepsJZAqql6HDDOVJ47fV+kEvO6ErFnVplbgSmu2VguLQ==', '2018-09-12 08:55:14', 0, 1, '0');
INSERT INTO `send_mail` VALUES (27, '', 27, 'iIveZVPzl+jndw4IvNVSOrx+f3+9PM5J6696ia1W1d7CD8Rgwkemd8EvX2udwupe', 'ZN39KqGHrwh73BVgbS7WMfCglO0WHc+pJe34bmd8Y4gtzImNpnjeIwBrVL3DTvctLvZjU/V63CguI/7DqFW+Ag==', '2018-09-12 08:56:06', 0, 1, '0');
INSERT INTO `send_mail` VALUES (28, '', 26, 'Leave Info', 'Okh I will be there', '2018-09-12 09:04:39', 0, 0, '1');
INSERT INTO `send_mail` VALUES (29, '', 26, 'Leave Request For Sick', 'Thanks for informing', '2018-09-12 09:05:00', 0, 0, '1');
INSERT INTO `send_mail` VALUES (30, '', 26, 'Leave Info', 'OK I will be there', '2018-09-12 09:09:42', 0, 0, '1');
INSERT INTO `send_mail` VALUES (31, '', 11, 'Test Email', 'Hi this is a test email from app', '2018-09-18 02:08:07', 0, 0, '1');
INSERT INTO `send_mail` VALUES (32, '', 1, 'Test', 'Hdhxhxhd', '2018-09-18 02:19:05', 0, 0, '1');
INSERT INTO `send_mail` VALUES (33, '', 10, 'Test', 'Hi how are you?', '2018-09-18 09:28:41', 0, 0, '1');
INSERT INTO `send_mail` VALUES (34, '', 10, 'Test1', 'For test only', '2018-09-18 09:31:07', 0, 0, '1');
INSERT INTO `send_mail` VALUES (35, '', 11, 'Test 1', 'Es 2', '2018-09-18 10:10:15', 0, 0, '1');
INSERT INTO `send_mail` VALUES (36, '', 26, 'Ug1WndspgvUe0LWgDXI5pYdU2BEcHgHmcfhTxE36swo=', 'WwS2tVbGPhbwPybGtSQqRZ3MHjiq9DdHRGszr+SXvKo=', '2018-09-19 04:26:42', 0, 1, '1');
INSERT INTO `send_mail` VALUES (37, '', 10, 'Test', 'Hsshsjsj', '2018-09-19 05:28:59', 0, 0, '1');
INSERT INTO `send_mail` VALUES (38, '', 11, 'Test3', 'Test 3', '2018-09-19 05:32:12', 0, 0, '1');
INSERT INTO `send_mail` VALUES (39, '', 11, 'Test4', 'Test4', '2018-09-19 05:32:56', 0, 0, '1');
INSERT INTO `send_mail` VALUES (40, '', 10, 'Teet', 'Huio', '2018-09-19 05:33:28', 0, 0, '1');
INSERT INTO `send_mail` VALUES (41, '', 10, 'Test', 'Dyuji', '2018-09-19 05:34:48', 0, 0, '1');
INSERT INTO `send_mail` VALUES (42, '', 26, 'Rq5oBp4GPXnRvdrYRndXOILH1Hmqe1M/ftKcQZ7s/6o=', 'xbUSjRPfhKEBvXUM85KCUZA1fQBvfzqP5EAtY5ZOuEo=', '2018-09-19 05:36:32', 0, 1, '1');
INSERT INTO `send_mail` VALUES (43, '', 11, 'Test5', 'Est5', '2018-09-19 05:37:11', 0, 0, '1');
INSERT INTO `send_mail` VALUES (44, '', 26, '97lHhQdjo9mOSS3I+dPr2wSbFQ63YHJGQWLe33TbZtA=', 'nyPGw2CqPvckv+suIf4AQcYpJKCC2N2kIkNvZZ/TNno=', '2018-09-19 05:39:46', 0, 1, '1');
INSERT INTO `send_mail` VALUES (45, '', 26, 'D/FmFE7M9LrUnNvlCGaExwn2BQkdCfx/qI5VHSsqzsw=', 'QGCwd/fFcHzzaiCkAN4s6L7N7CkJb8kx0nfFxTSK7Ss=', '2018-09-19 05:40:05', 0, 1, '1');
INSERT INTO `send_mail` VALUES (46, '', 1, 'Test', 'Jfhfjfjfhd', '2018-09-19 07:28:52', 0, 0, '0');
INSERT INTO `send_mail` VALUES (47, '', 26, 'NEEHZjTZfhUgzwS2ltgNr5AwE6+hvgeYxgXX639fkCc=', 'qZbF+JgMU4bidn9X6eQdvZBFIA8++YuT2inE5PM7mjU=', '2018-09-19 07:31:59', 0, 1, '0');
INSERT INTO `send_mail` VALUES (48, '', 26, '9zDAHuXQiav4v4tVIqyYFhMAKTqKKg+ZytBdjHND0Kg=', 'iuqidKBRJdKVU1S4WUmACPRrBhcmnO/l2nkaAJ7iQG0=', '2018-09-19 07:38:48', 0, 1, '0');
INSERT INTO `send_mail` VALUES (49, '', 26, 'k5+eSopBRKu2jgotrOG/w6nRqSK+lz7zr6aAp3fGeCY=', 'Bm+hQsIiZ6lvZewrr3MFuc4DE16Mcm9T2n/tvkJLjlY=', '2018-09-19 07:39:06', 0, 1, '0');
INSERT INTO `send_mail` VALUES (50, '', 10, 'Test', 'Shhzhzhxh', '2018-09-19 09:53:38', 0, 0, '0');
INSERT INTO `send_mail` VALUES (51, '', 11, 'Tedt5', 'Test 5', '2018-09-19 12:04:08', 0, 0, '0');
INSERT INTO `send_mail` VALUES (52, '', 13, 'Tedt5', 'Got your test emails in the app as well. But had a problem. Could not make the keyboard disappear to send the email. I had to select back to hit send.', '2018-09-19 15:10:03', 0, 0, '0');
INSERT INTO `send_mail` VALUES (53, '', 13, 'Test 1', 'Sachin. The history screen is not capturing all of the history. It only seems to be capturing location sends. Not emails or texts. It ahould be capturing all.', '2018-09-19 15:13:09', 0, 0, '0');
INSERT INTO `send_mail` VALUES (54, '', 11, 'Tedt5', 'Sure.. but this will create issue on other devices....', '2018-09-19 15:33:57', 0, 0, '0');
INSERT INTO `send_mail` VALUES (55, '', 13, 'Tedt5', 'Which issue. The history or the keyboard not moving', '2018-09-19 15:35:36', 0, 0, '0');
INSERT INTO `send_mail` VALUES (56, '', 11, 'Tedt5', 'When we will move text box and button above the keyboard.. it will cause issue in small size devices.. area may cut out... pressing back button to hide keyboard is default feature in Android... but if you say.. we can add a done button or allow user to tap on screen to hide the keyboard....', '2018-09-19 15:40:36', 0, 0, '0');
INSERT INTO `send_mail` VALUES (57, '', 11, 'Tedt5', 'Erik.. i understood.. i will make  it above the keyboard', '2018-09-19 15:46:46', 0, 0, '0');
INSERT INTO `send_mail` VALUES (58, '', 13, 'Tedt5', 'I tried responding from my email account on my desktop and it didnt work for some reason. \n\nNo problem on the keyboard. I use an apple so i just didnt know. Leave it like it is for now. Also i plan to do a test tomorrow. I will send you an image, data etc. Just let me know you receive. Also if you can, pls do the same for me and i will let you know. No need to do it simultaneously. I will start mid morning my time tomorrow', '2018-09-19 15:49:04', 0, 0, '0');
INSERT INTO `send_mail` VALUES (59, '', 11, 'Tedt5', 'Sure Erik.. just send me and i will write you back in same app.. \nplan to add live chatting and push notifications in this app...', '2018-09-19 15:51:44', 0, 0, '0');
INSERT INTO `send_mail` VALUES (60, '', 6, 'Testing App', 'I found issue on send location screen, that is,  my sent location are not showing in send list. Plz check this issues.', '2018-09-20 02:02:26', 0, 0, '0');
INSERT INTO `send_mail` VALUES (61, '', 6, 'App Test', 'App is testing.', '2018-09-24 10:39:40', 0, 0, '0');
INSERT INTO `send_mail` VALUES (62, '', 6, 'New List', 'This is app.', '2018-09-24 10:44:04', 0, 0, '0');
INSERT INTO `send_mail` VALUES (63, '', 6, 'Message Mail', 'Dghinvxxvnkmbvcxvbnjj', '2018-09-25 02:31:18', 0, 0, '0');
INSERT INTO `send_mail` VALUES (64, '', 10, 'Tesg', 'Gsshsh', '2018-09-25 03:25:01', 0, 0, '0');
INSERT INTO `send_mail` VALUES (65, '', 1, 'Shsh', 'Sgshsh', '2018-09-25 03:28:22', 0, 0, '0');
INSERT INTO `send_mail` VALUES (66, '', 1, 'Test', 'Test', '2018-09-25 03:30:41', 0, 0, '0');
INSERT INTO `send_mail` VALUES (67, '', 10, 'Yzj', 'Hsjsjdk', '2018-09-25 03:38:41', 0, 0, '0');
INSERT INTO `send_mail` VALUES (68, '', 1, 'My Testing', 'My testing', '2018-09-25 03:39:50', 0, 0, '0');
INSERT INTO `send_mail` VALUES (69, '', 1, 'New Emai', 'Ffff', '2018-09-25 05:42:39', 0, 0, '0');
INSERT INTO `send_mail` VALUES (70, '', 1, 'Panakj Testing', 'Lankan testing', '2018-09-25 10:08:56', 0, 0, '0');
INSERT INTO `send_mail` VALUES (71, '', 10, 'Yasg', 'Sbsbsb', '2018-09-25 10:09:25', 0, 0, '0');
INSERT INTO `send_mail` VALUES (72, '', 6, 'Messag', 'Hdhxhxhxhchchhcchch', '2018-09-25 10:15:19', 0, 0, '0');

-- --------------------------------------------------------

-- 
-- Table structure for table `users`
-- 

CREATE TABLE `users` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(25) NOT NULL,
  `last_name` varchar(25) NOT NULL,
  `phone` varchar(30) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `access_token` varchar(100) NOT NULL,
  `updated_date` date DEFAULT NULL,
  `created_date` date DEFAULT NULL,
  `device_type` varchar(300) NOT NULL,
  `device_token` varchar(300) NOT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1 AUTO_INCREMENT=34 ;

-- 
-- Dumping data for table `users`
-- 

INSERT INTO `users` VALUES (1, 'Pankaj', 'Sharma', '9754695496', 'pankaj.alobha@gmail.com', 'y2PgapNyWNbRxJTWc7xqpxGUWYG3Z8T/au6xPb4PuCc=', '5bb455bfaf99f-1538545087-1', '2018-07-25', '2018-07-11', '0', '0');
INSERT INTO `users` VALUES (2, 'Pk', 'Sharma', '7827668086', 'pankaj.sharma@alobhatechnologies.com', 'y2PgapNyWNbRxJTWc7xqpxGUWYG3Z8T/au6xPb4PuCc=', '5b5b1c9bb5207-1532697755-2', NULL, '2018-07-11', '0', '0');
INSERT INTO `users` VALUES (3, 'Raj', 'Kumar', '9999999999', 'raj@gmail.com', 'y2PgapNyWNbRxJTWc7xqpxGUWYG3Z8T/au6xPb4PuCc=', '5b94b33e36087-1536471870-3', NULL, '2018-07-11', '0', '0');
INSERT INTO `users` VALUES (4, 'Rrrrrrr', 'Kumar', '8826586263', 'ra@gmail.com', 'N1ADTsyK9s015WVMHyWHEodw54HmH0T0fjZIaZnhNlk=', '5b46ed2bc4719-1531374891-4', NULL, '2018-07-11', '0', '0');
INSERT INTO `users` VALUES (5, 'Anki', 'Dhaka', '8909409409', 'ankita.dhaka09@gmail.com', 'CtUv4Lc6qpcn3ZvOWtzdAwu3SmoxmzFhSOoxqAcp5yI=', '5b6061b16c30a-1533043121-5', '2018-07-25', '2018-07-11', '0', '0');
INSERT INTO `users` VALUES (6, 'Ankita', 'Dhaka', '8130540093', 'ankita.siwal09@gmail.com', 'cViEimM4Vx/Symuzt2tGQNiNuHNQhUx7ilFxj636i2M=', '5baa42c92836a-1537884873-6', NULL, '2018-07-11', '0', '0');
INSERT INTO `users` VALUES (7, 'Ranjeet', 'Kumar', '9999999999', 'raj1@gmail.com', 'zFhjdILuvsc1lr5f2OyxjKjBjKZY74LzrXNIEqaOkzU=', '5b487f8637113-1531477894-7', '2018-07-27', '2018-07-11', '0', '0');
INSERT INTO `users` VALUES (8, 'Raj2', 'Kumar', '8888888888', 'raj2@gmail.com', 'y2PgapNyWNbRxJTWc7xqpxGUWYG3Z8T/au6xPb4PuCc=', '5b927d56f2c24-1536326998-8', NULL, '2018-07-11', '0', '0');
INSERT INTO `users` VALUES (9, 'Rahul', 'Kumar', '8826586263', 'ranjeet.kumar@alobhatechnologies.com', 'y2PgapNyWNbRxJTWc7xqpxGUWYG3Z8T/au6xPb4PuCc=', '5b5ff65743a3a-1533015639-9', NULL, '2018-07-12', '0', '0');
INSERT INTO `users` VALUES (10, 'Rohit', 'Kumar', '7878797979', 'roHit.alobha@gmail.com', 'Nr01iPOYX+X2Kwems9h91Ory4NoYWRKHd99DGtYvGgI=', '5ba4c613809c1-1537525267-10', '2018-07-12', '2018-07-12', '0', '0');
INSERT INTO `users` VALUES (11, 'Sachin', 'Siwal', '7827331167', 'sachinsiwal@gmail.com', 'p/BZWD+mYsl1U5DO/J0eonaxwW9wlPyB6MeBhakOLv4=', '5ba215ff1aff0-1537349119-11', NULL, '2018-07-12', '0', '0');
INSERT INTO `users` VALUES (12, 'John ', 'Terry', '7033950257', 'john@espretech.com', 'YK5QtVbXhIXYcETZmXZAp1VBi3UPPp/EWe8B6bja1OU=', '5b9608aa5b285-1536559274-12', NULL, '2018-07-13', '0', '0');
INSERT INTO `users` VALUES (13, 'Erik', 'Vadersen', '9123083462', 'ev.intlrail@gmail.com', 'ClCGu+5yPzgCtEX7aPrjCUV4xnrdxNkU599gHhNX050=', '5babc49dce3cb-1537983645-13', NULL, '2018-07-13', '0', '0');
INSERT INTO `users` VALUES (14, 'Rahul', 'sharma', '123456789', 'rahul.alobha@gmail.com', 'ch4MS0UUqPudiFjHIcgfdopoXd/WH4h0iGVzCU01YNo=', '5b4c428c8fea4-1531724428-123456789', NULL, '2018-07-16', 'gffh', '123');
INSERT INTO `users` VALUES (15, 'Rahul', 'sharma', '9754695496', 'rahul1.alobha@gmail.com', 'OkKqdYTTvt87d6TRnlhVZVsRWmJqB7yTmuunlbv3cq8=', '5b5f1423b9c80-1532957731-15', NULL, '2018-07-16', '0', '0');
INSERT INTO `users` VALUES (16, 'Sachin', 'Siwal', '7965412564', 'sachinsiwa@gmail.com', 'wwG10eDZ5BVHT5+wkdn4LOzPKoBln96fHZRnYgPDD80=', '5b5b654340c3a-1532716355-7965412564', NULL, '2018-07-28', 'gffh', '123');
INSERT INTO `users` VALUES (17, 'Bholi', 'Gupta', '8909409409', 'gori@gmail.com', 'aunsNfNpbQ1qek7SB+7HBMTZC2Tx83WzddWcc53YW4Y=', '5b5b6d4c213d4-1532718412-17', '2018-07-28', '2018-07-28', '0', '0');
INSERT INTO `users` VALUES (18, 'SCA', 'PlusN', '7033950257', 'john.terry@plusn.com', 'xcTxpkky9t9a7DlDHHndosmUPWxVUzeGQGI+MPcSrsQ=', '5b5f44226944f-1532970018-7033950257', NULL, '2018-07-30', 'gffh', '123');
INSERT INTO `users` VALUES (19, 'Akr', 'Kr', '1231231231', 'a@a.com', 'XYeW31K+bhbEcLnKYq1zdH6ghnCa1bD/sWOhNWfLzeU=', '5b602aed4ad15-1533029101-19', NULL, '2018-07-31', '0', '0');
INSERT INTO `users` VALUES (20, 'Test', 'Kunar', '3868685757', 'kumar@gmail.com', 'n2A+YvDlY4v9P6IT0mf7v0mNA9lq9B/kc+uM8ftnSa4=', '5b602e1f08969-1533029919-3868685757', NULL, '2018-07-31', 'gffh', '123');
INSERT INTO `users` VALUES (21, 'Dnjd', 'Gzhs', '5645454545', 'a1@a.com', '4H7LLG+Ru68HOMYIQ8sFKN+ZqVxIOz2xdywMdCFa1s0=', '5b603a0dae2e6-1533032973-5645454545', NULL, '2018-07-31', 'gffh', '123');
INSERT INTO `users` VALUES (22, 'Pooja', 'Gupta', '8956321470', 'pooja@gmail.com', 'ygcwJVQRLjRS4rfDIzRRgftT/W7CCjcnyJCsO9xH6/0=', '5b6060c01b680-1533042880-8956321470', NULL, '2018-07-31', 'gffh', '123');
INSERT INTO `users` VALUES (23, 'Rashi', 'Gupta', '9874563210', 'rashi@gmail.com', 'wprUrZRQ1i7AbjQBBwNIbFnY7va4nIx45n7YqTYKgtY=', '5b696261ee0fe-1533633121-9874563210', NULL, '2018-08-07', 'gffh', '123');
INSERT INTO `users` VALUES (24, 'A', 'B', '9760936750', 'AnimeshBanik22@gmail.com', 'gSxLte3zQKJb2DxE59hV6Zel7aMDX9LswZCbuLEleHY=', '5b90e5681baeb-1536222568-9760936750', NULL, '2018-09-06', 'gffh', '123');
INSERT INTO `users` VALUES (25, 'Ather', 'Rashid', '9205567178', 'ather.rashid@alobhatechnologies.com', 'X/ViDlqR5xytvPnni5X5zuhcrPQdUZAjTt6GmcG39Uk=', '5b92279f61ec8-1536305055-9205567178', NULL, '2018-09-07', 'gffh', '123');
INSERT INTO `users` VALUES (26, 'P1', 'Sharma', '7827668086', 'p1@gmail.com', '/glMysmHQGgXAUlEh7mxUrDkUfkk1GpzKz2u1N2artw=', '5ba1eb8ec7055-1537338254-26', NULL, '2018-09-09', '0', '0');
INSERT INTO `users` VALUES (27, 'P2', 'Sharma', '7825466454', 'p2@gmail.com', 'rE9KXQrsVHI2VyOO7UFzGpvLU8NnBUTGYDyZPpyazxE=', '5ba9f170f0b25-1537864048-27', NULL, '2018-09-09', '0', '0');
INSERT INTO `users` VALUES (28, 'Jchalla', 'Terry', '4693486177', 'john.terry@terryconsult.com', 'ENEhL5+Zstw82GXwNf438kD9OCxMUsnd4e3sv30o7g4=', '5b9605766c48e-1536558454-4693486177', NULL, '2018-09-10', 'gffh', '123');
INSERT INTO `users` VALUES (29, 'Amiel', 'Terry', '7033495641', 'drjohnterry@hotmail.com', 'QXsU6l1oSSMZt7JYrL67q3NPvyv95QDsFzHKSZwYnQc=', '5ba2e5b29d978-1537402290-7033495641', NULL, '2018-09-20', 'gffh', '123');
INSERT INTO `users` VALUES (30, 'Praveen', 'Vishal', '8447061196', 'praveen.vishal10@gmail.com', 'FTd1vUsJBGLvGPoW9j+WanV1slFuImd6chdfDfBmRSQ=', '5babbd49ad453-1537981769-8447061196', NULL, '2018-09-26', 'gffh', '123');
INSERT INTO `users` VALUES (31, 'Praveen', 'Vishal', '8447061196', 'praveen.vishal28@gmail.com', 'PQL77M4qW0CqB+9vyquWHB9IXSAmb97R+cVZWXHgmRU=', '5bafb626d2712-1538242086-8447061196', NULL, '2018-09-29', 'gffh', '123');
INSERT INTO `users` VALUES (32, 'c', 'm', '288888', 'd@gmail.com', 'R9+GfyaScGtkOakjht626eul73BPVVwCQQ74TKcUNDg=', '5baffe97a8152-1538260631-288888', NULL, '2018-09-30', 'gffh', '123');
INSERT INTO `users` VALUES (33, 'PRACERC', 'Tgvhv', '8657453585', 'yggyygug@gmail.com', 'lmEpwlrEraVJWiXkXlvTr10tsrBD3ca836Df1Z+kfHQ=', '5bb1271f5339c-1538336543-8657453585', NULL, '2018-10-01', 'gffh', '123');

-- --------------------------------------------------------

-- 
-- Table structure for table `veg_count`
-- 

CREATE TABLE `veg_count` (
  `vegcount_id` int(11) NOT NULL AUTO_INCREMENT,
  `receiveId` int(11) NOT NULL,
  `type` varchar(50) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`vegcount_id`)
) ENGINE=InnoDB AUTO_INCREMENT=205 DEFAULT CHARSET=latin1 AUTO_INCREMENT=205 ;

-- 
-- Dumping data for table `veg_count`
-- 

INSERT INTO `veg_count` VALUES (4, 2, 'email', '2018-09-19 07:39:07');
INSERT INTO `veg_count` VALUES (5, 3, 'email', '2018-09-19 07:39:07');
INSERT INTO `veg_count` VALUES (7, 2, 'video/image', '2018-09-19 07:47:38');
INSERT INTO `veg_count` VALUES (8, 3, 'video/image', '2018-09-19 07:47:38');
INSERT INTO `veg_count` VALUES (14, 7, 'message', '2018-09-19 09:53:08');
INSERT INTO `veg_count` VALUES (15, 0, 'message', '2018-09-19 09:53:17');
INSERT INTO `veg_count` VALUES (17, 2, 'email', '2018-09-19 09:53:38');
INSERT INTO `veg_count` VALUES (18, 3, 'email', '2018-09-19 09:53:38');
INSERT INTO `veg_count` VALUES (20, 7, 'email', '2018-09-19 09:53:39');
INSERT INTO `veg_count` VALUES (22, 2, 'message', '2018-09-19 09:54:29');
INSERT INTO `veg_count` VALUES (23, 4, 'message', '2018-09-19 09:54:29');
INSERT INTO `veg_count` VALUES (24, 5, 'message', '2018-09-19 09:54:29');
INSERT INTO `veg_count` VALUES (26, 2, 'location', '2018-09-19 09:55:32');
INSERT INTO `veg_count` VALUES (27, 3, 'location', '2018-09-19 09:55:32');
INSERT INTO `veg_count` VALUES (28, 5, 'location', '2018-09-19 09:55:32');
INSERT INTO `veg_count` VALUES (30, 2, 'other', '2018-09-19 09:56:26');
INSERT INTO `veg_count` VALUES (31, 3, 'other', '2018-09-19 09:56:26');
INSERT INTO `veg_count` VALUES (32, 4, 'other', '2018-09-19 09:56:26');
INSERT INTO `veg_count` VALUES (34, 2, 'video/image', '2018-09-19 09:57:22');
INSERT INTO `veg_count` VALUES (35, 3, 'video/image', '2018-09-19 09:57:22');
INSERT INTO `veg_count` VALUES (36, 4, 'video/image', '2018-09-19 09:57:22');
INSERT INTO `veg_count` VALUES (37, 2, 'audio', '2018-09-19 10:00:42');
INSERT INTO `veg_count` VALUES (38, 4, 'audio', '2018-09-19 10:00:42');
INSERT INTO `veg_count` VALUES (39, 5, 'audio', '2018-09-19 10:00:42');
INSERT INTO `veg_count` VALUES (40, 7, 'audio', '2018-09-19 10:00:42');
INSERT INTO `veg_count` VALUES (41, 9, 'audio', '2018-09-19 10:00:42');
INSERT INTO `veg_count` VALUES (42, 11, 'audio', '2018-09-19 10:00:42');
INSERT INTO `veg_count` VALUES (43, 2, 'audio', '2018-09-19 10:18:58');
INSERT INTO `veg_count` VALUES (44, 3, 'audio', '2018-09-19 10:18:58');
INSERT INTO `veg_count` VALUES (45, 5, 'audio', '2018-09-19 10:18:58');
INSERT INTO `veg_count` VALUES (47, 2, 'video/image', '2018-09-19 11:20:40');
INSERT INTO `veg_count` VALUES (48, 3, 'video/image', '2018-09-19 11:20:40');
INSERT INTO `veg_count` VALUES (49, 3, 'video/image', '2018-09-19 11:23:41');
INSERT INTO `veg_count` VALUES (50, 4, 'video/image', '2018-09-19 11:23:41');
INSERT INTO `veg_count` VALUES (51, 7, 'video/image', '2018-09-19 11:23:41');
INSERT INTO `veg_count` VALUES (52, 3, 'audio', '2018-09-19 11:40:57');
INSERT INTO `veg_count` VALUES (53, 6, 'audio', '2018-09-19 11:40:57');
INSERT INTO `veg_count` VALUES (55, 2, 'other', '2018-09-19 11:42:53');
INSERT INTO `veg_count` VALUES (56, 4, 'other', '2018-09-19 11:42:53');
INSERT INTO `veg_count` VALUES (58, 2, 'video/image', '2018-09-19 11:43:24');
INSERT INTO `veg_count` VALUES (59, 3, 'video/image', '2018-09-19 11:43:24');
INSERT INTO `veg_count` VALUES (60, 5, 'video/image', '2018-09-19 11:43:24');
INSERT INTO `veg_count` VALUES (62, 2, 'email', '2018-09-19 12:04:08');
INSERT INTO `veg_count` VALUES (63, 5, 'email', '2018-09-19 12:04:08');
INSERT INTO `veg_count` VALUES (64, 7, 'email', '2018-09-19 12:04:09');
INSERT INTO `veg_count` VALUES (65, 12, 'email', '2018-09-19 12:04:09');
INSERT INTO `veg_count` VALUES (67, 16, 'location', '2018-09-19 15:06:01');
INSERT INTO `veg_count` VALUES (68, 16, 'location', '2018-09-19 15:06:21');
INSERT INTO `veg_count` VALUES (69, 16, 'message', '2018-09-19 15:07:11');
INSERT INTO `veg_count` VALUES (71, 16, 'email', '2018-09-19 15:13:09');
INSERT INTO `veg_count` VALUES (72, 16, 'video/image', '2018-09-19 15:13:55');
INSERT INTO `veg_count` VALUES (82, 12, 'message', '2018-09-19 20:12:08');
INSERT INTO `veg_count` VALUES (83, 5, 'location', '2018-09-20 01:59:42');
INSERT INTO `veg_count` VALUES (86, 5, 'email', '2018-09-20 02:02:26');
INSERT INTO `veg_count` VALUES (88, 11, 'email', '2018-09-20 02:02:26');
INSERT INTO `veg_count` VALUES (90, 16, 'location', '2018-09-20 02:09:45');
INSERT INTO `veg_count` VALUES (91, 23, 'location', '2018-09-20 02:09:45');
INSERT INTO `veg_count` VALUES (93, 2, 'location', '2018-09-20 02:16:12');
INSERT INTO `veg_count` VALUES (94, 3, 'location', '2018-09-20 02:16:12');
INSERT INTO `veg_count` VALUES (95, 4, 'location', '2018-09-20 02:16:12');
INSERT INTO `veg_count` VALUES (96, 12, 'location', '2018-09-20 11:14:37');
INSERT INTO `veg_count` VALUES (98, 2, 'video/image', '2018-09-21 06:22:05');
INSERT INTO `veg_count` VALUES (99, 3, 'video/image', '2018-09-21 06:22:05');
INSERT INTO `veg_count` VALUES (100, 1, 'audio', '2018-09-21 06:23:26');
INSERT INTO `veg_count` VALUES (101, 2, 'audio', '2018-09-21 06:23:26');
INSERT INTO `veg_count` VALUES (102, 3, 'audio', '2018-09-21 06:23:26');
INSERT INTO `veg_count` VALUES (104, 2, 'other', '2018-09-21 06:24:02');
INSERT INTO `veg_count` VALUES (105, 3, 'other', '2018-09-21 06:24:02');
INSERT INTO `veg_count` VALUES (106, 5, 'other', '2018-09-21 06:24:02');
INSERT INTO `veg_count` VALUES (107, 4, 'video/image', '2018-09-24 08:45:34');
INSERT INTO `veg_count` VALUES (108, 5, 'video/image', '2018-09-24 08:45:34');
INSERT INTO `veg_count` VALUES (109, 8, 'video/image', '2018-09-24 08:45:34');
INSERT INTO `veg_count` VALUES (110, 9, 'video/image', '2018-09-24 08:45:34');
INSERT INTO `veg_count` VALUES (111, 3, 'video/image', '2018-09-24 09:25:04');
INSERT INTO `veg_count` VALUES (112, 4, 'video/image', '2018-09-24 09:25:04');
INSERT INTO `veg_count` VALUES (113, 7, 'video/image', '2018-09-24 09:25:04');
INSERT INTO `veg_count` VALUES (114, 8, 'video/image', '2018-09-24 09:25:04');
INSERT INTO `veg_count` VALUES (115, 5, 'other', '2018-09-24 09:41:34');
INSERT INTO `veg_count` VALUES (116, 7, 'other', '2018-09-24 09:41:34');
INSERT INTO `veg_count` VALUES (117, 8, 'other', '2018-09-24 09:41:34');
INSERT INTO `veg_count` VALUES (118, 10, 'other', '2018-09-24 09:41:34');
INSERT INTO `veg_count` VALUES (119, 3, 'audio', '2018-09-24 09:42:33');
INSERT INTO `veg_count` VALUES (120, 7, 'audio', '2018-09-24 09:42:33');
INSERT INTO `veg_count` VALUES (121, 8, 'audio', '2018-09-24 09:42:33');
INSERT INTO `veg_count` VALUES (122, 9, 'audio', '2018-09-24 09:42:33');
INSERT INTO `veg_count` VALUES (123, 3, 'location', '2018-09-24 09:42:53');
INSERT INTO `veg_count` VALUES (124, 4, 'location', '2018-09-24 09:42:53');
INSERT INTO `veg_count` VALUES (125, 5, 'location', '2018-09-24 09:42:53');
INSERT INTO `veg_count` VALUES (126, 8, 'location', '2018-09-24 09:42:53');
INSERT INTO `veg_count` VALUES (127, 4, 'location', '2018-09-24 09:46:09');
INSERT INTO `veg_count` VALUES (128, 3, 'location', '2018-09-24 09:56:52');
INSERT INTO `veg_count` VALUES (129, 4, 'location', '2018-09-24 09:56:52');
INSERT INTO `veg_count` VALUES (130, 5, 'location', '2018-09-24 09:56:52');
INSERT INTO `veg_count` VALUES (131, 7, 'location', '2018-09-24 09:56:52');
INSERT INTO `veg_count` VALUES (132, 3, 'location', '2018-09-24 09:57:03');
INSERT INTO `veg_count` VALUES (133, 3, 'location', '2018-09-24 09:57:18');
INSERT INTO `veg_count` VALUES (134, 3, 'location', '2018-09-24 09:57:39');
INSERT INTO `veg_count` VALUES (135, 3, 'location', '2018-09-24 09:57:46');
INSERT INTO `veg_count` VALUES (136, 3, 'location', '2018-09-24 09:58:25');
INSERT INTO `veg_count` VALUES (138, 4, 'message', '2018-09-24 10:00:08');
INSERT INTO `veg_count` VALUES (139, 4, 'location', '2018-09-24 10:05:40');
INSERT INTO `veg_count` VALUES (140, 8, 'location', '2018-09-24 10:05:40');
INSERT INTO `veg_count` VALUES (141, 4, 'location', '2018-09-24 10:05:44');
INSERT INTO `veg_count` VALUES (142, 4, 'location', '2018-09-24 10:05:49');
INSERT INTO `veg_count` VALUES (143, 8, 'location', '2018-09-24 10:06:43');
INSERT INTO `veg_count` VALUES (144, 5, 'location', '2018-09-24 10:16:57');
INSERT INTO `veg_count` VALUES (146, 12, 'location', '2018-09-24 10:35:21');
INSERT INTO `veg_count` VALUES (148, 14, 'location', '2018-09-24 10:35:21');
INSERT INTO `veg_count` VALUES (149, 23, 'location', '2018-09-24 10:35:21');
INSERT INTO `veg_count` VALUES (151, 11, 'other', '2018-09-24 10:36:27');
INSERT INTO `veg_count` VALUES (152, 23, 'other', '2018-09-24 10:36:27');
INSERT INTO `veg_count` VALUES (153, 3, 'email', '2018-09-24 10:39:41');
INSERT INTO `veg_count` VALUES (154, 4, 'email', '2018-09-24 10:39:41');
INSERT INTO `veg_count` VALUES (155, 5, 'email', '2018-09-24 10:39:41');
INSERT INTO `veg_count` VALUES (157, 2, 'email', '2018-09-24 10:44:04');
INSERT INTO `veg_count` VALUES (158, 3, 'email', '2018-09-24 10:44:04');
INSERT INTO `veg_count` VALUES (160, 2, 'message', '2018-09-25 02:30:14');
INSERT INTO `veg_count` VALUES (161, 3, 'message', '2018-09-25 02:30:14');
INSERT INTO `veg_count` VALUES (162, 4, 'message', '2018-09-25 02:30:14');
INSERT INTO `veg_count` VALUES (164, 3, 'email', '2018-09-25 02:31:18');
INSERT INTO `veg_count` VALUES (165, 4, 'email', '2018-09-25 02:31:18');
INSERT INTO `veg_count` VALUES (167, 2, 'message', '2018-09-25 02:55:33');
INSERT INTO `veg_count` VALUES (168, 0, 'message', '2018-09-25 02:55:47');
INSERT INTO `veg_count` VALUES (170, 2, 'email', '2018-09-25 03:25:01');
INSERT INTO `veg_count` VALUES (171, 3, 'email', '2018-09-25 03:28:22');
INSERT INTO `veg_count` VALUES (172, 5, 'email', '2018-09-25 03:28:22');
INSERT INTO `veg_count` VALUES (174, 2, 'email', '2018-09-25 03:30:41');
INSERT INTO `veg_count` VALUES (180, 10, 'other', '2018-09-25 04:18:37');
INSERT INTO `veg_count` VALUES (181, 2, 'other', '2018-09-25 04:18:52');
INSERT INTO `veg_count` VALUES (182, 3, 'other', '2018-09-25 04:18:52');
INSERT INTO `veg_count` VALUES (183, 7, 'other', '2018-09-25 04:22:26');
INSERT INTO `veg_count` VALUES (185, 2, 'other', '2018-09-25 04:27:55');
INSERT INTO `veg_count` VALUES (188, 8, 'email', '2018-09-25 10:09:25');
INSERT INTO `veg_count` VALUES (189, 3, 'message', '2018-09-25 10:14:48');
INSERT INTO `veg_count` VALUES (190, 4, 'message', '2018-09-25 10:14:48');
INSERT INTO `veg_count` VALUES (192, 4, 'email', '2018-09-25 10:15:20');
INSERT INTO `veg_count` VALUES (193, 5, 'email', '2018-09-25 10:15:20');
INSERT INTO `veg_count` VALUES (194, 10, 'email', '2018-09-25 10:15:20');
INSERT INTO `veg_count` VALUES (195, 11, 'email', '2018-09-25 10:15:20');
INSERT INTO `veg_count` VALUES (198, 3, 'location', '2018-09-26 10:11:06');
INSERT INTO `veg_count` VALUES (199, 5, 'location', '2018-09-26 10:11:06');
INSERT INTO `veg_count` VALUES (200, 16, 'audio', '2018-09-26 13:15:42');
INSERT INTO `veg_count` VALUES (201, 11, 'message', '2018-09-26 13:41:43');
INSERT INTO `veg_count` VALUES (202, 2, 'message', '2018-09-29 13:42:24');
INSERT INTO `veg_count` VALUES (204, 2, 'audio', '2018-10-03 01:38:30');
